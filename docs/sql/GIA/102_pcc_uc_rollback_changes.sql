USE [SpeechMiner]
GO

/****** Object:  StoredProcedure [dbo].[pcc_uc_rollback_changes]    Script Date: 01/02/2019 04:59:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[pcc_uc_rollback_changes]
@tableName VARCHAR(50) = NULL,
@changedRecords VARCHAR(MAX) = NULL,
@nameOrExternalId BIT = 0

AS

BEGIN

	SET NOCOUNT ON
	
	DECLARE @sql VARCHAR(MAX)

	IF ISNULL(@tableName, '') <> ''
	BEGIN
		SET @sql = 'DROP TABLE ' + @tableName
		EXEC (@sql)
	END
	
	IF ISNULL(@changedRecords, '') <> ''
	BEGIN
		
		CREATE TABLE #agents
		(
			agentDetails VARCHAR(MAX),
			agentID INT,
			updated BIT,
			name VARCHAR(512),
			externalID VARCHAR(512),
			split BIT DEFAULT 0
		)
		
		INSERT INTO #agents(agentDetails)
		SELECT Item FROM fn_SplitString(@changedRecords, '|')
		
		DECLARE @agentDetails VARCHAR(MAX)
		SELECT @agentDetails = agentDetails FROM #agents WHERE split = 0
		
		WHILE ISNULL(@agentDetails, '') <> ''
		BEGIN
		
			CREATE TABLE #temp(ID INT IDENTITY(1,1), Item VARCHAR(MAX))
			
			INSERT INTO #temp(Item)
			SELECT Item
			FROM
			fn_SplitString(@agentDetails, ',')
			
			DECLARE @id INT, @name VARCHAR(512), @updated BIT
			
			SELECT @id = ISNULL(Item,'') FROM #temp WHERE ID = 1
			SELECT @updated = ISNULL(Item,'') FROM #temp WHERE ID = 2
			SELECT @name = ISNULL(Item,'') FROM #temp WHERE ID = 3
			
			UPDATE
			#agents
			SET agentID = @id, updated = @updated, name = @name, split = 1
			WHERE agentDetails = @agentDetails
			
			DROP TABLE #temp
			
			SET @agentDetails = ''
			SELECT @agentDetails = agentDetails FROM #agents WHERE split = 0
		
		END
		select * from #agents
		IF @nameOrExternalId = 0
		BEGIN
			UPDATE
			a
			SET a.name = ag.name
			FROM
			agentEntityTbl a
			INNER JOIN #agents ag ON a.id = ag.agentID
			WHERE ag.updated = 1
			
			DELETE a
			FROM 
			agentEntityTbl a 
			INNER JOIN #agents ag ON a.id = ag.agentID
			WHERE ag.updated = 0
		END
		
	END

END
GO



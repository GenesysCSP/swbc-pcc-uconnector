USE [SpeechMiner]
GO

/****** Object:  StoredProcedure [dbo].[pcc_uc_create_temp_table]    Script Date: 01/02/2019 04:59:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[pcc_uc_create_temp_table]  
 
AS  
BEGIN  
   
 DECLARE @sql VARCHAR(300)  
 DECLARE @tbl_name VARCHAR(50)
   
 SET @tbl_name = 'agentHierarchyTblUConnectorTmp' + REPLACE(REPLACE(CONVERT(VARCHAR(20), GETDATE(), 120), ':',''), '-','')  
   
 SET @tbl_name = REPLACE(@tbl_name, ' ','')  
   
 SET @sql =   
 'IF OBJECT_ID(''' + @tbl_name + ''') IS NOT NULL DROP TABLE ' + @tbl_name + '   
 CREATE TABLE ' + @tbl_name + '(parentId INT NOT NULL, childId INT NOT NULL)'  
   
 EXECUTE (@sql)  
 
 SELECT @tbl_name
  
END  
  
  
  
  
GO



USE [SpeechMiner]
GO

/****** Object:  StoredProcedure [dbo].[pcc_uc_update_agentHierarchy]    Script Date: 01/02/2019 05:00:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[pcc_uc_update_agentHierarchy]
@name VARCHAR(50),
@id VARCHAR(50),
@forceUniqueAgentNames BIT = 0

AS

BEGIN

	SET NOCOUNT ON

	DECLARE @res INT 
	DECLARE @updated INT
	DECLARE @oldName VARCHAR(50), @externalId VARCHAR(50)

	SET @updated = 0
	
	BEGIN TRAN

	IF @forceUniqueAgentNames = 1
	BEGIN

		IF exists(SELECT * FROM agentEntityTbl WHERE NAME=@name AND externalId<>@id) 
		BEGIN

			SELECT @res = id FROM agentEntityTbl WHERE NAME=@name AND externalId=@name
			
			IF NOT @res IS NULL BEGIN
				SELECT @externalId = externalId FROM agentEntityTbl WHERE id = @res
				UPDATE agentEntityTbl SET externalId=@id WHERE id=@res
				SET @updated = 1
			END

		END
		ELSE
		BEGIN

			SELECT @res = id FROM agentEntityTbl WHERE externalId=@id

			IF @res IS NULL
			BEGIN
				INSERT INTO agentEntityTbl VALUES(@id,@name,1)
				SELECT @res = @@identity

				SET @updated = 0
			END
			ELSE
			BEGIN
				SELECT @oldName = name FROM agentEntityTbl WHERE id = @res
				UPDATE agentEntityTbl SET NAME=@name WHERE id=@res
				SET @updated = 1
			END

		END
		
	END
	ELSE
	BEGIN
	
		SELECT @res = id FROM agentEntityTbl WHERE externalId=@id

		IF @res IS NULL
		BEGIN
			INSERT INTO agentEntityTbl VALUES(@id,@name,1)
			SELECT @res = @@identity

			SET @updated = 0
		END
		ELSE
		BEGIN
			SELECT @oldName = name FROM agentEntityTbl WHERE id = @res
			UPDATE agentEntityTbl SET NAME=@name WHERE id=@res
			SET @updated = 1
		END
	
	END
	
	COMMIT

	SELECT @res AS id, @updated AS updated, ISNULL(@oldName,'') AS oldName, ISNULL(@externalId,'') AS oldExternalId

END
GO



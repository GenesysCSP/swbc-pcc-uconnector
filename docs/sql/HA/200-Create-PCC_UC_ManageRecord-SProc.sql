
GO

/****** Object:  StoredProcedure [dbo].[PCC_UC_ManageRecord]    Script Date: 1/2/2019 5:34:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PCC_UC_ManageRecord] (@guid varchar(50), @val int, @ts datetime, @comment varchar(MAX))
AS
BEGIN
	if exists (select 1 from PURECONNECT_UCONNECTOR where guid = @guid and comment = @comment)
	  update  PURECONNECT_UCONNECTOR set ts = @ts where guid = @guid and comment = @comment
	else
	  insert into PURECONNECT_UCONNECTOR (guid, val, ts, comment) values (@guid, @val, @ts, @comment)
END
GO
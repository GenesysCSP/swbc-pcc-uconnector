GO

/****** Object:  Table [dbo].[PURECONNECT_UCONNECTOR]    Script Date: 1/2/2019 5:31:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PURECONNECT_UCONNECTOR](
	[guid] [varchar](50) NOT NULL,
	[val] [int] NULL,
	[ts] [datetime] NULL,
	[comment] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO



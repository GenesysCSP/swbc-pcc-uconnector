
GO

/****** Object:  StoredProcedure [dbo].[PCC_UC_InsertElectionRecord]    Script Date: 1/2/2019 5:34:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PCC_UC_InsertElectionRecord] (@guid varchar(50), @val int, @ts datetime, @comment varchar(MAX))
AS
BEGIN
INSERT INTO PURECONNECT_UCONNECTOR (guid, val, ts, comment) VALUES (@guid, @val, @ts, @comment)
END
GO



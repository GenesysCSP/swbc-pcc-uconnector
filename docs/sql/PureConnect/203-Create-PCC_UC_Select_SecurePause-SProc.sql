
GO
/****** Object:  StoredProcedure [dbo].[PCC_UC_Select_SecurePause]    Script Date: 1/2/2019 12:31:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PCC_UC_Select_SecurePause] (@recordingIds VARCHAR(MAX))
AS
BEGIN
	SET NOCOUNT ON;
	WITH recordingIds AS( 
      SELECT RecordingId
      FROM ir_event
      WHERE RecordingId in (select * from dbo.SplitString(@recordingIds,',')) AND EventType = '5'
      )
    SELECT RecordingId, EventType,EventDate,Duration
    FROM ir_event
    WHERE recordingID IN (SELECT RecordingId FROM recordingIds) AND EventType IN ('1','4','5') order by RecordingId,EventDate asc
END

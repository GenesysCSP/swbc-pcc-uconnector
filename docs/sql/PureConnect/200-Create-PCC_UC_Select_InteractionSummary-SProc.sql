
GO
/****** Object:  StoredProcedure [dbo].[PCC_UC_Select_InteractionSummary]    Script Date: 1/2/2019 12:20:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PCC_UC_Select_InteractionSummary] (@interactionIds VARCHAR(MAX))
AS
BEGIN
	DECLARE @Direction varchar
	--DECLARE @ProcDate datetime = DATEADD(day, -7, GETDATE())
	SET NOCOUNT ON;
	SELECT InteractionSummary.InteractionIDKey,
           InteractionID,
           InteractionSummary.SiteID, 
           InitiatedDateTimeUTC, 
           ConnectedDateTimeUTC, 
           TerminatedDateTimeUTC,
           --CampaignName,
           nTalk,
           InteractionSummary.tConnected,
           tHeld, 
           nHeld, 
           InteractionSummary.tSuspend,
           tConference,
           nConference,
           InteractionSummary.tACW, 
           nTransfer,
           tExternal,
           nExternal,
           tDialing,
           tAlert,
           tIVRWait,
           nPark,
           tPark,
           nSecuredIVR,
           tSecuredIVR,
           nQueueWait,
           tQueueWait,
           LastStationId, 
           LastLocalUserId, 
           LastAssignedWorkgroupID, 
           DNIS_LocalID, 
           RemoteNumberFmt,
           StartDTOffset,
           LineDuration,
           --length,
           --caresult,
           --isabandoned,
           --customdata1,
           --customdata2,
           (InteractionSummary.tConnected + tHeld + InteractionSummary.tSuspend + tConference) As CalculatedDuration,
		   (CASE WHEN CAST(Direction as Varchar(max)) = '1' THEN 'Inbound' WHEN CAST(Direction as varchar(max)) = '2' THEN 'Outbound' ELSE CAST(Direction as varchar(max)) END) AS Direction,
		   AccountCode,
           CallEventLog,
           CallNote,
           ConnectionType,
           Disposition,
           DispositionCode,
           FirstAssignedAcdSkillSet,
           IndivID,
           IsRecorded,
           IsSurveyed,
           LastLocalName,
           LastLocalNumber,
           LineId,
           MediaServerID,
           MediaType,
           nIVR,
           OrgID,
           PurposeCode,
           RemoteICUserID,
           RemoteID,
           RemoteName,
           RemoteNumberCallId,
           RemoteNumberCountry,
           RemoteNumberLoComp1,
           RemoteNumberLoComp2,
           InteractionSummary.SeqNo,
           StartDateTimeUTC,
		   InteractionWrapup.WrapupCategory,
		   InteractionWrapup.WrapupCode,
		   InteractionCustomAttributes.CustomString3 AS ScripterWrapupCode,
		   InteractionCustomAttributes.CustomString4 AS ScripterWrapupCategory,
		   InteractionCustomAttributes.CustomString2 AS IVR_InteractionID,
		   ISNULL((SELECT sum(Duration) FROM Custom_SegmentsParsed CS WHERE CS.InteractionIDKey = InteractionSummary.InteractionIDKey AND CS.EndCode = 'hold'),0) AS TotalHoldDuration,  
		   ISNULL((SELECT count(endcode) FROM Custom_SegmentsParsed CS WHERE CS.InteractionIDKey = InteractionSummary.InteractionIDKey AND CS.EndCode = 'hold'),0) AS NoOfHoldCount  
           FROM InteractionSummary
           --LEFT OUTER JOIN ININ_DIALER_40.CallHistory  on InteractionSummary.InteractionIDKey = ININ_DIALER_40.CallHistory.callidkey
		   LEFT OUTER JOIN InteractionWrapup on InteractionSummary.InteractionIDKey = InteractionWrapup.InteractionIDKey
		   LEFT OUTER JOIN InteractionCustomAttributes on InteractionSummary.InteractionIDKey = InteractionCustomAttributes.InteractionIDKey
           WHERE 
		   --InitiatedDateTimeUTC > @ProcDate AND
		   --callhistory.callconnectedtimeUTC > @ProcDate AND
		   --InteractionCustomAttributes.I3TimeStampGMT > @ProcDate AND 
		   InteractionSummary.InteractionIDKey IN (select * from dbo.SplitString(@interactionIds,','))
END

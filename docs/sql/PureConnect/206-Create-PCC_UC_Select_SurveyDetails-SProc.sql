/****** Object:  StoredProcedure [dbo].[PCC_UC_Select_SurveyDetails]    Script Date: 08-06-2021 11:15:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PCC_UC_Select_SurveyDetails]     
(    
 @interactionIds VARCHAR(MAX)    
)     
AS    
BEGIN     
 SELECT     
   SF.CallIdKey,     
   SF.MaxScore,     
   SF.Score,    
   SA.SurveyQuestionId,     
   SQ.QuestionText,     
   SA.NumericScore,  
   SA.freeformansweruri  
 FROM SurveyForm SF    
   INNER JOIN InteractionSummary ISum     
    ON SF.CallidKey=ISum.InteractionIDKey    
   INNER JOIN SurveyAnswer SA     
    ON SF.SurveyFormId=SA.SurveyFormID    
   INNER JOIN SurveyQuestion SQ     
    ON SQ.SurveyQuestionID = SA.SurveyQuestionId    
 WHERE CallIdKey IN (SELECT * FROM dbo.SplitString(@interactionIds,','))    
 ORDER BY SF.CallIdKey    
END 
GO



﻿Namespace Configuration
  ''' <summary>
  ''' Configuration object to hold the UConnector's configuration
  ''' </summary>
  ''' <remarks></remarks>
    Public Interface IConfig
        Inherits IConfigSection

        ReadOnly Property ImporterType() As Type
        ReadOnly Property Name() As String

        Sub Save()

    End Interface
End Namespace

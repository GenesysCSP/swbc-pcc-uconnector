﻿Namespace Configuration
  ''' <summary>
  ''' A partial configuration.
  ''' </summary>
  ''' <remarks></remarks>
    Public Interface IConfigSection

        Default Property Value(ByVal name As String) As String
        ReadOnly Property ValueList(ByVal name As String) As IList(Of String)

    End Interface
End Namespace

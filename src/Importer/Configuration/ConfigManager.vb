﻿Imports System.Xml.Linq
Imports System.Data.SqlClient
Imports utopy.bDBServices
Imports System.Reflection
Imports System.Configuration
Imports System.Data.Common

Namespace Configuration
    ''' <summary>
    ''' Manager to read the application and UConnector configurations.
    ''' </summary>
    Public Module ConfigManager

        Public Function GetConnectionSettings(
            name As String
            ) As ConnectionStringSettings

            If name Is Nothing Then
                Throw New ArgumentNullException("name")
            End If

            name = name.Trim()
            If String.IsNullOrEmpty(name) = True Then
                Throw New ArgumentException("no connection name specified")
            End If

            Return (From settings In ConfigurationManager.ConnectionStrings.OfType(Of ConnectionStringSettings)()
                    Where settings.Name = name
                    Select settings).FirstOrDefault()
        End Function

        Public Function GetConnectionString(
            name As String
            ) As String

            Dim connSettings = GetConnectionSettings(name)
            Return If(connSettings IsNot Nothing, connSettings.ConnectionString, Nothing)
        End Function

        ''' <summary>
        ''' Get the database object for a specified name.
        ''' </summary>
        ''' <param name="name">The logical name for the database</param>
        ''' <returns>The database object</returns>
        ''' <remarks>The connection string for the database has to be in the <c>ConnectionStrings</c> section in the application's .config file.
        ''' The database provider can be specified for non-MSSQL databases.</remarks>
        Public Function GetDatabase(
            name As String
            ) As DBServices

            Dim provider = GetType(SqlClientFactory).Namespace

            Dim connSettings = GetConnectionSettings(name)
            If connSettings Is Nothing Then
                Return Nothing
            End If

            If String.IsNullOrEmpty(connSettings.ProviderName) = False Then
                provider = connSettings.ProviderName
            End If

            Dim dbsvc As New DBServices(provider)
            dbsvc.setDB(connSettings.ConnectionString, False)

            Return dbsvc
        End Function

        Public Function GetDBConnection(
            name As String
            ) As IDbConnection

            Dim provider = GetType(SqlClientFactory).Namespace
            Dim connSettings = GetConnectionSettings(name)

            If connSettings IsNot Nothing AndAlso String.IsNullOrEmpty(connSettings.ProviderName) = False Then
                provider = connSettings.ProviderName
            End If

            Dim factory = DbProviderFactories.GetFactory(provider)
            If factory Is Nothing Then
                Throw New ApplicationException(String.Format(
                    "Unable to find the {0} DB provider for {1}", provider, name))
            End If

            Dim dbConn = factory.CreateConnection()
            dbConn.ConnectionString = connSettings.ConnectionString
            Return dbConn
        End Function

        ''' <summary>
        ''' Read the UConnector configuration file.
        ''' </summary>
        ''' <param name="configFile">Path to the file</param>
        ''' <returns>The configuration object</returns>
        ''' <remarks></remarks>
        Public Function ReadConfiguration(ByVal configFile As String) As IConfig
            Dim xmlDoc As XDocument = XDocument.Load(configFile)
            Return New Config(configFile, xmlDoc)
        End Function

    End Module

End Namespace
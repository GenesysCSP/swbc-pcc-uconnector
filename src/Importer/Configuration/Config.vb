﻿Imports System.Xml.Linq
Imports System.Xml
Imports GenUtils.TraceTopic
Imports QBEUConnector.Main

Namespace Configuration
    ''' <summary>
    ''' The UConnector's configuration
    ''' </summary>
    ''' <remarks></remarks>
    Public Class Config
        Implements IConfig

        ''' <summary>
        ''' Constructor for settings object.
        ''' </summary>
        ''' <param name="path">Path of the configuration file</param>
        ''' <param name="xml">XML document for the configuration</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal path As String, ByVal xml As XDocument)
            _path = path
            _subConfigs = New List(Of IConfig)
            _changedSettings = New HashSet(Of String)

            Using (TraceTopic.importerTopic.scope())
                Dim rootNode As XElement = xml.Root
                For Each node As XElement In rootNode.Elements()
                    Select Case node.Name
                        Case XML_SETTINGS_SYSTEM
                            _importerType = Type.GetType(node.Value.Trim())
                            If _importerType Is Nothing Then
                                Throw New ApplicationException("Unknown or invalid system type: " & node.Value)
                            End If
                        Case XML_SETTINGS_INCLUDE
                            Dim xmlPath As String = node.Value.Trim()
                            Try
                                Dim fullPath = IO.Path.Combine(IO.Path.GetDirectoryName(_path), xmlPath)
                                Dim xmlDoc = XDocument.Load(fullPath)
                                _subConfigs.Add(New Config(fullPath, xmlDoc))
                            Catch ex As Exception
                                TraceTopic.importerTopic.warning("Could not load included configuration file " & xmlPath, ex)
                            End Try
                        Case Else
                            Me.LoadSettings(node, "")
                    End Select
                Next

                If _importerType Is Nothing Then
                    _importerType = GetType(BaseImporter)
                End If
            End Using
        End Sub

#Region "Class Methods"

        Protected Friend Shared Function FormatName(
            ParamArray levels() As String
            ) As String

            If levels.Any() = False Then
                Return String.Empty
            End If

            Return String.Join(SETTINGS_SEPARATOR, levels)
        End Function

#End Region

        Private Sub LoadSettings(ByVal node As XElement, ByVal parentName As String)
            If node.HasElements Then
                For Each child As XElement In node.Elements
                    LoadSettings(child, parentName & SETTINGS_SEPARATOR & node.Name.LocalName)
                Next
            Else
                Dim key As String = (parentName & SETTINGS_SEPARATOR & node.Name.LocalName).TrimStart(SETTINGS_SEPARATOR)
                Dim value As String = node.Value
                If _settings.ContainsKey(key) Then
                    value = _settings(key) & SETTINGS_MULTI_VALUE_SEPARATOR & value
                End If
                _settings(key) = value
            End If
        End Sub

        ''' <summary>
        ''' The name of the configuration.
        ''' </summary>
        ''' <value>The name of the configuration</value>
        ''' <returns>The configuration file name, excluding the extension</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property Name() As String Implements IConfig.Name
            Get
                Return IO.Path.GetFileNameWithoutExtension(_path)
            End Get
        End Property

        Public ReadOnly Property ImporterType() As Type Implements IConfig.ImporterType
            Get
                Return _importerType
            End Get
        End Property

        ''' <summary>
        ''' Accessor for the configuration properties
        ''' </summary>
        ''' <param name="name">The name of the property</param>
        ''' <value>The value of the property</value>
        ''' <returns>The value of the property from the settings dictionary</returns>
        ''' <remarks><para>When getting a property, it will be returned from the first configuration that contains it.</para>
        ''' <para>The value is only set if the property already exists.</para></remarks>
        Default Public Overridable Property Value(ByVal name As String) As String Implements IConfig.Value
            Get
                If _settings.ContainsKey(name) Then
                    Return _settings(name)
                Else
                    For Each subConf As IConfig In _subConfigs
                        Dim ret As String = subConf(name)
                        If ret IsNot Nothing Then
                            Return ret
                        End If
                    Next
                End If
                Return Nothing
            End Get
            Set(ByVal value As String)
                ' Don't allow settings to have empty names.
                name = If(name IsNot Nothing, name.Trim(), Nothing)
                Using (TraceTopic.importerTopic.scope())
                    If String.IsNullOrEmpty(name) = True Then
                        TraceTopic.importerTopic.note("Attempt at modifying a {} setting with an empty name (Value={}) From:{}{}",
                            Me.GetType().Name, value, vbCrLf, New StackTrace())
                        Return
                    End If

                    If _settings.ContainsKey(name) Then
                        _settings(name) = value
                        _changedSettings.Add(name)
                    Else
                        For Each subConf As IConfig In _subConfigs
                            subConf(name) = value
                        Next
                    End If
                End Using
            End Set
        End Property

        ''' <summary>
        ''' Accessor for a property with multiple values
        ''' </summary>
        ''' <param name="name">The name of the property</param>
        ''' <value>The list of values</value>
        ''' <returns>The list of values</returns>
        ''' <remarks><para>When getting a property, you will get the values from all configurations that contain it. A value can appear more than once.</para>
        ''' <para>The value is only set if the property already exists.</para></remarks>
        Public ReadOnly Property ValueList(ByVal name As String) As IList(Of String) Implements IConfigSection.ValueList
            Get
                Dim values As New List(Of String)
                If _settings.ContainsKey(name) Then
                    values.AddRange(_settings(name).Split(New String() {SETTINGS_MULTI_VALUE_SEPARATOR}, StringSplitOptions.RemoveEmptyEntries))
                End If
                For Each subConf As IConfig In _subConfigs
                    Dim newValues As IList(Of String) = subConf.ValueList(name)
                    If newValues IsNot Nothing AndAlso newValues.Count > 0 Then
                        values.AddRange(newValues)
                    End If
                Next
                Return values
            End Get
        End Property

        ''' <summary>
        ''' Save the settings that were modified back to the configuration file.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub Save() Implements IConfig.Save
            Dim xmlDoc = XDocument.Load(_path)
            Dim root = xmlDoc.Root
            Using (TraceTopic.importerTopic.scope())
                If _changedSettings.Count > 0 Then
                    For Each key As String In _changedSettings
                        Dim value As String = Me(key)

                        Try
                            Dim ele As XElement = XPath.Extensions.XPathSelectElement(root, key)
                            ele.SetValue(value)
                        Catch ex As Exception
                            TraceTopic.importerTopic.warning("Encountered an error updating config with {}={} (Reason: {})",
                              key, value, ex.Message)
                        End Try
                    Next
                    xmlDoc.Save(_path)
                End If

                For Each subConf As IConfig In _subConfigs
                    subConf.Save()
                Next
            End Using
        End Sub

#Region "Class Data"

        Private Const XML_SETTINGS_SYSTEM As String = "System"
        Private Const XML_SETTINGS_INCLUDE As String = "Include"
        Protected Friend Const SETTINGS_SEPARATOR As Char = "/"c
        Protected Friend Const SETTINGS_MULTI_VALUE_SEPARATOR As String = vbNewLine

#End Region

#Region "Instance Data"

        Private _changedSettings As ICollection(Of String)
        Private ReadOnly _importerType As Type
        Private ReadOnly _path As String
        Private ReadOnly _settings As New Dictionary(Of String, String)(StringComparer.OrdinalIgnoreCase)
        Private ReadOnly _subConfigs As New List(Of IConfig)

#End Region

    End Class
End Namespace
﻿Public Class AgentsServices

  Private Class AgentsPair

    Private m_parent As Agent
    Private m_child As Agent

    Public ReadOnly Property Child() As Agent
      Get
        Return m_child
      End Get
    End Property

    Public ReadOnly Property Parent() As Agent
      Get
        Return m_parent
      End Get
    End Property

    Public Sub New(ByVal _parent As Agent, ByVal _child As Agent)
      m_parent = _parent
      m_child = _child
    End Sub

    Public Overrides Function Equals(ByVal obj As Object) As Boolean
      Return AgentsPair.Equals(obj, Me)
    End Function

    Public Overrides Function GetHashCode() As Integer
      Return (If(m_parent Is Nothing, "NOTHING", m_parent.ID) + m_child.ID).GetHashCode()
    End Function

    Public Overloads Shared Function Equals(ByVal objA As Object, ByVal objB As Object) As Boolean
      Dim pairA As AgentsPair = TryCast(objA, AgentsPair)
      Dim pairB As AgentsPair = TryCast(objB, AgentsPair)

      If pairA IsNot Nothing AndAlso pairB IsNot Nothing Then
        Return Agent.Equals(pairA.m_parent, pairB.m_parent) AndAlso _
               Agent.Equals(pairA.m_child, pairB.m_child)
      End If

      Return objA Is Nothing AndAlso objB Is Nothing
    End Function

  End Class 'AgentsPair

  Public Shared Function CompareAgentsHierarchies(ByVal _listA As Generic.IEnumerable(Of Agent), ByVal _listB As Generic.IEnumerable(Of Agent)) As Generic.IEnumerable(Of Agent)
    ' compare the hierarchy of the two agents list and return a list of agents that thier hierarchy was changed 
    ' (including all thier children, becuase the children hierarchy was changed too)
    ' note that the hierarchy is not a real tree, it may contain cycles
    ' to find those agents the function build the edge matrix for the agents list and compares them - it trys to find the edges that exist on one list
    ' and not in the other - this means that for such edge from A1 to A2 the hierarchy for A2 was changed, so we add A2 to the list 'missingAgentsAdges'
    ' at the end of this process we find all the descendants of all the agents in 'missingAgentsAdges', the complete list contains all the agents that thier
    ' hierarchy was changed.

    Dim listAEdges As New Generic.HashSet(Of AgentsPair)()
    Dim listBEdges As New Generic.HashSet(Of AgentsPair)()

    BuildAgentsEdges(Nothing, _listA, listAEdges)
    BuildAgentsEdges(Nothing, _listB, listBEdges)

    Dim missingAgentsAdges As New Generic.HashSet(Of Agent)

    ' get the differences between listA to listB
    For Each pair As AgentsPair In listAEdges
      If Not listBEdges.Contains(pair) _
         AndAlso Not missingAgentsAdges.Contains(pair.Child) Then
        missingAgentsAdges.Add(pair.Child)
      End If
    Next

    ' get the differences between listB to listA
    For Each pair As AgentsPair In listBEdges
      If Not listAEdges.Contains(pair) _
         AndAlso Not missingAgentsAdges.Contains(pair.Child) Then
        missingAgentsAdges.Add(pair.Child)
      End If
    Next

    ' now need to add all the descendants of all of the agents in 'missingAgentsAdges'
    Dim hierarchyChanges As New Generic.HashSet(Of Agent)

    For Each agent As Agent In missingAgentsAdges
      ' add the current agent
      If Not hierarchyChanges.Contains(agent) Then
        hierarchyChanges.Add(agent)

        ' add all of it's descendants
        For Each descendant As Agent In agent.GetAllDescendants()
          If Not hierarchyChanges.Contains(descendant) Then
            hierarchyChanges.Add(descendant)
          End If
        Next
      End If
    Next

    Return hierarchyChanges
  End Function

  Private Shared Sub BuildAgentsEdges(ByVal _parent As Agent, ByVal _childs As Generic.IEnumerable(Of Agent), ByVal _edges As Generic.HashSet(Of AgentsPair))
    For Each childNode As Agent In _childs
      Dim edge As New AgentsPair(_parent, childNode)

      If Not _edges.Contains(edge) Then
        _edges.Add(edge)
        BuildAgentsEdges(childNode, childNode.Children, _edges)
      End If
    Next
  End Sub

End Class 'AgentsServices

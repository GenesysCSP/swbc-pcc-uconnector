﻿Imports QBEUConnector.Configuration

Namespace Conversion
    ''' <summary>
    ''' Base class for the audio converters
    ''' </summary>
    ''' <remarks></remarks>
    Public MustInherit Class BaseConverter
        Implements IAudioConverter

        ''' <summary>
        ''' The configuration holding the conversion parameters
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_configuration As ConversionConfig

        ''' <summary>
        ''' Create a converter with the given configuration
        ''' </summary>
        ''' <param name="configuration">The configuration</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection)
            m_configuration = New ConversionConfig(configuration)
        End Sub

        ''' <summary>
        ''' Convert the audio file
        ''' </summary>
        ''' <param name="sourcePath">Path to the file to be converted</param>
        ''' <param name="targetPath">Path for the converted file</param>
        ''' <param name="sourceEncryption">Encryption key used for the source file</param>
        ''' <param name="targetEncryption">Encryption key to use for the target</param>
        ''' <returns>Whether the conversion succeeded or not</returns>
        ''' <remarks>For unencrypted files, use 0 for the encryption key</remarks>
        Public MustOverride Function Convert(ByVal sourcePath As String, ByVal targetPath As String, ByVal sourceEncryption As Integer, ByRef targetEncryption As Integer) As Boolean Implements IAudioConverter.Convert

        ''' <summary>
        ''' The extension of the file to be converted
        ''' </summary>
        ''' <value>The source file's extension</value>
        ''' <returns>The extension string</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property SourceExtension() As String Implements IAudioConverter.SourceExtension
            Get
                Return m_configuration.SourceExtension
            End Get
        End Property

        ''' <summary>
        ''' The extension to give the converted file
        ''' </summary>
        ''' <value>The target file's extension</value>
        ''' <returns>The extension string</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property TargetExtension() As String Implements IAudioConverter.TargetExtension
            Get
                Return m_configuration.TargetExtension
            End Get
        End Property
    End Class
End Namespace
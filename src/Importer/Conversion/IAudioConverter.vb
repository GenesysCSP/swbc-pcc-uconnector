﻿Namespace Conversion
  ''' <summary>
  ''' Converts audio between different formats
  ''' </summary>
  ''' <remarks></remarks>
  Public Interface IAudioConverter

    ''' <summary>
    ''' Convert the audio file
    ''' </summary>
    ''' <param name="sourcePath">Path to the file to be converted</param>
    ''' <param name="targetPath">Path for the converted file</param>
    ''' <param name="sourceEncryption">Encryption key used for the source file</param>
    ''' <param name="targetEncryption">Encryption key to use for the target</param>
    ''' <returns>Whether the conversion succeeded or not</returns>
    ''' <remarks>For unencrypted files, use 0 for the encryption key</remarks>
    Function Convert(ByVal sourcePath As String, ByVal targetPath As String, ByVal sourceEncryption As Integer, ByRef targetEncryption As Integer) As Boolean
    ''' <summary>
    ''' The extension of the file to be converted
    ''' </summary>
    ''' <value>The source file's extension</value>
    ''' <returns>The extension string</returns>
    ''' <remarks></remarks>
    ReadOnly Property SourceExtension() As String
    ''' <summary>
    ''' The extension to give the converted file
    ''' </summary>
    ''' <value>The target file's extension</value>
    ''' <returns>The extension string</returns>
    ''' <remarks></remarks>
    ReadOnly Property TargetExtension() As String

  End Interface
End Namespace
﻿Imports GenUtils.TraceTopic
Imports QBEUConnector.Configuration

Namespace Conversion
    ''' <summary>
    ''' Converter that uses an external process to convert the audio file.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class InternalConverter
        Inherits BaseConverter

        ''' <summary>
        ''' Create a converter with the given configuration
        ''' </summary>
        ''' <param name="configuration">The configuration</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection)
            MyBase.New(configuration)
        End Sub

        ''' <summary>
        ''' Convert the audio file
        ''' </summary>
        ''' <param name="sourcePath">Path to the file to be converted</param>
        ''' <param name="targetPath">Path for the converted file</param>
        ''' <param name="sourceEncryption">Encryption key used for the source file</param>
        ''' <param name="targetEncryption">Encryption key to use for the target</param>
        ''' <returns>Whether the conversion succeeded or not</returns>
        ''' <remarks>For unencrypted files, use 0 for the encryption key</remarks>
        Public Overrides Function Convert(ByVal sourcePath As String, ByVal targetPath As String, ByVal sourceEncryption As Integer, ByRef targetEncryption As Integer) As Boolean
            Dim audio As New utopy.aAudioUaudio.UAudio()
            Using (TraceTopic.importerTopic.scope())
                If audio.readFile(sourcePath, m_configuration.SourceFormat, sourceEncryption, 0) <> utopy.aAudioUaudio.FileStatus.OK Then
                    TraceTopic.importerTopic.warning("Could not read audio file " & sourcePath)
                    Return False
                End If
                Dim rc As Boolean = False
                Select Case m_configuration.TargetFormat
                    Case utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat.WAV_ADPCM
                        rc = audio.convertToWAVADPCM(audio.numChannels)
                    Case utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat.WAV_MULAW
                        rc = audio.convertToWAVMulaw(audio.numChannels)
                    Case utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat.WAV_ALAW
                        rc = audio.convertToWAVAlaw(audio.numChannels)
                    Case utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat.WAV_GSM610
                        rc = audio.convertToWAVGSM610()
                    Case utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat.WAV_PCM
                        rc = audio.convertToWAVPcm(audio.samplingRate, audio.numChannels)
                    Case utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat.WAV_TRUESPEECH
                        rc = audio.convertToWAVTruespeech()
                    Case Else
                        Throw New NotImplementedException("Conversion to " & m_configuration.TargetFormat.ToString() & " not implemented")
                End Select
                If Not rc Then
                    TraceTopic.importerTopic.warning("Could not convert audio file " & sourcePath & " to " & m_configuration.TargetFormat.ToString())
                    Return False
                End If
            End Using
            Return audio.writeFile(targetPath, targetEncryption)
        End Function
    End Class
End Namespace
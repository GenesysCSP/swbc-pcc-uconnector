﻿Imports GenUtils.TraceTopic
Imports QBEUConnector.Configuration
Imports QBEUConnector.Download

Namespace Conversion
    ''' <summary>
    ''' Converter that uses an external process to convert the audio file.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class ExternalConverter
        Inherits BaseConverter

        ''' <summary>
        ''' String to be replaced with the path to file to be converted.
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const SOURCE_HOLDER As String = "%INPUT%"
        ''' <summary>
        ''' String to be replaced with the path for writing the converted file.
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const TARGET_HOLDER As String = "%OUTPUT%"

        ''' <summary>
        ''' Create a converter with the given configuration
        ''' </summary>
        ''' <param name="configuration">The configuration</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection)
            MyBase.New(configuration)
        End Sub

        ''' <summary>
        ''' Convert the audio file
        ''' </summary>
        ''' <param name="sourcePath">Path to the file to be converted</param>
        ''' <param name="targetPath">Path for the converted file</param>
        ''' <param name="sourceEncryption">Encryption key used for the source file</param>
        ''' <param name="targetEncryption">Encryption key to use for the target</param>
        ''' <returns>Whether the conversion succeeded or not</returns>
        ''' <remarks>For unencrypted files, use 0 for the encryption key</remarks>
        Public Overrides Function Convert(ByVal sourcePath As String, ByVal targetPath As String, ByVal sourceEncryption As Integer, ByRef targetEncryption As Integer) As Boolean
            Dim ret As Boolean = False
            Using (TraceTopic.importerTopic.scope())


                If String.IsNullOrEmpty(m_configuration.ConverterPath) Then
                    Throw New Exception("Cannot convert " & sourcePath & ": Audio Converter Path not set")
                End If

                Dim outTmpPath As String = FileUtils.GetTempPath(".wav")
                Dim convertCmd As String = m_configuration.ConverterPath
                Dim convertArg As String = m_configuration.ConversionArguments
                convertArg = convertArg.Replace(SOURCE_HOLDER, sourcePath)
                convertArg = convertArg.Replace(TARGET_HOLDER, outTmpPath)

                TraceTopic.importerTopic.always("Running: '" & convertCmd & " " & convertArg & "'")

                Dim proc As New Diagnostics.Process()
                proc.StartInfo.FileName = convertCmd
                proc.StartInfo.Arguments = convertArg
                If Not String.IsNullOrEmpty(m_configuration.WorkingDirectory) Then
                    proc.StartInfo.WorkingDirectory = m_configuration.WorkingDirectory
                End If
                proc.StartInfo.UseShellExecute = False
                proc.StartInfo.CreateNoWindow = True
                proc.StartInfo.RedirectStandardOutput = False
                proc.StartInfo.RedirectStandardError = False
                proc.StartInfo.ErrorDialog = False

                proc.Start()
                If Not proc.WaitForExit(CInt(m_configuration.ConversionTimeout.TotalMilliseconds)) Then
                    proc.Kill()
                End If

                proc.Dispose()

                If IO.File.Exists(outTmpPath) Then
                    IO.File.Copy(outTmpPath, targetPath, True)
                    ret = True
                    IO.File.Delete(outTmpPath)
                End If
            End Using
            Return ret
        End Function
    End Class
End Namespace

Imports System.Threading
Imports System.IO
Imports GenUtils.TraceTopic

Public Class convertAudio
    Enum ConvertType
        NO_CONVERSION_TYPE = 0
        G729_TYPE = 1
        G723_TYPE = 2
        G726_TYPE = 3
        GSM610_TYPE = 4
        ALAW = 5
    End Enum
    Public m_source As String
    Public m_target As String
    Public m_type As ConvertType
    Private m_inUse As Boolean = False
    Private m_toolsPath As String

    Private Shared G726ConvertCmd As String = "ffmpeg.exe"
    Private Shared G726ConvertArg As String = "-y -acodec g726 -ab 8000 -ar 16000 -ac 1 -i ""%INPUT%"" -acodec pcm_s16le -ac 1 -ar 8000 -ab 16000 ""%OUTPUT%"""

    Private Shared G729ConvertCmd As String = "ffmpeg.exe"
    Private Shared G729ConvertArg As String = "-y -acodec g729 -i ""%INPUT%"" -acodec pcm_s16le -ac 1 -ar 8000 ""%OUTPUT%"""

    Private Shared GSM610Convert As String = "sox.exe"
    Private Shared GSM610ConvertArg As String = "-e gsm ""%INPUT%"" -e gsm ""%OUTPUT%"""

    Private Shared ALAWConvertCmd As String = "ffmpeg.exe"
    Private Shared ALAWConvertArg As String = "-i ""%INPUT%""  -ac 2 ""%OUTPUT%"""

    Private Const INPUT_FILE As String = "%INPUT%"
    Private Const OUTPUT_FILE As String = "%OUTPUT%"

    Public Property toolsPath() As String
        Get
            Return m_toolsPath
        End Get
        Set(ByVal value As String)
            m_toolsPath = value
        End Set
    End Property

    Public Shared Property convertCmd(ByVal _type As ConvertType) As String
        Get
            Select Case _type
                Case ConvertType.G729_TYPE
                    Return G729ConvertCmd
                Case ConvertType.G726_TYPE
                    Return G726ConvertCmd
                Case ConvertType.GSM610_TYPE
                    Return GSM610Convert
                Case Else
                    Return Nothing
            End Select
        End Get
        Set(ByVal value As String)
            Select Case _type
                Case ConvertType.G729_TYPE
                    G729ConvertCmd = value
                Case ConvertType.G726_TYPE
                    G726ConvertCmd = value
                Case ConvertType.GSM610_TYPE
                    GSM610Convert = value
            End Select
        End Set
    End Property

    Public Shared Property convertArgs(ByVal _type As ConvertType) As String
        Get
            Select Case _type
                Case ConvertType.G729_TYPE
                    Return G729ConvertArg
                Case ConvertType.G726_TYPE
                    Return G726ConvertArg
                Case ConvertType.GSM610_TYPE
                    Return GSM610ConvertArg
                Case Else
                    Return Nothing
            End Select
        End Get
        Set(ByVal value As String)
            Select Case _type
                Case ConvertType.G729_TYPE
                    G729ConvertArg = value
                Case ConvertType.G726_TYPE
                    G726ConvertArg = value
                Case ConvertType.GSM610_TYPE
                    GSM610ConvertArg = value
            End Select
        End Set
    End Property

    Public ReadOnly Property isInUse() As Boolean
        Get
            Return m_inUse
        End Get
    End Property

    Public Sub setInUse()
        m_inUse = True
    End Sub

    Public Sub setNotInUse()
        m_inUse = False
    End Sub

    Dim m_t As Thread
    Public Sub go()

        m_t = New Thread(AddressOf doConvertWork)
        m_t.Start()

    End Sub

    Public Sub abort()
        If m_t IsNot Nothing AndAlso m_t.IsAlive Then
            m_t.Abort()
        End If
    End Sub

    Private Sub doConvertWork()
        Dim targetXml As String = IO.Path.ChangeExtension(m_target, "xml")
        Dim sourceXml As String = IO.Path.ChangeExtension(m_source, "xml")
        Using (TraceTopic.importerTopic.scope())
            Try
                Dim status As Boolean
                Dim outTmpPath As String = IO.Path.GetTempFileName
                status = convertFile(m_type, m_source, outTmpPath, toolsPath)
                If status = True Then
                    ' update wav & xml system time to call recording time
                    Dim callTime As DateTime = getTimeFromXML(sourceXml)
                    Dim copied As Boolean = False
                    While copied = False
                        If Importer.GetAvailableSpaceGB(IO.Path.GetDirectoryName(m_target)) > 1 Then
                            Try
                                If IO.File.Exists(m_target) Then
                                    IO.File.Delete(m_target)
                                End If
                                IO.File.Move(outTmpPath, m_target)
                                copied = True
                            Catch ex As Exception
                                TraceTopic.importerTopic.warning("Failed copying audio to system input folder (" & m_target & "). Will wait and try again. " & vbCrLf & ex.Message & vbCrLf & ex.StackTrace)
                                Thread.Sleep(10 * 60 * 1000) ' sleep 10 minutes to give the fetcher time to pull some calls
                            End Try
                        Else
                            TraceTopic.importerTopic.warning("storage is low on input folder (" & m_target & "). Will wait and try again. ")
                            Thread.Sleep(10 * 60 * 1000) ' sleep 10 minutes to give the fetcher time to pull some calls
                        End If
                    End While
                    If IO.File.Exists(targetXml) = True Then
                        IO.File.Delete(targetXml)
                    End If
                    IO.File.Move(sourceXml, targetXml)
                    IO.File.Delete(m_source)
                    Try
                        IO.File.SetLastWriteTime(m_target, callTime)
                        IO.File.SetLastWriteTime(targetXml, callTime)
                        IO.File.SetCreationTime(m_target, callTime)
                        IO.File.SetCreationTime(targetXml, callTime)
                    Catch ex As Exception
                        TraceTopic.importerTopic.warning("failed setting creation time on " & m_target & ".", ex)
                    End Try
                End If
            Catch ex As Exception
                TraceTopic.importerTopic.warning("Exception converting the audio file. ", ex)
            Finally
                m_inUse = False
            End Try
        End Using
    End Sub

    Private Function getTimeFromXML(ByVal _path As String) As DateTime
        Dim ret As DateTime = Now
        Try
            Dim doc As New System.Xml.XmlDocument
            doc.Load(_path)
            ret = CDate(doc(Importer.XML_OUTPUT_CALLINFORMATION)(Importer.XML_OUTPUT_CALLTIME).InnerText())
        Catch ex As Exception

        End Try

        Return ret
    End Function

    Public Shared Function convertFile(ByVal type As ConvertType, ByVal _source As String, ByVal _target As String, Optional ByVal _toolsPath As String = "") As Boolean
        Dim ret As Boolean = True
        Dim outTmpPath As String = ""
        Dim tmpFinalPath As String = ""
        Dim tmpFile As String = ""
        Using (TraceTopic.importerTopic.scope())
            Try
                Dim success As Boolean = False
                Select Case type
                    Case ConvertType.G729_TYPE
                        tmpFile = removeHeader(_source, success)
                        If success = True Then
                            Dim fi As New IO.FileInfo(_source)
                            outTmpPath = IO.Path.GetTempFileName
                            g729toWAVPCM(tmpFile, outTmpPath, _toolsPath)
                            tmpFinalPath = IO.Path.GetTempFileName()
                            writeWavWithHeader(outTmpPath, tmpFinalPath)
                            If IO.File.Exists(_target) = True Then
                                IO.File.Delete(_target)
                            End If
                            IO.File.Move(tmpFinalPath, _target)
                        End If

                    Case ConvertType.G726_TYPE
                        g726toWAVPCM(_source, _target, _toolsPath)

                    Case ConvertType.G723_TYPE
                        g723toWAVPCM(_source, _target, "", "")

                    Case ConvertType.GSM610_TYPE
                        Gsm610toGsm610(_source, _target, _toolsPath)
                    Case ConvertType.ALAW
                        alawtoWAVPCM(_source, _target, _toolsPath)
                    Case ConvertType.NO_CONVERSION_TYPE
                        Try
                            If IO.File.Exists(_target) Then
                                IO.File.Delete(_target)
                            End If
                            IO.File.Move(_source, _target)
                        Catch ex As Exception
                            IO.File.Copy(_source, _target)
                        End Try

                End Select
            Catch ex As Exception
                ret = False
                TraceTopic.importerTopic.error("failed converting file." & vbCrLf & ex.Message & vbCrLf & ex.StackTrace)
            Finally
                If IO.File.Exists(outTmpPath) = True Then
                    IO.File.Delete(outTmpPath)
                End If
                If IO.File.Exists(tmpFinalPath) = True Then
                    IO.File.Delete(tmpFinalPath)
                End If
                If IO.File.Exists(tmpFile) = True Then
                    IO.File.Delete(tmpFile)
                End If
            End Try
        End Using
        Return ret
    End Function

    Private Shared Function cleanFileName(ByVal _fileName As String) As String
        Return _fileName.Replace("{"c, "").Replace("}"c, "").Replace("-"c, "")
    End Function

    Private Shared Sub writeWavWithHeader(ByVal _inPath As String, ByVal _outPath As String)
        Dim aud As New utopy.aAudioWavfile.WavFile()
        Dim buff() As Byte = IO.File.ReadAllBytes(_inPath)
        aud.config(8000, 16, 1, utopy.aAudioWavfile.WavFile.WAV_FORMAT_PCM)
        aud.setNumSamples(CULng(buff.Length / 2))

        Using fs As New FileStream(_outPath, FileMode.Create, FileAccess.Write)
            Dim bw As New BinaryWriter(fs)
            aud.writeHeader(bw)
            aud.writeData(bw, buff)
        End Using
    End Sub
    Private Shared Sub g729toWAVPCM(ByVal _inPath As String, ByVal _outPath As String, ByVal _toolsPath As String)
        DoToolConvert(_inPath, _outPath, _toolsPath, G729ConvertCmd, G729ConvertArg)
    End Sub

    Private Shared Sub g723toWAVPCM(ByVal _inPath As String, ByVal _outPath As String, ByVal m_g723toWMACommandPath As String, ByVal m_WMAtoWAVPCMcommandPath As String)
        'Dim cmd As String
        'Dim _midConvPath As String = _inPath.Replace("wav", "wma")
        'cmd = "cscript.exe """ & m_g723toWMACommandPath & """ -input """ & _inPath & """ -output """ & _midConvPath & """ -profile a64"
        'Interaction.Shell(cmd, AppWinStyle.Hide, True, -1)
        '' delete g723 temp wav file
        'IO.File.Delete(_inPath)
        'cmd = m_WMAtoWAVPCMcommandPath & " -i """ & _midConvPath & """ -acodec pcm_s16le -ar 8000 -ac 1 """ & _outPath & """"
        'Interaction.Shell(cmd, AppWinStyle.Hide, True)
        'delete WMA file
        'IO.File.Delete(_midConvPath)

        Thread.Sleep(100)
    End Sub

    Private Shared Sub g726toWAVPCM(ByVal _inPath As String, ByVal _outPath As String, ByVal _toolsPath As String)
        DoToolConvert(_inPath, _outPath, _toolsPath, G726ConvertCmd, G726ConvertArg)
    End Sub
    Private Shared Sub alawtoWAVPCM(ByVal _inPath As String, ByVal _outPath As String, ByVal _toolsPath As String)
        DoToolConvert(_inPath, _outPath, _toolsPath, ALAWConvertCmd, ALAWConvertArg)
    End Sub
    Private Shared Sub Gsm610toGsm610(ByVal _inPath As String, ByVal _outPath As String, ByVal _toolsPath As String)
        DoToolConvert(_inPath, _outPath, _toolsPath, GSM610Convert, GSM610ConvertArg)
    End Sub

    Private Shared Sub DoToolConvert(ByVal _inPath As String, ByVal _outPath As String, ByVal _toolsPath As String, ByVal _convertCmd As String, ByVal _convertArgs As String)
        If String.IsNullOrEmpty(_toolsPath) Then
            Throw New Exception("Cannot convert " & _inPath & ": tools path not set")
        End If

        Dim tmpPath As String = IO.Path.GetTempFileName()
        Using (TraceTopic.importerTopic.scope())
            Try
                Dim outTmpPath As String = tmpPath & ".wav"
                Dim convertCmd As String = _toolsPath & "\" & _convertCmd
                Dim convertArg As String = _convertArgs.Replace(INPUT_FILE, _inPath)
                convertArg = convertArg.Replace(OUTPUT_FILE, outTmpPath)

                TraceTopic.importerTopic.note("Running: '" & convertCmd & " " & convertArg & "'")

                Dim proc As New Diagnostics.Process()
                proc.StartInfo.FileName = convertCmd
                proc.StartInfo.Arguments = convertArg
                proc.StartInfo.UseShellExecute = False
                proc.StartInfo.CreateNoWindow = True
                proc.StartInfo.RedirectStandardOutput = False
                proc.StartInfo.RedirectStandardError = False
                proc.StartInfo.ErrorDialog = False

                proc.Start()
                proc.WaitForExit()

                proc.Dispose()

                If IO.File.Exists(outTmpPath) Then
                    IO.File.Copy(outTmpPath, _outPath, True)
                    IO.File.Delete(outTmpPath)
                End If
            Finally
                If IO.File.Exists(tmpPath) Then
                    IO.File.Delete(tmpPath)
                End If
            End Try
        End Using
        Thread.Sleep(100)
    End Sub

    Private Shared Function removeHeader(ByVal _file As String, ByRef _success As Boolean) As String
        Dim newFilePath As String = IO.Path.GetTempFileName()
        Dim fs As New IO.FileStream(_file, IO.FileMode.Open, IO.FileAccess.Read)
        advanceToStart(fs, _success)

        If _success = True Then
            Dim outFs As New IO.FileStream(newFilePath, IO.FileMode.Create, IO.FileAccess.Write)
            While fs.Position < fs.Length
                outFs.WriteByte(CByte(fs.ReadByte))
            End While


            outFs.Close()
        End If
        fs.Close()
        Return newFilePath
    End Function

    Private Shared Sub advanceToStart(ByVal _fs As IO.FileStream, ByRef _success As Boolean)
        'Dim fr As New IO.StreamReader(_fs)
        Dim found As Boolean = False
        Dim c1, c2, c3, c4 As Char
        While found = False AndAlso _fs.Position < _fs.Length
            While c1 <> "d"c AndAlso _fs.Position < _fs.Length
                c1 = ChrW(_fs.ReadByte())
            End While
            c2 = ChrW(_fs.ReadByte())
            c3 = ChrW(_fs.ReadByte())
            c4 = ChrW(_fs.ReadByte())
            If c2 = "a"c AndAlso c3 = "t"c AndAlso c4 = "a"c Then
                _fs.ReadByte()
                _fs.ReadByte()
                _fs.ReadByte()
                _fs.ReadByte()
                found = True
                _success = True
            Else
                If _fs.Position < _fs.Length Then
                    _fs.Position = _fs.Position - 3
                End If
                found = False
                c1 = CChar("")
            End If
        End While
    End Sub
End Class

﻿Imports QBEUConnector.Data

Namespace Download
    ''' <summary>
    ''' Downloader for interaction content
    ''' </summary>
    ''' <remarks></remarks>
    Public Interface IContentDownloader
        ''' <summary>
        ''' Download the content
        ''' </summary>
        ''' <param name="interaction">Data about the interaction</param>
        ''' <param name="targetPath">Place to put the downloaded content</param>
        ''' <returns>True if it succeeds</returns>
        ''' <remarks></remarks>
        Function GetFile(ByVal interaction As IInteractionData, ByRef targetPath As String) As Boolean
    End Interface
End Namespace
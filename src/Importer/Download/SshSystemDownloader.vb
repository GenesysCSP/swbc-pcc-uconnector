﻿Imports System.IO
Imports QBEUConnector.Configuration
Imports QBEUConnector.Data

Namespace Download
    ''' <summary>
    ''' Content downloader that uses secure shell to copy files
    ''' </summary>
    ''' <remarks></remarks>
    Public Class SshDownloader
        Implements IContentDownloader

        ''' <summary>
        ''' The configuration that specifies parameters for downloading content
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_configuration As DownloadConfig

        ''' <summary>
        ''' Create the downloader with its configuration
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection)
            m_configuration = New DownloadConfig(configuration)
        End Sub

        ''' <summary>
        ''' Copy the file
        ''' </summary>
        ''' <param name="interaction">Data about the interaction. Includes the file's location</param>
        ''' <param name="targetPath">Place to put the copied file</param>
        ''' <returns>True if it succeeds</returns>
        ''' <remarks></remarks>
        Public Function GetFile(ByVal interaction As IInteractionData, ByRef targetPath As String) As Boolean Implements IContentDownloader.GetFile
            Dim sshCopy As Tamir.SharpSsh.Scp = sshConnect()
            Try
                sshCopy.From(interaction.ContentFilePath, targetPath)
                Return True
            Finally
                sshCopy.Close()
            End Try
            Return True
        End Function

        ''' <summary>
        ''' Connect to secure shell
        ''' </summary>
        ''' <returns>Secure copy object</returns>
        ''' <remarks></remarks>
        <CLSCompliant(False)> _
        Protected Function sshConnect() As Tamir.SharpSsh.Scp
            SyncLock GetType(Tamir.SharpSsh.Scp)
                Dim scpCon As New Tamir.SharpSsh.Scp(m_configuration.Host, m_configuration.User, m_configuration.Password)
                scpCon.Connect()
                Return scpCon
            End SyncLock
        End Function

    End Class
End Namespace
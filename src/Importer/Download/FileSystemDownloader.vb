﻿Imports System.IO
Imports GenUtils.TraceTopic
Imports QBEUConnector.Configuration
Imports QBEUConnector.Data

Namespace Download
    ''' <summary>
    ''' Content downloader that copies from the file system
    ''' </summary>
    ''' <remarks></remarks>
    Public Class FileSystemDownloader
        Implements IContentDownloader

        ''' <summary>
        ''' The configuration that specifies parameters for downloading content
        ''' </summary>
        ''' <remarks>This simple implementation does not use any configuration parameters. The configuration is there for inheriting classes.</remarks>
        Protected m_configuration As DownloadConfig

        ''' <summary>
        ''' Create the downloader with its configuration
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection)
            m_configuration = New DownloadConfig(configuration)
        End Sub

        ''' <summary>
        ''' Copy the file
        ''' </summary>
        ''' <param name="interaction">Data about the interaction. Includes the file's location</param>
        ''' <param name="targetPath">Place to put the copied file</param>
        ''' <returns>True if it succeeds</returns>
        ''' <remarks></remarks>
        Public Overridable Function GetFile(ByVal interaction As IInteractionData, ByRef targetPath As String) As Boolean Implements IContentDownloader.GetFile
            Dim sourcePath As String = interaction.ContentFilePath
            If Not File.Exists(sourcePath) Then
                HandleMissingFile(sourcePath)
                Return False
            End If
            File.Copy(sourcePath, targetPath, True)
            Return True
        End Function

        ''' <summary>
        ''' Handle the case when the content file does not exist
        ''' </summary>
        ''' <param name="path">Path where the file should have been</param>
        ''' <remarks></remarks>
        Protected Overridable Sub HandleMissingFile(ByVal path As String)
            Using (TraceTopic.importerTopic.scope())
                TraceTopic.importerTopic.warning("Content file " & path & " was not found")
            End Using
        End Sub
    End Class
End Namespace
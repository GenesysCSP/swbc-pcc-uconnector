﻿Namespace Users
  ''' <summary>
  ''' Data for a SpeechMiner user
  ''' </summary>
  ''' <remarks></remarks>
  Public Interface IUserData
    ''' <summary>
    ''' Accessor for the user profile properties
    ''' </summary>
    ''' <param name="name">The name of the property</param>
    ''' <value>The value of the property</value>
    ''' <returns>The value of the property</returns>
    ''' <remarks></remarks>
    Default Property Value(ByVal name As String) As String
    ''' <summary>
    ''' Whether or not the user can log in
    ''' </summary>
    ''' <value>User is active</value>
    ''' <returns>Whether or not the user is active</returns>
    ''' <remarks></remarks>
    Property Active() As Boolean
    ''' <summary>
    ''' The user's login name
    ''' </summary>
    ''' <value>Login name</value>
    ''' <returns>The user's login name</returns>
    ''' <remarks></remarks>
    Property Login() As String
    ''' <summary>
    ''' The type of authentication to be used
    ''' </summary>
    ''' <value>Authentication type</value>
    ''' <returns>The user's authentication type</returns>
    ''' <remarks>Valid values are <c>SPEECHMINER</c> and <c>WINDOWS</c>. 
    ''' <c>GENESYS</c> should not be used, as these users are not maintained in Speechminer.</remarks>
    Property Authentication() As utopy.bSecurityManager.Manager.AuthenticationType
    ''' <summary>
    ''' The user's password
    ''' </summary>
    ''' <value>User password</value>
    ''' <returns>The user's password</returns>
    ''' <remarks>Not needed for Windows authentication</remarks>
    Property Password() As String
    ''' <summary>
    ''' Whether the user must change the password when they log in
    ''' </summary>
    ''' <value>Whether the user must change their password</value>
    ''' <returns>True if the password must be changed</returns>
    ''' <remarks></remarks>
    Property MustChangePassword() As Boolean
    ''' <summary>
    ''' The user's first name
    ''' </summary>
    ''' <value>First name</value>
    ''' <returns>The user's first name</returns>
    ''' <remarks></remarks>
    Property FirstName() As String
    ''' <summary>
    ''' The user's last name
    ''' </summary>
    ''' <value>Last name</value>
    ''' <returns>The user's last name</returns>
    ''' <remarks></remarks>
    Property LastName() As String
    ''' <summary>
    ''' The user's title
    ''' </summary>
    ''' <value>Title</value>
    ''' <returns>The user's title</returns>
    ''' <remarks></remarks>
    Property Title() As String
    ''' <summary>
    ''' The organization that the user belongs to
    ''' </summary>
    ''' <value>User's organization</value>
    ''' <returns>The user's organization</returns>
    ''' <remarks></remarks>
    Property Organization() As String
    ''' <summary>
    ''' The user's phone number
    ''' </summary>
    ''' <value>User's phone</value>
    ''' <returns>The user's phone</returns>
    ''' <remarks></remarks>
    Property Phone() As String
    ''' <summary>
    ''' The user's email address
    ''' </summary>
    ''' <value>User's email</value>
    ''' <returns>The user's email</returns>
    ''' <remarks></remarks>
    Property Email() As String
    ''' <summary>
    ''' The user's home page URL
    ''' </summary>
    ''' <value>User's home page</value>
    ''' <returns>The user's home page</returns>
    ''' <remarks></remarks>
    Property HomePage() As String
    ''' <summary>
    ''' Comments about the user
    ''' </summary>
    ''' <value>Comments</value>
    ''' <returns>Comments about the user</returns>
    ''' <remarks></remarks>
    Property Comments() As String
    ''' <summary>
    ''' The ID in the agent hierarchy that matches this user
    ''' </summary>
    ''' <value>User's agent ID</value>
    ''' <returns>The user's agent ID</returns>
    ''' <remarks></remarks>
    Property AgentID() As String
    ''' <summary>
    ''' SpeechMiner roles to which the user belongs
    ''' </summary>
    ''' <value>User's roles</value>
    ''' <returns>The user's roles</returns>
    ''' <remarks></remarks>
    Property Roles() As ICollection(Of String)
    ''' <summary>
    ''' SpeechMiner groups to which the user belongs
    ''' </summary>
    ''' <value>User's groups</value>
    ''' <returns>The user's groups</returns>
    ''' <remarks></remarks>
    Property Groups() As ICollection(Of String)
    ''' <summary>
    ''' SpeechMiner workgroups and partitions to which the user has access
    ''' </summary>
    ''' <value>User's paritions</value>
    ''' <returns>The user's paritions</returns>
    ''' <remarks></remarks>
    Property Partitions() As ICollection(Of String)
  End Interface
End Namespace
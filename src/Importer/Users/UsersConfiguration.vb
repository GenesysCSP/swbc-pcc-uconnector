﻿Namespace Users
  ''' <summary>
  ''' Configuration for the SpeechMiner users updates
  ''' </summary>
  ''' <remarks></remarks>
  Public Class UsersConfiguration
    Inherits ConfigurationSection
    Implements IConfigurationSection

    ''' <summary>
    ''' Tag for the users update section. Value: Users
    ''' </summary>
    ''' <remarks></remarks>
    Protected Const XML_SETTINGS_USERS_SECTION_NAME As String = "Users"

    ''' <summary>
    ''' Tag for the fully qualified class name for the User Manager. Value: UserManagerType
    ''' </summary>
    ''' <remarks>The class must implement <see cref="IUserManager"/> and have a constructor that accepts <see cref="IConfigurationSection"/>.</remarks>
    Protected Const XML_SETTINGS_USER_MANAGER_TYPE As String = XML_SETTINGS_USERS_SECTION_NAME & ConfigurationImpl.SETTINGS_SEPARATOR & "UserManagerType"
    ''' <summary>
    ''' Tag for how often the user update should run. Value: UsersUpdateIntervalMinutes
    ''' </summary>
    ''' <remarks><para>Value is in minutes.</para>
    ''' <para>The default is to update once a day.</para></remarks>
    Protected Const XML_SETTINGS_USERS_UPDATE_INTERVAL As String = "UsersUpdateIntervalMinutes"

    ''' <summary>
    ''' Tag for the role provider to use. Value: RoleProviderName
    ''' </summary>
    ''' <remarks>The role provider needs to be configured in the .config file.</remarks>
    Protected Const XML_SETTINGS_USERS_ROLE_PROVIDER_NAME As String = "RoleProviderName"
    ''' <summary>
    ''' Tag for the membership provider to use. Value: MembershipProviderName
    ''' </summary>
    ''' <remarks>The membership provider needs to be configured in the .config file.</remarks>
    Protected Const XML_SETTINGS_USERS_MEMBERSHIP_PROVIDER_NAME As String = "MembershipProviderName"
    ''' <summary>
    ''' Tag for the profile provider to use. Value: ProfileProviderName
    ''' </summary>
    ''' <remarks>The profile provider needs to be configured in the .config file.</remarks>
    Protected Const XML_SETTINGS_USERS_PROFILE_PROVIDER_NAME As String = "ProfileProviderName"

    ''' <summary>
    ''' Tag for the profile properties to set for a user. Value: UserProfileProperties
    ''' </summary>
    ''' <remarks>Properties are comma separated. To use a profile properties than the user data property use the format <c>userdata=profile</c></remarks>
    Protected Const XML_SETTINGS_USER_PROFILE_PROPERTIES As String = "UserProfileProperties"

    ''' <summary>
    ''' Create a new user update configuration object.
    ''' </summary>
    ''' <param name="configuration">The entire UConnector configuration</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal configuration As IConfigurationSection)
      MyBase.New(configuration, XML_SETTINGS_USERS_SECTION_NAME)
    End Sub

    ''' <summary>
    ''' The class of User Manager to create.
    ''' <seealso cref="XML_SETTINGS_USER_MANAGER_TYPE"/>
    ''' </summary>
    ''' <param name="configuration">The entire UConnector configuration</param>
    ''' <value>The class of User Manager to create</value>
    ''' <returns>The Type to create</returns>
    ''' <remarks>The class must implement <see cref="IUserManager"/> and have a constructor that accepts <see cref="IConfigurationSection"/>.</remarks>
    Public Shared ReadOnly Property ManagerType(ByVal configuration As IConfigurationSection) As Type
      Get
        Dim mgrType As String = configuration(XML_SETTINGS_USER_MANAGER_TYPE)
        If String.IsNullOrEmpty(mgrType) Then
          Return Nothing
        Else
          Return Type.GetType(mgrType)
        End If
      End Get
    End Property

    ''' <summary>
    ''' How often should the user update run?
    ''' <seealso cref="XML_SETTINGS_USERS_UPDATE_INTERVAL"/>
    ''' </summary>
    ''' <value>The interval for updates</value>
    ''' <returns>A TimeSpan containing the update interval</returns>
    ''' <remarks></remarks>
    Public Overridable ReadOnly Property UsersUpdateInterval() As TimeSpan
      Get
        If String.IsNullOrEmpty(Me(XML_SETTINGS_USERS_UPDATE_INTERVAL)) Then
          Return DefaultUsersUpdateInterval
        Else
          Return TimeSpan.FromMinutes(CDbl(Me(XML_SETTINGS_USERS_UPDATE_INTERVAL)))
        End If
      End Get
    End Property

    ''' <summary>
    ''' Override this to change the default users update interval, used if the configuration does not specify it.
    ''' </summary>
    ''' <value>The default interval for updates</value>
    ''' <returns>One day</returns>
    ''' <remarks></remarks>
    Protected Overridable ReadOnly Property DefaultUsersUpdateInterval() As TimeSpan
      Get
        Return TimeSpan.FromDays(1)
      End Get
    End Property

    ''' <summary>
    ''' The role provider to use.
    ''' <seealso cref="XML_SETTINGS_USERS_ROLE_PROVIDER_NAME"/>
    ''' </summary>
    ''' <value>Provider name</value>
    ''' <returns>The provider's name</returns>
    ''' <remarks>The role provider needs to be configured in the .config file.</remarks>
    Public Overridable ReadOnly Property RoleProviderName() As String
      Get
        Return Me(XML_SETTINGS_USERS_ROLE_PROVIDER_NAME)
      End Get
    End Property

    ''' <summary>
    ''' The membership provider to use.
    ''' <seealso cref="XML_SETTINGS_USERS_MEMBERSHIP_PROVIDER_NAME"/>
    ''' </summary>
    ''' <value>Provider name</value>
    ''' <returns>The provider's name</returns>
    ''' <remarks>The membership provider needs to be configured in the .config file.</remarks>
    Public Overridable ReadOnly Property MembershipProviderName() As String
      Get
        Return Me(XML_SETTINGS_USERS_MEMBERSHIP_PROVIDER_NAME)
      End Get
    End Property

    ''' <summary>
    ''' The profile provider to use.
    ''' <seealso cref="XML_SETTINGS_USERS_PROFILE_PROVIDER_NAME"/>
    ''' </summary>
    ''' <value>Provider name</value>
    ''' <returns>The provider's name</returns>
    ''' <remarks>The profile provider needs to be configured in the .config file.</remarks>
    Public Overridable ReadOnly Property ProfileProviderName() As String
      Get
        Return Me(XML_SETTINGS_USERS_PROFILE_PROVIDER_NAME)
      End Get
    End Property

    ''' <summary>
    ''' Profile properties to set for the user.
    ''' <seealso cref="XML_SETTINGS_USER_PROFILE_PROPERTIES"/>
    ''' </summary>
    ''' <value>Mapping of properties to name in profile</value>
    ''' <returns>Mapping of properties to name in profile</returns>
    ''' <remarks>Properties are comma separated. To use a profile properties than the user data property use the format <c>userdata=profile</c></remarks>
    Public Overridable ReadOnly Property UserProfileProperties() As IDictionary(Of String, String)
      Get
        If String.IsNullOrEmpty(Me(XML_SETTINGS_USER_PROFILE_PROPERTIES)) Then
          Return DefaultUserProfileProperties
        Else
          Return ConfigurationUtils.ParseKeyValuePairs(Me(XML_SETTINGS_USER_PROFILE_PROPERTIES))
        End If
      End Get
    End Property

    ''' <summary>
    ''' Default user profile properties
    ''' </summary>
    ''' <value>Properties to be set by default</value>
    ''' <returns>Mapping of default profile properties</returns>
    ''' <remarks></remarks>
    Public Overridable ReadOnly Property DefaultUserProfileProperties() As IDictionary(Of String, String)
      Get
        Return ConfigurationUtils.ParseKeyValuePairs("FirstName,LastName,Email,Comments,AuthenType,Title,Organization,Phone")
      End Get
    End Property
  End Class
End Namespace

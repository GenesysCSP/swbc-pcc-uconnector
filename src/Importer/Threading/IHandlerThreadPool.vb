﻿Imports QBEUConnector.Data
Imports QBEUConnector.Output

Namespace Threading
    ''' <summary>
    ''' Pool for threads to import interactions
    ''' </summary>
    ''' <remarks></remarks>
    Public Interface IHandlerThreadPool

        ''' <summary>
        ''' Initialize the threads in the pool
        ''' </summary>
        ''' <param name="contentWriter">The writer for interaction content files</param>
        ''' <param name="dataWriter">The writer for interaction metadata files</param>
        ''' <remarks></remarks>
        Sub InitializeHandlerThreads(ByVal contentWriter As IContentWriter, ByVal dataWriter As IDataWriter)
        ''' <summary>
        ''' Creates a thread for importing interactions
        ''' </summary>
        ''' <param name="contentWriter">The writer for interaction content files</param>
        ''' <param name="dataWriter">The writer for interaction metadata files</param>
        ''' <returns>Interaction processing thread</returns>
        ''' <remarks></remarks>
        Function CreateHandlerThread(ByVal contentWriter As IContentWriter, ByVal dataWriter As IDataWriter) As IInteractionHandlerThread
        ''' <summary>
        ''' Gets an available interaction processing thread
        ''' </summary>
        ''' <returns>Interaction processing thread</returns>
        ''' <remarks></remarks>
        Function GetHandlerThread() As IInteractionHandlerThread
        ''' <summary>
        ''' Get the number of threads actively processing interactions
        ''' </summary>
        ''' <value>The number of active threads</value>
        ''' <returns>The number of active threads</returns>
        ''' <remarks></remarks>
        ReadOnly Property NumberOfThreadsRunning() As Integer

        ''' <summary>
        ''' Event raised when the interaction content is processed
        ''' </summary>
        ''' <param name="interaction">The interaction data</param>
        ''' <param name="success">Whether processing succeeded or not</param>
        ''' <param name="exception">In case of failure, the exception causing it</param>
        ''' <remarks></remarks>
        Event ContentHandled(ByVal interaction As IInteractionData, ByVal success As Boolean, ByVal exception As Exception)
        ''' <summary>
        ''' Event raised when the interaction metadata is processed
        ''' </summary>
        ''' <param name="interaction">The interaction data</param>
        ''' <param name="success">Whether processing succeeded or not</param>
        ''' <param name="exception">In case of failure, the exception causing it</param>
        ''' <remarks></remarks>
        Event MetaHandled(ByVal interaction As IInteractionData, ByVal success As Boolean, ByVal exception As Exception)
    End Interface
End Namespace
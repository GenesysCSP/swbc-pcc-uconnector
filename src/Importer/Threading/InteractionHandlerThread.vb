﻿Imports System.Threading
Imports QBEUConnector.Data
Imports QBEUConnector.Output

Namespace Threading
    ''' <summary>
    ''' Thread to handle processing of an interaction
    ''' </summary>
    ''' <remarks></remarks>
    Public Class InteractionHandlerThread
        Implements IInteractionHandlerThread


        ''' <summary>
        ''' Event raised when the interaction content is processed
        ''' </summary>
        ''' <param name="interaction">The interaction data</param>
        ''' <param name="success">Whether processing succeeded or not</param>
        ''' <param name="exception">In case of failure, the exception causing it</param>
        ''' <remarks></remarks>
        Public Event ContentHandled(ByVal interaction As IInteractionData, ByVal success As Boolean, ByVal exception As Exception) Implements IInteractionHandlerThread.ContentHandled
        ''' <summary>
        ''' Event raised when the interaction metadata is processed
        ''' </summary>
        ''' <param name="interaction">The interaction data</param>
        ''' <param name="success">Whether processing succeeded or not</param>
        ''' <param name="exception">In case of failure, the exception causing it</param>
        ''' <remarks></remarks>
        Public Event MetaHandled(ByVal interaction As IInteractionData, ByVal success As Boolean, ByVal exception As Exception) Implements IInteractionHandlerThread.MetaHandled

        ''' <summary>
        ''' Create an interaction processing thread
        ''' </summary>
        ''' <param name="contentWriter">The writer for interaction content files</param>
        ''' <param name="dataWriter">The writer for interaction metadata files</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal contentWriter As IContentWriter, ByVal dataWriter As IDataWriter)
            _contentWriter = contentWriter
            _dataWriter = dataWriter
            _isInUse = False
        End Sub

        ''' <summary>
        ''' Set the processing data to the thread
        ''' </summary>
        ''' <param name="interaction">The interaction to process</param>
        ''' <param name="targetPath">The output folder for the interaction's files</param>
        ''' <remarks></remarks>
        Public Overridable Sub SetData(ByVal interaction As IInteractionData, ByVal targetPath As String) Implements IInteractionHandlerThread.SetData
            _ixnData = interaction
            _targetPath = targetPath
        End Sub

        ''' <summary>
        ''' Is the thread currently processing
        ''' </summary>
        ''' <value>Is the thread being used</value>
        ''' <returns>Is the thread being used</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property isInUse() As Boolean Implements IInteractionHandlerThread.IsInUse
            Get
                Return _isInUse
            End Get
        End Property

        ''' <summary>
        ''' Get the thread for using, and mark it as being used
        ''' </summary>
        ''' <returns>True if the thread was available</returns>
        ''' <remarks></remarks>
        Public Function TryToUse() As Boolean Implements IInteractionHandlerThread.TryToUse
            SyncLock Me
                If _isInUse Then
                    Return False
                End If
                _isInUse = True
                Return True
            End SyncLock
        End Function

        ''' <summary>
        ''' Start processing the interaction
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub Go() Implements IInteractionHandlerThread.Go
            _workerThread = New Thread(AddressOf doWork)
            _workerThread.Name = "IxnWorkerThread"
            _workerThread.Start()
        End Sub

        ''' <summary>
        ''' Main function to run on the thread, and actually process the interaction
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overridable Sub doWork()
            If _ixnData Is Nothing Then
                Return
            End If

            Try
                Dim status As Boolean = _contentWriter.GenerateContentFile(_targetPath, _ixnData)
                If status Then
                    RaiseEvent ContentHandled(_ixnData, True, Nothing)
                    Try
                        status = _dataWriter.GenerateMetaFile(_targetPath, _ixnData)
                        RaiseEvent MetaHandled(_ixnData, status, Nothing)
                    Catch ex As Exception
                        RaiseEvent MetaHandled(_ixnData, False, ex)
                    End Try
                Else
                    RaiseEvent ContentHandled(_ixnData, False, Nothing)
                End If
            Catch ex As Exception
                RaiseEvent ContentHandled(_ixnData, False, ex)
            Finally
                _isInUse = False
            End Try
        End Sub

#Region "Instance Data"

        Protected _contentWriter As IContentWriter
        Protected _dataWriter As IDataWriter
        Protected _isInUse As Boolean
        Protected _ixnData As IInteractionData
        Protected _targetPath As String
        Protected _workerThread As Thread

#End Region

    End Class
End Namespace
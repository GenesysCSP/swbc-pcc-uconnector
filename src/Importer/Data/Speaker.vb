﻿Namespace Data
    ''' <summary>
    ''' Speaker information for an interaction
    ''' </summary>
    ''' <remarks></remarks>
    Public NotInheritable Class Speaker

#Region "Instance Properties"

        ''' <summary>
        ''' The time in the call when this speaker left
        ''' </summary>
        ''' <value>Time relative to start of call, in seconds</value>
        ''' <returns>Time relative to start of call, in seconds</returns>
        ''' <remarks></remarks>
        Public Property EndTime As Decimal?
            Get
                Return _endTime
            End Get
            Set(ByVal value As Decimal?)
                _endTime = value
            End Set
        End Property

        ''' <summary>
        ''' The ID of the speaker
        ''' </summary>
        ''' <value>The ID of the speaker</value>
        ''' <returns>The ID of the speaker</returns>
        ''' <remarks></remarks>
        Public Property ID As String
            Get
                Return _id
            End Get
            Set(ByVal value As String)
                _id = value
            End Set
        End Property

        Public Property StartTime As Decimal?
            Get
                Return _startTime
            End Get
            Set(ByVal value As Decimal?)
                _startTime = value
            End Set
        End Property

        ''' <summary>
        ''' The speaker's workgroup
        ''' </summary>
        ''' <value>Speaker's workgroup</value>
        ''' <returns>Speaker's workgroup</returns>
        ''' <remarks></remarks>
        Public Property Workgroup As String
            Get
                Return _workgroup
            End Get
            Set(ByVal value As String)
                _workgroup = value
            End Set
        End Property

#End Region

#Region "Constructors"

        Public Sub New(
            Optional id As String = Nothing,
            Optional workgroup As String = Nothing
            )
            _id = id
            _workgroup = workgroup
        End Sub

#End Region

#Region "Instance Data"

        Private _endTime As Decimal?
        Private _id As String
        Private _startTime As Decimal?
        Private _workgroup As String

#End Region

    End Class
End Namespace
﻿Imports System.IO
Imports System.Xml
Imports QBEUConnector.Agents
Imports QBEUConnector.Configuration
Imports QBEUConnector.Main

Namespace Data
    Public Class StitchingDataManager
        Implements IDataManager
        ''' <summary>
        ''' The inner data manager for the application
        ''' </summary>   
        ''' <remarks></remarks>
        Protected m_dataManager As IDataManager
        ''' <summary>
        ''' The data manager's configuration
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_configuration As DataConfig
        ''' <summary>
        ''' The part of the configuration for the main importer process
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_importerConfiguration As ImporterConfig

        ''' <summary>
        ''' Create a new Stitching Data Manager
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <param name="agentManager">The agent manager</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection, ByVal agentManager As IAgentManager)
            m_configuration = New DataConfig(configuration)
            m_importerConfiguration = New ImporterConfig(configuration)
            m_dataManager = CreateDataManager(configuration, agentManager)
        End Sub

        ''' <summary>
        ''' Create the data manager for the application
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <param name="agentManager">The agent manager</param>
        ''' <returns>The inner data manager object</returns>
        ''' <remarks></remarks>
        Protected Overridable Function CreateDataManager(ByVal configuration As IConfigSection, ByVal agentManager As IAgentManager) As IDataManager
            Dim mgrType As Type = DataConfig.InnerManagerType(configuration)
            If mgrType Is Nothing Then
                Throw New TypeLoadException("Assembly for data manager " & mgrType.Name & " not found")
            End If
            Dim ci As System.Reflection.ConstructorInfo = mgrType.GetConstructor(New Type() {GetType(IConfigSection), GetType(IAgentManager)})
            Dim mgr As IDataManager
            If ci IsNot Nothing Then
                mgr = CType(ci.Invoke(New Object() {configuration, agentManager}), IDataManager)
            Else
                ci = mgrType.GetConstructor(New Type() {GetType(IConfigSection)})
                If ci Is Nothing Then
                    Throw New MissingMethodException("Constructor not found for " & mgrType.Name)
                End If
                mgr = CType(ci.Invoke(New Object() {configuration}), IDataManager)
            End If
            Return mgr
        End Function

        ''' <summary>
        ''' Get a list of the next interactions to process
        ''' </summary>
        ''' <param name="batchSize">The number of interactions to get</param>
        ''' <returns>A collection of interaction data</returns>
        ''' <remarks></remarks>
        Public Overridable Function GetNextInteractions(ByVal batchSize As Integer) As IEnumerable(Of IInteractionData) Implements IDataManager.GetNextInteractions

            Dim ixns = m_dataManager.GetNextInteractions(0) ' 0 - get all interactions
            Dim ret As New List(Of IInteractionData)
            Dim mergedInteractions As New Dictionary(Of String, ISegmentedInteractionData)
            For Each interaction As IAudioInteractionData In ixns
                Dim key As String = GetInteractionIdentifierKey(interaction)
                If Not mergedInteractions.ContainsKey(key) Then
                    Dim segmentedInteraction As ISegmentedInteractionData = CreateSegmentedInteractionData()
                    segmentedInteraction.InteractionMetaFiles.Add(interaction)
                    mergedInteractions(key) = segmentedInteraction
                Else
                    mergedInteractions(key).InteractionMetaFiles.Add(interaction)
                End If
                Dim segmentedContentFile As New SegmentedContentFile()
                segmentedContentFile.ContentFilePath = interaction.ContentFilePath
                segmentedContentFile.StartTime = interaction.StartTime
                mergedInteractions(key).ContentFiles.Add(segmentedContentFile)
            Next
            For Each interaction As SegmentedInteractionData In mergedInteractions.Values
                ret.Add(interaction)
                If ret.Count = batchSize Then
                    Exit For
                End If
            Next
            Return ret
        End Function
        ''' <summary>
        ''' Create a new segmented interaction data
        ''' </summary>
        ''' <returns>A new Instance of a segmented interaction data</returns>
        ''' <remarks>The segmented interaction data type is retrieved from the configuration xml</remarks>
        Protected Overridable Function CreateSegmentedInteractionData() As ISegmentedInteractionData
            Dim segType As Type = DataConfig.SegmentedInteractionDataType(m_configuration)
            If segType Is Nothing Then
                Return Nothing
            End If
            Dim ci As System.Reflection.ConstructorInfo = segType.GetConstructor(New Type() {})
            If ci Is Nothing Then
                Throw New MissingMethodException("Constructor not found for " & segType.Name)
            End If
            Dim segmentedInteractionData As ISegmentedInteractionData = CType(ci.Invoke(New Object() {}), ISegmentedInteractionData)
            Return segmentedInteractionData
        End Function

        ''' <summary>
        ''' Gets the common identifier of the a call with segmentes
        ''' </summary>
        ''' <param name="interaction">The segmented interaction data</param>
        ''' <returns>Returns the value of the common identifier for the segmented call</returns>
        ''' <remarks></remarks>
        Protected Overridable Function GetInteractionIdentifierKey(ByVal interaction As IInteractionData) As String
            Return interaction(m_configuration.SegmentedInteractionIdField)
        End Function

        ''' <summary>
        ''' The number of interactions still waiting to be imported.
        ''' </summary>
        ''' <returns>The number of interactions still waiting to be imported</returns>
        ''' <remarks>Return -1 if the number is unknown.</remarks>
        Public Overridable Function IxnsAvailable() As Boolean Implements IDataManager.IxnsAvailable
            Return m_dataManager.IxnsAvailable()
        End Function
        ''' <summary>
        ''' Callback for when segmented interaction is handled.
        ''' </summary>
        ''' <param name="interaction">The segmented interaction data</param>
        ''' <param name="status">Success/failure information</param>
        ''' <remarks>The function loops over all the segments of the interaction</remarks>
        Public Overridable Sub InteractionHandled(ByVal interaction As IInteractionData, ByVal status As IItemStatus) Implements IDataManager.InteractionHandled
            Dim segmentedInteraction As ISegmentedInteractionData = TryCast(interaction, ISegmentedInteractionData)
            For Each segment As IAudioInteractionData In segmentedInteraction.InteractionMetaFiles
                m_dataManager.InteractionHandled(segment, status)
            Next
        End Sub
    End Class
End Namespace

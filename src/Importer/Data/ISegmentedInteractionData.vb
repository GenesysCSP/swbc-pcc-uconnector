﻿Namespace Data
    Public Interface ISegmentedInteractionData
        Inherits IAudioInteractionData

        ''' <summary>
        ''' Get a list of the segmented content files.
        ''' </summary>
        ''' <returns>A collection of segmented content files</returns>
        ''' <remarks></remarks>
        ReadOnly Property ContentFiles As IList(Of ISegmentedContentFile)
        ''' <summary>
        ''' Get a list of the interaction data files.
        ''' </summary>
        ''' <returns>A collection of the interaction data files</returns>
        ''' <remarks></remarks>
        ReadOnly Property InteractionMetaFiles As IList(Of IAudioInteractionData)
    End Interface
End Namespace

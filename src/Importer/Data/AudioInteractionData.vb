﻿Imports utopy.bDBOffline

Namespace Data
    ''' <summary>
    ''' Data for audio calls
    ''' </summary>
    ''' <remarks></remarks>
    Public Class AudioInteractionData
        Inherits InteractionData
        Implements IAudioInteractionData

        ''' <summary>
        ''' Key for the call duration
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const DATA_DURATION As String = "Duration"
        ''' <summary>
        ''' Key for the audio format
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const DATA_FORMAT As String = "Format"
        ''' <summary>
        ''' Key for the left channel speaker type
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const DATA_LEFT_SPEAKER As String = "LeftSpeaker"
        ''' <summary>
        ''' Key for the right channel speaker type
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const DATA_RIGHT_SPEAKER As String = "RightSpeaker"
        ''' <summary>
        ''' Key for the encryption key ID
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const DATA_ENCRYPTION_KEY As String = "EncryptionKeyID"

        ''' <summary>
        ''' Create a new interaction data object
        ''' </summary>
        ''' <param name="interactionId">The ID of the interaction</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal interactionId As String)
            MyBase.New(interactionId)
        End Sub

        ''' <summary>
        ''' The duration of the call
        ''' </summary>
        ''' <value>Duration in seconds</value>
        ''' <returns>Duration in seconds</returns>
        ''' <remarks></remarks>
        Public Property Duration As Decimal Implements IAudioInteractionData.Duration
            Get
                Return CDec(Me(DATA_DURATION))
            End Get
            Set(ByVal value As Decimal)
                Me(DATA_DURATION) = value.ToString(Globalization.CultureInfo.InvariantCulture)
            End Set
        End Property
        ''' <summary>
        ''' The audio format of the call
        ''' </summary>
        ''' <value>The internal value for the format</value>
        ''' <returns>The internal value for the format</returns>
        ''' <remarks></remarks>
        Public Property AudioFormat As audioConversionType.InternalSupportedAudioFormat Implements IAudioInteractionData.AudioFormat
            Get
                Dim format As String = Me(DATA_FORMAT)
                Dim ret = audioConversionType.InternalSupportedAudioFormat.UNKNOWN
                If Not String.IsNullOrEmpty(format) Then
                    ret = CType([Enum].Parse(GetType(audioConversionType.InternalSupportedAudioFormat), format, True), audioConversionType.InternalSupportedAudioFormat)
                End If
                Return ret
            End Get
            Set(ByVal value As audioConversionType.InternalSupportedAudioFormat)
                Me(DATA_FORMAT) = value.ToString()
            End Set
        End Property

        ''' <summary>
        ''' The type of speaker on the left audio channel
        ''' </summary>
        ''' <value>The type of speaker</value>
        ''' <returns>The type of speaker</returns>
        ''' <remarks></remarks>
        Public Property LeftChannelSpeaker As String Implements IAudioInteractionData.LeftChannelSpeaker
            Get
                Return Me(DATA_LEFT_SPEAKER)
            End Get
            Set(ByVal value As String)
                Me(DATA_LEFT_SPEAKER) = value
            End Set
        End Property

        ''' <summary>
        ''' The type of speaker on the right audio channel
        ''' </summary>
        ''' <value>The type of speaker</value>
        ''' <returns>The type of speaker</returns>
        ''' <remarks></remarks>
        Public Property RightChannelSpeaker As String Implements IAudioInteractionData.RightChannelSpeaker
            Get
                Return Me(DATA_RIGHT_SPEAKER)
            End Get
            Set(ByVal value As String)
                Me(DATA_RIGHT_SPEAKER) = value
            End Set
        End Property

        ''' <summary>
        ''' The ID of the key used to encrypt the audio
        ''' </summary>
        ''' <value>The key ID</value>
        ''' <returns>The key ID</returns>
        ''' <remarks></remarks>
        Public Property EncryptionKeyID As Integer Implements IAudioInteractionData.EncryptionKeyID
            Get
                Return CInt(Me(DATA_ENCRYPTION_KEY))
            End Get
            Set(ByVal value As Integer)
                Me(DATA_ENCRYPTION_KEY) = value.ToString(Globalization.CultureInfo.InvariantCulture)
            End Set
        End Property
    End Class
End Namespace
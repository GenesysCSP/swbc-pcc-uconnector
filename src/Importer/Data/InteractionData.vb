﻿Namespace Data
    ''' <summary>
    ''' Data for an interaction
    ''' </summary>
    ''' <remarks></remarks>
    Public Class InteractionData
        Implements IInteractionData

        ''' <summary>
        ''' Key for the path to the interaction's content file
        ''' </summary>
        Private Const DATA_CONTENT_FILE As String = "ContentFile"
        ''' <summary>
        ''' Key for the path to the interaction's metadata file
        ''' </summary>
        Private Const DATA_META_FILE As String = "MetaFile"
        ''' <summary>
        ''' Key for the program ID to handle the interaction
        ''' </summary>
        Private Const DATA_PROGRAM_ID As String = "ProgramID"
        ''' <summary>
        ''' Key for the ID of the agent in the interaction
        ''' </summary>
        Private Const DATA_AGENT_ID As String = "AgentId"
        ''' <summary>
        ''' Key for the workgroup of the agent in the interaction
        ''' </summary>
        Private Const DATA_WORKGROUP As String = "Workgroup"

        ''' <summary>
        ''' The ID of the interaction
        ''' </summary>
        Private _id As String
        ''' <summary>
        ''' A dictionary containing the interaction's properties
        ''' </summary>
        Private _data As IDictionary(Of String, String)
        ''' <summary>
        ''' The date and time the interaction started
        ''' </summary>
        Private _startTime As DateTime
        ''' <summary>
        ''' The speakers in the interaction
        ''' </summary>
        Private _speakers As ICollection(Of Speaker)

        ''' <summary>
        ''' Create a new interaction data object
        ''' </summary>
        ''' <param name="ixnId">The ID of the interaction</param>
        Public Sub New(ixnId As String)
            _id = ixnId
            _data = New Dictionary(Of String, String)(StringComparer.OrdinalIgnoreCase)
        End Sub

        Public ReadOnly Property ID As String Implements IInteractionData.ID
            Get
                Return _id
            End Get
        End Property

        ''' <summary>
        ''' Accessor for the interaction's properties
        ''' </summary>
        ''' <param name="name">The name of the property</param>
        ''' <value>The value of the property</value>
        ''' <returns>The value of the property from the dictionary</returns>
        ''' <remarks></remarks>
        Default Public Overridable Property Value(ByVal name As String) As String Implements IInteractionData.Value
            Get
                If _data.ContainsKey(name) Then
                    Return _data(name)
                Else
                    Return Nothing
                End If
            End Get
            Set(ByVal value As String)
                _data(name) = value
            End Set
        End Property

        ''' <summary>
        ''' The path to the file containing the interaction content (e.g. audio)
        ''' </summary>
        ''' <value>Path to the content file</value>
        ''' <returns>Path to the content file</returns>
        ''' <remarks></remarks>
        Public Overridable Property ContentFilePath As String Implements IInteractionData.ContentFilePath
            Get
                Return Me(DATA_CONTENT_FILE)
            End Get
            Set(ByVal value As String)
                Me(DATA_CONTENT_FILE) = value
            End Set
        End Property

        ''' <summary>
        ''' The path to the file containing the interaction metadata
        ''' </summary>
        ''' <value>Path to the metadata file</value>
        ''' <returns>Path to the metadata file</returns>
        ''' <remarks></remarks>
        Public Overridable Property MetaFilePath As String Implements IInteractionData.MetaFilePath
            Get
                Return Me(DATA_META_FILE)
            End Get
            Set(ByVal value As String)
                Me(DATA_META_FILE) = value
            End Set
        End Property

        ''' <summary>
        ''' The ID of the SpeechMiner program that should handle the interaction
        ''' </summary>
        ''' <value>Program's external ID</value>
        ''' <returns>Program's external ID</returns>
        ''' <remarks></remarks>
        Public Overridable Property ProgramID() As String Implements IInteractionData.Program
            Get
                Return Me(DATA_PROGRAM_ID)
            End Get
            Set(ByVal value As String)
                Me(DATA_PROGRAM_ID) = value
            End Set
        End Property

        ''' <summary>
        ''' The date and time when the interaction started
        ''' </summary>
        ''' <value>Date and time of the interaction</value>
        ''' <returns>Date and time of the interaction</returns>
        ''' <remarks></remarks>
        Public Overridable Property StartTime() As Date Implements IInteractionData.StartTime
            Get
                Return _startTime
            End Get
            Set(ByVal value As Date)
                _startTime = value
            End Set
        End Property

        ''' <summary>
        ''' The speakers who participated in the interaction
        ''' </summary>
        ''' <value>A collection of the speakers</value>
        ''' <returns>A collection of the speakers</returns>
        ''' <remarks>If speakers list is not set, this is created from the agent ID and workgroup properties.</remarks>
        Public Overridable Property Speakers As ICollection(Of Speaker) Implements IInteractionData.Speakers
            Get
                If _speakers Is Nothing AndAlso Not String.IsNullOrEmpty(Me(DATA_AGENT_ID)) Then
                    Dim ret As New List(Of Speaker)
                    ret.Add(New Speaker(Me(DATA_AGENT_ID), Me(DATA_WORKGROUP)))
                    Return ret
                End If
                Return _speakers
            End Get
            Set(ByVal value As ICollection(Of Speaker))
                _speakers = New List(Of Speaker)(value)
            End Set
        End Property

        ''' <summary>
        ''' The ID of the agent in the interaction
        ''' </summary>
        ''' <value>Agent ID</value>
        ''' <remarks>Only to be used when there is a single agent. Can be used for the speaker.</remarks>
        Public Overridable Property AgentID As String Implements IInteractionData.AgentID
            Get
                Return Me(DATA_AGENT_ID)
            End Get
            Set(ByVal value As String)
                Me(DATA_AGENT_ID) = value
            End Set
        End Property

        ''' <summary>
        ''' The workgroup of the agent in the interaction
        ''' </summary>
        ''' <value>workgroup</value>
        ''' <remarks>Only to be used when there is a single agent. Can be used for the speaker.</remarks>
        Public Overridable Property Workgroup As String Implements IInteractionData.Workgroup
            Get
                Return Me(DATA_WORKGROUP)
            End Get
            Set(ByVal value As String)
                Me(DATA_WORKGROUP) = value
            End Set
        End Property

    End Class
End Namespace
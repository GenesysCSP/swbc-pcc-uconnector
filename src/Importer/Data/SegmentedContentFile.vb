﻿Imports QBEUConnector.Data

Public Class SegmentedContentFile
    Implements ISegmentedContentFile
    ''' <summary>
    ''' The path to the file containing the segment content (e.g. audio)
    ''' </summary>
    ''' <remarks></remarks>
    Protected m_contentFilePath As String
    ''' <summary>
    ''' The date and time the interaction started
    ''' </summary>
    ''' <remarks></remarks>
    Protected m_startTime As DateTime

    ''' <summary>
    ''' The date and time the segment ended
    ''' </summary>
    ''' <remarks></remarks>
    Protected m_endTime As DateTime

    ''' <summary>
    ''' The path to the file containing the segment content (e.g. audio)
    ''' </summary>
    ''' <value>Path to the content file</value>
    ''' <returns>Path to the content file</returns>
    ''' <remarks></remarks>
    Public Property ContentFilePath() As String Implements ISegmentedContentFile.ContentFilePath
        Get
            Return m_contentFilePath
        End Get
        Set(ByVal value As String)
            m_contentFilePath = value
        End Set
    End Property
    ''' <summary>
    ''' The date and time when the segment ended
    ''' </summary>
    ''' <value>Date and time when the segment ended</value>
    ''' <returns>Date and time when the segment ended</returns>
    ''' <remarks></remarks>
    Public Property EndTime() As DateTime Implements ISegmentedContentFile.EndTime
        Get
            Return m_endTime
        End Get
        Set(ByVal value As DateTime)
            m_endTime = value
        End Set
    End Property
    ''' <summary>
    ''' The date and time when the segment started
    ''' </summary>
    ''' <value>Date and time when the segment started</value>
    ''' <returns>Date and time when the segment started</returns>
    ''' <remarks></remarks>
    Public Property StartTime() As DateTime Implements ISegmentedContentFile.StartTime
        Get
            Return m_startTime
        End Get
        Set(ByVal value As DateTime)
            m_startTime = value
        End Set
    End Property
End Class

﻿Imports System.IO
Imports GenUtils.TraceTopic
Imports QBEUConnector.Agents
Imports QBEUConnector.Configuration

Namespace Data
    ''' <summary>
    ''' Data manager for getting interaction data from a file
    ''' </summary>
    ''' <remarks></remarks>
    Public MustInherit Class FileSystemDataManager
        Inherits BaseDataManager
        Implements IDataManager

        ''' <summary>
        ''' Create the data manager with its configuration
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection)
            MyBase.New(configuration)
            CreateDirectories()
        End Sub

        ''' <summary>
        ''' Create the data manager with its configuration and an agent manager
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <param name="agentManager">The agent manager</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection, ByVal agentManager As IAgentManager)
            MyBase.New(configuration, agentManager)
            CreateDirectories()
        End Sub

        Private Sub CreateDirectories()
            If ShouldBackupCompletedFiles Then
                If Not Directory.Exists(Me.Config.BackupFilesFolder) Then
                    Directory.CreateDirectory(Me.Config.BackupFilesFolder)
                End If
            End If
            If ShouldBackupFailedFiles Then
                If Not Directory.Exists(Me.Config.FailedFilesFolder) Then
                    Directory.CreateDirectory(Me.Config.FailedFilesFolder)
                End If
            End If
        End Sub

        ''' <summary>
        ''' Get a list of the next interactions to process
        ''' </summary>
        ''' <param name="limitTo">The number of interactions to get</param>
        ''' <returns>A collection of interaction data</returns>
        ''' <remarks></remarks>
        Protected Overrides Function OnGetNextInteractions(
            limitTo As Integer
            ) As IEnumerable(Of IInteractionData)

            Dim ret As New List(Of IInteractionData)
            If Directory.Exists(Me.Config.DataFolder) = False Then
                Return ret
            End If
            Using (TraceTopic.importerTopic.scope())
                Dim files As FileInfo() = Me.GetFileList()
                For Each fi As FileInfo In files
                    Try
                        Dim data As IInteractionData = GetInteractionData(fi)
                        If data IsNot Nothing AndAlso Me.IsCallFromAgentAllowed(data.AgentID) = True Then
                            ret.Add(data)
                        End If
                    Catch ex As Exception
                        TraceTopic.importerTopic.error("Failed to get data for " & fi.FullName, ex)
                        HandleFailedFile(fi.FullName)
                    End Try
                    If limitTo > 0 AndAlso ret.Count = limitTo Then
                        Exit For
                    End If
                Next
            End Using
            Return ret
        End Function

        ''' <summary>
        ''' The number of interactions still waiting to be imported.
        ''' </summary>
        ''' <returns>The number of interactions still waiting to be imported</returns>
        ''' <remarks></remarks>
        Public Overrides Function IxnsAvailable() As Boolean
            Return If(Directory.Exists(Me.Config.DataFolder) = True, _
              Me.GetFileList().Count > 0, False)
        End Function

        ''' <summary>
        ''' Callback for when one interaction is handled.
        ''' </summary>
        ''' <param name="interaction">The interaction data</param>
        ''' <param name="status">Success/failure information</param>
        ''' <remarks></remarks>
        Public Overrides Sub InteractionHandled(ByVal interaction As IInteractionData, ByVal status As IItemStatus)
            For Each f As String In GetInteractionFiles(interaction)
                HandleCompletedFile(f, status)
            Next
        End Sub

        ''' <summary>
        ''' Get the list of files to be imported.
        ''' </summary>
        ''' <returns>The full list of files in the folder.</returns>
        ''' <remarks>Can be configured to go recursively into the folder, or to just read the top level folder.</remarks>
        Protected Overridable Function GetFileList() As FileInfo()
            Dim directoryInfo As New DirectoryInfo(Me.Config.DataFolder)
            Dim searchType As SearchOption
            If Me.Config.ReadSubfolders Then
                searchType = SearchOption.AllDirectories
            Else
                searchType = SearchOption.TopDirectoryOnly
            End If
            Return directoryInfo.GetFiles(Me.Config.DataFilePattern, searchType)
        End Function

        ''' <summary>
        ''' Get the data for an interaction from a file.
        ''' </summary>
        ''' <param name="fi">The file</param>
        ''' <returns>The interaction data</returns>
        ''' <remarks></remarks>
        Protected MustOverride Function GetInteractionData(ByVal fi As FileInfo) As IInteractionData

        ''' <summary>
        ''' Handle a file for an interaction that was processed.
        ''' </summary>
        ''' <param name="path">The path to the file</param>
        ''' <param name="status">Success/failure information</param>
        ''' <remarks></remarks>
        Protected Overridable Sub HandleCompletedFile(ByVal path As String, ByVal status As IItemStatus)
            If status.Success Then
                HandleSuccessfulFile(path)
            Else
                HandleFailedFile(path)
            End If
        End Sub

        ''' <summary>
        ''' Handles a file when the interaction was successfully processed.
        ''' </summary>
        ''' <param name="filePath">Path to the file</param>
        ''' <remarks>Backs up and/or deletes the file</remarks>
        Protected Overridable Sub HandleSuccessfulFile(ByVal filePath As String)
            Using (TraceTopic.importerTopic.scope())
                If ShouldBackupCompletedFiles Then
                    Try
                        File.Copy(filePath, Path.Combine(Me.Config.BackupFilesFolder, Path.GetFileName(filePath)), True)
                    Catch ex As Exception
                        TraceTopic.importerTopic.error("Failed to backup file " & filePath, ex)
                    End Try
                End If
                If ShouldDeleteCompletedFiles Then
                    Try
                        File.Delete(filePath)
                    Catch ex As Exception
                        TraceTopic.importerTopic.error("Failed to delete file " & filePath, ex) 'SMSM: add prefix everywhere
                    End Try
                End If
            End Using
        End Sub

        ''' <summary>
        ''' Handles a file when the interaction processing failed.
        ''' </summary>
        ''' <param name="filePath">Path to the file</param>
        ''' <remarks>Backs up and/or deletes the file</remarks>
        Protected Overridable Sub HandleFailedFile(ByVal filePath As String)
            Using (TraceTopic.importerTopic.scope())
                If ShouldBackupFailedFiles Then
                    Try
                        File.Copy(filePath, Path.Combine(Me.Config.FailedFilesFolder, Path.GetFileName(filePath)), True)
                    Catch ex As Exception
                        TraceTopic.importerTopic.error("Failed to backup failed file " & filePath)
                    End Try
                End If
                If ShouldDeleteCompletedFiles Then
                    Try
                        File.Delete(filePath)
                    Catch ex As Exception
                        TraceTopic.importerTopic.error("Failed to delete file " & filePath, ex)
                    End Try
                End If
            End Using
        End Sub

        ''' <summary>
        ''' Get a list of files for an interaction. Usually there is a metadata file and a content (e.g. audio) file.
        ''' </summary>
        ''' <param name="interaction">Data object for the interaction</param>
        ''' <returns>A list of the paths to the files relating to the interaction</returns>
        ''' <remarks></remarks>
        Protected Overridable Function GetInteractionFiles(ByVal interaction As IInteractionData) As ICollection(Of String)
            Dim ret As New List(Of String)
            Dim f As String
            f = GetInteractionContentFile(interaction)
            If Not String.IsNullOrEmpty(f) Then
                ret.Add(f)
            End If
            f = GetInteractionMetaFile(interaction)
            If Not String.IsNullOrEmpty(f) Then
                ret.Add(f)
            End If
            Return ret
        End Function

        ''' <summary>
        ''' Get the content (e.g. audio) file for the interaction
        ''' </summary>
        ''' <param name="interaction">Data object for the interaction</param>
        ''' <returns>The path to the content file</returns>
        ''' <remarks></remarks>
        Protected Overridable Function GetInteractionContentFile(ByVal interaction As IInteractionData) As String
            Return interaction.ContentFilePath
        End Function

        ''' <summary>
        ''' Get the metadata file for the interaction.
        ''' </summary>
        ''' <param name="interaction">Data object for the interaction</param>
        ''' <returns>The path to the metadata file</returns>
        ''' <remarks></remarks>
        Protected Overridable Function GetInteractionMetaFile(ByVal interaction As IInteractionData) As String
            Return interaction.MetaFilePath
        End Function

        ''' <summary>
        ''' Whether to backup files for interactions that were processed successfully.
        ''' </summary>
        ''' <value>Whether to backup the files</value>
        ''' <returns>True if the backup folder exists</returns>
        ''' <remarks></remarks>
        Protected Overridable ReadOnly Property ShouldBackupCompletedFiles() As Boolean
            Get
                Return Me.Config.BackupFilesFolder IsNot Nothing
            End Get
        End Property

        ''' <summary>
        ''' Whether to backup files for interactions when processing fails.
        ''' </summary>
        ''' <value>Whether to backup the files</value>
        ''' <returns>True if the folder for failed files exists</returns>
        ''' <remarks></remarks>
        Protected Overridable ReadOnly Property ShouldBackupFailedFiles() As Boolean
            Get
                Return Me.Config.FailedFilesFolder IsNot Nothing
            End Get
        End Property

        ''' <summary>
        ''' Whether to delete files after the interaction is processed.
        ''' </summary>
        ''' <value>Whether to delete the files</value>
        ''' <returns>True</returns>
        ''' <remarks>Override if the files should stay in place.</remarks>
        Protected Overridable ReadOnly Property ShouldDeleteCompletedFiles() As Boolean
            Get
                Return True
            End Get
        End Property
    End Class
End Namespace
﻿Namespace Data
  ''' <summary>
  ''' Data for audio calls
  ''' </summary>
  ''' <remarks></remarks>
  Public Interface IAudioInteractionData
    Inherits IInteractionData

    ''' <summary>
    ''' The duration of the call
    ''' </summary>
    ''' <value>Duration in seconds</value>
    ''' <returns>Duration in seconds</returns>
    ''' <remarks></remarks>
    Property Duration() As Decimal
    ''' <summary>
    ''' The audio format of the call
    ''' </summary>
    ''' <value>The internal value for the format</value>
    ''' <returns>The internal value for the format</returns>
    ''' <remarks></remarks>
    Property AudioFormat() As utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat
    ''' <summary>
    ''' The type of speaker on the left audio channel
    ''' </summary>
    ''' <value>The type of speaker</value>
    ''' <returns>The type of speaker</returns>
    ''' <remarks></remarks>
    Property LeftChannelSpeaker() As String
    ''' <summary>
    ''' The type of speaker on the right audio channel
    ''' </summary>
    ''' <value>The type of speaker</value>
    ''' <returns>The type of speaker</returns>
    ''' <remarks></remarks>
    Property RightChannelSpeaker() As String
    ''' <summary>
    ''' The ID of the key used to encrypt the audio
    ''' </summary>
    ''' <value>The key ID</value>
    ''' <returns>The key ID</returns>
    ''' <remarks></remarks>
    Property EncryptionKeyID() As Integer

  End Interface
End Namespace
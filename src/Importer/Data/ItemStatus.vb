﻿Namespace Data
    ''' <summary>
    ''' Status of a single item processing
    ''' </summary>
    ''' <remarks></remarks>
    Public Class ItemStatus
        Implements IItemStatus

        Private m_success As Boolean
        Private m_message As String
        Private m_exception As Exception

        ''' <summary>
        ''' Create a new item processing status
        ''' </summary>
        ''' <param name="success">Whether processing was successful or not</param>
        ''' <param name="message">Error message in case of failure</param>
        ''' <param name="exception">Exception that caused the failure</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal success As Boolean, Optional ByVal message As String = Nothing, Optional ByVal exception As Exception = Nothing)
            m_success = success
            m_message = message
            m_exception = exception
        End Sub

        ''' <summary>
        ''' Whether processing was successful or not
        ''' </summary>
        ''' <value>True for successful processing</value>
        ''' <returns>True for successful processing</returns>
        ''' <remarks></remarks>
        Public Property Success() As Boolean Implements IItemStatus.Success
            Get
                Return m_success
            End Get
            Set(ByVal value As Boolean)
                m_success = value
            End Set
        End Property

        ''' <summary>
        ''' Message describing why the processing failed
        ''' </summary>
        ''' <value>A message describing the failure</value>
        ''' <returns>A message describing the failure</returns>
        ''' <remarks></remarks>
        Public Property Message() As String Implements IItemStatus.Message
            Get
                Return m_message
            End Get
            Set(ByVal value As String)
                m_message = value
            End Set
        End Property

        ''' <summary>
        ''' Exception that caused the processing to fail
        ''' </summary>
        ''' <value>The exception causing the failure</value>
        ''' <returns>The exception causing the failure</returns>
        ''' <remarks></remarks>
        Public Property Exception() As System.Exception Implements IItemStatus.Exception
            Get
                Return m_exception
            End Get
            Set(ByVal value As System.Exception)
                m_exception = value
            End Set
        End Property

        ''' <summary>
        ''' Return a string representation of the status, for logging purposes
        ''' </summary>
        ''' <returns>Success or failure + error message &amp; exception message, when they exist</returns>
        ''' <remarks></remarks>
        Public Function ToLogString() As String Implements IItemStatus.ToLogString
            Dim ret As String
            If m_success Then
                ret = "Success"
            Else
                ret = "Failure"
            End If
            If Not String.IsNullOrEmpty(m_message) Then
                ret &= ". " & m_message
            End If
            If m_exception IsNot Nothing Then
                ret &= vbCrLf & m_exception.Message
            End If
            Return ret
        End Function
    End Class
End Namespace
﻿Imports GenUtils
Imports QBEUConnector.Agents
Imports QBEUConnector.Configuration

Namespace Data
    ''' <summary>
    ''' Data manager for getting interaction data from a database
    ''' </summary>
    ''' <remarks></remarks>
    Public Class DatabaseDataManager
        Inherits BaseDataManager
        Implements IDataManager

        ''' <summary>
        ''' The database object
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_db As utopy.bDBServices.DBServices

        ''' <summary>
        ''' Create the data manager with its configuration
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection)
            MyBase.New(configuration)
            SetDB()
        End Sub

        ''' <summary>
        ''' Create the data manager with its configuration and an agent manager
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <param name="agentManager">The agent manager</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection, ByVal agentManager As IAgentManager)
            MyBase.New(configuration, agentManager)
            SetDB()
        End Sub

        Private Sub SetDB()
            m_db = ConfigManager.GetDatabase(Config.DataConnectionName)
            If m_db Is Nothing Then
                Throw New ApplicationException(String.Format(
                    "No connection defined for {0}", Config.DataConnectionName))
            End If
        End Sub

        Protected Overrides Function OnGetNextInteractions(
            limitTo As Integer
            ) As IEnumerable(Of IInteractionData)

            ' limitTo parameter is ignored, because any limiting of results should be done
            ' by the database. Having the database handle it will improve execution time and
            ' local memory usage.

            Dim ixns = New List(Of IInteractionData)
            Dim results = m_db.getDataTable(Me.DataQuery)
            results.ForEach(Sub(row)
                                ixns.Add(Me.GetInteractionData(row))
                            End Sub)

            Return ixns
        End Function

        ''' <summary>
        ''' Callback for when one interaction is handled. Updates the configuration with the last interaction handled.
        ''' </summary>
        ''' <param name="interaction">The interaction data</param>
        ''' <param name="status">Success/failure information</param>
        ''' <remarks></remarks>
        Public Overrides Sub InteractionHandled(ByVal interaction As IInteractionData, ByVal status As IItemStatus)
            Dim interactionOrderField As String = Config.InteractionOrderField
            Dim interactionOrderValue As String
            If String.IsNullOrEmpty(interactionOrderField) Then
                interactionOrderValue = interaction.ID
            Else
                interactionOrderValue = interaction(interactionOrderField)
            End If
            If CompareInteractionOrder(interactionOrderValue, Config.LastHandledInteraction) > 0 Then
                Config.LastHandledInteraction = interactionOrderValue
            End If
        End Sub

        ''' <summary>
        ''' The SQL query to use for getting the interactions.
        ''' </summary>
        ''' <value>The SQL query</value>
        ''' <returns>The SQL query, with the last interaction handled injected in its place</returns>
        ''' <remarks></remarks>
        Protected Overridable ReadOnly Property DataQuery() As String
            Get
                Dim query As String = Config.DataQuery
                If Not String.IsNullOrEmpty(Config.LastHandledInteraction) Then
                    query = query.Replace(Config.LastInteractionHolder, Config.LastHandledInteraction)
                End If
                Return query
            End Get
        End Property

        ''' <summary>
        ''' The SQL query used to retrieve the number of new interactions waiting to be processed
        ''' </summary>
        ''' <value>The SQL query</value>
        ''' <returns>The SQL query, with the last interaction handled injected in its place</returns>
        ''' <remarks></remarks>
        Protected Overridable ReadOnly Property DataQueueQuery() As String
            Get
                Dim query As String = Config.DataQueueQuery
                If Not String.IsNullOrEmpty(Config.LastHandledInteraction) Then
                    query = query.Replace(Config.LastInteractionHolder, Config.LastHandledInteraction)
                End If
                Return query
            End Get
        End Property

        ''' <summary>
        ''' Get the data for an interaction from a row in the database.
        ''' </summary>
        ''' <param name="row">The database row</param>
        ''' <returns>The interaction data</returns>
        ''' <remarks></remarks>
        Protected Overridable Function GetInteractionData(ByVal row As DataRow) As IInteractionData
            Dim colIdxId = row.Table.Columns(InteractionIdKey)
            Dim ret As IInteractionData = CreateInteractionData(CStr(row(colIdxId)))
            For Each column As DataColumn In row.Table.Columns
                If column Is colIdxId Then
                    Continue For
                End If

                Dim columnName As String = column.ColumnName
                Dim dataKey As String = ColumnToDataMapping(columnName)
                If String.IsNullOrEmpty(dataKey) Then
                    dataKey = "Field" & column.Ordinal
                End If
                Dim val As String
                If IsDBNull(row(column)) Then
                    val = Nothing
                Else
                    val = CStr(row(column))
                End If
                SetInteractionValue(ret, dataKey, val)
            Next
            Return ret
        End Function

        ''' <summary>
        ''' Map column names in the database to property keys in the interaction data.
        ''' </summary>
        ''' <param name="columnName">Name of the column in the database</param>
        ''' <returns>Interaction data property key</returns>
        ''' <remarks></remarks>
        Protected Overridable Function ColumnToDataMapping(ByVal columnName As String) As String
            Return columnName
        End Function

        Public Overrides Function IxnsAvailable() As Boolean
            Return m_db.getOneValue(Of Boolean)(DataQueueQuery)
        End Function

        ''' <summary>
        ''' Compare two interactions to see which is first in the query
        ''' </summary>
        ''' <param name="firstValue">The value of the compare key for the first interaction</param>
        ''' <param name="secondValue">The value of the compare key for the second interaction</param>
        ''' <returns>An integer indicating the relationship between the two. 
        ''' A negative number means the first one is first.
        ''' A positive number means the second one is first.
        ''' Zero means they are the same.</returns>
        ''' <remarks></remarks>
        Protected Overridable Function CompareInteractionOrder(ByVal firstValue As String, ByVal secondValue As String) As Integer
            Return OrderCompareFunction()(firstValue, secondValue)
        End Function

        ''' <summary>
        ''' Function used for comparison of the values. Override to use a different comparison type.
        ''' </summary>
        ''' <returns>An integer indicating the relationship between the two. 
        ''' A negative number means the first one is first.
        ''' A positive number means the second one is first.
        ''' Zero means they are the same.</returns>
        ''' <remarks>The default is regular string comparison</remarks>
        Protected Overridable Function OrderCompareFunction() As Func(Of String, String, Integer)
            Return AddressOf String.Compare
        End Function

        ''' <summary>
        ''' Can be used as the <see cref="OrderCompareFunction"/> to compare the strings as dates.
        ''' </summary>
        ''' <param name="s1">The first date string</param>
        ''' <param name="s2">The second date string</param>
        ''' <returns>An integer indicating the relationship between the two. 
        ''' A negative number means the first one is first.
        ''' A positive number means the second one is first.
        ''' Zero means they are the same.</returns>
        ''' <remarks></remarks>
        Protected Function CompareStringsAsDates(ByVal s1 As String, ByVal s2 As String) As Integer
            Dim d1 As Date = Date.Parse(s1, Globalization.CultureInfo.InvariantCulture)
            Dim d2 As Date = Date.Parse(s2, Globalization.CultureInfo.InvariantCulture)
            Return Date.Compare(d1, d2)
        End Function

        ''' <summary>
        ''' Can be used as the <see cref="OrderCompareFunction"/> to compare the strings as numbers.
        ''' </summary>
        ''' <param name="s1">The first number string</param>
        ''' <param name="s2">The second number string</param>
        ''' <returns>An integer indicating the relationship between the two. 
        ''' A negative number means the first one is first.
        ''' A positive number means the second one is first.
        ''' Zero means they are the same.</returns>
        ''' <remarks></remarks>
        Protected Function CompareStringsAsNumbers(ByVal s1 As String, ByVal s2 As String) As Integer
            Dim n1 As Decimal = Decimal.Parse(s1, Globalization.CultureInfo.InvariantCulture)
            Dim n2 As Decimal = Decimal.Parse(s2, Globalization.CultureInfo.InvariantCulture)
            Return Decimal.Compare(n1, n2)
        End Function

    End Class
End Namespace
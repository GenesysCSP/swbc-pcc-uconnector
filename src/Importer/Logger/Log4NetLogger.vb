﻿Imports log4net

Namespace Logging

    'TODO: Resolve this.
    Public Class Log4NetLogger
        Implements ILogger

#Region "Constructors"

        Public Sub New(
            log As ILog
            )

            If log Is Nothing Then
                Throw New ArgumentNullException("log")
            End If

            _log = log
        End Sub

#End Region

#Region "ILogger Methods"

        Public Sub FATAL(
            message As String,
            Optional exception As Exception = Nothing
            ) Implements ILogger.FATAL

            If _log.IsFatalEnabled = False Then
                Return
            End If

            _log.Fatal(message, exception)
        End Sub

        Public Sub LOG(
            message As String,
            Optional exception As Exception = Nothing
            ) Implements ILogger.LOG

            If _log.IsInfoEnabled = False Then
                Return
            End If

            _log.Info(message, exception)
        End Sub

        Public Sub LOG_DEBUG(
            message As String,
            Optional exception As Exception = Nothing
            ) Implements ILogger.LOG_DEBUG

            If _log.IsDebugEnabled = False Then
                Return
            End If

            _log.Debug(message, exception)
        End Sub

        Public Sub WARNING(
            message As String,
            Optional exception As Exception = Nothing
            ) Implements ILogger.WARNING

            If _log.IsWarnEnabled = False Then
                Return
            End If

            _log.Warn(message, exception)
        End Sub

#End Region

#Region "Instance Data"

        Private ReadOnly _log As ILog

#End Region

    End Class

End Namespace
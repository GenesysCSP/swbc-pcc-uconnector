﻿Imports QBEUConnector.Configuration

Namespace Output
    ''' <summary>
    ''' Manage the UConnector's output folders
    ''' </summary>
    ''' <remarks></remarks>
    Public Class FolderManagerImpl
        Implements IFolderManager

        ''' <summary>
        ''' The configuration for the folder manager
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_configuration As OutputConfig

        ''' <summary>
        ''' All of the folders defined
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_folders As ICollection(Of String)
        ''' <summary>
        ''' Dictionary to map from folder to the amount of free space in it
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_diskSpaceHash As IDictionary(Of String, SpaceTimeStruct) = New Dictionary(Of String, SpaceTimeStruct)
        ''' <summary>
        ''' Next folder to use as an output folder
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_nextFolder As Integer
        ''' <summary>
        ''' Minimum amount of free space (in GB) required to allow using a folder
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_minimumSpace As Double
        ''' <summary>
        ''' Minimum amount of free space (in GB) for preferring a folder. Folders with less space are used only if there are no preferred folders.
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_preferredSpace As Double

        ''' <summary>
        ''' Create a folder manager with its configuration
        ''' </summary>
        ''' <param name="configuration">The configuration</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection)
            m_configuration = New OutputConfig(configuration)
        End Sub

        ''' <summary>
        ''' Initialize the folder manager, using its configuration
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overridable Sub Init() Implements IFolderManager.Init
            Init(m_configuration.OutputFolders, m_configuration.RequiredFolderSpace, m_configuration.PreferredFolderSpace)
        End Sub

        ''' <summary>
        ''' Initialize the folder manager with specified values
        ''' </summary>
        ''' <param name="folders">The list of output folders</param>
        ''' <param name="minimumSpace">Required free space to use a folder, in GB</param>
        ''' <param name="preferredSpace">Free space needed to prefer a folder, in GB</param>
        ''' <remarks></remarks>
        Protected Sub Init(ByVal folders As ICollection(Of String), ByVal minimumSpace As Double, ByVal preferredSpace As Double)
            m_folders = New List(Of String)(folders)
            m_nextFolder = 0
            m_minimumSpace = minimumSpace
            m_preferredSpace = preferredSpace
        End Sub

        ''' <summary>
        ''' Get the next available output folder
        ''' </summary>
        ''' <returns>Returns the next output folder with the preferred amount of free space, 
        ''' or, if such a folder is not found, the next one with the minimum amount. 
        ''' Returns null if no folder has the minimum amount of free space.</returns>
        ''' <remarks></remarks>
        Public Overridable Function GetOutputFolder() As String Implements IFolderManager.GetOutputFolder
            SyncLock m_folders
                Dim availableFolder As Integer? = Nothing
                For i As Integer = 0 To m_folders.Count - 1
                    Dim currentFolder As Integer = (m_nextFolder + i) Mod m_folders.Count
                    Dim folderPath As String = m_folders(currentFolder)
                    Dim folderSpace As Double = GetFolderFreeSpace(m_folders(currentFolder))
                    If folderSpace >= m_preferredSpace Then
                        availableFolder = currentFolder
                        Exit For
                    ElseIf folderSpace >= m_minimumSpace Then
                        If availableFolder Is Nothing Then
                            availableFolder = currentFolder
                        End If
                    End If
                Next
                If availableFolder Is Nothing Then
                    Return Nothing
                Else
                    m_nextFolder = (availableFolder.Value + 1) Mod m_folders.Count
                    Return m_folders(availableFolder.Value)
                End If
            End SyncLock
        End Function

        ''' <summary>
        ''' Get the total amount of space in all of the output folders.
        ''' </summary>
        ''' <returns>Total free space in GB</returns>
        ''' <remarks>Folders that are defined in the same disk are only counted once, unless different aliases are used.</remarks>
        Public Overridable Function GetTotalFreeSpace() As Double Implements IFolderManager.GetTotalFreeSpace
            ' Make sure all folders are updated
            For Each f As String In m_folders
                GetFolderFreeSpace(f)
            Next
            Dim totalSpace As Double
            For Each st As SpaceTimeStruct In m_diskSpaceHash.Values
                totalSpace += st.space
            Next
            Return totalSpace / 1073741824
        End Function

        ''' <summary>
        ''' Get the amount of free space in a folder.
        ''' </summary>
        ''' <param name="folder">The folder's path</param>
        ''' <returns>The number of free bytes in the folder.</returns>
        ''' <remarks></remarks>
        Protected Overridable Function GetFolderFreeSpace(ByVal folder As String) As Double
            Dim st As SpaceTimeStruct
            Dim root As String = IO.Path.GetPathRoot(folder)
            If Not m_diskSpaceHash.TryGetValue(root, st) OrElse (Now - st.timestamp) > FolderSpaceCheckInterval Then
                Dim f As New utopy.bIOFolder.Folder(root)
                st.space = f.GetAvailableSpace()
                st.timestamp = Now
                m_diskSpaceHash(root) = st
            End If
            Return st.space / 1073741824
        End Function

        ''' <summary>
        ''' Structure for keeping track of folder space
        ''' </summary>
        ''' <remarks></remarks>
        Protected Structure SpaceTimeStruct
            ''' <summary>
            ''' The amount of free space in the folder
            ''' </summary>
            ''' <remarks></remarks>
            Dim space As Double
            ''' <summary>
            ''' The last time free space was checked
            ''' </summary>
            ''' <remarks></remarks>
            Dim timestamp As DateTime
        End Structure

        ''' <summary>
        ''' How often to recheck free space in folder
        ''' </summary>
        ''' <value>Check interval</value>
        ''' <returns>Check interval</returns>
        ''' <remarks>Default is 2 minutes </remarks>
        Protected Overridable ReadOnly Property FolderSpaceCheckInterval() As TimeSpan
            Get
                Return TimeSpan.FromMinutes(2)
            End Get
        End Property
    End Class
End Namespace
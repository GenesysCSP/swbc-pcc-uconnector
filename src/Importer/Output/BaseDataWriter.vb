﻿Imports System.IO
Imports System.Xml
Imports QBEUConnector.Configuration
Imports QBEUConnector.Data

Namespace Output
    ''' <summary>
    ''' Writes the interaction content to the UConnector's output folder
    ''' </summary>
    ''' <remarks></remarks>
    Public MustInherit Class BaseDataWriter
        Implements IDataWriter

        ''' <summary>
        ''' Tag for file comment
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_OUTPUT_COMMENT As String = "This file includes the meta information for the call"
        ''' <summary>
        ''' Tag for root element
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_OUTPUT_CALLINFORMATION As String = "callInformation"
        ''' <summary>
        ''' Tag for program ID to handle the interaction
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_OUTPUT_PROGRAMID As String = "programID"
        ''' <summary>
        ''' Tag for interaction speakers
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_OUTPUT_SPEAKERS As String = "speakers"
        ''' <summary>
        ''' Tag for a single interaction speaker
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_OUTPUT_SPEAKER As String = "speaker"
        ''' <summary>
        ''' Attribute for ID of interaction speaker
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_OUTPUT_SPEAKER_ID As String = "id"
        ''' <summary>
        ''' Attribute for type of interaction speaker
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_OUTPUT_SPEAKER_TYPE As String = "speakerType"
        ''' <summary>
        ''' Attribute for workgroup of interaction speaker
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_OUTPUT_SPEAKER_WORKGROUP As String = "workgroup"
        ''' <summary>
        ''' Attribute for time when interaction speaker joined
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_OUTPUT_SPEAKER_STARTTIME As String = "startTime"
        ''' <summary>
        ''' Attribute for time when interaction speaker left
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_OUTPUT_SPEAKER_ENDTIME As String = "endTime"

        ''' <summary>
        ''' Tag for extra interaction metadata
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_OUTPUT_OTHER As String = "other"

        ''' <summary>
        ''' The configuration for the data writer
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_configuration As DataConfig

        ''' <summary>
        ''' Create a data writer with its configuration
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <remarks></remarks>
        Protected Sub New(ByVal configuration As IConfigSection)
            m_configuration = New DataConfig(configuration)
        End Sub

        ''' <summary>
        ''' Write the metadata file for the interaction to the output folder
        ''' </summary>
        ''' <param name="folder">The output folder</param>
        ''' <param name="interaction">The interaction data</param>
        ''' <returns>True if the file was sucessfully written</returns>
        ''' <remarks></remarks>
        Public Overridable Function GenerateMetaFile(ByVal folder As String, ByVal interaction As IInteractionData) As Boolean Implements IDataWriter.GenerateMetaFile
            Dim targetPath As String = Path.Combine(folder, interaction.ID) & ".xml"
            Dim xmlDoc As New XmlDocument()
            Dim decl As XmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "us-ascii", "no")
            xmlDoc.AppendChild(decl)
            Dim comment As XmlComment = xmlDoc.CreateComment(XML_OUTPUT_COMMENT)
            xmlDoc.AppendChild(comment)
            Dim rootElement As XmlElement = xmlDoc.CreateElement(XML_OUTPUT_CALLINFORMATION)
            AddMediaType(rootElement, interaction)
            AddProgram(rootElement, interaction)
            AddTime(rootElement, interaction)
            AddFormat(rootElement, interaction)
            AddSpeakers(rootElement, interaction)
            AddSpecificData(rootElement, interaction)
            AddExtraData(rootElement, interaction)
            xmlDoc.AppendChild(rootElement)
            xmlDoc.Save(targetPath)
            Return True
        End Function

        ''' <summary>
        ''' Add the media type to the metadata file
        ''' </summary>
        ''' <param name="parent">The parent node</param>
        ''' <param name="interaction">The interaction data</param>
        ''' <remarks></remarks>
        Protected MustOverride Sub AddMediaType(ByVal parent As XmlElement, ByVal interaction As IInteractionData)

        ''' <summary>
        ''' Add the ID of the program to handle the interaction to the metadata file
        ''' </summary>
        ''' <param name="parent">The parent node</param>
        ''' <param name="interaction">The interaction data</param>
        ''' <remarks></remarks>
        Protected Overridable Sub AddProgram(ByVal parent As XmlElement, ByVal interaction As IInteractionData)
            AddElement(parent, XML_OUTPUT_PROGRAMID, GetProgram(interaction))
        End Sub

        ''' <summary>
        ''' Get the program for the interaction
        ''' </summary>
        ''' <param name="interaction">The interaction data</param>
        ''' <returns>The program set in the interaction or in the configuration</returns>
        ''' <remarks></remarks>
        Protected Overridable Function GetProgram(ByVal interaction As IInteractionData) As String
            If String.IsNullOrEmpty(interaction.Program) Then
                Return m_configuration.Program
            Else
                Return interaction.Program
            End If
        End Function

        ''' <summary>
        ''' Add the interaction start time to the metadata file
        ''' </summary>
        ''' <param name="parent">The parent node</param>
        ''' <param name="interaction">The interaction data</param>
        ''' <remarks></remarks>
        Protected MustOverride Sub AddTime(ByVal parent As XmlElement, ByVal interaction As IInteractionData)

        ''' <summary>
        ''' Add the interaction start time to the metadata file
        ''' </summary>
        ''' <param name="parent">The parent node</param>
        ''' <param name="timeTag">The tag to use for the start time element</param>
        ''' <param name="interaction">The interaction data</param>
        ''' <remarks></remarks>
        Protected Sub AddTime(ByVal parent As System.Xml.XmlElement, ByVal timeTag As String, ByVal interaction As IInteractionData)
            AddElement(parent, timeTag, interaction.StartTime.ToString("s"))
        End Sub

        ''' <summary>
        ''' Add the format of the content file to the metadata file
        ''' </summary>
        ''' <param name="parent">The parent node</param>
        ''' <param name="interaction">The interaction data</param>
        ''' <remarks></remarks>
        Protected MustOverride Sub AddFormat(ByVal parent As XmlElement, ByVal interaction As IInteractionData)

        ''' <summary>
        ''' Adds a speaker to the given parent XML node
        ''' </summary>
        ''' <param name="speakersEle">The parent speakers node</param>
        ''' <param name="sp">The speaker to add</param>
        Protected Overridable Sub AddSpeaker(ByVal speakersEle As XmlElement, ByVal sp As Speaker)
            Dim doc As XmlDocument = speakersEle.OwnerDocument
            Dim speakerEle As XmlElement = doc.CreateElement(XML_OUTPUT_SPEAKER)
            Dim idAttr As XmlAttribute = doc.CreateAttribute(XML_OUTPUT_SPEAKER_ID)
            idAttr.Value = sp.ID
            speakerEle.Attributes.Append(idAttr)
            If sp.StartTime.HasValue Then
                Dim startAttr As XmlAttribute = doc.CreateAttribute(XML_OUTPUT_SPEAKER_STARTTIME)
                startAttr.Value = sp.StartTime.Value.ToString(Globalization.CultureInfo.InvariantCulture)
                speakerEle.Attributes.Append(startAttr)
            End If
            If sp.EndTime.HasValue Then
                Dim endAttr As XmlAttribute = doc.CreateAttribute(XML_OUTPUT_SPEAKER_ENDTIME)
                endAttr.Value = sp.EndTime.Value.ToString(Globalization.CultureInfo.InvariantCulture)
                speakerEle.Attributes.Append(endAttr)
            End If
            Dim typeEle As XmlAttribute = doc.CreateAttribute(XML_OUTPUT_SPEAKER_TYPE)
            typeEle.Value = "agent"
            speakerEle.Attributes.Append(typeEle)
            Dim workgroup As String
            If sp.Workgroup IsNot Nothing Then
                workgroup = sp.Workgroup
            Else
                workgroup = m_configuration.Workgroup
            End If
            AddElement(speakerEle, XML_OUTPUT_SPEAKER_WORKGROUP, workgroup)
            speakersEle.AppendChild(speakerEle)
        End Sub

        ''' <summary>
        ''' Add the interaction speakers to the metadata file
        ''' </summary>
        ''' <param name="parent">The parent node</param>
        ''' <param name="interaction">The interaction data</param>
        ''' <remarks></remarks>
        Protected Overridable Sub AddSpeakers(ByVal parent As XmlElement, ByVal interaction As IInteractionData)
            Dim doc As XmlDocument = parent.OwnerDocument
            Dim speakers As ICollection(Of Speaker) = interaction.Speakers
            If speakers IsNot Nothing AndAlso speakers.Count > 0 Then
                Dim speakersEle As XmlElement = doc.CreateElement(XML_OUTPUT_SPEAKERS)
                For Each speaker As Speaker In speakers
                    AddSpeaker(speakersEle, speaker)
                Next
                parent.AppendChild(speakersEle)
            End If
        End Sub

        ''' <summary>
        ''' Add data specific to the interaction type to the metadata file
        ''' </summary>
        ''' <param name="parent">The parent node</param>
        ''' <param name="interaction">The interaction data</param>
        ''' <remarks></remarks>
        Protected Overridable Sub AddSpecificData(ByVal parent As XmlElement, ByVal interaction As IInteractionData)
        End Sub

        ''' <summary>
        ''' Add extra interaction data to the metadata file
        ''' </summary>
        ''' <param name="parent">The parent node</param>
        ''' <param name="interaction">The interaction data</param>
        ''' <remarks></remarks>
        Protected Sub AddExtraData(ByVal parent As XmlElement, ByVal interaction As IInteractionData)
            parent.AppendChild(CreateExtraDataElement(parent.OwnerDocument, interaction))
        End Sub

        ''' <summary>
        ''' Create the element for the extra interaction data
        ''' </summary>
        ''' <param name="doc">The XML document</param>
        ''' <param name="interaction">The interaction data</param>
        ''' <returns>The XML element</returns>
        ''' <remarks></remarks>
        Protected Function CreateExtraDataElement(ByVal doc As XmlDocument, ByVal interaction As IInteractionData) As XmlElement
            Dim ret As XmlElement = doc.CreateElement(XML_OUTPUT_OTHER)
            AddExtraFields(ret, interaction)
            Return ret
        End Function

        ''' <summary>
        ''' Fill the element for the extra interaction data
        ''' </summary>
        ''' <param name="parent">The XML element for extra data</param>
        ''' <param name="interaction">The interaction data</param>
        ''' <remarks></remarks>
        Protected Overridable Sub AddExtraFields(ByVal parent As XmlElement, ByVal interaction As IInteractionData)
            Dim extraFields As IDictionary(Of String, String) = m_configuration.ExtraMetaFields
            If extraFields IsNot Nothing Then
                For Each f As KeyValuePair(Of String, String) In extraFields
                    AddElement(parent, interaction, f.Value, f.Key)
                Next
            End If
        End Sub

        ''' <summary>
        ''' Add an element to the metadata XML
        ''' </summary>
        ''' <param name="parent">The parent node</param>
        ''' <param name="ixnData">The interaction data</param>
        ''' <param name="tag">The tag of the element</param>
        ''' <param name="key">The key of the property in the interaction data</param>
        ''' <param name="defaultValue">The value to use if the property was not found</param>
        ''' <remarks></remarks>
        Protected Sub AddElement(
            parent As XmlElement,
            ixnData As IInteractionData,
            tag As String,
            key As String,
            Optional defaultValue As String = Nothing
            )

            Dim val As String = ixnData(key)
            If val Is Nothing Then
                val = defaultValue
            End If
            AddElement(parent, tag, val)
        End Sub

        ''' <summary>
        ''' Add an element to the metadata XML
        ''' </summary>
        ''' <param name="parent">The parent node</param>
        ''' <param name="tag">The tag of the element</param>
        ''' <param name="value">The element's value</param>
        ''' <remarks></remarks>
        Protected Sub AddElement(ByVal parent As XmlElement, ByVal tag As String, ByVal value As String)
            If String.IsNullOrWhiteSpace(value) = True Then
                Return
            End If

            tag = tag.Replace(" "c, "_"c)
            Dim ele As XmlElement = parent.OwnerDocument.CreateElement(tag)
            ele.InnerText = value
            parent.AppendChild(ele)
        End Sub

    End Class
End Namespace
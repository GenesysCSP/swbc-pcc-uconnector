﻿Imports QBEUConnector.Data

Namespace Output
    ''' <summary>
    ''' Writes the interaction content to the UConnector's output folder
    ''' </summary>
    ''' <remarks></remarks>
    Public Interface IDataWriter
        ''' <summary>
        ''' Write the metadata file for an interaction to the output folder
        ''' </summary>
        ''' <param name="folder">The output folder</param>
        ''' <param name="interaction">The interaction data</param>
        ''' <returns>True if the file was sucessfully written</returns>
        ''' <remarks></remarks>
        Function GenerateMetaFile(ByVal folder As String, ByVal interaction As IInteractionData) As Boolean
    End Interface
End Namespace
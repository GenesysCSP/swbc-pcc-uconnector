﻿Namespace Output
  ''' <summary>
  ''' Manage the UConnector's output folders
  ''' </summary>
  ''' <remarks></remarks>
  Public Interface IFolderManager
    ''' <summary>
    ''' Initialize the folder manager
    ''' </summary>
    ''' <remarks></remarks>
    Sub Init()
    ''' <summary>
    ''' Get an available output folder
    ''' </summary>
    ''' <returns>One of the output folders that has enough free space</returns>
    ''' <remarks></remarks>
    Function GetOutputFolder() As String
    ''' <summary>
    ''' Get the total free space in all the output folders
    ''' </summary>
    ''' <returns>Total free space in GB</returns>
    ''' <remarks></remarks>
    Function GetTotalFreeSpace() As Double
  End Interface
End Namespace
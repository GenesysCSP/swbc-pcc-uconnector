﻿Imports System.Xml
Imports QBEUConnector.Configuration
Imports QBEUConnector.Conversion
Imports QBEUConnector.Data

Namespace Output
    ''' <summary>
    ''' Writes a call's metadata content to the UConnector's output folder
    ''' </summary>
    ''' <remarks></remarks>
    Public Class AudioDataWriter
        Inherits BaseDataWriter
        Implements IDataWriter

        ''' <summary>
        ''' Tag for call start time
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_OUTPUT_CALL_TIME As String = "callTime"
        ''' <summary>
        ''' Tag for the audio format
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_OUTPUT_AUDIO_FORMAT As String = "audioFormat"

        ''' <summary>
        ''' Tag for the call duration
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_OUTPUT_CALL_LENGTH As String = "callLength"

        ''' <summary>
        ''' Tag for call channel speaker types
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_OUTPUT_CHANNELS As String = "channels"
        ''' <summary>
        ''' Attribute for type of speaker in the left audio channel
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_OUTPUT_CHANNELS_LEFT As String = "left"
        ''' <summary>
        ''' Attribute for type of speaker in the right audio channel
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_OUTPUT_CHANNELS_RIGHT As String = "right"

        ''' <summary>
        ''' Create a writer for the metadata
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection)
            MyBase.New(configuration)
        End Sub

        ''' <summary>
        ''' Add the audio format to the metadata file
        ''' </summary>
        ''' <param name="parent">The parent node</param>
        ''' <param name="interaction">The interaction data</param>
        ''' <remarks></remarks>
        Protected Overrides Sub AddFormat(ByVal parent As System.Xml.XmlElement, ByVal interaction As IInteractionData)
            Dim ele As XmlElement = parent.OwnerDocument.CreateElement(XML_OUTPUT_AUDIO_FORMAT)
            ele.InnerText = AudioFormat(CType(interaction, IAudioInteractionData)).ToString()
            parent.AppendChild(ele)
        End Sub

        ''' <summary>
        ''' Add the media type to the metadata file
        ''' </summary>
        ''' <param name="parent">The parent node</param>
        ''' <param name="interaction">The interaction data</param>
        ''' <remarks>Since media type is not needed for calls, we do nothing.</remarks>
        Protected Overrides Sub AddMediaType(ByVal parent As System.Xml.XmlElement, ByVal interaction As IInteractionData)
        End Sub

        ''' <summary>
        ''' Add the call start time to the metadata file
        ''' </summary>
        ''' <param name="parent">The parent node</param>
        ''' <param name="interaction">The interaction data</param>
        ''' <remarks></remarks>
        Protected Overrides Sub AddTime(ByVal parent As System.Xml.XmlElement, ByVal interaction As IInteractionData)
            AddTime(parent, XML_OUTPUT_CALL_TIME, interaction)
        End Sub

        ''' <summary>
        ''' Add data specific to calls to the metadata file
        ''' </summary>
        ''' <param name="parent">The parent node</param>
        ''' <param name="interaction">The interaction data</param>
        ''' <remarks></remarks>
        Protected Overrides Sub AddSpecificData(ByVal parent As System.Xml.XmlElement, ByVal interaction As IInteractionData)
            MyBase.AddSpecificData(parent, interaction)
            AddCallLength(parent, CType(interaction, IAudioInteractionData))
            AddChannels(parent, CType(interaction, IAudioInteractionData))
        End Sub

        ''' <summary>
        ''' Add the call channel speakers to the metadata file
        ''' </summary>
        ''' <param name="parent">The parent node</param>
        ''' <param name="interaction">The interaction data</param>
        ''' <remarks></remarks>
        Protected Overridable Sub AddChannels(ByVal parent As System.Xml.XmlElement, ByVal interaction As IAudioInteractionData)
            If Not String.IsNullOrEmpty(interaction.LeftChannelSpeaker) AndAlso Not String.IsNullOrEmpty(interaction.RightChannelSpeaker) Then
                Dim ele As XmlElement = parent.OwnerDocument.CreateElement(XML_OUTPUT_CHANNELS)
                Dim left As XmlAttribute = parent.OwnerDocument.CreateAttribute(XML_OUTPUT_CHANNELS_LEFT)
                left.Value = interaction.LeftChannelSpeaker
                ele.Attributes.Append(left)
                Dim right As XmlAttribute = parent.OwnerDocument.CreateAttribute(XML_OUTPUT_CHANNELS_RIGHT)
                right.Value = interaction.RightChannelSpeaker
                ele.Attributes.Append(right)
                parent.AppendChild(ele)
            End If
        End Sub

        ''' <summary>
        ''' Add the call length to the metadata file
        ''' </summary>
        ''' <param name="parent">The parent node</param>
        ''' <param name="interaction">The interaction data</param>
        ''' <remarks></remarks>
        Protected Overridable Sub AddCallLength(ByVal parent As System.Xml.XmlElement, ByVal interaction As IAudioInteractionData)
            If interaction.Duration > 0 Then
                AddElement(parent, XML_OUTPUT_CALL_LENGTH, interaction.Duration.ToString(Globalization.CultureInfo.InvariantCulture))
            End If
        End Sub

        ''' <summary>
        ''' Get the audio format for the call
        ''' </summary>
        ''' <param name="interaction">Interaction data</param>
        ''' <value>The call's audio format</value>
        ''' <returns>The format defined in the interaction, or in the configuration</returns>
        ''' <remarks></remarks>
        Protected Overridable ReadOnly Property AudioFormat(ByVal interaction As IAudioInteractionData) As utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat
            Get
                If interaction.AudioFormat = utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat.UNKNOWN Then
                    Dim audioConfig As New ConversionConfig(m_configuration)
                    Return audioConfig.TargetFormat
                Else
                    Return interaction.AudioFormat
                End If
            End Get
        End Property
    End Class
End Namespace
﻿Imports QBEUConnector.Configuration

Namespace Output
    ''' <summary>
    ''' Configuration for UConnector output
    ''' </summary>
    ''' <remarks></remarks>
    Public Class OutputConfig
        Inherits ConfigSection
        Implements IConfigSection

        ''' <summary>
        ''' Tag for the output section. Value: Output
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_SETTINGS_OUTPUT_SECTION_NAME As String = "Output"

        ''' <summary>
        ''' Tag for the fully qualified class name of the Data Writer. Value: DataWriterType
        ''' </summary>
        ''' <remarks><para>The class must implement <see cref="IDataWriter"/> and have a constructor that accepts <see cref="IConfigSection"/>.</para>
        ''' <para>You can also use <c>Audio</c>. The default type is <c>Audio</c>.</para></remarks>
        Protected Shared ReadOnly XML_SETTINGS_DATA_WRITER_TYPE As String = Config.FormatName(XML_SETTINGS_OUTPUT_SECTION_NAME, "DataWriterType")

        ''' <summary>
        ''' Tag for the fully qualified class name of the Folder Manager. Value: FolderManagerType
        ''' </summary>
        ''' <remarks><para>The class must implement <see cref="IFolderManager"/> and have a constructor that accepts <see cref="IConfigSection"/>.</para>
        ''' <para>You can also use <c>SpeechMiner</c> or <c>Configuration</c>. The default type is <c>SpeechMiner</c>.</para></remarks>
        Protected Shared ReadOnly XML_SETTINGS_FOLDER_MANAGER_TYPE As String = Config.FormatName(XML_SETTINGS_OUTPUT_SECTION_NAME, "FolderManagerType")

        ''' <summary>
        ''' Tag for a folder to use for UConnector output. Value: OutputFolder
        ''' </summary>
        ''' <remarks>
        ''' You can have more than one of these tags.
        ''' </remarks>
        Protected Const XML_SETTINGS_OUTPUT_FOLDER As String = "OutputFolder"

        ''' <summary>
        ''' Tag for the minimum free space an output folder must have to be used. Value: RequiredFolderSpace
        ''' </summary>
        ''' <remarks>Value is in GB</remarks>
        Protected Const XML_SETTINGS_REQUIRED_SPACE As String = "RequiredFolderSpace"

        ''' <summary>
        ''' Tag for the minimum free space an output folder should have to be preferred over others. Value: PreferredFolderSpace
        ''' </summary>
        ''' <remarks>Value is in GB</remarks>
        Protected Const XML_SETTINGS_PREFERRED_SPACE As String = "PreferredFolderSpace"

        ''' <summary>
        ''' Create a new output configuration section
        ''' </summary>
        ''' <param name="configuration"></param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection)
            MyBase.New(configuration, XML_SETTINGS_OUTPUT_SECTION_NAME)
        End Sub

        ''' <summary>
        ''' The class of Data Writer to create.
        ''' <seealso cref="XML_SETTINGS_DATA_WRITER_TYPE"/>
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <value>The class of Data Writer to create</value>
        ''' <returns>The Type to create</returns>
        ''' <remarks>The class must implement <see cref="IDataWriter"/> and have a constructor that accepts <see cref="IConfigSection"/>.</remarks>
        Public Shared ReadOnly Property DataWriterType(ByVal configuration As IConfigSection) As Type
            Get
                Dim writerType As String = configuration(XML_SETTINGS_DATA_WRITER_TYPE)
                If String.IsNullOrEmpty(writerType) OrElse writerType.Equals("Audio", StringComparison.InvariantCultureIgnoreCase) Then
                    Return GetType(AudioDataWriter)
                Else
                    Return Type.GetType(writerType)
                End If
            End Get
        End Property

        ''' <summary>
        ''' The class of Folder Manager to create.
        ''' <seealso cref="XML_SETTINGS_FOLDER_MANAGER_TYPE"/>
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <value>The class of Folder manager to create</value>
        ''' <returns>The Type to create</returns>
        ''' <remarks>The class must implement <see cref="IFolderManager"/> and have a constructor that accepts <see cref="IConfigSection"/>.</remarks>
        Public Shared ReadOnly Property FolderManagerType(ByVal configuration As IConfigSection) As Type
            Get
                Dim mgrType As String = configuration(XML_SETTINGS_FOLDER_MANAGER_TYPE)
                If String.IsNullOrEmpty(mgrType) OrElse mgrType.Equals("SpeechMiner", StringComparison.InvariantCultureIgnoreCase) Then
                    Return GetType(SpeechMinerFolderManager)
                ElseIf mgrType.Equals("Configuration") Then
                    Return GetType(FolderManagerImpl)
                Else
                    Return Type.GetType(mgrType)
                End If
            End Get
        End Property

        ''' <summary>
        ''' Folders to use for UConnector output.
        ''' <seealso cref="XML_SETTINGS_OUTPUT_FOLDER"/>
        ''' </summary>
        ''' <value>The list of output folders</value>
        ''' <returns>The list of output folders</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property OutputFolders() As ICollection(Of String)
            Get
                Return ValueList(XML_SETTINGS_OUTPUT_FOLDER)
            End Get
        End Property

        ''' <summary>
        ''' Minimum free space an output folder must have to be used
        ''' </summary>
        ''' <value>Required free space</value>
        ''' <returns>Required free space</returns>
        ''' <remarks>Value is in GB</remarks>
        Public Overridable ReadOnly Property RequiredFolderSpace() As Double
            Get
                If m_configuration(XML_SETTINGS_REQUIRED_SPACE) Is Nothing Then
                    Return DefaultRequiredFolderSpace
                End If
                Return CDbl(m_configuration(XML_SETTINGS_REQUIRED_SPACE))
            End Get
        End Property

        ''' <summary>
        ''' Override this to change the default required folder space, used if not specified
        ''' </summary>
        ''' <value>The default required folder space</value>
        ''' <returns>15 GB</returns>
        ''' <remarks></remarks>
        Protected Overridable ReadOnly Property DefaultRequiredFolderSpace() As Double
            Get
                Return 15
            End Get
        End Property

        ''' <summary>
        ''' Minimum free space an output folder should have to be preferred over others
        ''' </summary>
        ''' <value>Preferred free space</value>
        ''' <returns>Preferred free space</returns>
        ''' <remarks>Value is in GB</remarks>
        Public Overridable ReadOnly Property PreferredFolderSpace() As Double
            Get
                If m_configuration(XML_SETTINGS_PREFERRED_SPACE) Is Nothing Then
                    Return Nothing
                End If
                Return CDbl(m_configuration(XML_SETTINGS_PREFERRED_SPACE))
            End Get
        End Property

        ''' <summary>
        ''' Override this to change the default preferred folder space, used if not specified
        ''' </summary>
        ''' <value>The default preferred folder space</value>
        ''' <returns>25 GB</returns>
        ''' <remarks></remarks>
        Protected Overridable ReadOnly Property DefaultPreferredFolderSpace() As Double
            Get
                Return 25
            End Get
        End Property
    End Class
End Namespace

Imports System.Web.Security
Imports System.Web.Profile
Imports System.Data.SqlClient

Public MustInherit Class UsersCreator

    Protected m_dbMaster As utopy.bDBServices.DBServices
    Protected m_roleProvider As RoleProvider
    Protected m_membershipProvider As MembershipProvider
    Protected m_profileProvider As ProfileProvider
    Private m_userDomain As String

    Protected Const USER_PROPERTY_PHONE_NUMBER As String = "Phone"
    Protected Const USER_PROPERTY_TITLE As String = "Title"
    Protected Const USER_PROPERTY_ORGANIZATION As String = "UnitName"

    Public Sub New(ByVal _dbMaster As utopy.bDBServices.DBServices, Optional ByVal _roleProviderName As String = Nothing, _
                   Optional ByVal _membershipProviderName As String = Nothing, _
                   Optional ByVal _profileProviderName As String = Nothing, _
                   Optional ByVal _usePartitionsForAgents As Boolean = True)
        m_dbMaster = _dbMaster
        If _roleProviderName Is Nothing Then
            m_roleProvider = Roles.Provider
        Else
            m_roleProvider = Roles.Providers(_roleProviderName)
        End If
        If _membershipProviderName Is Nothing Then
            m_membershipProvider = Membership.Provider
        Else
            m_membershipProvider = Membership.Providers(_membershipProviderName)
        End If
        If m_profileProvider Is Nothing Then
            m_profileProvider = ProfileManager.Provider
        Else
            m_profileProvider = ProfileManager.Providers(_profileProviderName)
        End If
    End Sub

    Protected Overridable Function DoPartitionCache() As Boolean
        Return True
    End Function

    Protected Overridable Function UpdateIfExists() As Boolean
        Return False
    End Function

    Protected Overridable Function GetRowId(ByVal _row As DataRow) As Integer
        Return CInt(_row("id"))
    End Function

    Protected Overridable Function GetRowName(ByVal _row As DataRow) As String
        Return CStr(_row("name"))
    End Function


#Region "Get Users to Create"
    Protected Overridable Function GetSqlUsersToCreate() As String
        Return "select * from agentEntityTbl where isPerson=1 and id not in (select agentId from userAgentTbl)"
    End Function

    Protected Overridable Function GetUsersToCreate() As DataTable

        Dim sql As String = GetSqlUsersToCreate()
        Dim dt As DataTable = m_dbMaster.getDataTable(sql)

        Return dt
    End Function
#End Region

    Public Sub CreateMissingUsers()

        Dim dt As DataTable = Me.GetUsersToCreate()

        m_logger.LOG("User Creator: starting creation process")
        If dt.Rows.Count > 0 Then
            Dim rolesCache As New Generic.Dictionary(Of String, Boolean)
            Dim groupsCache As New Generic.Dictionary(Of String, Integer)
            Dim partitionsCache As New Generic.Dictionary(Of String, Integer)
            Dim nCreated As Integer = 0
            SyncLock (GetType(UsersCreator))
                ' Lock is needed because the profile provider is set on a shared member. 
                ' It is not likely that we will have different profile providers, but we are now ready for it.
                ' The cost of locking should not be big, because users are only created once in a while, and it should usually be a fairly quick operation.
                Dim en As IEnumerator = ProfileBase.Properties.GetEnumerator()
                While en.MoveNext
                    Dim pr As System.Configuration.SettingsProperty = CType(en.Current, System.Configuration.SettingsProperty)
                    pr.Provider = m_profileProvider
                End While
                For Each row As DataRow In dt.Rows
                    Try
                        If Me.DoPartitionCache() Then
                            If Not partitionsCache.ContainsKey(Me.GetRowName(row)) Then
                                partitionsCache.Add(Me.GetRowName(row), Me.GetRowId(row))
                            End If
                        End If

                        If CreateUser(Me.GetRowName(row), Me.GetRowId(row), rolesCache, groupsCache, partitionsCache, row) Then
                            nCreated += 1
                        End If

                    Catch ex As Exception
                        m_logger.WARNING("Failed to create user for " & Me.GetRowName(row), ex)
                    End Try
                Next
                If nCreated > 0 Then
                    m_logger.LOG("Created or updated " & nCreated & " new users")
                End If
            End SyncLock
            m_logger.LOG("User Creator: Finished creating " + nCreated.ToString + " users")
        End If
    End Sub

    Protected Overridable Function EditSingleUser(ByVal _id As Integer, ByVal _firstName As String, ByVal _lastName As String, ByVal _login As String, _
                                                  ByVal _password As String, ByVal _email As String, ByVal _comments As String, _
                                                  ByVal _roles As String(), ByVal _groups As String(), ByVal _partitions As String(), _
                                                  ByVal _agent As String, ByVal _mode As utopy.bSecurityManager.Manager.AuthenticationType, _
                                                  ByRef _rolesCache As Generic.Dictionary(Of String, Boolean), ByRef _groupsCache As Generic.Dictionary(Of String, Integer), _
                                                  ByRef _partitionsCache As Generic.Dictionary(Of String, Integer)) As Boolean

        Dim user As MembershipUser = Nothing

        If _login Is Nothing OrElse (_mode = utopy.bSecurityManager.Manager.AuthenticationType.SPEECHMINER AndAlso _password Is Nothing) OrElse _firstName Is Nothing OrElse _lastName Is Nothing Then
            Throw New Exception("Data is missing")
        Else
            Try
                user = m_membershipProvider.GetUser(_login, False)

                If user Is Nothing Then
                    Throw New Exception("User Creator: Cannot update user - " & _login & ": user does not exist")
                Else
                    m_logger.LOG("Updating user " & _login)
                    Me.UpdateUserInfo(user, _id, _login, _firstName, _lastName, _roles, _groups, _partitions, _email, _comments, _agent, _mode, _rolesCache, _groupsCache, _partitionsCache)
                End If
            Catch ex As Exception
                m_logger.WARNING("Error updating user " & _login, ex)
                Return False
            End Try
        End If

        Return True
    End Function


    Protected Sub SetUserPhoneNumber(ByVal _login As String, ByVal _phoneNumber As String)
        Dim myProfileBase As ProfileBase = Nothing
        Try
            myProfileBase = ProfileBase.Create(_login)
            Me.SetPhoneNumber(myProfileBase, _phoneNumber)
            myProfileBase.Save()
        Catch ex As Exception
            m_logger.WARNING("User Generation: unable to set phone number for " & _login & " with phone number " & _phoneNumber)
        End Try
    End Sub
    Protected Sub SetUserOrganization(ByVal _login As String, ByVal _organization As String)
        Dim myProfileBase As ProfileBase = Nothing
        Try
            myProfileBase = ProfileBase.Create(_login)
            Me.SetOrganization(myProfileBase, _organization)
            myProfileBase.Save()
        Catch ex As Exception
            m_logger.WARNING("User Generation: unable to set organization for " & _login & " with phone number " & _organization)
        End Try
    End Sub
    Protected Sub SetUserTitle(ByVal _login As String, ByVal _title As String)
        Dim myProfileBase As ProfileBase = Nothing
        Try
            myProfileBase = ProfileBase.Create(_login)
            Me.SetTitle(myProfileBase, _title)
            myProfileBase.Save()
        Catch ex As Exception
            m_logger.WARNING("User Generation: unable to set title for " & _login & " with phone number " & _title)
        End Try
    End Sub

    Protected Overridable Function CreateSingleUser(ByVal _id As Integer, ByVal _firstName As String, ByVal _lastName As String, ByVal _login As String, _
                                                    ByVal _password As String, ByVal _email As String, ByVal _comments As String, _
                                                    ByVal _roles As String(), ByVal _groups As String(), ByVal _partitions As String(), _
                                                    ByVal _agent As String, ByVal _mode As utopy.bSecurityManager.Manager.AuthenticationType, _
                                                    ByRef _rolesCache As Generic.Dictionary(Of String, Boolean), ByRef _groupsCache As Generic.Dictionary(Of String, Integer), _
                                                    ByRef _partitionsCache As Generic.Dictionary(Of String, Integer)) As Boolean

        If _login Is Nothing OrElse (_mode = utopy.bSecurityManager.Manager.AuthenticationType.SPEECHMINER AndAlso _password Is Nothing) OrElse _firstName Is Nothing OrElse _lastName Is Nothing Then
            Throw New Exception("Data is missing")
        Else
            Dim user As MembershipUser
            If _mode = utopy.bSecurityManager.Manager.AuthenticationType.WINDOWS Then
                _password = "password" ' Temporary
            End If
            Dim loginSuffix As Integer = 2
            Dim baseLogin As String = _login
            Dim status As MembershipCreateStatus
            Try
                If m_membershipProvider.GetUser(_login, False) IsNot Nothing Then
                    Return EditSingleUser(_id, _firstName, _lastName, _login, _password, _email, _comments, _roles, _groups, _partitions, _agent, _mode, _rolesCache, _groupsCache, _partitionsCache)
                End If
                Do
                    user = m_membershipProvider.CreateUser(_login, _password, _email, Nothing, Nothing, True, Nothing, status)
                    If status = MembershipCreateStatus.DuplicateUserName Then
                        _login = baseLogin & "_" & loginSuffix
                        loginSuffix += 1
                    End If
                Loop While status = MembershipCreateStatus.DuplicateUserName

                If status <> MembershipCreateStatus.Success Then
                    Throw New Exception(status.ToString())
                Else
                    m_logger.LOG("Creating user " & _login)
                    Me.UpdateUserInfo(user, _id, _login, _firstName, _lastName, _roles, _groups, _partitions, _email, _comments, _agent, _mode, _rolesCache, _groupsCache, _partitionsCache)
                End If
            Catch ex As Exception
                m_logger.WARNING("Error creating user " & _login, ex)
                Me.DeleteUserFromPartitions(_login)
                Me.DeleteUserFromGroups(_login)
                Me.DeleteUserFromRoles(_login, _roles, _rolesCache)
                m_membershipProvider.DeleteUser(_login, True)
                Return False
            End Try
        End If

        Return True
    End Function

    Protected Sub SetPhoneNumber(ByRef _userProfile As ProfileBase, ByVal _phoneNumber As String)
        _userProfile.SetPropertyValue(USER_PROPERTY_PHONE_NUMBER, _phoneNumber)
    End Sub

    Protected Sub SetOrganization(ByRef _userProfile As ProfileBase, ByVal _organization As String)
        _userProfile.SetPropertyValue(USER_PROPERTY_ORGANIZATION, _organization)
    End Sub

    Protected Sub SetTitle(ByRef _userProfile As ProfileBase, ByVal _title As String)
        _userProfile.SetPropertyValue(USER_PROPERTY_TITLE, _title)
    End Sub



    Protected Overridable Sub UpdateUserInfo(ByVal user As MembershipUser, ByVal _id As Integer, ByVal _login As String, _
                                            ByVal _firstName As String, ByVal _lastName As String, _
                                            ByVal _roles As String(), ByVal _groups As String(), ByVal _partitions As String(), _
                                            ByVal _email As String, ByVal _comments As String, _
                                            ByVal _agent As String, ByVal _mode As utopy.bSecurityManager.Manager.AuthenticationType, _
                                            ByRef _rolesCache As Generic.Dictionary(Of String, Boolean), ByRef _groupsCache As Generic.Dictionary(Of String, Integer), _
                                            ByRef _partitionsCache As Generic.Dictionary(Of String, Integer))

        user.Email = _email
        Dim userProfile As ProfileBase = ProfileBase.Create(_login)
        userProfile.SetPropertyValue("FirstName", _firstName)
        userProfile.SetPropertyValue("LastName", _lastName)
        If _email IsNot Nothing AndAlso utopy.bSecurityUsers.Users.emailIsValid(_email) Then
            userProfile.SetPropertyValue("Email", _email)
        End If
        userProfile.SetPropertyValue("Comments", _comments)
        utopy.bSecurityUsers.Users.setUserAgentPath(m_dbMaster, _login, _agent)
        userProfile.SetPropertyValue("AuthenType", [Enum].GetName(GetType(utopy.bSecurityManager.Manager.AuthenticationType), _mode))
        m_logger.LOG("Saving properties for user " & _login & " " & _firstName & "," & _lastName & "," & _email & "," & _comments)
        userProfile.Save()
        m_logger.LOG("Updating profile for " & _login)
        m_membershipProvider.UpdateUser(user)
        If _roles IsNot Nothing Then
            m_logger.LOG("Adding roles to " & _login)
            AddRoles(_login, _roles, _rolesCache)
        Else
            m_logger.LOG("Not adding roles to " & _login)
        End If
        If _groups IsNot Nothing Then
            m_logger.LOG("Adding groups to " & _login)
            AddGroups(_login, _groups, _groupsCache)
        Else
            m_logger.LOG("Not adding groups to " & _login)
        End If
        If _partitions IsNot Nothing Then
            m_logger.LOG("Adding partitions to " & _login)
            AddPartitions(_login, _partitions, _partitionsCache)
        Else
            m_logger.LOG("Not adding partitions to " & _login)
        End If
    End Sub


    Protected Overridable Function CreateUser(ByVal _person As String, ByVal _id As Integer, _
                                              ByVal _rolesCache As Generic.Dictionary(Of String, Boolean), _
                                              ByVal _groupsCache As Generic.Dictionary(Of String, Integer), _
                                              ByVal _partitionsCache As Generic.Dictionary(Of String, Integer), _
                                              Optional ByVal _row As DataRow = Nothing) As Boolean
        Dim login As String = Nothing
        Dim mode As utopy.bSecurityManager.Manager.AuthenticationType = Me.GetAuthenticationMode(_row)
        Dim password As String = Nothing
        Dim roles() As String
        Dim groups() As String
        Dim partitions() As String
        Dim firstName As String = Nothing
        Dim lastName As String = Nothing
        Dim email As String = Nothing
        Dim comments As String

        GetUserLogin(_person, login, mode, password, _row)
        roles = GetUserRoles(_person, _row)
        groups = GetUserGroups(_person, _row)
        partitions = GetUserPartitions(_person, _row)
        GetUserDetails(_person, firstName, lastName, email, _row)
        comments = GetUserComments(_person, _row)

        Return Me.CreateSingleUser(_id, firstName, lastName, login, password, email, comments, roles, groups, partitions, _person, mode, _rolesCache, _groupsCache, _partitionsCache)
    End Function

    Protected Sub DeleteUserFromAgentTbl(ByVal _login As String)
        Dim sqlStr As New Text.StringBuilder
        sqlStr.Append("delete from userAgentTbl where userLogin='" & _login & "'")
        m_dbMaster.runSQL(sqlStr.ToString)
    End Sub

    Protected Sub DeleteUserFromPartitions(ByVal _login As String)
        Dim sqlStr As New Text.StringBuilder
        sqlStr.Append("delete from userPartitionTbl where userLogin='" & _login & "'")
        m_dbMaster.runSQL(sqlStr.ToString)
    End Sub

    Protected Sub DeleteUserFromGroups(ByVal _login As String)
        Dim sqlStr As New Text.StringBuilder
        sqlStr.Append("delete from userGroupsTbl where userLogin='" & _login & "'")
        m_dbMaster.runSQL(sqlStr.ToString)
    End Sub

    Protected Sub DeleteUserFromRoles(ByVal _login As String, ByVal _roles() As String, ByVal _rolesCache As Generic.Dictionary(Of String, Boolean))
        Dim rolesToAdd() As String = Me.GetRoles(_roles, _rolesCache)
        Try
            m_roleProvider.RemoveUsersFromRoles(New String() {_login}, rolesToAdd)
        Catch pex As System.Configuration.Provider.ProviderException
            m_logger.WARNING("User Creator: Unable to delete " + _login + " from roles", pex)
        End Try
    End Sub
    Protected Function GetRoles(ByVal _roles() As String, ByVal _rolesCache As Generic.Dictionary(Of String, Boolean)) As String()
        Dim rolesToAdd As New List(Of String)
        For Each role As String In _roles
            Dim exists As Boolean
            If _rolesCache.ContainsKey(role) Then
                exists = _rolesCache(role)
            Else
                exists = m_roleProvider.RoleExists(role)
                _rolesCache(role) = exists
                If Not exists Then
                    m_logger.WARNING("Creating user: role " & role & " does not exist")
                End If
            End If
            If exists Then
                rolesToAdd.Add(role)
            End If
        Next
        Return rolesToAdd.ToArray()
    End Function

    Private Sub AddRoles(ByVal _login As String, ByVal _roles() As String, ByVal _rolesCache As Generic.Dictionary(Of String, Boolean))
        Dim rolesToAdd() As String = Me.GetRoles(_roles, _rolesCache)

        For Each role As String In rolesToAdd
            If m_roleProvider.IsUserInRole(_login, role) Then
                Continue For
            End If

            m_roleProvider.AddUsersToRoles(New String() {_login}, New String() {role})
        Next

    End Sub

    Private Sub AddGroups(ByVal _login As String, ByVal _groups() As String, ByVal _groupsCache As Generic.Dictionary(Of String, Integer))
        For Each group As String In _groups
            Dim groupId As Integer
            If _groupsCache.ContainsKey(group) Then
                groupId = _groupsCache(group)
            Else
                groupId = utopy.bSecurityGroups.Groups.getGroupId(m_dbMaster, group)
                _groupsCache(group) = groupId
                If groupId <= 0 Then
                    m_logger.WARNING("Creating user: group " & group & " does not exist")
                End If
            End If
            If groupId > 0 Then
                If Not utopy.bSecurityGroups.Groups.getUserGroupIDs(m_dbMaster, _login).Contains(groupId) Then
                    utopy.bSecurityGroups.Groups.setUserGroup(m_dbMaster, _login, groupId)
                End If
            End If
        Next
    End Sub

    Private Sub AddPartitions(ByVal _login As String, ByVal _partitions() As String, ByVal _partitionsCache As Generic.Dictionary(Of String, Integer))
        Dim partitionsList As New List(Of Integer)
        For Each partition As String In _partitions
            Dim partitionId As Integer = -1
            If _partitionsCache.ContainsKey(partition) Then
                partitionId = _partitionsCache(partition)
            Else
                Dim sql As String = "select id from partitionTbl where name=@path and type=2"
                Dim pathParam As New SqlParameter("@path", SqlDbType.VarChar)
                pathParam.Value = partition
                Dim dt As DataTable = m_dbMaster.getDataTable(sql, New SqlParameter() {pathParam})
                If dt.Rows.Count = 1 Then
                    partitionId = CInt(dt.Rows(0)("id"))
                End If
                _partitionsCache(partition) = partitionId
                If partitionId < 0 Then
                    m_logger.WARNING("Creating user: partition " & partition & " does not exist")
                End If
            End If
            If partitionId >= 0 Then

                partitionsList.Add(partitionId)
            End If
        Next
        utopy.bSecurityUsers.Users.setUserPartitions(m_dbMaster, Nothing, _login, partitionsList.ToArray(), Nothing)
    End Sub

#Region "Overrideable Groups, Roles, Partitions"
    Protected Overridable Function GetAuthenticationMode(Optional ByVal _row As DataRow = Nothing) As utopy.bSecurityManager.Manager.AuthenticationType
        Return utopy.bSecurityManager.Manager.AuthenticationType.SPEECHMINER
    End Function

    Protected MustOverride Sub GetUserLogin(ByVal _person As String, ByRef _login As String, ByRef _mode As utopy.bSecurityManager.Manager.AuthenticationType, ByRef _password As String, Optional ByVal _row As DataRow = Nothing)

    Protected Overridable Function GetUserRoles(ByVal _person As String, Optional ByVal _row As DataRow = Nothing) As String()
        Return Nothing
    End Function

    Protected Overridable Function GetUserGroups(ByVal _person As String, Optional ByVal _row As DataRow = Nothing) As String()
        Return Nothing
    End Function

    Protected Overridable Function GetUserPartitions(ByVal _person As String, Optional ByVal _row As DataRow = Nothing) As String()
        Return Nothing
    End Function

    Protected MustOverride Sub GetUserDetails(ByVal _person As String, ByRef _firstName As String, ByRef _lastName As String, ByRef _email As String, Optional ByVal _row As DataRow = Nothing)

    Protected Overridable Function GetUserComments(ByVal _person As String, Optional ByVal _row As DataRow = Nothing) As String
        Dim now As DateTime = DateTime.Now
        Return "Created automatically by UConnector at " & now.ToShortDateString() & " " & now.ToShortTimeString()
    End Function

#End Region
End Class

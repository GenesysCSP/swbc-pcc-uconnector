﻿Namespace Agents
    ''' <summary>
    ''' Holds information about a single agent.
    ''' </summary>
    ''' <remarks></remarks>
    Public Interface IAgentData

        ''' <summary>
        ''' ID of the agent in the external system
        ''' </summary>
        ''' <value>Agent ID</value>
        ''' <returns>Agent ID</returns>
        ''' <remarks>There should be a constant one-to-one relationship between this ID and a person. 
        ''' I.e., the person should be uniquely identified by this ID, have only one ID, and always have the same ID.
        ''' </remarks>
        Property ID As String

        ''' <summary>
        ''' The name of the agent
        ''' </summary>
        ''' <value>The name to set for the agent</value>
        ''' <returns>The name of the agent</returns>
        ''' <remarks></remarks>
        Property Name As String

        ''' <summary>
        ''' The ID of the agent's supervisor
        ''' </summary>
        ''' <value>The ID to set as the supervisor</value>
        ''' <returns>The supervisor's ID</returns>
        ''' <remarks>SupervisorId should match the AgentId for the supervisor's IAgentData object, if such an object exists.</remarks>
        Property SupervisorID As String

    End Interface
End Namespace
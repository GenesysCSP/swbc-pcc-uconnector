﻿Namespace Agents
    ''' <summary>
    ''' The AgentManager updates the agent hierarchy in the SpeechMiner database.
    ''' </summary>
    ''' <remarks></remarks>
    Public Interface IAgentManager

#Region "Events"

        Event UpdateFailed(cause As Exception)
        Event UpdateStarted()
        Event UpdateSuccessful()

#End Region

#Region "Methods"

        Function GetWorkgroup(agentId As String) As String
        Function IsAgentInHierarchy(agentId As String) As Boolean
        Sub StartUpdating()
        Sub StopUpdating()
        Function UpdateHierarchy() As Boolean

#End Region

    End Interface

End Namespace
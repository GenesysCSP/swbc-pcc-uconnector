﻿Imports utopy.bAgentsAgents
Imports System.IO
Imports QBEUConnector.Configuration

Namespace Agents
    ''' <summary>
    ''' An agent manager implementation that gets the agents from a file.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class FileAgentManager
        Inherits BaseAgentManager

        ''' <summary>
        ''' Path to the agents file.
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_agentsFile As String
        ''' <summary>
        ''' 0-based index for the agent ID column.
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_agentIdIndex As Integer = -1
        ''' <summary>
        ''' 0-based index for the agent name column.
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_agentNameIndex As Integer = -1
        ''' <summary>
        ''' 0-based index for the supervisor ID column.
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_supervisorIdIndex As Integer = -1

        ''' <summary>
        ''' Constructor to create the agent manager with its configuration
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection)
            MyBase.New(configuration)
            m_agentsFile = m_configuration.AgentFile
        End Sub

        ''' <summary>
        ''' Get the list of agents.
        ''' </summary>
        ''' <returns>A hierarchical collection of utopy.bAgentsAgents.Agent objects</returns>
        ''' <remarks></remarks>
        Protected Overloads Overrides Function GetAgents() As System.Collections.Generic.ICollection(Of Agent)
            If Not File.Exists(m_agentsFile) Then
                Throw New FileNotFoundException("Agents file was not found", m_agentsFile)
            End If
            Dim agentData As New List(Of IAgentData)
            Dim lines As String() = File.ReadAllLines(m_agentsFile)
            If lines.Length > 0 Then
                Dim headerLine As Boolean = ReadHeaders(lines(0))
                For Each line As String In lines
                    If headerLine Then
                        headerLine = False
                    Else
                        Dim ad As IAgentData = GetAgentData(line)
                        If ad IsNot Nothing Then
                            agentData.Add(ad)
                        End If
                    End If
                Next
            End If
            Return GetAgents(agentData)
        End Function

        ''' <summary>
        ''' Read the headers from the given line.
        ''' </summary>
        ''' <param name="line">The line (normally the first line in the file)</param>
        ''' <returns>Whether the line was actually read.</returns>
        ''' <remarks><para>Override and return false if the first line does not contain headers. In that case, make sure to set the column indexes.</para>
        ''' <para>Headers should be separated the same way as the columns.</para></remarks>
        Protected Overridable Function ReadHeaders(ByVal line As String) As Boolean
            Dim headers As String() = line.Split(m_configuration.AgentFileColumnSeparators)
            For i As Integer = 0 To headers.Length - 1
                Dim header As String = headers(i)
                Select Case header.ToLower()
                    Case m_configuration.AgentIdColumn.ToLower()
                        m_agentIdIndex = i
                    Case m_configuration.AgentNameColumn.ToLower()
                        m_agentNameIndex = i
                    Case m_configuration.SupervisorIdColumn.ToLower()
                        m_supervisorIdIndex = i
                End Select
            Next
            Return True
        End Function

        ''' <summary>
        ''' Get the agent data from a line in the file.
        ''' </summary>
        ''' <param name="line">The line</param>
        ''' <returns>Agent data object</returns>
        ''' <remarks></remarks>
        Protected Overridable Function GetAgentData(ByVal line As String) As IAgentData
            Dim fields As String() = line.Split(m_configuration.AgentFileColumnSeparators)
            Dim data As New AgentData()
            If m_agentIdIndex > -1 AndAlso m_agentIdIndex < fields.Length Then
                data.ID = fields(m_agentIdIndex)
            End If
            If m_agentNameIndex > -1 AndAlso m_agentNameIndex < fields.Length Then
                data.Name = fields(m_agentNameIndex)
            End If
            If m_supervisorIdIndex > -1 AndAlso m_supervisorIdIndex < fields.Length Then
                data.SupervisorID = fields(m_supervisorIdIndex)
            End If
            Return data
        End Function

    End Class
End Namespace
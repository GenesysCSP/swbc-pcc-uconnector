﻿Imports System.Data.SqlClient
Imports Genesys.SpeechMiner.Database
Imports System.Timers
Imports Genesys.SpeechMiner.Agents
Imports GenUtils
Imports GenUtils.TraceTopic

Imports System.Reflection
Imports QBEUConnector.Configuration
Imports QBEUConnector.Main

Namespace Agents

    <CLSCompliant(False)>
    Public MustInherit Class AgentManagerBase
        Implements IAgentManager

#Region "Events"

        Public Event UpdateFailed(cause As Exception) Implements IAgentManager.UpdateFailed
        Public Event UpdateStarted() Implements IAgentManager.UpdateStarted
        Public Event UpdateSuccessful() Implements IAgentManager.UpdateSuccessful

#End Region

#Region "Instance Properties"

        Public ReadOnly Property Config As AgentConfig
            Get
                Return _config
            End Get
        End Property

        Protected ReadOnly Property SMDB As ISMDatabase
            Get
                Return _smdb
            End Get
        End Property

#End Region

#Region "Constructors"

        Public Sub New(
            config As IConfigSection
            )
            Using (TraceTopic.importerTopic.scope())
                _config = New AgentConfig(config)

                Dim connStringName As String = New ImporterConfig(config).SpeechMinerDBName
                Dim dbConn = CType(ConfigManager.GetDBConnection(connStringName), SqlConnection)
                If dbConn Is Nothing Then
                    TraceTopic.importerTopic.error("Unable to update agent hierarchy (Reason: {} connection string not found)", connStringName)
                    Return
                End If

                _smdb = SMDatabase.Connect(dbConn)
                _timer = New Timer(_config.AgentUpdateInterval.TotalMilliseconds)
            End Using
        End Sub

#End Region

#Region "Instance Methods"

        Public Overridable Function GetWorkgroup(
            agentId As String
            ) As String Implements IAgentManager.GetWorkgroup

            Return String.Empty
        End Function

        Public Function IsAgentInHierarchy(
            agentId As String
            ) As Boolean Implements IAgentManager.IsAgentInHierarchy

            If agentId Is Nothing Then
                Throw New ArgumentNullException("agentId")
            End If

            Return _agentsInHierarchy.Contains(agentId)
        End Function

        Protected MustOverride Function BuildHierarchy() As Hierarchy

        Public Sub StartUpdating() Implements IAgentManager.StartUpdating
            Me.UpdateHierarchy()
            _timer.AutoReset = False
            _timer.Start()
        End Sub

        Public Sub StopUpdating() Implements IAgentManager.StopUpdating
            _timer.Stop()
            _timer.Close()
        End Sub

        Protected Overridable Sub TimerElapsed() Handles _timer.Elapsed
            Try
                Me.UpdateHierarchy()
            Finally
                _timer.Start()
            End Try
        End Sub

        Public Function UpdateHierarchy() As Boolean Implements IAgentManager.UpdateHierarchy
            Using (TraceTopic.importerTopic.scope())
                Try
                    RaiseEvent UpdateStarted()
                    TraceTopic.importerTopic.note("Updating agent hierarchy...")

                    Dim newHierarchy = Me.BuildHierarchy()
                    If newHierarchy Is Nothing Then
                        TraceTopic.importerTopic.warning("No hierarchy to update. Leaving it untouched.")
                        RaiseEvent UpdateSuccessful()
                        Return True
                    End If

                    _smdb.UpdateAgentHierarchy(newHierarchy, _supportForMultiExtIds)
                    _agentsInHierarchy.Clear()
                    newHierarchy.ForEachEntity(
                        Sub(current As Entity)
                            current.IDs.ForEach(
                                Sub(id As String)
                                    _agentsInHierarchy.Add(id)
                                End Sub)
                        End Sub)

                    TraceTopic.importerTopic.note("Agent hierarchy update complete.")
                    RaiseEvent UpdateSuccessful()
                    Return True
                Catch e As Exception

                    Dim appE = TryCast(e, ApplicationException)
                    Dim cause = If(appE IsNot Nothing, appE.InnerException, e)

                    'If Me.Log.IsDebugEnabled = True Then
                    TraceTopic.importerTopic.note("Encountered an error updating the agent hierarchy (Reason: {})\n{}", e.Message, e.StackTrace)
                    'ElseIf Me.Log.IsErrorEnabled = True Then
                    '    TraceTopic.importerTopic.error(e.Message)
                    'End If

                    RaiseEvent UpdateFailed(e)
                    Return False
                End Try
            End Using
        End Function

#End Region

#Region "Instance Data"

        Private ReadOnly _agentsInHierarchy As New HashSet(Of String)
        Private ReadOnly _config As AgentConfig
        Private ReadOnly _smdb As ISMDatabase
        Protected _supportForMultiExtIds As Boolean = False
        Private WithEvents _timer As Timer

#End Region

    End Class
End Namespace


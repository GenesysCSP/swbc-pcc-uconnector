﻿Imports utopy.bAgentsAgents
Imports utopy.bDBServices
Imports System.Timers
Imports GenUtils.TraceTopic
Imports QBEUConnector.Configuration
Imports QBEUConnector.Main

Namespace Agents
    ''' <summary>
    ''' Base class for updating the agent hierarchy in SpeechMiner.
    ''' </summary>
    ''' <remarks></remarks>
    Public MustInherit Class BaseAgentManager
        Implements IAgentManager

#Region "Events"

        Public Event UpdateFailed(ByVal exception As System.Exception) Implements IAgentManager.UpdateFailed
        Public Event UpdateStarted() Implements IAgentManager.UpdateStarted
        Public Event UpdateSuccessful() Implements IAgentManager.UpdateSuccessful

#End Region

#Region "Constructors"

        Public Sub New(
            config As IConfigSection
            )
            m_configuration = New AgentConfig(config)
            m_db = ConfigManager.GetDatabase(New ImporterConfig(config).SpeechMinerDBName)
        End Sub

#End Region

#Region "Class Methods"

        Private Shared Sub AddIDsToSet(
            parent As Agent,
            idSet As HashSet(Of String)
            )

            If parent Is Nothing Then
                Throw New ArgumentNullException("parent")
            ElseIf idSet Is Nothing Then
                Throw New ArgumentNullException("idSet")
            End If

            idSet.Add(parent.ID)
            For Each child As Agent In parent.Children
                AddIDsToSet(child, idSet)
            Next
        End Sub

#End Region

        ''' <summary>
        ''' Create the agent's supervisor based on the data of the agent
        ''' </summary>
        ''' <param name="agent">The agent's data</param>
        ''' <returns>The supervisor entity</returns>
        ''' <remarks></remarks>
        Protected Overridable Function CreateAgentSupervisor(ByVal agent As IAgentData) As Agent
            Return New Agent(agent.SupervisorID, agent.SupervisorID)
        End Function

        ''' <summary>
        ''' Start periodic updating of the agent hierarchy.
        ''' </summary>
        ''' <remarks>The method updates the hierarchy, then starts a timer for the next time.</remarks>
        Public Sub StartUpdating() Implements IAgentManager.StartUpdating
            UpdateHierarchy()
            InitTimer()
        End Sub

        ''' <summary>
        ''' Stop periodic updating of the agent hierarchy.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub StopUpdating() Implements IAgentManager.StopUpdating
            m_timer.Stop()
            m_timer.Close()
        End Sub

        ''' <summary>
        ''' Perform the agent hierarchy update.
        ''' </summary>
        ''' <returns>True if it succeeds</returns>
        ''' <remarks></remarks>
        Public Function UpdateHierarchy() As Boolean Implements IAgentManager.UpdateHierarchy
            RaiseEvent UpdateStarted()
            Try
                Dim agents As ICollection(Of Agent) = GetAgents()
                SetAgents(agents)
                RaiseEvent UpdateSuccessful()
                Return True
            Catch ex As Exception
                RaiseEvent UpdateFailed(ex)
                Return False
            End Try
        End Function

        ''' <summary>
        ''' Starts the timer for periodic updating of the agent hierarchy.
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overridable Sub InitTimer()
            m_timer = New Timers.Timer(m_configuration.AgentUpdateInterval.TotalMilliseconds)
            m_timer.AutoReset = False
            m_timer.Start()
        End Sub

        ''' <summary>
        ''' Handle the event specifying that the update interval finished, and restart the timer afterwards.
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overridable Sub TimerElapsed() Handles m_timer.Elapsed
            Try
                UpdateHierarchy()
            Finally
                m_timer.Start()
            End Try
        End Sub

        ''' <summary>
        ''' Get the list of agents.
        ''' </summary>
        ''' <returns>A hierarchical collection of utopy.bAgentsAgents.Agent objects</returns>
        ''' <remarks></remarks>
        Protected MustOverride Function GetAgents() As ICollection(Of Agent)

        ''' <summary>
        ''' Build the list of agents based on the agent data.
        ''' </summary>
        ''' <param name="agentData">Collection of agent data</param>
        ''' <returns>A hierarchical collection of <c>utopy.bAgentsAgents.Agent</c> objects</returns>
        ''' <remarks>If the supervisor ID matches the ID of an agent in the agent data, that is assumed to be the agent's supervisor.
        ''' If the ID does not match any agent's, the supervisor is insert as a top-level node.</remarks>
        Protected Overridable Function GetAgents(ByVal agentData As ICollection(Of IAgentData)) As ICollection(Of Agent)
            Dim agentById As New Dictionary(Of String, Agent)
            Dim agents As New List(Of Agent)
            m_agentDataById.Clear()
            For Each ad As IAgentData In agentData
                If m_agentDataById.ContainsKey(ad.ID) Then
                    If Not HandleDuplicateAgent(ad) Then
                        Continue For
                    End If
                End If
                m_agentDataById.Add(ad.ID, ad)
                Dim agent As Agent = Nothing
                If Not agentById.TryGetValue(ad.ID, agent) Then
                    agent = New Agent(ad.Name, ad.ID)
                    agentById(agent.ID) = agent
                Else
                    If agents.Contains(agent) Then
                        agents.Remove(agent)
                    End If
                    agent.Name = ad.Name
                End If

                Dim supervisor As Agent = Nothing
                If String.IsNullOrEmpty(ad.SupervisorID) Then
                    agents.Add(agent)
                Else
                    If Not agentById.TryGetValue(ad.SupervisorID, supervisor) Then
                        supervisor = CreateAgentSupervisor(ad)
                        agentById(supervisor.ID) = supervisor
                        supervisor.Children.Add(agent)
                        agents.Add(supervisor)
                    End If
                End If
            Next
            Return agents
        End Function

        ''' <summary>
        ''' Update the agents in the SpeechMiner database.
        ''' </summary>
        ''' <param name="agents">A hierarchical collection of utopy.bAgentsAgents.Agent objects</param>
        ''' <remarks></remarks>
        Protected Overridable Sub SetAgents(ByVal agents As ICollection(Of Agent))
            SyncLock _agentsInHierarchy
                Dim mgr As New utopy.bAgentsAgents.AgentManager(m_db)
                mgr.forceUniqueAgentNames = m_configuration.ForceUniqueAgentNames
                mgr.setAgents(agents)

                _agentsInHierarchy.Clear()
                For Each current As Agent In agents
                    AddIDsToSet(current, _agentsInHierarchy)
                Next
            End SyncLock
        End Sub

        ''' <summary>
        ''' Return the workgroup for a specified agent.
        ''' </summary>
        ''' <param name="agentId">The ID of the agent</param>
        ''' <returns>The agent's workgroup</returns>
        ''' <remarks></remarks>
        Public Overridable Function GetWorkgroup(ByVal agentId As String) As String Implements IAgentManager.GetWorkgroup
            Dim ret As String = ""
            Dim agent As IAgentData = Nothing
            If m_agentDataById.TryGetValue(agentId, agent) Then
                If Not String.IsNullOrEmpty(agent.SupervisorID) Then
                    ret = GetWorkgroup(agent.SupervisorID) & "/" & agent.SupervisorID
                End If
            End If
            Return ret
        End Function

        ''' <summary>
        ''' Convenience method for storing the agent data for workgroups when it is not created in the process of getting the agents.
        ''' </summary>
        ''' <param name="agents">Hierarchical agent data</param>
        ''' <remarks></remarks>
        Protected Overridable Sub StoreAgentData(ByVal agents As ICollection(Of Agent))
            For Each a As Agent In agents
                For Each child As Agent In a.Children
                    If Not m_agentDataById.ContainsKey(child.ID) Then
                        Dim ad As New AgentData()
                        ad.ID = child.ID
                        ad.Name = child.Name
                        ad.SupervisorID = a.ID
                        m_agentDataById(child.ID) = ad
                    End If
                Next
                StoreAgentData(a.Children)
            Next
        End Sub

        ''' <summary>
        ''' Handle the case of more than one agent in the data with the same ID
        ''' </summary>
        ''' <param name="agent">Data about the duplicate agent found</param>
        ''' <returns>True if the agent should be processed anyway</returns>
        ''' <remarks>Only return true if you change the agent's ID, remove the previous one,
        ''' or have somehow changed the code to handle duplicate IDs.</remarks>
        Protected Overridable Function HandleDuplicateAgent(ByVal agent As IAgentData) As Boolean
            Using (TraceTopic.importerTopic.scope())
                TraceTopic.importerTopic.warning("Agent " & agent.ID & " was found more than once")
            End Using
            Return False
        End Function

        Public Function IsAgentInHierarchy(
            agentId As String
            ) As Boolean Implements IAgentManager.IsAgentInHierarchy

            If agentId Is Nothing Then
                Throw New ArgumentNullException("agentId")
            End If

            Return _agentsInHierarchy.Contains(agentId)
        End Function

#Region "Instance Data"

        Private ReadOnly _agentsInHierarchy As New HashSet(Of String)
        Private ReadOnly m_agentDataById As IDictionary(Of String, IAgentData) = New Dictionary(Of String, IAgentData)
        Protected Shared ReadOnly m_agentUpdateLockObj As New Object()
        Protected ReadOnly m_configuration As AgentConfig
        Protected ReadOnly m_db As DBServices
        Protected WithEvents m_timer As Timer

#End Region

    End Class
End Namespace
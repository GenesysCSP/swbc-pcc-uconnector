﻿Namespace Monitoring
  ''' <summary>
  ''' Monitors the UConnector status and interaction processing by registering it in the SpeechMiner database
  ''' </summary>
  ''' <remarks>This monitor does not concern itself with the UConnector's status updates at this time.</remarks>
  Public Class SpeechMinerMonitor
    Implements IMonitor

    ''' <summary>
    ''' The UConnector's configuration
    ''' </summary>
    ''' <remarks></remarks>
    Protected m_configuration As MonitorConfiguration
    ''' <summary>
    ''' The SpeechMiner database
    ''' </summary>
    ''' <remarks></remarks>
    Protected m_db As utopy.bDBServices.DBServices
    ''' <summary>
    ''' The folder manager, for monitoring the status of the output folder/s
    ''' </summary>
    ''' <remarks></remarks>
    Protected m_folderManager As IFolderManager
    ''' <summary>
    ''' The data manager, for monitoring the queue of interactions to import
    ''' </summary>
    ''' <remarks></remarks>
    Protected m_dataManager As IDataManager
    ''' <summary>
    ''' Whether or not to log the processed interactions
    ''' </summary>
    ''' <remarks></remarks>
    Protected m_logInteractions As Boolean = False
    ''' <summary>
    ''' Table holding a log of the interactions that were not yet committed to the database
    ''' </summary>
    ''' <remarks></remarks>
    Protected m_interactionLogRecords As DataTable
    ''' <summary>
    ''' A timer for periodic committing of the interactions log to the database
    ''' </summary>
    ''' <remarks></remarks>
    Protected WithEvents m_timer As Timers.Timer
    ''' <summary>
    ''' The number of interactions handled so far
    ''' </summary>
    ''' <remarks></remarks>
    Protected m_numHandled As Integer = 0
    ''' <summary>
    ''' The ID of the last handled interaction
    ''' </summary>
    ''' <remarks></remarks>
    Protected m_lastId As String

    Private m_processTimeColumnName As String

    ''' <summary>
    ''' Create a new SpeechMiner monitor
    ''' </summary>
    ''' <param name="configuration">The UConnector configuartion</param>
    ''' <param name="db">The SpeechMiner database</param>
    ''' <param name="folderManager">The folder manager</param>
    ''' <param name="dataManager">The data manager</param>
    ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfig, ByVal db As utopy.bDBServices.DBServices, ByVal folderManager As IFolderManager, ByVal dataManager As IDataManager)
            If configuration Is Nothing Then
                Throw New ArgumentNullException("configuration")
            End If
            If db Is Nothing Then
                Throw New ArgumentNullException("db")
            End If
            m_configuration = New MonitorConfiguration(configuration)
            m_db = db
            m_folderManager = folderManager
            m_dataManager = dataManager
            InitInteractionLog()
        End Sub

        ''' <summary>
        ''' Start monitoring the UConnector
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub StartMonitor() Implements IMonitor.StartMonitor
            InitTimer()
        End Sub

        ''' <summary>
        ''' Stop monitoring the UConnector
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub StopMonitor() Implements IMonitor.StopMonitor
            m_timer.Stop()
            m_timer.Close()
            UpdateInteractionsStatus()
        End Sub

        ''' <summary>
        ''' Store a log of an interaction being processed
        ''' </summary>
        ''' <param name="interaction">The interaction's data</param>
        ''' <param name="status">The status of the interaction's processing</param>
        ''' <remarks></remarks>
        Public Sub UpdateInteraction(ByVal interaction As IInteractionData, ByVal status As IItemStatus) Implements IMonitor.UpdateInteraction
            Dim nameParam As New SqlClient.SqlParameter("@name", m_configuration.Name)
            Dim computerParam As New SqlClient.SqlParameter("@computer", ComputerName)
            Dim idParam As New SqlClient.SqlParameter("@callId", interaction.ID)
            m_db.runSQL("UPDATE monitorConnectorsTbl SET lastCall=@callId,lifesign=" & m_db.getDBTime() & " WHERE name=@name AND computer=@computer", _
                                   New SqlClient.SqlParameter() {nameParam, computerParam, idParam})
            If m_logInteractions Then
                Dim interactionFields As New List(Of String)
                interactionFields.Add(interaction.ID)
                For Each f As String In m_configuration.InteractionLogFields
                    interactionFields.Add(interaction(f))
                Next
                LogInteraction(status, interactionFields.ToArray())
            End If
            m_numHandled += 1
            m_lastId = interaction.ID
        End Sub

        ''' <summary>
        ''' Update when the UConnector's state changes
        ''' </summary>
        ''' <param name="status">The new UConnector state</param>
        ''' <param name="description">A textual description of the state</param>
        ''' <remarks>This implementation ignores this update</remarks>
        Public Overridable Sub UpdateStatus(ByVal status As IImporter.Status, ByVal description As String) Implements IMonitor.UpdateStatus
        End Sub

        ''' <summary>
        ''' The name of the current computer
        ''' </summary>
        ''' <value>The current host name</value>
        ''' <returns>The current host name in the DNS</returns>
        ''' <remarks></remarks>
        Protected ReadOnly Property ComputerName() As String
            Get
                Return System.Net.Dns.GetHostName()
            End Get
        End Property

        ''' <summary>
        ''' Start the timer for periodic committing of interaction logs to the database.
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overridable Sub InitTimer()
            m_timer = New Timers.Timer(TimerInterval.TotalMilliseconds)
            m_timer.AutoReset = False
            m_timer.Start()
        End Sub

        ''' <summary>
        ''' Interval for committing interaction logs to the database
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Overridable ReadOnly Property TimerInterval() As TimeSpan
            Get
                Return TimeSpan.FromMinutes(1)
            End Get
        End Property

        ''' <summary>
        ''' Commit interaction logs to the database when the time elapses.
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overridable Sub TimerElapsed() Handles m_timer.Elapsed
            Try
                UpdateInteractionsStatus()
                DeleteOldLogEntries()
            Finally
                m_timer.Start()
            End Try
        End Sub

        ''' <summary>
        ''' Commit the interaction logs to the database
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overridable Sub UpdateInteractionsStatus()
            If m_logInteractions And m_configuration.InteractionLogTable IsNot Nothing Then
                SyncLock m_interactionLogRecords
                    m_db.BulkInsert(m_interactionLogRecords, m_configuration.InteractionLogTable)
                    m_interactionLogRecords.Clear()
                End SyncLock
            End If
        End Sub

        ''' <summary>
        ''' Delete interaction logs that are older than the configured time to keep them
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overridable Sub DeleteOldLogEntries()
            If m_configuration.InteractionLogTable IsNot Nothing Then
                m_db.runSQL("DELETE FROM " & m_configuration.InteractionLogTable & " WHERE " & m_processTimeColumnName & " < '" & DateTime.UtcNow.Subtract(m_configuration.InteractionLogPeriod).ToString("s") & "'")
            End If
        End Sub

        ''' <summary>
        ''' The number of interactions handled by the importer
        ''' </summary>
        ''' <value>Number of handled interactions</value>
        ''' <returns>Number of handled interactions</returns>
        ''' <remarks></remarks>
        Protected Overridable ReadOnly Property NumberOfHandledInteractions() As Integer
            Get
                Return m_numHandled
            End Get
        End Property

        ''' <summary>
        ''' The number of interactions waiting to be handled by the UConnector
        ''' </summary>
        ''' <value>Number of interactions in the queue</value>
        ''' <returns>Number of interactions in the queue</returns>
        ''' <remarks>This depends on the data manager. Returns -1 if it doesn't exist.</remarks>
        Protected Overridable ReadOnly Property NumberOfWaitingInteractions() As Integer
            Get
                If m_dataManager Is Nothing Then
                    Return -1
                Else
                    Return m_dataManager.NumberOfWaitingInteractions
                End If
            End Get
        End Property

        ''' <summary>
        ''' The last interaction handled by the UConnector
        ''' </summary>
        ''' <value>The last interaction's ID</value>
        ''' <returns>The last interaction's ID</returns>
        ''' <remarks></remarks>
        Protected Overridable ReadOnly Property LastInteractionID() As String
            Get
                Return m_lastId
            End Get
        End Property

        ''' <summary>
        ''' The amount of free space in the UConnector's output folders
        ''' </summary>
        ''' <value>The total amount of free space in all folders, in GB</value>
        ''' <returns>The total amount of free space in all folders, in GB</returns>
        ''' <remarks></remarks>
        Protected Overridable ReadOnly Property FreeSpace() As Double
            Get
                Return m_folderManager.GetTotalFreeSpace()
            End Get
        End Property

        ''' <summary>
        ''' Initialize the table to store the interaction logs
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overridable Sub InitInteractionLog()
            If m_configuration.InteractionLogTable IsNot Nothing Then
                m_logInteractions = True
                m_interactionLogRecords = New DataTable()
                m_interactionLogRecords.Columns.Add("ProcessTime", GetType(DateTime))
                m_interactionLogRecords.Columns.Add("Status", GetType(String))
                m_interactionLogRecords.Columns.Add("ID", GetType(String))
                If m_configuration.InteractionLogFields IsNot Nothing Then
                    For Each f As String In m_configuration.InteractionLogFields
                        m_interactionLogRecords.Columns.Add(f)
                    Next
                End If
                Dim dt As DataTable = m_db.getDataTable("select * from " & m_configuration.InteractionLogTable & " where 1=0")
                m_processTimeColumnName = dt.Columns(0).ColumnName
            End If
        End Sub

        Private ReadOnly Property NumLogFields() As Integer
            Get
                If m_configuration.InteractionLogFields Is Nothing Then
                    Return 1
                End If
                Return 1 + m_configuration.InteractionLogFields.Count()
            End Get
        End Property

        ''' <summary>
        ''' Store an interaction log record
        ''' </summary>
        ''' <param name="status">The status of the interaction's processing</param>
        ''' <param name="fields">Field values to log</param>
        ''' <remarks></remarks>
        Protected Overridable Sub LogInteraction(ByVal status As IItemStatus, ByVal ParamArray fields As String())
            If m_logInteractions Then
                Dim fieldsLength As Integer = 0
                If fields IsNot Nothing Then
                    fieldsLength = fields.Length
                End If
                If fieldsLength <> NumLogFields Then
                    Throw New Exception("Call log record size mismatch")
                End If
                SyncLock m_interactionLogRecords
                    Dim row As DataRow = m_interactionLogRecords.NewRow()
                    row(0) = DateTime.UtcNow()
                    row(1) = status.ToLogString()
                    For i As Integer = 0 To fieldsLength - 1
                        row(i + 2) = fields(i)
                    Next
                    m_interactionLogRecords.Rows.Add(row)
                End SyncLock
            End If
        End Sub
    End Class

    ''' <summary>
    ''' SpeechMiner monitor for SpeechMiner systems from version 8.1 and up
    ''' </summary>
    ''' <remarks></remarks>
    Public Class SpeechMinerMonitor81
        Inherits SpeechMinerMonitor

        ''' <summary>
        ''' Create a new SpeechMiner monitor
        ''' </summary>
        ''' <param name="configuration">The UConnector configuartion</param>
        ''' <param name="db">The SpeechMiner database</param>
        ''' <param name="folderManager">The folder manager</param>
        ''' <param name="dataManager">The data manager</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfig, ByVal db As utopy.bDBServices.DBServices, ByVal folderManager As IFolderManager, ByVal dataManager As IDataManager)
            MyBase.New(configuration, db, folderManager, dataManager)
        End Sub

        ''' <summary>
        ''' Logs the UConnector's state to the database.
        ''' </summary>
        ''' <param name="status">The new UConnector state</param>
        ''' <param name="description">A textual description of the state</param>
        ''' <remarks></remarks>
        Public Overrides Sub UpdateStatus(ByVal status As IImporter.Status, ByVal description As String)
            Dim nameParam As New SqlClient.SqlParameter("@name", m_configuration.Name)
            Dim computerParam As New SqlClient.SqlParameter("@computer", ComputerName)
            Dim systemParam As New SqlClient.SqlParameter("@system", m_configuration.ImporterType)
            Dim statusParam As New SqlClient.SqlParameter("@status", description)
            m_db.runSQL("IF EXISTS(SELECT * FROM monitorConnectorsTbl WHERE name=@name AND computer=@computer)" & vbCrLf & _
                        "  UPDATE monitorConnectorsTbl SET status=@status,lifesign=" & m_db.getDBTime() & " WHERE name=@name AND computer=@computer" & vbCrLf & _
                        "ELSE" & vbCrLf & _
                        "  INSERT INTO monitorConnectorsTbl VALUES(@name, @computer, @system, @status, 1, 0, 0, 0, null, " & m_db.getDBTime() & ")", _
                        New SqlClient.SqlParameter() {nameParam, computerParam, systemParam, statusParam})
        End Sub

        ''' <summary>
        ''' Store the handled interactions in the database
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overrides Sub UpdateInteractionsStatus()
            Dim nameParam As New SqlClient.SqlParameter("@name", m_configuration.Name)
            Dim computerParam As New SqlClient.SqlParameter("@computer", ComputerName)
            m_db.runSQL("UPDATE monitorConnectorsTbl SET handledCalls=" & NumberOfHandledInteractions & ", waitingCalls=" & NumberOfWaitingInteractions & _
                        ", freeSpace=" & FreeSpace * 1024 & ", lifesign=" & m_db.getDBTime() & " WHERE name=@name AND computer=@computer", _
                        New SqlClient.SqlParameter() {nameParam, computerParam})
            MyBase.UpdateInteractionsStatus()
        End Sub
    End Class

    ''' <summary>
    ''' SpeechMiner monitor for SpeechMiner systems from version 6.0 to 8.0
    ''' </summary>
    ''' <remarks></remarks>
    Public Class SpeechMinerMonitor60
        Inherits SpeechMinerMonitor

        ''' <summary>
        ''' Create a new SpeechMiner monitor
        ''' </summary>
        ''' <param name="configuration">The UConnector configuartion</param>
        ''' <param name="db">The SpeechMiner database</param>
        ''' <param name="folderManager">The folder manager</param>
        ''' <param name="dataManager">The data manager</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfig, ByVal db As utopy.bDBServices.DBServices, ByVal folderManager As IFolderManager, ByVal dataManager As IDataManager)
            MyBase.New(configuration, db, folderManager, dataManager)
        End Sub

        ''' <summary>
        ''' Store the handled interactions in the database
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overrides Sub UpdateInteractionsStatus()
            Dim computerParam As New SqlClient.SqlParameter("@computer", ComputerName)
            Dim systemParam As New SqlClient.SqlParameter("@system", m_configuration.Name)
            m_db.runSQL("delete from monitorConnectorsTbl where computer=@computer and system=@system " & _
                        "insert into monitorConnectorsTbl values(@system, @computer, " & NumberOfHandledInteractions & ", " & NumberOfWaitingInteractions & ", " & _
                        FreeSpace * 1024 & "," & m_db.getDBTime() & ")", _
                        New SqlClient.SqlParameter() {systemParam, computerParam})
            MyBase.UpdateInteractionsStatus()
        End Sub
    End Class
End Namespace
﻿Imports QBEUConnector.Configuration

Namespace Main
    ''' <summary>
    ''' Configuration for the main importer process
    ''' </summary>
    ''' <remarks></remarks>
    Public Class ImporterConfig
        Inherits ConfigSection

        ''' <summary>
        ''' Tag for the importer process section. Value: Importer
        ''' </summary>
        ''' <remarks></remarks>
        Private Const XML_SETTINGS_IMPORTER_SECTION_NAME As String = "Importer"

        ''' <summary>
        ''' Tag for the logical name for the SpeechMiner database connection string. Value: SpeechMinerConnectionStringName
        ''' </summary>
        ''' <remarks>The connection string itself should be defined in the .config file.</remarks>
        Private Const XML_SETTINGS_SPEECHMINER_CONNECTION_STRING_NAME As String = "SpeechMinerConnectionStringName"

        ''' <summary>
        ''' Tag for the time of day to start running interactions import. Value: ImportStartTime
        ''' </summary>
        ''' <remarks><para>Interactions will only be imported between <c>ImportStartTime</c> and <c>ImportEndTime</c>.</para>
        ''' <para>The time is in local time.</para></remarks>
        Private Const XML_SETTINGS_START_TIME As String = "ImportStartTime"

        ''' <summary>
        ''' Tag for the time of day to stop running interactions import. Value: ImportEndTime
        ''' </summary>
        ''' <remarks><para>Interactions will only be imported between <c>ImportStartTime</c> and <c>ImportEndTime</c>.</para>
        ''' <para>The time is in local time.</para></remarks>
        Private Const XML_SETTINGS_END_TIME As String = "ImportEndTime"

        ''' <summary>
        ''' Tag for the interval at which to check and import the next batch of interactions. Value: BatchInterval
        ''' </summary>
        ''' <remarks><para>This interval is only used after there are no interactions to import. 
        ''' While there are still interactions to import, the UConnector works continuously.</para>
        ''' <para>The value is in seconds.</para></remarks>
        Private Const XML_SETTINGS_BATCH_INTERVAL As String = "BatchInterval"

        ''' <summary>
        ''' Tag for the number of interactions to include in each batch. Value: BatchSize
        ''' </summary>
        ''' <remarks></remarks>
        Private Const XML_SETTINGS_BATCH_SIZE As String = "BatchSize"

        ''' <summary>
        ''' Tag for how often to restart the UConnector service. Value: RestartInterval
        ''' </summary>
        ''' <remarks>Only relevant when the UConnector is running as a service.</remarks>
        Private Const XML_SETTINGS_RESTART_INTERVAL As String = "RestartInterval"

        ''' <summary>
        ''' Create a new importer configuration object
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection)
            MyBase.New(configuration, XML_SETTINGS_IMPORTER_SECTION_NAME)
        End Sub

        ''' <summary>
        ''' The logical name for the SpeechMiner database connection string
        ''' <seealso cref="XML_SETTINGS_SPEECHMINER_CONNECTION_STRING_NAME"/>
        ''' </summary>
        ''' <value>The connection string name</value>
        ''' <returns>The connection string name</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property SpeechMinerDBName() As String
            Get
                Return Me(XML_SETTINGS_SPEECHMINER_CONNECTION_STRING_NAME)
            End Get
        End Property

        ''' <summary>
        ''' The time of day to start running interactions import
        ''' <seealso cref="XML_SETTINGS_START_TIME"/>
        ''' </summary>
        ''' <value>The local time to start importing</value>
        ''' <returns><c>TimeSpan</c> representing the time since the local start of day. <c>TimeSpan.MinValue</c> signifies that import runs throughout the day.</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property ImportStartTime() As TimeSpan
            Get
                Dim ret As TimeSpan
                If TimeSpan.TryParse(Me(XML_SETTINGS_START_TIME), ret) Then
                    Return ret
                Else
                    Return TimeSpan.MinValue
                End If
            End Get
        End Property

        ''' <summary>
        ''' The time of day to end running interactions import
        ''' <seealso cref="XML_SETTINGS_END_TIME"/>
        ''' </summary>
        ''' <value>The local time to end importing</value>
        ''' <returns><c>TimeSpan</c> representing the time since the local start of day. <c>TimeSpan.MinValue</c> signifies that import runs throughout the day.</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property ImportEndTime() As TimeSpan
            Get
                Dim ret As TimeSpan
                If TimeSpan.TryParse(Me(XML_SETTINGS_END_TIME), ret) Then
                    Return ret
                Else
                    Return TimeSpan.MinValue
                End If
            End Get
        End Property

        ''' <summary>
        ''' The interval at which to check and import the next batch of interactions.
        ''' <seealso cref="XML_SETTINGS_BATCH_INTERVAL"/>
        ''' </summary>
        ''' <value>The batch interval</value>
        ''' <returns>The batch interval</returns>
        ''' <remarks>This interval is only used after there are no interactions to import. 
        ''' While there are still interactions to import, the UConnector works continuously.</remarks>
        Public Overridable ReadOnly Property BatchInterval() As TimeSpan
            Get
                If Me(XML_SETTINGS_BATCH_INTERVAL) Is Nothing Then
                    Return DefaultBatchInterval
                Else
                    Return TimeSpan.FromSeconds(CDbl(Me(XML_SETTINGS_BATCH_INTERVAL)))
                End If
            End Get
        End Property

        ''' <summary>
        ''' The default interval at which to check and import the next batch of interactions.
        ''' </summary>
        ''' <value>The default batch interval</value>
        ''' <returns>5 minutes</returns>
        ''' <remarks></remarks>
        Protected Overridable ReadOnly Property DefaultBatchInterval() As TimeSpan
            Get
                Return TimeSpan.FromMinutes(5)
            End Get
        End Property

        ''' <summary>
        ''' The number of interactions to include in each batch.
        ''' <seealso cref="XML_SETTINGS_BATCH_SIZE"/>
        ''' </summary>
        ''' <value>Batch size</value>
        ''' <returns>Batch size</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property BatchSize() As Integer
            Get
                If Me(XML_SETTINGS_BATCH_SIZE) Is Nothing Then
                    Return DefaultBatchSize
                Else
                    Return CInt(Me(XML_SETTINGS_BATCH_SIZE))
                End If
            End Get
        End Property

        ''' <summary>
        ''' The default number of interactions to include in each batch.
        ''' </summary>
        ''' <value>The default batch size</value>
        ''' <returns>30</returns>
        ''' <remarks></remarks>
        Protected Overridable ReadOnly Property DefaultBatchSize() As Integer
            Get
                Return 30
            End Get
        End Property

        ''' <summary>
        ''' The default number of maximum segments in a single interaction.
        ''' </summary>
        ''' <value>The default maximum number of segments</value>
        ''' <returns>10</returns>
        ''' <remarks></remarks>
        Protected Overridable ReadOnly Property DefaultMaximumSegmentsPerInteraction() As Integer
            Get
                Return 10
            End Get
        End Property

        ''' <summary>
        ''' How often to restart the UConnector service.
        ''' <seealso cref="XML_SETTINGS_RESTART_INTERVAL"/>
        ''' </summary>
        ''' <value>The restart interval</value>
        ''' <returns>The restart interval</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property RestartInterval() As TimeSpan
            Get
                If String.IsNullOrEmpty(Me(XML_SETTINGS_RESTART_INTERVAL)) Then
                    Return TimeSpan.Zero
                End If
                Return TimeSpan.FromHours(CDbl(Me(XML_SETTINGS_RESTART_INTERVAL)))
            End Get
        End Property

    End Class
End Namespace
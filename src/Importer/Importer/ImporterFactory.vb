﻿Imports System.ComponentModel
Imports QBEUConnector.Configuration
Imports QBEUConnector.Main

''' <summary>
''' Factory to create the importer for the UConnector
''' </summary>
''' <remarks></remarks>
Public Module ImporterFactory

    ''' <summary>
    ''' Creates a new importer
    ''' </summary>
    ''' <param name="importerType">Fully qualified class name for the importer</param>
    ''' <param name="config">The UConnector configuration</param>
    ''' <returns>The importer</returns>
    ''' <remarks>The class must implement <see cref="IImporter"/> and have a constructor that accepts <see cref="IConfig"/>.
    ''' Old importers, written before the new framework, are created elsewhere.</remarks>
    Public Function CreateImporter(
        importerType As String,
        config As IConfig
        ) As IImporter

        Dim assemblyType = Type.GetType(importerType)
        If assemblyType Is Nothing Then
            Throw New TypeLoadException("Assembly for importer " & importerType & " not found")
        End If

        Return CreateImporter(assemblyType, config)
    End Function

    Public Function CreateImporter(
        importerType As Type,
        config As IConfig
        ) As IImporter

        If importerType Is Nothing Then
            Throw New ArgumentNullException("importerType")
        End If

        Dim ci = importerType.GetConstructor(New Type() {GetType(IConfig)})
        If ci Is Nothing Then
            ' Old importer
            Return Nothing
        End If

        Return CType(ci.Invoke(New Object() {config}), IImporter)
    End Function

End Module
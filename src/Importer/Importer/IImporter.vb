﻿Imports QBEUConnector.Data

Namespace Main
    ''' <summary>
    ''' The importer is the main class that kicks off everything done in the UConnector
    ''' </summary>
    ''' <remarks></remarks>
    Public Interface IImporter
        Inherits IDisposable

        ''' <summary>
        ''' Do any initialization required before the importer starts working
        ''' </summary>
        ''' <remarks></remarks>
        Sub Init()
        ''' <summary>
        ''' Called when the process stops running
        ''' </summary>
        ''' <remarks></remarks>
        Sub Quit()
        ''' <summary>
        ''' Start running the importer to import interactions
        ''' </summary>
        ''' <remarks></remarks>
        Sub Start()
        ''' <summary>
        ''' Stop running the importer to import interactions
        ''' </summary>
        ''' <remarks></remarks>
        Sub Cancel()

        ''' <summary>
        ''' Raised when the importer changes its state
        ''' </summary>
        ''' <param name="status">The new state of the importer</param>
        ''' <param name="description">A textual description of the state</param>
        ''' <remarks></remarks>
        Event StatusChanged(ByVal status As Status, ByVal description As String)
        ''' <summary>
        ''' Raised when an interaction is handled
        ''' </summary>
        ''' <param name="interaction">The data about the interaction</param>
        ''' <param name="status">How the interaction was completed (success/failure and failure information)</param>
        ''' <remarks></remarks>
        Event InteractionHandled(ByVal interaction As IInteractionData, ByVal status As IItemStatus)

        ''' <summary>
        ''' Get the next batch of interactions, and imports them
        ''' </summary>
        ''' <returns>True if there were any interactions to import</returns>
        ''' <remarks></remarks>
        Function ImportNextBatch() As Boolean

        ''' <summary>
        ''' Whether or not importer should be periodically restarted
        ''' </summary>
        ''' <value>True if periodic restart needed</value>
        ''' <returns>True if periodic restart needed</returns>
        ''' <remarks></remarks>
        ReadOnly Property DoRestart() As Boolean
        ''' <summary>
        ''' How often to restart the UConnector service
        ''' </summary>
        ''' <value>The interval between restarts</value>
        ''' <returns>The interval between restarts</returns>
        ''' <remarks>Only relevant when running as a service</remarks>
        ReadOnly Property RestartInterval() As TimeSpan

        ''' <summary>
        ''' Update the agent hierarchy
        ''' </summary>
        ''' <remarks>Called only when user explicitly asks for an update (via the application)</remarks>
        Sub UpdateAgents()

        ''' <summary>
        ''' State of the importer
        ''' </summary>
        ''' <remarks></remarks>
        Enum Status As Integer
            OTHER = 0
            STARTING
            FETCHING
            STOPPED
            IDLE
            [ERROR]
        End Enum
    End Interface
End Namespace
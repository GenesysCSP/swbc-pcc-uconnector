﻿using System;
using System.Collections.Generic;
using System.Data;
using PureConnect.I3Audio;
using utopy.bDBServices;
using GenUtils.TraceTopic;
using ININ.IceLib.Data.TransactionBuilder;
using System.Collections;
using System.Linq;
using System.Text.RegularExpressions;

namespace PureConnect.Data
{
    /// <summary>
    /// Class to process the InteractionSummary db query
    /// </summary>
    internal class InteractionSummaryQueryData : BaseQueryData
    {

        public InteractionSummaryQueryData()
        {
        }

        /// <summary>
        /// </summary>
        /// <param name="m_selectMetaQuery"></param>
        /// <param name="m_lastImportedDate"></param>
        /// <param name="batchSize"></param>
        /// <param name="sInteractionIds"></param>
        /// <param name="m_callMetaFields"></param>
        /// <param name="calls"></param>
        /// <returns></returns>
        public bool GetData(string interactionIds, Dictionary<string, Dictionary<string, string>> m_allMetaFields, Dictionary<string, CallData> calls)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    TraceTopic.pureConnectTopic.note("Interaction Summary Details for the Interaction Ids: {}", interactionIds);

                    var session = CICSession.Instance.GetSession();

                    var transactionData = new TransactionData { ProcedureName = "PCC_UC_Select_InteractionSummary" };

                    var sqlParamInteractionIds = new TransactionParameter
                    {
                        Value = interactionIds,
                        Type = ParameterType.In,
                        DataType = ParameterDataType.String
                    };

                    transactionData.Parameters.Add(sqlParamInteractionIds);

                    DataTable dtInteractionSummary = GetData(session, transactionData);

                    //Getting data from the NexidiaCustomMD table
                  //  var transactionDataNexidia = new TransactionData { ProcedureName = "PCC_UC_Select_NexidiaCustomMD" };

                   /* var sqlParamInteractionIdsNexidia = new TransactionParameter
                    {
                        Value = interactionIds,
                        Type = ParameterType.In,
                        DataType = ParameterDataType.String
                    };

                    transactionDataNexidia.Parameters.Add(sqlParamInteractionIdsNexidia);

                    DataTable dtNexidiaCustomMD = GetData(session, transactionDataNexidia);*/

                    DataRowCollection rows = dtInteractionSummary.Rows;
                    foreach (DataRow row in rows)
                    {
                        string callId = (string)row["InteractionIDKey"];
                        if (!calls.ContainsKey(callId))
                        {
                            TraceTopic.pureConnectTopic.note("Recording with QueueObjectIdKey {} was not found in list.", callId);
                            continue;
                        }

                        CallData call = calls[callId];

                        int StartTimeDSTOffset = Convert.ToInt32(row["StartDTOffset"]);
                        if (StartTimeDSTOffset != 0)
                        {
                            //Log.Debug("DST OffSet");
                        }
                        Dictionary<string, string> m_metaFields = m_allMetaFields[call[Constants.RecordingInteractionType]];

                        foreach (KeyValuePair<string, string> field in m_metaFields)
                        {
                            try
                            {
                                //Filter out the fields that are not for this table
                                string key = field.Key;
                                if (row.Table.Columns.Contains(key) && !row.IsNull(key))
                                {
                                    switch (key)
                                    {
                                        // Get the DateTime without milliseconds and also adjust DST
                                        case "InitiatedDateTimeUTC":
                                        case "ConnectedDateTimeUTC":
                                        case "TerminatedDateTimeUTC":
                                            call[key] = GetDateTimeNoMilliSeconds(row, key, StartTimeDSTOffset, "yyyy-MM-dd HH:mm:ss");
                                            break;

                                        default:
                                            call[key] = GetColumnStrValue(row, key);
                                            break;
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                TraceTopic.pureConnectTopic.error("Error Adding MetaData: " + field.Key, e);
                            }
                        }

                        call[CallData.AGENT_GROUP] = GetColumnStrValue(row, CallData.AGENT_GROUP);
                        //int duration = CalculateDuration(row);

                        //Overwrite the values from the nexidia table
                        /*if (call[Constants.RecordingInteractionType].ToLower() == "voice")
                        {
                            if (dtNexidiaCustomMD.Rows.Count == 0 || dtNexidiaCustomMD.Columns.Count ==0 || !dtNexidiaCustomMD.Columns.Contains("CallID"))
                                continue;

                            dtNexidiaCustomMD.PrimaryKey = new DataColumn[] { dtNexidiaCustomMD.Columns["CallID"] };
                            DataRow nexidiaRow = dtNexidiaCustomMD.Rows.Find(new object[] { callId });
                            if (nexidiaRow == null)
                                continue;

                            IEnumerable<string> metaDataName = nexidiaRow["MetaDataName"].ToString().Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                            IEnumerable<string> metaDataValue = nexidiaRow["MetaDataValue"].ToString().Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries).ToList();

                            Dictionary<string, string> metaData = metaDataName.Zip(metaDataValue, (name, value) => new { name, value }).ToDictionary(x => Regex.Replace(x.name, @"\s", "").ToLower(), x => x.value);

                            foreach (KeyValuePair<string, string> field in m_metaFields)
                            {
                                try
                                {
                                    //Filter out the fields that are not for this table
                                    string key = field.Key;
                                    if (metaData.ContainsKey(key.ToLower()))
                                    {
                                        switch (key)
                                        {
                                            // Get the DateTime without milliseconds and also adjust DST
                                            case "InitiatedDateTimeUTC":
                                            case "ConnectedDateTimeUTC":
                                            case "TerminatedDateTimeUTC":
                                                DateTime date = DateTime.Parse(metaData[key.ToLower()].ToString());
                                                if (StartTimeDSTOffset != 0)
                                                {
                                                    date = date.AddSeconds(StartTimeDSTOffset);
                                                }
                                                call[key] = date.ToString("yyyy-MM-dd HH:mm:ss");
                                                break;

                                            default:
                                                call[key] = metaData[key.ToLower()].ToString();
                                                break;
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    TraceTopic.pureConnectTopic.error("Error Adding MetaData: " + field.Key, e);
                                }
                            }
                        }*/
                    }
                }
                catch (Exception ex)
                {
                    TraceTopic.pureConnectTopic.exception(ex);
                }
            }
            return true;
        }

        /// <summary>
        /// NOT USED, now do this in the SQL statement
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        private int CalculateDuration(DataRow row)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    TraceTopic.pureConnectTopic.note("Calculate Duration");
                    Decimal duration = 0;
                    duration += GetDecimal(row, "tConnected");
                    duration += GetDecimal(row, "tHold");
                    duration += GetDecimal(row, "tSuspend");
                    duration += GetDecimal(row, "tConference");
                    return Convert.ToInt32(duration);
                }
                catch (Exception ex)
                {
                    TraceTopic.pureConnectTopic.exception(ex);
                    return -1;
                }
            }
        }

        private decimal GetDecimal(DataRow row, string key)
        {
            decimal res = 0;
            try
            {
                if (row.Table.Columns.Contains(key))
                {
                    res = (decimal)row[key];
                }
            }
            catch (Exception)
            {

            }
            return res;
        }
    }
}
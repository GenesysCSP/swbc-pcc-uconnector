﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace PureConnect.Data
{
    /// <summary>
    /// A few constants!!
    /// </summary>
    public class Constants
    {
        public const string HA_BACKUP_ROLE = "2";
        public const string HA_COMMENT_DICE = "throwing dice";
        public const string HA_COMMENT_HEARTBEAT = "heartbeat";
        public const string HA_COMMENT_TIMESTAMP = "timestamp";
        public const string HA_PRIMARY_ROLE = "1";
        public const string HA_PRIMARY_SITE = "1";
        public const string HA_SECONDARY_SITE = "2";
        public const string ISO_8601_FORMAT = "yyyy-MM-ddTHH:mm:ss.fff";
        public const string METADATA_TIME_FORMAT = "yyyy-MM-ddTHH:mm:ss";
        public const string SYNC_TABLE = "PURECONNECT_UCONNECTOR";
        public const string XML_SETTINGS_AGENT_FILENAME_PATTERN = "AgentFilenamePattern";
        public const string XML_SETTINGS_AGENT_FILES_FOLDER = "AgentFilesFolder";
        public const string XML_SETTINGS_AUDIO_FETCH_TEMP_PATH = "AudioFetchingTempPath";
        public const string XML_SETTINGS_CALL_METAFIELDS = "CallMetaFields";
        public const string XML_SETTINGS_CHAT_METAFIELDS = "ChatMetaFields";
        public const string XML_SETTINGS_EMAIL_METAFIELDS = "EmailMetaFields";
        public const string XML_SETTINGS_SURVEY_METAFIELDS = "SurveyMetaFields";
        public const string XML_SETTINGS_TIMEOUT = "Timeout";
        public const string XML_SETTINGS_RETRYCOUNT = "RetryCount";

        public const string XML_SETTINGS_DEBUG_FAKE_DOWNLOAD = "DebugFakeDownload";
        public const string XML_SETTINGS_BACKUP_XML = "BackupXML";
        public const string XML_SETTINGS_DELAY_BETWEEN_AUDIO_DOWNLOADS_MS = "DelayBetweenAudioDownloadsMs";
        public const string XML_SETTINGS_FFMPEG_ARGS = "ffmpegArgs";

        public const string XML_SETTINGS_HA_ROLE = "HARole";
        public const string XML_SETTINGS_HA_SITE = "HASite";
        public const string XML_SETTINGS_HEARTBEAT_INTERVAL_SECS = "HeartbeatIntervalSecs";

        public const string XML_SETTINGS_I3_HOST = "I3Host";
        public const string XML_SETTINGS_I3_PASSWORD = "I3Password";
        public const string XML_SETTINGS_I3_USER_NAME = "I3UseName";
        public const string XML_SETTINGS_RECORDING_TAG = "RecordingTag";


        public const string XML_SETTINGS_LAST_IMPORTED_DATE = "LastImportedDate";
        public const string XML_SETTINGS_MAX_ATTEMPTS_TO_DOWNLOAD_AUDIO = "MaxAttemptsToDownloadAudio";
        public const string XML_SETTINGS_RESET_LAST_IMPORTED_DATE = "ResetLastImportedDate";
        public const string XML_SETTINGS_SLEEP_TIME_AFTER_AUDIO_DOWNLOAD_FAILURE_IN_MINUTES = "SleepTimeAfterAudioDownloadFailureInMinutes";

        //public const string XML_SETTINGS_MSSQL = "Mssql";
        public const string XML_SETTINGS_HA_CONNECTION_STRING_NAME = "HaDataConnectionStringName";
        public const string XML_SETTINGS_I3IC_CONNECTION_STRING_NAME = "I3_ICMetaDataConnectionStringName";
        //public const string XML_SETTINGS_CIC_PRIMARY_CONNECTION_STRING_NAME = "CICPrimaryDataConnectionStringName";
        //public const string XML_SETTINGS_CIC_BACKUP_CONNECTION_STRING_NAME = "CICBackupDataConnectionStringName";
        public const string XML_SETTINGS_AGENTS = "Agent";
        public const string XML_SETTINGS_AGENTS_HIERARCHY = "hierarchy";

        public const string SpeechMinerDataSource = "SpeechMinerDataSource";
        public const string DeleteElectionRecordSproc = "PCC_UC_DeleteElectionRecord";
        public const string DeleteLastHandledRecordsSproc = "PCC_UC_DeleteLastHandledRecords";
        public const string GetLastHandledInteractionSproc = "PCC_UC_GetLastHandledInteraction";
        public const string GetRecentHeartbeatRecordsSproc = "PCC_UC_GetRecentHeartbeatRecords";
        public const string InsertElectionRecordSproc = "PCC_UC_InsertElectionRecord";
        public const string ManageRecordSproc = "PCC_UC_ManageRecord";
        public const string SelectOtherElectionRecordsSproc = "PCC_UC_SelectOtherElectionRecords";

        public const string FromConnValue = "FromConnValue";
        public const string ToConnValue = "ToConnValue";

        public const string CreateTemporaryTableSproc = "pcc_uc_create_temp_table";
        public const string UpdateTemporaryTableSproc = "pcc_uc_update_temptable";
        public const string UpdateAgentHierarchyTblSproc = "sp_updateAgentHierarchy";
        public const string DeleteTemporaryTableSproc = "pcc_uc_delete_temp_table";
        public const string UpdateAgentEntityTblSproc = "pcc_uc_update_agentHierarchy";
        public const string RollbackChangesSproc = "pcc_uc_rollback_changes";

        public const string XML_SETTINGS_SUPERVISOR = "supervisor";
        public const string XML_SETTINGS_CUSTOM_ATTRIBUTE = "customAttribute";

        public const string DateTimeFormat = "yyyy-MM-dd HH:mm:ss";

        public const string RecordingInteractionType = "RecordingInteractionType";
        public const string Chat = "Chat";
        public const string EMail = "EMail";
        public const string Voice = "Voice";
        public const string Survey = "Survey";
        public const string Language = "Language";

        public const string EMLExtension = ".eml";
        public const string TXTExtension = ".txt";

        public const string XML_SETTINGS_DEFAULT_PROGRAMID = "Program";
        public const string XML_SETTINGS_DELAY = "Delay";
        public const string XML_SETTINGS_SPANISH_PROGRAMID = "WorkgroupProgram";
        public const string XML_SETTINGS_VOICE_CALL_PROGRAMID = "VoiceCallProgram";
        public const string XML_SETTINGS_CHAT_PROGRAMID = "ChatProgram";
        public const string XML_SETTINGS_EMAIL_PROGRAMID = "EmailProgram";
        public const string XML_SETTINGS_WORKGROUP_MAPPING = "WorkGroupMapping";
        public const string XML_SETTINGS_PROGRAM = "ProgramName";
        public const string XML_SETTINGS_WORKGROUP_PROGRAMID = "WorkgroupProgram";
        public const string XML_SETTINGS_DEFAULT_WORKGROUP_PROGRAM = "DefaultWorkgroupProgram";
        public const string XML_SETTINGS_SURVEY_AUDIO_FILE_PATH = "SurveyAudioFilePath";

        public const string XML_SETTINGS_DOWNLOAD_THRESHOLD = "DownloadThresholdTimeInMilliseconds";
        public const string XML_SETTINGS_TRANSCODE_THRESHOLD = "TranscodeThresholdTimeInMilliseconds";
        public const string XML_SETTINGS_TRANSCODE_FOLDER_MONITOR_TIME = "TranscodeFolderMonitorIntervalInHours";
        public const string XML_SETTINGS_TRANSCODE_FOLDER_LIFE_TIME = "TranscodeFolderFileLifeInDays";
        public const string XML_SETTINGS_TRANSCODE_FOLDER_FILE_COUNT = "TranscodeFolderFileCount";

        public const string ERROR_SOURCE = "UConnector";
    }
}

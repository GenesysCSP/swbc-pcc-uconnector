﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ININ.IceLib.Connection;
using ININ.IceLib.Data.TransactionBuilder;
using GenUtils.TraceTopic;
using System.Data.SqlClient;
using PureConnect.I3Audio;
using System.Diagnostics;

namespace PureConnect.Data
{
    public class BaseQueryData
    {

        public static string GetColumnStrValue(DataRow row, string column)
        {
            return row.IsNull(column) ? "" : row[column].ToString();
        }

        /// <summary>
        /// Get a DateTime. Add the DSTOffset (in Seconds)
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <param name="DSTOffset"></param>
        /// <returns></returns>
        public static string GetDateTimeNoMilliSeconds(DataRow row, string column, int DSTOffset, string format = null)
        {
            if (row.IsNull(column))
            {
                return "";
            }
            DateTime date = DateTime.Parse(row[column].ToString());
            if (DSTOffset != 0)
            {
                date = date.AddSeconds(DSTOffset);
            }
            if (format == null)
            {
                return date.ToString("yyyy-MM-ddTHH:mm:ss");
            }
            else
            {
                return date.ToString(format);
            }
            //return row.IsNull(column) ? "" : ((DateTime)row[column]).ToString("yyyy-MM-ddTHH:mm:ss");
        }

        protected DataTable GetData(Session session, string sql)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                TraceTopic.pureConnectTopic.always("Intialising transactionClient");
                var transactionClient = new TransactionClient(session);
                transactionClient.TransactionSettingsTransactionScope.RowLimit = 100000;
                transactionClient.TransactionSettingsGlobalScope.IncludeParameterMetadata = true;
                transactionClient.TransactionSettingsTransactionScope.IncludeRowSetMetadata = true;
                transactionClient.TransactionSettingsTransactionScope.Timeout = CICSession.Instance.Timeout;

                var transactionData = new TransactionData { ProcedureName = "SP_SQLEXEC" };

                var sqlQuery = new TransactionParameter
                {
                    Value = sql,
                    Type = ParameterType.In,
                    DataType = ParameterDataType.String
                };

                transactionData.Parameters.Add(sqlQuery);

                try
                {
                    if (transactionClient.Execute(ref transactionData))
                    {
                        if (transactionData.ResultSet.Tables.Count != 2)
                        {
                            TraceTopic.pureConnectTopic.error(
                                $"The TransactionBuilder result set does not contain 2 tables, count = {transactionData.ResultSet.Tables.Count}");
                            return new DataTable();
                        }
                        return transactionData.ResultSet.Tables[1];
                    }
                    TraceTopic.pureConnectTopic.error($"Error when running stored procedure {"SP_SQLEXEC"}: {transactionData}");
                    TraceTopic.pureConnectTopic.warning(transactionData.WarningMessage);
                    TraceTopic.pureConnectTopic.error(transactionClient.LastError);
                    return new DataTable();
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.error(e.Message);
                    return new DataTable();
                }
            }
        }

        protected DataTable GetData(Session session, TransactionData transactionData, string dataSource = "", bool includeRowSetMetadata = true, bool throwError = false)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                TraceTopic.pureConnectTopic.always("Intialising transactionClient");
                var transactionClient = new TransactionClient(session);
                transactionClient.TransactionSettingsTransactionScope.RowLimit = 100000;
                transactionClient.TransactionSettingsGlobalScope.IncludeParameterMetadata = true;
                transactionClient.TransactionSettingsTransactionScope.Timeout = CICSession.Instance.Timeout;
                int fetchCount = 1;

                if (includeRowSetMetadata)
                {
                    transactionClient.TransactionSettingsTransactionScope.IncludeRowSetMetadata = includeRowSetMetadata;
                }
                transactionClient.TransactionSettingsGlobalScope.ReadOnly = false;
                if (!string.IsNullOrWhiteSpace(dataSource))
                {
                    transactionClient.TransactionSettingsGlobalScope.ConnectionInformation = dataSource;
                }
                try
                {
                    while(fetchCount <= CICSession.Instance.RetryCount)
                    {
                        TraceTopic.pureConnectTopic.note("Executing the DB call ExecutionCycle: {}", fetchCount);
                        if (transactionClient.Execute(ref transactionData))
                        {
                            if (transactionData.ResultSet.Tables.Count == 2)
                            {
                                return transactionData.ResultSet.Tables[1];
                            }
                            else
                            {
                                TraceTopic.pureConnectTopic.error(
                                    "The TransactionBuilder result set does not contain 2 tables, count = {}", transactionData.ResultSet.Tables.Count);
                                //EventLog.WriteEntry("UConnector", string.Format("The TransactionBuilder result set does not contain 2 tables, count = {0}, SP = {1}", transactionData.ResultSet.Tables.Count, transactionData.ProcedureName)
                                //    , EventLogEntryType.Warning, 8001);
                                //return new DataTable();
                            }
                        }
                        else
                        {
                            TraceTopic.pureConnectTopic.error("The Execution of the SQL statement failed for SP {}", transactionData.ResultSet.Tables.Count);
                            //EventLog.WriteEntry("UConnector", string.Format("The Execution of the SQL statement failed for SP {0}", transactionData.ProcedureName)
                            //         , EventLogEntryType.Warning, 8001);
                        }
                        fetchCount++;
                    }
                    TraceTopic.pureConnectTopic.error("Error when running stored procedure {}: {}", transactionData.ProcedureName, transactionData);
                    TraceTopic.pureConnectTopic.warning(transactionData.WarningMessage);
                    TraceTopic.pureConnectTopic.error(transactionClient.LastError);
                    if (throwError)
                    {
                        throw new Exception(transactionData.WarningMessage);
                    }
                    return new DataTable();
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.error(e.Message);
                    if (throwError)
                    {
                        throw;
                    }
                    return new DataTable();
                }
            }
        }
        protected DataTable GetDataSql(string procedure, string connectionString, Dictionary<string,List<string>> parameters)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(procedure, con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = CICSession.Instance.Timeout;

            if (parameters != null)
            {
                SqlParameter param;

                foreach(var p in parameters)
                {
                    if(p.Value.Count == 2)
                    {
                        param = cmd.Parameters.Add(p.Key, GetType(p.Value[1]));
                    }
                    else
                    {
                        param = cmd.Parameters.Add(p.Key, GetType(p.Value[1]), Convert.ToInt32(p.Value[2]));
                    }
                    
                    param.Value = p.Value[0];
                }   
            }

            DataTable table = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);

            con.Open();
            adapter.Fill(table);
            con.Close();

            return table;

        }

        protected DataTable GetDataSql(string procedure, Dictionary<string, List<string>> parameters, SqlConnection con, SqlTransaction transaction)
        {
            SqlCommand cmd = new SqlCommand(procedure, con);
            cmd.Transaction = transaction;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = CICSession.Instance.Timeout;

            if (parameters != null)
            {
                SqlParameter param;

                foreach (var p in parameters)
                {
                    if (p.Value.Count == 2)
                    {
                        param = cmd.Parameters.Add(p.Key, GetType(p.Value[1]));
                    }
                    else
                    {
                        param = cmd.Parameters.Add(p.Key, GetType(p.Value[1]), Convert.ToInt32(p.Value[2]));
                    }

                    param.Value = p.Value[0];
                }
            }

            DataTable table = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            
            adapter.Fill(table);

            return table;

        }

        protected SqlDbType GetType(string type)
        {
            switch(type)
            {
                case "int":
                    return SqlDbType.Int;
                case "varchar":
                    return SqlDbType.VarChar;
                case "datetime":
                    return SqlDbType.DateTime;
                case "bit":
                    return SqlDbType.Bit;
                case "nvarchar":
                    return SqlDbType.NVarChar;
                default:
                    return SqlDbType.Int;
            }
        }
    }
}

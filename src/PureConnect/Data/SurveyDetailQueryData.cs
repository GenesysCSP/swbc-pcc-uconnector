﻿using GenUtils.TraceTopic;

using ININ.IceLib.Data.TransactionBuilder;

using PureConnect.I3Audio;
using System.IO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PureConnect.Data
{
    /// <summary>
    /// Class to process and add Survey details
    /// </summary>
    internal class SurveyDetailQueryData : BaseQueryData
    {
        public SurveyDetailQueryData()
        {

        }

        /// <summary>
        /// Function to add the survey details to callData
        /// </summary>
        /// <param name="sInteractionIds"></param>
        /// <param name="calls"></param>
        internal void GetData(string interactionIds, Dictionary<string, CallData> calls, string surveyAudioFileLoc = "")
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    TraceTopic.pureConnectTopic.note("Survey Details for the Interaction Ids: {}", interactionIds);

                    var session = CICSession.Instance.GetSession();

                    var transactionData = new TransactionData { ProcedureName = "PCC_UC_Select_SurveyDetails" };

                    var sqlParamInteractionIds = new TransactionParameter
                    {
                        Value = interactionIds,
                        Type = ParameterType.In,
                        DataType = ParameterDataType.String
                    };

                    transactionData.Parameters.Add(sqlParamInteractionIds);

                    DataTable dtSurveyDetails = GetData(session, transactionData);

                    var surveyDetails = from surveyDetail in dtSurveyDetails.AsEnumerable()
                                        group surveyDetail by surveyDetail["CallIdKey"] into callSurveys
                                        orderby callSurveys.Key
                                        select callSurveys;


                    foreach (var surveyDetail in surveyDetails)
                    {
                        string callId = surveyDetail.Key.ToString();
                        if (!calls.ContainsKey(callId))
                        {
                            TraceTopic.pureConnectTopic.note("Recording with QueueObjectIdKey {} was not found in list.", callId);
                            continue;
                        }

                        CallData call = calls[callId];
                        var listOfSurveyForInetraction = new List<SurveyDetail>();
                        foreach (DataRow row in surveyDetail)
                        {
                            if (row["CallIdKey"].ToString() == callId)
                            {
                                var surveyInfo = new SurveyDetail()
                                {
                                    MaxScore = !row.IsNull("MaxScore") ? Convert.ToDecimal(row["MaxScore"]) : 0,
                                    Score = !row.IsNull("Score") ? Convert.ToDecimal(row["Score"]) : 0,
                                    NumericScore = !row.IsNull("NumericScore") ? Convert.ToDecimal(row["NumericScore"]) : 0,
                                    AudioPath = !row.IsNull("FreeFormAnswerURI") ? Path.Combine(surveyAudioFileLoc, row["FreeFormAnswerURI"].ToString()) : "",
                                    QuestionText = !row.IsNull("QuestionText") ? row["QuestionText"].ToString() : "",
                                };
                                listOfSurveyForInetraction.Add(surveyInfo);
                            }
                        }
                        call.AddSurveyDetails(callId, listOfSurveyForInetraction);
                    }
                }
                catch (Exception ex)
                {
                    TraceTopic.pureConnectTopic.exception(ex);
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using PureConnect.I3Audio;
using GenUtils.TraceTopic;
using ININ.IceLib.Data.TransactionBuilder;

namespace PureConnect.Data
{
    /// <summary>
    /// Class that gets a list of recordings from the Pure Connect I3_IC Database
    /// </summary>
    internal class RecordingQueryData :BaseQueryData
    {
        public Dictionary<string, CallData> calls = new Dictionary<string, CallData>();
        public ConcurrentDictionary<string, string> interactionIdstoRecordingIds = new ConcurrentDictionary<string, string>();
        public ConcurrentDictionary<string, string> recordingIdsToInteractionIds = new ConcurrentDictionary<string, string>();
        public ConcurrentDictionary<string, string> relatedRecordingIds = new ConcurrentDictionary<string, string>();

        public RecordingQueryData()
        {
        }

        public bool EndAfterThisImport { get; set; }

        public bool GetData(string lastImportedDate, int batchSize, Dictionary<string, Dictionary<string, string>> m_allMetaFields)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                TraceTopic.pureConnectTopic.note("Stored Procedure: PCC_UC_Select_Recordings");

                var session = CICSession.Instance.GetSession();

                var transactionData = new TransactionData { ProcedureName = "PCC_UC_Select_Recordings" };
                //var transactionData = new TransactionData { ProcedureName = "PCC_UC_Select_Recordings_NotProcessed" };

                var sqlParamBatchSize = new TransactionParameter
                {
                    Value = batchSize.ToString(),
                    Type = ParameterType.In,
                    DataType = ParameterDataType.String
                };
                var sqlParamLastCallTime = new TransactionParameter
                {
                    Value = lastImportedDate,
                    Type = ParameterType.In,
                    DataType = ParameterDataType.String
                };
               

                transactionData.Parameters.Add(sqlParamBatchSize);
                transactionData.Parameters.Add(sqlParamLastCallTime);

                //DataTable dtRecordings = MetadataDb.getDataTable(sql, null, 60 * 60);
                DataTable dtRecordings = GetData(session, transactionData);

                DataRowCollection rows = dtRecordings.Rows;

                TraceTopic.pureConnectTopic.verbose("Got rows: {}", rows.Count);
                if (rows.Count == 0)
                {
                    return false;
                }
                //bool bImport_Finish = false;
                if (rows.Count < batchSize)
                {
                    TraceTopic.pureConnectTopic.note("Rows Returned: " + rows.Count + " is less than BatchSize: " + batchSize + " End Batch Import when this import has finished");
                    EndAfterThisImport = true;
                }

                //Get Data from IR_RecordingMedia.
                //DateTimes have a DST Offset in Minutes.
                //StringBuilder interactionIds = new StringBuilder();

                foreach (DataRow row in rows)
                {
                    Guid recordingId = new Guid(row["RecordingId"].ToString()); // PK, not null

                    if (row.IsNull("QueueObjectIdKey"))
                    {
                        TraceTopic.pureConnectTopic.always("QueueObjectIdKey column is null for RecordingId = {}, skipping.", recordingId);
                        continue;
                    }
                    string callId = (string)row["QueueObjectIdKey"]; // nullable

                    if (string.IsNullOrEmpty(callId))
                    {
                        TraceTopic.pureConnectTopic.always("RecordingID '{}' had empty QueueObjectIdKey '{}' is already present in list, skipping.", recordingId, callId);
                        continue;
                    }

                    if (calls.ContainsKey(callId))
                    {
                        TraceTopic.pureConnectTopic.always("Recording with QueueObjectIdKey '{}' is already present in list, skipping.", callId);
                        continue;
                    }

                    if (row.IsNull("Duration"))
                    {
                        TraceTopic.pureConnectTopic.always("Duration column is null for RecordingId = {}, skipping.", recordingId);
                        continue;
                    }




                    DateTime callTime = DateTime.Parse(row["RecordingDate"].ToString()); ; // not null
                    var test = row["RecordingDate"];
                    int DSTOffset = Convert.ToInt32(row["RecordingDateOffset"]);
                    /*
                    if (DSTOffset != 0)
                    {
                        callTime = callTime.AddMinutes(DSTOffset);
                    }
                    */

                    CallData call = new CallData(callId, recordingId, callTime, DSTOffset);

                    //call[CallData.DIRECTION] = GetColumnStrValue(row, "Direction");

                    calls[callId] = call;
                    
                    if (!row.IsNull(Constants.FromConnValue))
                    {
                        call[Constants.FromConnValue] = row[Constants.FromConnValue].ToString();
                    }

                    if (!row.IsNull(Constants.ToConnValue))
                    {
                        call[Constants.ToConnValue] = row[Constants.ToConnValue].ToString();
                    }

                    if (!row.IsNull("RelatedRecordingId"))
                    {
                        Guid relatedRecordingId = new Guid(row["RecordingId"].ToString());
                        TraceTopic.pureConnectTopic.note("Added RelatedRecordingId to InteractionId: " + callId);
                        relatedRecordingIds.TryAdd(relatedRecordingId.ToString(), callId);
                        //call.RelatedRecordingId = relatedRecordingId.ToString();
                    }
                    
                    if (!row.IsNull("MediaType"))
                    {
                        var mediaType = row["MediaType"].ToString();
                        //Voice
                        if(mediaType == "0")
                        {
                            call[Constants.RecordingInteractionType] = Constants.Voice;
                        }
                        //Chat
                        else if (mediaType == "1")
                        {
                            call[Constants.RecordingInteractionType] = Constants.Chat;
                        }
                        //EMail
                        else if (mediaType == "5")
                        {
                            call[Constants.RecordingInteractionType] = Constants.EMail;
                        }
                    }
                    Dictionary<string, string> m_metaFields = m_allMetaFields[call[Constants.RecordingInteractionType]];

                    //Iterate through the CallMetaData
                    //Just pick out the fields for the table
                    foreach (KeyValuePair<string, string> field in m_metaFields)
                    {
                        try
                        {
                            string key = field.Key;
                            //Filter out the fields that are not for this table
                            if (row.Table.Columns.Contains(key) && !row.IsNull(key))
                            {
                                switch (key)
                                {
                                    case "ExpirationDate":
                                        call[key] = GetDateTimeNoMilliSeconds(row, key, DSTOffset);
                                        break;

                                    case "Duration":
                                        try
                                        {
                                            double myDuration = 0;
                                            if (!row[key].Equals("") & !row[key].Equals(null))
                                            {
                                                myDuration = Convert.ToInt32(row[key]);
                                            }
                                            if (myDuration > 0)
                                            {
                                                myDuration = myDuration / 1000.0;
                                            }
                                            call[key] = myDuration.ToString("0");
                                        }
                                        catch (FormatException e)
                                        {
                                            // the FormatException is thrown when the string text does not represent a valid integer.
                                            TraceTopic.pureConnectTopic.error("Error Adding MetaData: " + field.Key, e);
                                        }

                                        break;

                                    default:
                                        call[key] = GetColumnStrValue(row, key);
                                        break;
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            TraceTopic.pureConnectTopic.error("Error Adding MetaData: " + field.Key, e);
                        }
                    }
                    //interactionIds.Append("'" + callId + "'" + ",");
                    interactionIdstoRecordingIds.TryAdd(callId, recordingId.ToString());
                    recordingIdsToInteractionIds.TryAdd(recordingId.ToString(), callId);
                }
                return true;
            }
        }
    }
}
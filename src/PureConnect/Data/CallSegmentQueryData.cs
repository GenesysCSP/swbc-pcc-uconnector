﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using PureConnect.I3Audio;
using utopy.bDBServices;
using GenUtils.TraceTopic;
using ININ.IceLib.Data.TransactionBuilder;

namespace PureConnect.Data
{
    public class CallSegmentQueryData :BaseQueryData
    {

        public CallSegmentQueryData()
        {
        }

        public bool GetData(string sInteractionIds, Dictionary<string, CallData> calls)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    TraceTopic.pureConnectTopic.verbose("Call Segment Details for Interaction Ids: {}", sInteractionIds);

                    var session = CICSession.Instance.GetSession();

                    var transactionData = new TransactionData { ProcedureName = "PCC_UC_Select_SegmentData" };

                    var sqlParamInteractionIds = new TransactionParameter
                    {
                        Value = sInteractionIds,
                        Type = ParameterType.In,
                        DataType = ParameterDataType.String
                    };

                    transactionData.Parameters.Add(sqlParamInteractionIds);

                    DataTable dtCallSegment = GetData(session, transactionData);

                    DataRowCollection rows = dtCallSegment.Rows;
                    foreach (DataRow row in rows)
                    {
                        string callId = (string)row["CallIDKey"];
                        if (!calls.ContainsKey(callId))
                        {
                            TraceTopic.pureConnectTopic.verbose("Recording with QueueObjectIdKey {} was not found in list.", callId);
                            continue;
                        }

                        CallData call = calls[callId];

                        if (row.Table.Columns.Contains("ICUserID"))
                        {
                            call.AddSegmentAgent(GetColumnStrValue(row, "ICUserID"));
                        }

                        if (row.Table.Columns.Contains("AssignedWorkgroup"))
                        {
                            call.AddSegmentWorkgroup(GetColumnStrValue(row, "AssignedWorkgroup"));
                        }

                        if (row.Table.Columns.Contains("Duration"))
                        {
                            call.AddSegmentDuration(GetColumnStrValue(row, "Duration"));
                        }
                    }
                }
                catch(Exception e)
                {
                    TraceTopic.pureConnectTopic.exception(e);
                }
                return true;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

using GenUtils.TraceTopic;

namespace PureConnect.Data
{
    /// <summary>
    /// Keep details about each call
    /// </summary>
    public class CallData
    {

        private int ir_current_duration = 0;
        //private int ir_hold_duration_total = 0;
        //private int ir_secure_pause_total = 0;
        private List<IR_Events> ir_events = new List<IR_Events>();
        private DateTime recordingStart;
        private List<string> time_of_secure_pause = new List<string>();

        #region Instance Properties

        public string CallId
        {
            get
            {
                return _data[CALL_ID];
            }
            set
            {
                UpdateData(CALL_ID, value);
            }
        }

        public DateTime CallTime
        {
            get
            {
                return _callTime;
            }
            set
            {
                _callTime = value;
                UpdateData(CALL_START_TIME, ToISOString(value));
            }
        }

        public DateTime RealCallTime
        {
            get

            {
                return _realCallTime;
            }
            set
            {
                _realCallTime = value;
            }
        }

        public Guid RecordingId
        {
            get
            {
                return _recordingId;
            }
            set
            {
                _recordingId = value;
                UpdateData(RECORDING_ID, value.ToString());
            }
        }

        public string UserId
        {
            get
            {
                return this[USER_ID];
            }
            set
            {
                UpdateData(USER_ID, value);
            }
        }

        public string this[string name]
        {
            get
            {
                return _data.ContainsKey(name) ? _data[name] : null;
            }
            set
            {
                _data[name] = value;
            }
        }

        #endregion Instance Properties

        #region Constructors

        public CallData(string callId, Guid recordingId, DateTime callTime, int DSTOffset)
        {
            CallId = callId;
            RecordingId = recordingId;
            RealCallTime = callTime;
            //if (DSTOffset != 0)
            //{
            //    callTime = callTime.AddMinutes(DSTOffset);
            //}
            CallTime = callTime;
        }

        #endregion Constructors

        #region Secure Pause

        /// <summary>
        /// Was Secure Pause used
        /// </summary>
        public bool IsSecurePause
        {
            get
            {
                bool res = false;
                if (time_of_secure_pause.Count > 0)
                {
                    res = true;
                }
                return res;
            }
        }

        /// <summary>
        /// List of the Timestamps of the Secure Pause
        /// </summary>
        public List<string> TimeOfSecurePause
        {
            get
            {
                return time_of_secure_pause;
            }
        }

        /// <summary>
        /// Add an event from the ir_event table Used for Secure Pause
        /// </summary>
        /// <param name="ir_event"></param>
        public void AddIREvent(IR_Events ir_event)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    switch (ir_event.EventType)
                    {
                        case 1:
                            //Call goes on hold, increase our hold duration
                            //ir_hold_duration_total += ir_event.Duration;
                            break;
                        case 4:
                            //Recording Start
                            recordingStart = ir_event.EventDate;
                            break;

                        case 5:
                            //Secure Pause
                            if (recordingStart != null)
                            {
                                //Work out the calculated SecurePause time, get the timestamp of the secure pause
                                //subtract the recordings startime from secure pause timestamp
                                //Then subtract any previous hold time or secure pause time (the recordings do not include hold or secure pause)
                                DateTime pause_time = ir_event.EventDate;
                                TraceTopic.pureConnectTopic.verbose("##SP CallId: " + CallId + " - StartTime: " + recordingStart + "  SecurePauseTime: " + pause_time);
                                TimeSpan time = pause_time.Subtract(recordingStart);
                                TraceTopic.pureConnectTopic.verbose("##SP CallId: " + CallId + " - CurrentDuration: " + ir_current_duration + " TimeStamp: " + time.ToString());
                                TimeSpan calculated_time = time.Subtract(new TimeSpan(0, 0, 0, 0, ir_current_duration));
                                string calc_time = new DateTime(calculated_time.Ticks).ToString("HH:mm:ss");
                                TraceTopic.pureConnectTopic.verbose("##SP CallId: " + CallId + " - CalculatedTime: " + calc_time);
                                time_of_secure_pause.Add(calc_time);
                                //ir_secure_pause_total += ir_event.Duration;
                            }
                            break;
                    }

                    //Add to the call duration
                    if (recordingStart != null && ir_event.Duration > 0)
                    {
                        ir_current_duration += ir_event.Duration;
                    }
                    ir_events.Add(ir_event);
                }
                catch (Exception ex)
                {
                    TraceTopic.pureConnectTopic.exception(ex);
                }
            }
        }

        /// <summary>
        /// Create a comma separated list of Secure Pause Times
        /// </summary>
        /// <returns></returns>
        internal string GetSecurePauseString()
        {
            string s = string.Join(",", time_of_secure_pause);
            return s;
        }

        /// <summary>
        /// Get a Count of Secure Pauses
        /// </summary>
        /// <returns></returns>
        internal int GetSecurePauseCount()
        {
            return time_of_secure_pause.Count;
        }

        #endregion Secure Pause

        #region Mutli Segment Calls

        /// <summary>
        /// Add Agent to Segment List
        /// </summary>
        /// <param name="agent"></param>
        public void AddSegmentAgent(string agent)
        {
            segment_agent.Add(agent);
        }

        /// <summary>
        /// Add Duration to Segment List
        /// </summary>
        /// <param name="duration"></param>
        public void AddSegmentDuration(string duration)
        {
            segment_duration.Add(duration);
        }

        /// <summary>
        /// Add Workgroup to Segment List
        /// </summary>
        /// <param name="workgroup"></param>
        public void AddSegmentWorkgroup(string workgroup)
        {
            segment_workgroup.Add(workgroup);
        }

        public int CountSegmentAgent()
        {
            return segment_agent.Count;
        }

        /// <summary>
        /// Create the comma separated Agent List
        /// </summary>
        /// <returns></returns>
        public string GetSegmentAgentString()
        {
            string s = string.Join(",", segment_agent);
            return s;
        }

        /// <summary>
        /// Create the comma separated Duration List
        /// </summary>
        /// <returns></returns>
        public string GetSegmentDurationString()
        {
            string s = string.Join(",", segment_duration);
            return s;
        }

        /// <summary>
        /// Create the comma separated Workgroup List
        /// </summary>
        /// <returns></returns>
        public string GetSegmentWorkgroupString()
        {
            string s = string.Join(",", segment_workgroup);
            return s;
        }

        /// <summary>
        /// Have multiple Agents handled this call If must be different Agent names in the List
        /// </summary>
        /// <returns></returns>
        public bool IsMultipleAgents()
        {
            if (segment_agent.TrueForAll(i => i.Equals(segment_agent.FirstOrDefault())))
            {
                return false;
            }
            return true;
        }

        public List<string> Keys()
        {
            return _data.Keys.ToList();
        }

        #endregion Mutli Segment Calls

        #region Instance Methods

        protected static string ToISOString(DateTime _dtm)
        {
            return _dtm.ToString(ISO_8601_FORMAT);
        }

        protected void RemoveData(string name)
        {
            _data.Remove(name);
        }

        protected void UpdateData(string name, string value)
        {
            if (null == value)
            {
                RemoveData(name);
                return;
            }

            _data[name] = value;
        }


        private int CalculatedDuration { get; set; }

        /// <summary>
        /// Sets the survey details for the call
        /// </summary>
        public void AddSurveyDetails(string questionName, List<SurveyDetail> surveyDetail)
        {
            this.surveyDetails.Add(questionName, surveyDetail);
        }

        /// <summary>
        /// Returns the survey details 
        /// </summary>
        public Dictionary<string, List<SurveyDetail>> GetSurveyDetails()
        {
            return this.surveyDetails;
        }


        #endregion Instance Methods

        #region Class data

        //TODO Tidy up these..

        public const string AGENT_GROUP = "LastAssignedWorkgroupID";

        public const string AGENT_NAME = "LastLocalUserId";

        public const string ANI = "RemoteNumberFmt";

        public const string CALL_DATE = "InitiatedDateTimeUTC";

        public const string CALL_DURATION = "Duration";

        // InteractionSummary.InitiatedDateTimeUTC InteractionSummary.ConnectedDateTimeUTC
        public const string CALL_END_TIME = "TerminatedDateTimeUTC";

        // Field name constants IR_RecordingMedia.QueueObjectIdKey
        public const string CALL_ID = "QueueObjectIdKey"; // InteractionSummary.InteractionKey

        public const string CALL_START_TIME = "InitiatedDateTimeUTC";

        // Duplicate?
        public const string CUSTOMER_DIALED_NUMBER = "DNIS_LocalID";

        // Not available in Phase 1 Is this not the same as ANI?
        public const string CUSTOMER_NUMBER = "RemoteNumberFmt";

        public const string DIRECTION = "Direction";

        // InteractionSummary.DNIS_LocalID
        public const string DNIS = "DNIS_LocalID";

        public const string INTERACTION_ID = "InteractionId";
        public const string N_HOLDS = "nHeld";
        public const string N_TRANSFERS = "nTransfer";
        public const string RECORDING_ID = "RecordingId";

        // Not available in Phase 1
        //public const string CUSTOMER_ID = "customerid";

        // InteractionSummary.DNIS_LocalID

        // InteractionSummary.RemoteNumberFmt
        // Not available in Phase 1
        //public const string CALL_TAKING_AGENT_ID = "";

        // InteractionSummary.LastLocalUserId InteractionSummary.LastAssignedWorkgroupID
        public const string SITE_ID = "SiteID";

        // InteractionSummary.nTransfer InteractionSummary.nHeld
        public const string TOTAL_HOLD_TIME = "tHeld"; // interactionSummary.tHeld

        // InteractionSummary.InitiatedDateTimeUTC InteractionSummary.TerminatedDateTimeUTC IR_RecordingMedia.Duration

        public const string USER_ID = "LastLocalUserId";

        // InteractionSummary.SiteID

        // IR_RecordingMedia.Direction

        public const string WRAP_UP_TIME = "tACW"; // interactionSummary.tACW
                                                   //public string RelatedRecordingId { get; set; }

        public static readonly string[] MandatoryFields = new string[]
        {
            CALL_ID, RECORDING_ID, USER_ID, CALL_START_TIME
        };

        private static readonly string ISO_8601_FORMAT = "yyyy-MM-ddTHH:mm:ss";

        #endregion Class data

        #region Instance data

        private readonly Dictionary<string, string> _data = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
        private DateTime _callTime;
        //private double _duration;
        private DateTime _realCallTime;
        private Guid _recordingId;

        //private List<string> securePause = new List<string>();
        private List<string> segment_agent = new List<string>();

        private List<string> segment_duration = new List<string>();
        private List<string> segment_workgroup = new List<string>();
        private readonly Dictionary<string, List<SurveyDetail>> surveyDetails = new Dictionary<string, List<SurveyDetail>>();

        #endregion Instance data
    }

    public class SurveyDetail
    {
        public string CallId { get; set; }
        public decimal MaxScore { get; set; }
        public decimal Score { get; set; }
        public string SurveyQuestionID { get; set; }
        public decimal NumericScore { get; set; }
        public string AudioPath { get; set; }
        public string QuestionText { get; set; }
    }
}
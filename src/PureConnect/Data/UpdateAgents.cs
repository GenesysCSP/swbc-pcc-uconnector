﻿using ININ.IceLib.Data.TransactionBuilder;
using PureConnect.I3Audio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PureConnect.Data
{
    class UpdateAgents:BaseQueryData
    {
        public string GetTableName(string procName, SqlConnection con, SqlTransaction transaction)
        {
            
            DataTable results = GetDataSql(procName, null, con, transaction);

            return results.Rows[0][0].ToString();
        }

        public void UpdateTempTable(int parentid, int childid, string tablename, string procName, SqlConnection con, SqlTransaction transaction)
        {
            Dictionary<string, List<string>> param = new Dictionary<string, List<string>>();
            param.Add("@parentId", new List<string>() { parentid.ToString(), "int" });
            param.Add("@childId", new List<string>() { childid.ToString(), "int" });
            param.Add("@table_name", new List<string>() { tablename, "varchar", "50" });

            GetDataSql(procName, param, con, transaction);
        }

        public DataTable UpdateAgentEntity(string agentId, string name, string procName, SqlConnection con, SqlTransaction transaction)
        {
            Dictionary<string, List<string>> param = new Dictionary<string, List<string>>();
            param.Add("@name", new List<string>() { name, "varchar", "50" });
            param.Add("@id", new List<string>() { agentId.ToString(), "varchar", "50" });

            DataTable results = GetDataSql(procName, param, con, transaction);

            return results;
        }
        public void UpdateAgentHierarchy(string tablename, string procName, SqlConnection con, SqlTransaction transaction)
        {
            Dictionary<string, List<string>> param = new Dictionary<string, List<string>>();
            param.Add("@newAgentHierarchyTableName", new List<string>() { tablename, "nvarchar", "100" });

            GetDataSql(procName, param, con, transaction);
        }

        public void DeleteTempTable(string tablename, string procName, SqlConnection con, SqlTransaction transaction)
        {
            Dictionary<string, List<string>> param = new Dictionary<string, List<string>>();
            param.Add("@table_name", new List<string>() { tablename, "varchar", "50" });

            GetDataSql(procName, param, con, transaction);
        }

        public void RollbackChanges(string tablename, string changesrec, string procName, string dataSource)
        {
            var session = CICSession.Instance.GetSession();

            var transactionData = new TransactionData { ProcedureName = procName };

            var tableName = new TransactionParameter
            {
                Value = tablename,
                Type = ParameterType.In,
                DataType = ParameterDataType.String
            };
            transactionData.Parameters.Add(tableName);

            var changedRecords = new TransactionParameter
            {
                Value = changesrec,
                Type = ParameterType.In,
                DataType = ParameterDataType.String
            };
            transactionData.Parameters.Add(changedRecords);

            var nameOrexternalID = new TransactionParameter
            {
                Value = "0",
                Type = ParameterType.In,
                DataType = ParameterDataType.Boolean
            };
            transactionData.Parameters.Add(nameOrexternalID);

            GetData(session, transactionData, dataSource, false);

        }
    }
}

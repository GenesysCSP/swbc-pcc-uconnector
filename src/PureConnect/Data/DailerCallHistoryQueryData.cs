﻿using GenUtils.TraceTopic;
using ININ.IceLib.Data.TransactionBuilder;
using PureConnect.I3Audio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PureConnect.Data
{
    /// <summary>
    /// Class to process the Dialer history query data
    /// </summary>
    internal class DailerCallHistoryQueryData : BaseQueryData
    {

        public DailerCallHistoryQueryData()
        {
        }

        /// <summary>
        /// </summary>
        /// <param name="m_selectMetaQuery"></param>
        /// <param name="m_lastImportedDate"></param>
        /// <param name="batchSize"></param>
        /// <param name="sInteractionIds"></param>
        /// <param name="m_callMetaFields"></param>
        /// <param name="calls"></param>
        /// <returns></returns>
        public bool GetData(string interactionIds, Dictionary<string, Dictionary<string, string>> m_allMetaFields, Dictionary<string, CallData> calls)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    TraceTopic.pureConnectTopic.note("Dialer Call history Details for the Interaction Ids: {}", interactionIds);

                    var session = CICSession.Instance.GetSession();

                    var transactionData = new TransactionData { ProcedureName = "PCC_UC_Select_Dialer_CallHistory" };

                    var sqlParamInteractionIds = new TransactionParameter
                    {
                        Value = interactionIds,
                        Type = ParameterType.In,
                        DataType = ParameterDataType.String
                    };

                    transactionData.Parameters.Add(sqlParamInteractionIds);

                    DataTable dtDialerCallHistory = GetData(session, transactionData);

                    DataRowCollection rows = dtDialerCallHistory.Rows;
                    foreach (DataRow row in rows)
                    {
                        string callId = (string)row["InteractionIDKey"];
                        if (!calls.ContainsKey(callId))
                        {
                            TraceTopic.pureConnectTopic.note("Recording with QueueObjectIdKey {} was not found in list.", callId);
                            continue;
                        }

                        CallData call = calls[callId];
                        Dictionary<string, string> m_metaFields = m_allMetaFields[call[Constants.RecordingInteractionType]];

                        foreach (KeyValuePair<string, string> field in m_metaFields)
                        {
                            try
                            {
                                //Filter out the fields that are not for this table
                                string key = field.Key;
                                if (row.Table.Columns.Contains(key) && !row.IsNull(key))
                                {
                                    call[key] = GetColumnStrValue(row, key);
                                }
                            }
                            catch (Exception e)
                            {
                                TraceTopic.pureConnectTopic.error("Error Adding MetaData: " + field.Key, e);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    TraceTopic.pureConnectTopic.exception(ex);
                }
            }
            return true;
        }
    }
}

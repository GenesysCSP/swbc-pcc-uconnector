﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using PureConnect.CICConfig;
using PureConnect.Data;
using PureConnect.HA;
using PureConnect.I3Audio;
using QBEUConnector;
using utopy.bAgentsAgents;
using utopy.bDBServices;
using ININ.IceLib.Configuration;
using System.Collections.ObjectModel;
using GenUtils.TraceTopic;
using System.Data.SqlClient;
using System.Xml.Linq;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using System.Security;
using System.Threading;
using PureConnect.Logging;

namespace PureConnect
{
    public class Importer_PureConnect : Importer
    {
        #region Instance properties

        //private DBServices HaDb { get; set; }
        private DBServices MetadataDb { get; set; }

        #endregion Instance properties

        #region Configuration key constants

        private TimeSpan DEFAULT_DELAY_BETWEEN_AUDIO_DOWNLOADS =
            new TimeSpan(0, 0, 0, 0, 800);

        #endregion Configuration key constants

        #region Configuration parameters

        private string m_agentFilenamePattern;
        private string m_agentFilesFolder;
        private string m_audioFetchingTempPath;
        private IDictionary<string, string> m_callMetaFields = new Dictionary<string, string>();
        private IDictionary<string, string> m_chatMetaFields = new Dictionary<string, string>();
        private IDictionary<string, string> m_emailMetaFields = new Dictionary<string, string>();
        private Dictionary<string, Dictionary<string, string>> m_allMetaFileds = new Dictionary<string, Dictionary<string, string>>();

        //private string m_i3AudioFetcherPath;
        //private CICCredentials m_cic_Credentials = null;

        private string m_cicDataConnectionString;
        private bool m_debugFakeDownload = false;
        private bool m_backupXML = false;
        private TimeSpan m_delayBetweenAudioDownloads;
        private string m_ffmpegArgs;
        private string m_ffmpegPath;
        private string m_haConnectionString;
        private TimeSpan m_haHeartbeatSecs;
        //private bool m_haPrimarySiteIsDown = false;

        // HA_PRIMARY_SITE or HA_SECONDARY_SITE
        private string m_haRole;

        // HA_PRIMARY_ROLE or HA_BACKUP_ROLE
        private string m_haSite;

        //private DateTime m_lastImportedDate;
        private int m_maxAttemptsToDownloadAudio;

        private string m_metaDataConnectionString;

        // I3_IC
        //private bool m_resetLastImportedDate;

        private TimeSpan m_sleepTimeAfterAudioDownloadFailure;
        private string m_speechMinerConnectionString;
        private string m_agentsHierarhy = "";
        private string m_supervisor = "";
        private string m_customAttribute = "";
        private List<string> m_recordingTag = null;

        private ReadOnlyCollection<UserConfiguration> _userConfigurationList;

        //Threshold values
        private int m_downloadThreshold = 10000;
        private int m_transcodeThreshold = 10000;
        private int m_transcodeFolderMonitorTime = 12;
        private int m_transcodeFolderLifeTime = 3;
        private int m_transcodeFolderFileCount = 100;
        private Timer m_folderMonitorTimerThread = null;

        #endregion Configuration parameters

        #region Instance fields

        //private Random _rnd = new Random();
        private List<string> agentFilesToDelete = new List<string>();

        private HAController haController = new HAController();
        private Dictionary<string, string> m_agentWorkgroups = new Dictionary<string, string>();
        private bool m_audioDownloadFailed;
        private CountdownLatch m_countdownLatch;
        private DateTime? m_currentDate;
        private bool m_dbConnected = false;

        //private bool m_haRecoveryActive = false;

        //private Timer m_heartbeatTimer;

        //private static I3Fetcher m_i3Fetcher;
        private int m_numOfFailedAudioDownloads = 0;

        //Keep track of last processed date time.
        private ConcurrentStack<DateTime> processedDates = new ConcurrentStack<DateTime>();

        private DateTime startTime = DateTime.UtcNow;

        //Logger
        WindowsEventLogger logger = new WindowsEventLogger();

        #endregion Instance fields

        private readonly object locker = new object();

        private int iCount = 0;
        private bool bUpdateAgentsInProgress = false;
        protected object m_agentUpdateLockObj = new object();
        private string m_defaulProgramID = "";

        //private int m_delay =0;
        //private string m_spanishProgramID = "";
        private string m_defaultWorkgroupProgram = "";

        private string m_voiceCallProgramID = "";
        private string m_chatProgramID = "";
        private string m_emailProgramID = "";
        private Dictionary<string, string> m_languageProgramIDs = new Dictionary<string, string>();
        private Dictionary<string, List<string>> m_workgroupMappings = new Dictionary<string, List<string>>();
        private Dictionary<string, string> m_workgroupProgramIDs = new Dictionary<string, string>();
        private string m_surveyAudioFilePath = "";

        public Importer_PureConnect(System.ComponentModel.ISynchronizeInvoke _isi, string _configFile)
            : base(_isi, _configFile)
        {
            //SegmentDetails sd = new SegmentDetails();
        }

        #region MetaData

        public void GenerateMetaFile(string _fileName, CallData call, bool isSurveyInteraction = false)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                string tmpXMLPath = Path.Combine(m_audioFetchingTempPath, Path.GetFileName(_fileName));
                try
                {
                    using (XmlTextWriter metaFile = new XmlTextWriter(tmpXMLPath, Encoding.ASCII))
                    {
                        var metaFileds = new Dictionary<string, string>();
                        metaFile.Formatting = Formatting.Indented;
                        metaFile.WriteStartDocument(false);
                        metaFile.WriteComment(XML_OUTPUT_COMMENT);
                        metaFile.WriteStartElement(XML_OUTPUT_CALLINFORMATION);

                        metaFile.WriteElementString(XML_OUTPUT_PROGRAMID, GetProgramID(call[Constants.RecordingInteractionType], call.GetSegmentWorkgroupString()));
                        //chat or email
                        if (call[Constants.RecordingInteractionType] == Constants.Chat || call[Constants.RecordingInteractionType] == Constants.EMail)
                        {
                            //SM does not use the milliseconds in DateTime so don't sent it..
                            metaFile.WriteElementString(XML_OUTPUT_TEXTTIME, call.CallTime.ToString(Constants.METADATA_TIME_FORMAT));
                            //chat
                            if (call[Constants.RecordingInteractionType] == Constants.Chat)
                            {
                                metaFileds = m_allMetaFileds[Constants.Chat];
                                metaFile.WriteElementString(XML_OUTPUT_MEDIATYPE, Constants.Chat);
                                metaFile.WriteElementString(XML_OUTPUT_TEXTFORMAT, "Text");
                            }
                            //email
                            else
                            {
                                metaFileds = m_allMetaFileds[Constants.EMail];
                                metaFile.WriteElementString(XML_OUTPUT_MEDIATYPE, Constants.EMail);
                                metaFile.WriteElementString(XML_OUTPUT_TEXTFORMAT, "EML");
                            }
                        }
                        else
                        {
                            metaFileds = m_allMetaFileds[Constants.Voice];
                            //SM does not use the milliseconds in DateTime so don't sent it..
                            metaFile.WriteElementString(XML_OUTPUT_CALLTIME, call.CallTime.ToString(Constants.METADATA_TIME_FORMAT));

                            metaFile.WriteElementString(XML_OUTPUT_AUDIOFORMAT, utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat.WAV_PCM.ToString());

                            if (m_leftChannelSpeaker != null && m_rightChannelSpeaker != null)
                            {
                                metaFile.WriteStartElement(XML_OUTPUT_CHANNELS);
                                metaFile.WriteAttributeString(XML_OUTPUT_CHANNEL_LEFT, m_leftChannelSpeaker);
                                metaFile.WriteAttributeString(XML_OUTPUT_CHANNEL_RIGHT, m_rightChannelSpeaker);
                                metaFile.WriteEndElement(); // channels
                            }
                        }

                        metaFile.WriteStartElement(XML_OUTPUT_OTHER);
                        // generateSurveyResults(metaFile, call);
                        generateOptionals(metaFile, call, metaFileds);

                        string multipelAgents = "n";
                        //if (call.CountSegmentAgent() > 1)
                        if (call.IsMultipleAgents())
                        {
                            multipelAgents = "y";
                        }

                        //Added Agents, WorkGroups and Durations
                        metaFile.WriteElementString("multipleAgents", multipelAgents);
                        metaFile.WriteElementString("agent_list", call.GetSegmentAgentString());
                        metaFile.WriteElementString("workgroup_list", call.GetSegmentWorkgroupString());
                        //metaFile.WriteElementString("duration_list", call.GetSegmentDurationString());

                        metaFile.WriteElementString("secure_pause_count", "" + call.GetSecurePauseCount());

                        if (call.IsSecurePause)
                        {
                            metaFile.WriteElementString("secure_pause_list", call.GetSecurePauseString());
                        }

                        string value = call["RelatedInteractionIdKey"];

                        if (!string.IsNullOrWhiteSpace(value))
                        {
                            TraceTopic.pureConnectTopic.note("############## RELATED RECORDING ID: " + call.CallId + " #################################");
                            metaFile.WriteElementString("RelatedRecordingInteractionIDKey", value);
                        }

                        metaFile.WriteEndElement(); // other

                        metaFile.WriteStartElement(XML_OUTPUT_SPEAKERS);
                        metaFile.WriteStartElement(XML_OUTPUT_SPEAKER);
                        if (string.IsNullOrEmpty(m_agentId))
                            metaFile.WriteAttributeString(XML_OUTPUT_SPEAKER_ID, call.UserId);
                        else
                            metaFile.WriteAttributeString(XML_OUTPUT_SPEAKER_ID, call[m_agentId]);

                        metaFile.WriteAttributeString(XML_OUTPUT_SPEAKER_TYPE, XML_OUTPUT_SPEAKER_TYPE_AGENT);
                        metaFile.WriteElementString(XML_OUTPUT_SPEAKER_WORKGROUP, "/" + call[CallData.AGENT_GROUP]);
                        metaFile.WriteEndElement(); // speaker
                        metaFile.WriteEndElement(); // speakers

                        metaFile.WriteEndElement(); // callInformation
                        metaFile.WriteEndDocument();
                        metaFile.Close();

                        //Create the xml file to a local directory and then move to the file share.

                        try
                        {
                            if (File.Exists(tmpXMLPath))
                            {
                                if (File.Exists(_fileName))
                                {
                                    File.Delete(_fileName);
                                }
                                TraceTopic.pureConnectTopic.note("m_backupXML:" + m_backupXML);
                                if (m_backupXML)
                                {
                                    TraceTopic.pureConnectTopic.always("MetaData Copy: " + tmpXMLPath + " To: " + _fileName);
                                    File.Copy(tmpXMLPath, _fileName);
                                }
                                else
                                {
                                    TraceTopic.pureConnectTopic.always("MetaData Move: " + tmpXMLPath + " To: " + _fileName);
                                    File.Move(tmpXMLPath, _fileName);
                                }

                                raiseCallImported(call.CallId);
                                TraceTopic.pureConnectTopic.always("RealCallTime: MetaData Generated for: " + call.CallId + " Time: " + call.RealCallTime.ToString());
                                processedDates.Push(call.RealCallTime);
                            }
                        }
                        catch (Exception e)
                        {
                            TraceTopic.pureConnectTopic.error("Error Moving File from location: {} TO: {}", tmpXMLPath, _fileName);
                            TraceTopic.pureConnectTopic.exception(e);
                        }
                    }
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.error("Error in generating metadata file {}", tmpXMLPath);
                    TraceTopic.pureConnectTopic.exception(e);
                }
                finally
                {
                    try
                    {
                        if (!m_backupXML && File.Exists(tmpXMLPath))
                        {
                            File.Delete(tmpXMLPath);
                        }
                    }
                    catch (Exception e)
                    {
                        TraceTopic.pureConnectTopic.error("Error Deleting File: {}", tmpXMLPath);
                        TraceTopic.pureConnectTopic.exception(e);
                    }
                }
            }
        }

        private void generateSurveyResults(XmlTextWriter metaFile, CallData call)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    metaFile.WriteStartElement(XML_OUTPUT_SURVEY);
                    foreach (var field in call.GetSurveyDetails())
                    {
                        if (field.Key == call.CallId)
                        {
                            foreach (var item in field.Value)
                            {
                                metaFile.WriteStartElement(XML_OUTPUT_QUESTION);
                                metaFile.WriteElementString(XML_OUTPUT_NUMERICSCORE, item.NumericScore.ToString());
                                metaFile.WriteElementString(XML_OUTPUT_QUESTIONTEXT, item.QuestionText);
                                metaFile.WriteElementString(XML_OUTPUT_SCORE, item.Score.ToString());
                                metaFile.WriteEndElement();
                                if (!string.IsNullOrEmpty(item.AudioPath))
                                {
                                    I3Fetcher i3Fetcher = new I3Fetcher(m_debugFakeDownload);
                                    var path = string.Empty;
                                    var fileName = call.CallId;
                                    var outputFolder = GetOutputFolder();
                                    fileName += ".wav";
                                    path = Path.Combine(outputFolder, "Survey", fileName);
                                    i3Fetcher.DownloadFileSurveyAudio(item.AudioPath, path);
                                }
                            }

                        }
                    }
                    metaFile.WriteEndElement();
                }
                catch (Exception ex)
                {
                    TraceTopic.pureConnectTopic.exception(ex);
                }
            }
        }

        private string GetProgramID(string recordingType, string workgroup)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                var programID = string.Empty;
                try
                {
                    if (!string.IsNullOrEmpty(workgroup))
                    {
                        string workgroupProgramId = m_defaultWorkgroupProgram;
                        foreach (KeyValuePair<string, List<string>> entry in m_workgroupMappings)
                        {
                            List<string> workgroups = entry.Value;
                            foreach (string configWorkgroup in workgroups)
                            {
                                if (workgroup.Contains(configWorkgroup))
                                {
                                    workgroupProgramId = entry.Key;
                                    break;
                                }
                            }
                            if (!workgroupProgramId.Equals(m_defaultWorkgroupProgram))
                                break;

                        }
                        TraceTopic.pureConnectTopic.note("Program added for workgroup {} is {}", workgroup, workgroupProgramId);
                        return workgroupProgramId;
                    }
                    else
                    {
                        TraceTopic.pureConnectTopic.note("Program added for workgroup {} is {}", workgroup, m_defaulProgramID);
                        return m_defaulProgramID;
                    }
                }
                catch (Exception ex)
                {
                    TraceTopic.pureConnectTopic.exception(ex);
                }
                return programID;
            }
        }
        /*private string GetProgramID(string recordingType, string language)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                var programID = string.Empty;
                try
                {
                    if (!string.IsNullOrEmpty(language) && m_languageProgramIDs.Keys.Contains(language.ToLowerInvariant()))
                    {
                        TraceTopic.pureConnectTopic.note("Program added for language {} is {}", language, m_languageProgramIDs[language.ToLowerInvariant()]);
                        return m_languageProgramIDs[language.ToLowerInvariant()];
                    }
                    else
                    {
                        TraceTopic.pureConnectTopic.note("Program added for language {} is {}", language, m_defaulProgramID);
                        return m_defaulProgramID;
                    }
                }
                catch (Exception ex)
                {
                    TraceTopic.pureConnectTopic.exception(ex);
                }
                return programID;
            }
        }*/

        public override void GenerateMetaFile(string _fileName, System.Data.DataRow _row)
        {
            // todo No need?
        }

        protected void generateOptionals(XmlTextWriter metaFile, CallData call, IDictionary<string, string> m_metaFileds)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    foreach (KeyValuePair<string, string> field in m_metaFileds)
                    {
                        string name = field.Key;
                        string newName = field.Value;
                        if (string.IsNullOrWhiteSpace(newName)) newName = name;
                        string value = call[name];
                        if (string.IsNullOrWhiteSpace(value)) continue;
                        metaFile.WriteElementString(newName, value);
                    }
                }
                catch (Exception ex)
                {
                    TraceTopic.pureConnectTopic.exception(ex);
                }
            }
        }

        #endregion MetaData

        public override void init()
        {
            //Add this to enable last interaction updates to monitorConnectorsTbl
            using (TraceTopic.pureConnectTopic.scope())
            {
                base.init();
                string error = "";
                try
                {
                    error = "There was error loading configuration file. Exception - {}";
                    loadFromXML();

                    Cancelled += Importer_PureConnect_Cancelled;
                    Completed += Importer_PureConnect_Completed;

                    if (!ConnectToDB()) return;

                    updateStatus(QBEUConnector.Main.IImporter.Status.STARTING, "Starting");
                    error = "There was error starting heartbeat timer task. Exception - {}";
                    haController.SetHAController(m_pollIntervalSecs, m_haHeartbeatSecs, m_haSite, m_haRole, m_xmlDoc, XML_SETTINGS, this);
                    haController.StartHeartbeatThread();

                    TraceTopic.pureConnectTopic.note("Starting the thread for monitoring the transcode folder");
                    int folderCheckMilliseconds = m_transcodeFolderMonitorTime * 60 * 60 * 1000;
                    TimerCallback timerCallback = new TimerCallback(TrancodeMonitorTimeElapsed);
                    m_folderMonitorTimerThread = new Timer(timerCallback, null, 1000, folderCheckMilliseconds);
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.error(error, e);
                }
            }
        }

        /// <summary>
        /// Elapsed function for monitoring the transcode folder
        /// </summary>
        /// <param name="state"></param>
        private void TrancodeMonitorTimeElapsed(object state)
        {
            int numberOfFiles = Directory.GetFiles(m_audioFetchingTempPath).Length;
            TraceTopic.pureConnectTopic.note("Transcode folder monitor thread: Number of files in transcoding folder: {}", numberOfFiles);
            if (numberOfFiles > m_transcodeFolderFileCount)
            {
                string errorMessage = string.Format("Number of files in the trancode folder is {0}, exceeded threshold value by {1}", numberOfFiles, numberOfFiles - m_transcodeFolderFileCount);
                TraceTopic.pureConnectTopic.warning(errorMessage);
                logger.WriteEntry(errorMessage, EventLogEntryType.Warning, (int)ErrorCodes.TRANSCODE_FOLDER_FILE_COUNT_EXCEEDED);
            }

            int numberOfOlderFiles = Directory.GetFiles(m_audioFetchingTempPath).Where(x => File.GetLastAccessTime(x).Date < DateTime.Now.AddDays((m_transcodeFolderLifeTime * -1)).Date).Count();

            if (numberOfOlderFiles > 0)
            {
                string errorMessage = string.Format("Number of files in the trancode folder older that {0} days : {1}", m_transcodeFolderLifeTime, numberOfOlderFiles);
                TraceTopic.pureConnectTopic.warning(errorMessage);
                logger.WriteEntry(errorMessage, EventLogEntryType.Warning, (int)ErrorCodes.TRANSCODE_FOLDER_FILE_LIFE_EXCEED);
            }
        }

        /// <summary>
        /// Quit
        /// </summary>
        public override void Quit()
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    TraceTopic.pureConnectTopic.warning("Quit");
                    if (haController != null)
                    {
                        haController.StopHeartbeatThread();
                    }
                    CICSession.Instance.Close();
                    m_folderMonitorTimerThread.Dispose();
                }
                catch (Exception ex)
                {
                    TraceTopic.pureConnectTopic.exception(ex);
                }
            }
        }

        /// <summary>
        /// At bit of a fudge to enable the HA Controller to call the saveToXML method..
        /// </summary>
        public void SaveToXML()
        {
            this.saveToXML();
        }

        /// <summary>
        /// saveToXML overriden from Importer class
        /// </summary>
        protected override void saveToXML()
        {
            base.saveToXML();
        }

        /// <summary>
        /// Connect to the DBs
        /// </summary>
        /// <returns></returns>
        protected bool ConnectToDB()
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    if (m_dbConnected) return true;

                    m_localdbMaster = new DBServices();
                    TraceTopic.pureConnectTopic.always("Speechminer connection string: {}", m_speechMinerConnectionString);
                    m_localdbMaster.setDB(m_speechMinerConnectionString, true);

                    //TraceTopic.pureConnectTopic.always("CIC meta data DB Connection string: {}", m_metaDataConnectionString);
                    //MetadataDb = new DBServices();
                    //MetadataDb.setDB(m_metaDataConnectionString, true);

                    TraceTopic.pureConnectTopic.always("HA DB Connection string: {}", m_haConnectionString);
                    haController.HaDb = new DBServices();
                    haController.HaDb.setDB(m_haConnectionString, true);
                    m_dbConnected = true;

                    return true;
                }
                catch (Exception ex)
                {
                    TraceTopic.pureConnectTopic.exception(ex);
                    return false;
                }
            }
        }

        /// <summary>
        /// Wrapt the string in double quotes
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private static string WrapArgument(string arg)
        {
            return "\"" + arg + "\"";
        }

        /// <summary>
        /// Delete all files in a Directory
        /// </summary>
        /// <param name="directoryPath"></param>
        private void DeleteAllFiles(string directoryPath)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    var directory = new DirectoryInfo(directoryPath);
                    List<FileInfo> fileInfos = directory.EnumerateFiles("*.*", System.IO.SearchOption.AllDirectories).ToList();
                    foreach (FileInfo f in fileInfos)
                        File.Delete(f.FullName);
                    TraceTopic.pureConnectTopic.always("Deleted files from {}", directoryPath);
                }
                catch (Exception ex)
                {
                    TraceTopic.pureConnectTopic.exception(ex);
                }
            }
        }

        /// <summary>
        /// Delete all the files in the list and then remove from the list.
        /// </summary>
        /// <param name="filesToDelete"></param>
        private void DeleteAllFilesInList(List<string> filesToDelete)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                foreach (string file in filesToDelete.ToList())
                {
                    try
                    {
                        TraceTopic.pureConnectTopic.note("Attempting to Delete File: " + file);
                        File.Delete(file);
                        TraceTopic.pureConnectTopic.note("Deleted File: " + file);
                        filesToDelete.Remove(file);
                    }
                    catch (Exception e)
                    {
                        TraceTopic.pureConnectTopic.error("Error deleting File: {}.", file);
                        TraceTopic.pureConnectTopic.exception(e);
                    }
                }
            }
        }

        [SuppressMessage("Microsoft.Security", "CA2118:ReviewSuppressUnmanagedCodeSecurityUsage"), SuppressUnmanagedCodeSecurity]
        [DllImport("Kernel32", SetLastError = true, CharSet = CharSet.Auto)]
        [return: MarshalAs(UnmanagedType.Bool)]

        private static extern bool GetDiskFreeSpaceEx
        (
            string lpszPath,                    // Must name a folder, must end with '\'.
            ref long lpFreeBytesAvailable,
            ref long lpTotalNumberOfBytes,
            ref long lpTotalNumberOfFreeBytes
        );

        /// <summary>
        /// Download recording, convert and create metadata xml
        /// </summary>
        /// <param name="call"></param>
        /// <param name="i3Fetcher"></param>
        private void GenerateFromRow(CallData call, I3Fetcher i3Fetcher)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    string callId = call.CallId;
                    string recordingId = call.RecordingId.ToString();

                    TraceTopic.pureConnectTopic.note("Generating call for call ID {}, recording ID {}.", callId, recordingId);
                    var path = string.Empty;
                    var fileName = callId;
                    var outputFolder = GetOutputFolder();

                    //call
                    if (call[Constants.RecordingInteractionType] == Constants.Voice)
                    {
                        fileName += ".wav";
                        path = Path.Combine(outputFolder, fileName);

                        //Call DownloadAudio because we still want to convert the audio.
                        DownloadAudio(recordingId, path, i3Fetcher);
                    }
                    //chat
                    else if (call[Constants.RecordingInteractionType] == Constants.Chat)
                    {
                        fileName += Constants.TXTExtension;
                        path = Path.Combine(outputFolder, fileName);
                        DownloadText(recordingId, path, i3Fetcher, call);
                    }
                    //email
                    else if (call[Constants.RecordingInteractionType] == Constants.EMail)
                    {
                        fileName += Constants.EMLExtension;
                        path = Path.Combine(outputFolder, fileName);
                        DownloadText(recordingId, path, i3Fetcher, call);
                    }

                    if (!File.Exists(path))
                    {
                        TraceTopic.pureConnectTopic.warning("Audio file for recording ID {} was not downloaded successfully.", recordingId);
                    }
                    else
                    {
                        string xmlPath = Path.ChangeExtension(path, ".xml");
                        GenerateMetaFile(xmlPath, call);

                        //Generate Survey metadata file
                        if (call.GetSurveyDetails().Count() > 0)
                        {
                            string surveyOutputFolder = GetOutputFolder();
                            string surveyXML = Path.Combine(surveyOutputFolder, "survey", $"{call.CallId}.xml");
                            GenerateMetaFile(surveyXML, call, true);
                        }


                        if (File.Exists(xmlPath))
                        {
                            if (m_recordingTag != null)
                            {
                                i3Fetcher.AddLabel(recordingId, m_recordingTag);
                            }
                            IncCountHandled();
                        }
                        else
                        {
                            TraceTopic.pureConnectTopic.warning("XML generation failed for recording ID {}. Removing the audio file.", recordingId);
                            File.Delete(path);
                        }
                    }
                }
                catch (Exception ex)
                {
                    TraceTopic.pureConnectTopic.exception(ex);
                }
            }
        }

        /// <summary>
        /// Replace the PlaceHoders in the SQL Statements
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="sInteractionIds"></param>
        /// <param name="RecordingsIds"></param>
        /// <returns></returns>
        private string GenerateSQL(string sql, string sInteractionIds, string sRecordingIds)
        {
            sql = sql
            .Replace("[LAST_CALL_TIME]", haController.LastImportedDate.ToString(Constants.ISO_8601_FORMAT))
        .Replace("[BATCH_SIZE]", m_batchSize.ToString())
        .Replace("[LIST_OF_INTERACTIONS]", sInteractionIds)
            .Replace("[LIST_OF_RECORDINGS]", sRecordingIds);
            return sql;
        }

        /// <summary>
        /// Get a DateTime. Add the DSTOffset (in Seconds)
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <param name="DSTOffset"></param>
        /// <returns></returns>
        private string GetDateTimeNoMilliSeconds(DataRow row, string column, int DSTOffset)
        {
            if (row.IsNull(column))
            {
                return "";
            }
            DateTime date = (DateTime)row[column];
            if (DSTOffset != 0)
            {
                date = date.AddSeconds(DSTOffset);
            }
            return date.ToString("yyyy-MM-ddTHH:mm:ss");
            //return row.IsNull(column) ? "" : ((DateTime)row[column]).ToString("yyyy-MM-ddTHH:mm:ss");
        }

        private string GetFromXMLElement(XmlElement xmlElement, string key)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    return xmlElement[key].InnerText;
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.error("Error Getting Option '{}'", key);
                }
            }
            return null;
        }

        /// <summary>
        /// Get the Date in ISO 8601 Format
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        private string GetIsoDateStrValue(DataRow row, string column)
        {
            return row.IsNull(column) ? "" : ((DateTime)row[column]).ToString(Constants.ISO_8601_FORMAT);
        }

        private void Importer_PureConnect_Cancelled(object sender, EventArgs e)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                TraceTopic.pureConnectTopic.always("Cancelled");
            }
        }

        private void Importer_PureConnect_Completed(object sender, EventArgs e)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                TraceTopic.pureConnectTopic.always("Completed");
            }
        }

        /// <summary>
        /// Performs the importer tasks. Iterates through the db query results Downloads the audio files Converts the audio files Moves the audio file to the fetcher folder Creates the meta data and moves it to the fetcher folder
        /// </summary>
        /// <param name="obj"></param>
        private void ImporterThreadWorker(object obj)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                int iCount = 0;
                var watch = Stopwatch.StartNew();
                TraceTopic.pureConnectTopic.note("Importer worker thread started.");
                I3Fetcher i3Fetcher = new I3Fetcher(m_debugFakeDownload);
                try
                {
                    List<CallData> calls = (List<CallData>)obj;

                    TraceTopic.pureConnectTopic.note("Worker thread: got {} calls.", calls.Count);

                    foreach (CallData call in calls)
                    {
                        try
                        {
                            //TODO PETE use this to select certain types of recordings during testing!!!!
                            //if (call.IsSecurePause)
                            //string value = call["RelatedInteractionIdKey"];
                            //if (!string.IsNullOrEmpty(value))
                            if (true)
                            {
                                GenerateFromRow(call, i3Fetcher);
                                iCount++;
                                TraceTopic.pureConnectTopic.always("##Elapsed Recordings Imported: " + iCount + " InteractionId: " + call.CallId + " Last CallTime: " + call.CallTime.ToString(Constants.ISO_8601_FORMAT) + " Processed in: " + watch.ElapsedMilliseconds + "ms");
                            }
                            else
                            {
                                //Log.Info("TEST#### DONT GET AUIDO OR GENERATE XML");
                            }
                        }
                        catch (Exception e)
                        {
                            TraceTopic.pureConnectTopic.error("Error generating call data", e);
                        }
                        if (this.CancelRequested)
                        {
                            TraceTopic.pureConnectTopic.note("Cancelling Importer. Stop ImporterThreadWorker");
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.error("Error in importer worker", e);
                }
                finally
                {
                    TraceTopic.pureConnectTopic.always("Importer worker - work completed");
                    if (i3Fetcher != null)
                    {
                        i3Fetcher.Dispose();
                        i3Fetcher = null;
                    }

                    TraceTopic.pureConnectTopic.always("##Elapsed Thread Ended, total Time running: " + watch.ElapsedMilliseconds + "ms");
                    m_countdownLatch.Signal();
                }
            }
        }

        /// <summary>
        /// Increment the Number of Records Handled
        /// </summary>
        private void IncCountHandled()
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    lock (locker)
                    {
                        iCount++;
                        TraceTopic.pureConnectTopic.always("Total Recordings Handled: {} Since: {}", iCount, startTime.ToString(Constants.ISO_8601_FORMAT));
                        if (iCount == int.MaxValue)
                        {
                            iCount = 0;
                        }
                    }
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.exception(e);
                }
            }
        }

        /// <summary>
        /// Load the Config from XML
        /// </summary>
        private void loadFromXML()
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    base.LoadXML();
                    LoadBaseXML(m_xmlDoc);

                    XmlElement settings = m_xmlDoc[XML_SETTINGS];

                    string tmpStringName = settings[XML_SETTINGS_MSSQL][XML_SETTINGS_SPEECHMINER_CONNECTION_STRING_NAME].InnerText;
                    m_speechMinerConnectionString = loadConnectionString(tmpStringName);

                    tmpStringName = settings[XML_SETTINGS_MSSQL][Constants.XML_SETTINGS_HA_CONNECTION_STRING_NAME].InnerText;
                    m_haConnectionString = loadConnectionString(tmpStringName);

                    m_defaulProgramID = GetFromXMLElement(settings, Constants.XML_SETTINGS_DEFAULT_PROGRAMID);
                    //m_spanishProgramID = GetFromXMLElement(settings, Constants.XML_SETTINGS_SPANISH_PROGRAMID);
                    m_defaultWorkgroupProgram = GetFromXMLElement(settings, Constants.XML_SETTINGS_DEFAULT_WORKGROUP_PROGRAM);
                    m_surveyAudioFilePath = GetFromXMLElement(settings, Constants.XML_SETTINGS_SURVEY_AUDIO_FILE_PATH);
                    GetWorkGroupMappings(settings);

                    //m_voiceCallProgramID = GetFromXMLElement(settings, Constants.XML_SETTINGS_VOICE_CALL_PROGRAMID);
                    // m_chatProgramID = GetFromXMLElement(settings, Constants.XML_SETTINGS_CHAT_PROGRAMID);
                    // m_emailProgramID = GetFromXMLElement(settings, Constants.XML_SETTINGS_EMAIL_PROGRAMID);
                    //GetLanguageMappings(settings[Constants.XML_SETTINGS_LANGUAGE_PROGRAM]);

                    haController.LastImportedDate = DateTime.Parse(settings[Constants.XML_SETTINGS_LAST_IMPORTED_DATE].InnerText);
                    haController.ResetLastImportedDate = "1".Equals(settings[Constants.XML_SETTINGS_RESET_LAST_IMPORTED_DATE].InnerText);

                    m_audioFetchingTempPath = settings[Constants.XML_SETTINGS_AUDIO_FETCH_TEMP_PATH].InnerText;

                    if (!string.IsNullOrEmpty(GetFromXMLElement(settings, Constants.XML_SETTINGS_RECORDING_TAG)))
                    {
                        m_recordingTag = new List<string>();
                        m_recordingTag.Add(GetFromXMLElement(settings, Constants.XML_SETTINGS_RECORDING_TAG));
                    }

                    try
                    {
                        string cic_connection_string = loadConnectionString(settings[XML_SETTINGS_CIC][XML_SETTINGS_CIC_PRIMARY_CONNECTION_STRING_NAME].InnerText);
                        CICSession.Instance.PrimaryCredentials = new CICCredentials(cic_connection_string);

                        if (settings[XML_SETTINGS_CIC][XML_SETTINGS_CIC_BACKUP_CONNECTION_STRING_NAME] != null)
                        {
                            cic_connection_string = loadConnectionString(settings[XML_SETTINGS_CIC][XML_SETTINGS_CIC_BACKUP_CONNECTION_STRING_NAME].InnerText);
                            if (!string.IsNullOrEmpty(cic_connection_string))
                            {
                                CICSession.Instance.BackupCredentials = new CICCredentials(cic_connection_string);
                            }
                        }

                        if (!String.IsNullOrEmpty(GetFromXMLElement(settings, Constants.XML_SETTINGS_TIMEOUT)))
                        {
                            CICSession.Instance.Timeout = Int32.Parse(GetFromXMLElement(settings, Constants.XML_SETTINGS_TIMEOUT));
                        }

                        if (!String.IsNullOrEmpty(GetFromXMLElement(settings, Constants.XML_SETTINGS_RETRYCOUNT)))
                        {
                            CICSession.Instance.RetryCount = Int32.Parse(GetFromXMLElement(settings, Constants.XML_SETTINGS_RETRYCOUNT));
                        }
                        //m_cic_Credentials = new CICCredentials(cic_connection_string);
                    }
                    catch (Exception e)
                    {
                        TraceTopic.pureConnectTopic.error("Error Getting CIC Connection String", e);
                    }

                    string tempDebugFakeDownload = GetFromXMLElement(settings, Constants.XML_SETTINGS_DEBUG_FAKE_DOWNLOAD);
                    if (!string.IsNullOrEmpty(tempDebugFakeDownload))
                    {
                        m_debugFakeDownload = "1".Equals(tempDebugFakeDownload);
                    }
                    string backupXML = GetFromXMLElement(settings, Constants.XML_SETTINGS_BACKUP_XML);
                    if (!string.IsNullOrEmpty(backupXML))
                    {
                        m_backupXML = "1".Equals(backupXML);
                    }
                    m_ffmpegPath = GetFromXMLElement(settings, XML_SETTINGS_FFMPEG_PATH);
                    m_ffmpegArgs = GetFromXMLElement(settings, Constants.XML_SETTINGS_FFMPEG_ARGS);

                    m_maxAttemptsToDownloadAudio =
                        int.Parse(settings[Constants.XML_SETTINGS_MAX_ATTEMPTS_TO_DOWNLOAD_AUDIO].InnerText);
                    m_sleepTimeAfterAudioDownloadFailure =
                        new TimeSpan(
                            0,
                            int.Parse(settings[Constants.XML_SETTINGS_SLEEP_TIME_AFTER_AUDIO_DOWNLOAD_FAILURE_IN_MINUTES].InnerText),
                            0);

                    string tmpDelay = GetFromXMLElement(settings, Constants.XML_SETTINGS_DELAY_BETWEEN_AUDIO_DOWNLOADS_MS);
                    m_delayBetweenAudioDownloads = string.IsNullOrEmpty(tmpDelay) ?
                        DEFAULT_DELAY_BETWEEN_AUDIO_DOWNLOADS :
                        new TimeSpan(0, 0, 0, 0, int.Parse(tmpDelay));

                    m_haRole = settings[Constants.XML_SETTINGS_HA_ROLE].InnerText;
                    m_haSite = settings[Constants.XML_SETTINGS_HA_SITE].InnerText;
                    m_haHeartbeatSecs = new TimeSpan(
                            0,
                            0,
                            int.Parse(settings[Constants.XML_SETTINGS_HEARTBEAT_INTERVAL_SECS].InnerText));

                    m_pollIntervalSecs = int.Parse(settings[XML_SETTINGS_POLL_INTERVAL].InnerText);
                    m_batchSize = int.Parse(settings[XML_SETTINGS_BATCH_SIZE].InnerText);

                    m_numThreads = int.Parse(settings[XML_SETTINGS_NUM_THREADS].InnerText);

                    m_shouldUpdateAgents = bool.Parse(settings[XML_SETTINGS_UPDATE_AGENTS].InnerText);

                    string meta;

                    if (!string.IsNullOrEmpty(GetFromXMLElement(settings, Constants.XML_SETTINGS_CALL_METAFIELDS)))
                    {
                        meta = settings[Constants.XML_SETTINGS_CALL_METAFIELDS].InnerText;
                        m_allMetaFileds[Constants.Voice] = ParseKeyValuePairs(meta);
                    }
                    if (!string.IsNullOrEmpty(GetFromXMLElement(settings, Constants.XML_SETTINGS_CHAT_METAFIELDS)))
                    {
                        meta = settings[Constants.XML_SETTINGS_CHAT_METAFIELDS].InnerText;
                        m_allMetaFileds[Constants.Chat] = ParseKeyValuePairs(meta);
                    }
                    if (!string.IsNullOrEmpty(GetFromXMLElement(settings, Constants.XML_SETTINGS_EMAIL_METAFIELDS)))
                    {
                        meta = settings[Constants.XML_SETTINGS_EMAIL_METAFIELDS].InnerText;
                        m_allMetaFileds[Constants.EMail] = ParseKeyValuePairs(meta);
                    }

                    if (!string.IsNullOrEmpty(GetFromXMLElement(settings, Constants.XML_SETTINGS_SURVEY_METAFIELDS)))
                    {
                        meta = settings[Constants.XML_SETTINGS_SURVEY_METAFIELDS].InnerText;
                        m_allMetaFileds[Constants.Survey] = ParseKeyValuePairs(meta);
                    }

                    m_leftChannelSpeaker = GetFromXMLElement(settings, XML_SETTINGS_LEFT_CHANNEL_SPEAKER);
                    m_rightChannelSpeaker = GetFromXMLElement(settings, XML_SETTINGS_RIGHT_CHANNEL_SPEAKER);
                    m_agentId = GetFromXMLElement(settings, XML_SETTINGS_AGENT_ID);

                    if (settings[Constants.XML_SETTINGS_AGENTS] != null)
                    {
                        if (settings[Constants.XML_SETTINGS_AGENTS].HasChildNodes)
                        {
                            foreach (XmlNode node in settings[Constants.XML_SETTINGS_AGENTS].ChildNodes)
                            {
                                if (node.Name.ToString() == Constants.XML_SETTINGS_AGENTS_HIERARCHY)
                                    m_agentsHierarhy = node.InnerText.ToLower();
                            }
                        }
                    }
                    if (settings[Constants.XML_SETTINGS_SUPERVISOR] != null)
                    {
                        m_supervisor = settings[Constants.XML_SETTINGS_SUPERVISOR].InnerText.ToLower();
                    }
                    else
                    {
                        m_supervisor = "supervisor";
                    }

                    if (settings[Constants.XML_SETTINGS_CUSTOM_ATTRIBUTE] != null)
                    {
                        m_customAttribute = settings[Constants.XML_SETTINGS_CUSTOM_ATTRIBUTE].InnerText.ToLower();
                    }
                    else
                    {
                        m_customAttribute = "agent_hierarchy";
                    }

                    if (settings[Constants.XML_SETTINGS_DOWNLOAD_THRESHOLD] != null)
                    {
                        m_downloadThreshold = int.Parse(settings[Constants.XML_SETTINGS_DOWNLOAD_THRESHOLD].InnerText);
                    }


                    if (settings[Constants.XML_SETTINGS_TRANSCODE_THRESHOLD] != null)
                    {
                        m_transcodeThreshold = int.Parse(settings[Constants.XML_SETTINGS_TRANSCODE_THRESHOLD].InnerText);
                    }

                    if (settings[Constants.XML_SETTINGS_TRANSCODE_FOLDER_MONITOR_TIME] != null)
                    {
                        m_transcodeFolderMonitorTime = int.Parse(settings[Constants.XML_SETTINGS_TRANSCODE_FOLDER_MONITOR_TIME].InnerText);
                    }

                    if (settings[Constants.XML_SETTINGS_TRANSCODE_FOLDER_LIFE_TIME] != null)
                    {
                        m_transcodeFolderLifeTime = int.Parse(settings[Constants.XML_SETTINGS_TRANSCODE_FOLDER_LIFE_TIME].InnerText);
                    }

                    if (settings[Constants.XML_SETTINGS_TRANSCODE_FOLDER_FILE_COUNT] != null)
                    {
                        m_transcodeFolderFileCount = int.Parse(settings[Constants.XML_SETTINGS_TRANSCODE_FOLDER_FILE_COUNT].InnerText);
                    }

                }
                catch (Exception ex)
                {
                    TraceTopic.pureConnectTopic.error("Error in loadFromXML", ex);
                    throw;
                }
            }
        }

        /*  private void GetLanguageMappings(XmlElement languageMappings)
          {
              if (languageMappings != null && languageMappings.ChildNodes.Count > 0)
              {
                  foreach (XmlNode language in languageMappings.ChildNodes)
                  {
                      if (language.Attributes.Count > 0)
                      {
                          m_languageProgramIDs[language.Attributes["name"].Value.ToLowerInvariant()] = language.InnerText;
                      }
                  }
              }

          }*/
        private void GetWorkGroupMappings(XmlElement settings)
        {
            if (settings[Constants.XML_SETTINGS_WORKGROUP_PROGRAMID] != null && settings[Constants.XML_SETTINGS_WORKGROUP_PROGRAMID].HasChildNodes)
            {
                foreach (XmlNode node in settings[Constants.XML_SETTINGS_WORKGROUP_PROGRAMID].ChildNodes)
                {
                    string nodeName = node.Name.ToString();
                    if (nodeName.Contains(Constants.XML_SETTINGS_PROGRAM))
                    {
                        if (!m_workgroupProgramIDs.ContainsKey(nodeName))
                        {
                            m_workgroupProgramIDs.Add(nodeName, node.InnerText);
                        }
                    }
                }
            }

            if (settings[Constants.XML_SETTINGS_WORKGROUP_MAPPING] != null && settings[Constants.XML_SETTINGS_WORKGROUP_MAPPING].HasChildNodes)
            {
                foreach (XmlNode node in settings[Constants.XML_SETTINGS_WORKGROUP_MAPPING].ChildNodes)
                {
                    string nodeName = node.Name.ToString();
                    if (nodeName.Contains(Constants.XML_SETTINGS_PROGRAM))
                    {
                        List<String> workgroups = node.InnerText.Split(',').ToList();
                        string key = m_workgroupProgramIDs.ContainsKey(nodeName) ? m_workgroupProgramIDs[nodeName] : null;
                        if (key != null && (!m_workgroupMappings.ContainsKey(key)))
                            m_workgroupMappings.Add(key, workgroups);
                    }
                }
            }
        }

        /// <summary>
        /// Move a File
        /// </summary>
        /// <param name="filesToMove"></param>
        private void MoveAllFilesInList(List<string> filesToMove)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    foreach (string file in filesToMove.ToList())
                    {
                        try
                        {
                            string folder = Path.GetDirectoryName(file);
                            folder = folder + "\\Backup\\";
                            string fileName = Path.GetFileName(file);
                            string backupFile = folder + fileName;
                            Directory.CreateDirectory(folder);
                            TraceTopic.pureConnectTopic.note("Attempting to Move File: {} To: {}", file, backupFile);
                            //File.Move(file, backupFile);
                            File.Copy(file, backupFile, true);
                            File.Delete(file);
                            TraceTopic.pureConnectTopic.note("Moved File: {} To: {}", file, backupFile);
                            filesToMove.Remove(file);
                        }
                        catch (Exception e)
                        {
                            TraceTopic.pureConnectTopic.error("Error Moving File: {}", file);
                            TraceTopic.pureConnectTopic.exception(e);
                        }
                    }
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.error("Error Moving All Files");
                    TraceTopic.pureConnectTopic.exception(e);
                }
            }
        }

        #region Agents

        public override void UpdateAgents()
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                bUpdateAgentsInProgress = true;
                try
                {

                    base.LoadXML();
                    this.loadFromXML();

                    Agent[] agentEntities = GetAgents();

                    if (agentEntities == null)
                    {
                        TraceTopic.pureConnectTopic.note("No Agent Data to Update. The value was NULL");
                        return;
                    }

                    if (agentEntities.Length == 0)
                    {
                        TraceTopic.pureConnectTopic.note("No Agent Data to Update. AgentEntities was Empty");
                        return;
                    }

                    PrintAgentHierarchy(agentEntities);

                    SetAgents(agentEntities);

                    TraceTopic.pureConnectTopic.note("Updated agents");
                }
                catch (Exception e)
                {
                    doError(e.Message, e.ToString());
                }
                finally
                {
                    bUpdateAgentsInProgress = false;
                }
            }
        }

        private void SetAgents(Agent[] agentEntities)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                Dictionary<string, string[]> doneAgents = new Dictionary<string, string[]>();
                UpdateAgents upd = new UpdateAgents();
                string tableName = "";

                SqlConnection con = new SqlConnection(m_localdbMaster.conStr);
                con.Open();
                SqlTransaction transaction = con.BeginTransaction();

                try
                {

                    lock (m_localdbMaster)
                    {
                        tableName = upd.GetTableName(Constants.CreateTemporaryTableSproc, con, transaction);

                        foreach (var agent in agentEntities)
                        {
                            int agentId = setAgent(agent, doneAgents, upd, tableName.ToString(), con, transaction);
                            if (agentId >= 0)
                            {
                                upd.UpdateTempTable(-1, agentId, tableName, Constants.UpdateTemporaryTableSproc, con, transaction);
                            }
                        }

                        upd.UpdateAgentHierarchy(tableName, Constants.UpdateAgentHierarchyTblSproc, con, transaction);

                        upd.DeleteTempTable(tableName, Constants.DeleteTemporaryTableSproc, con, transaction);

                        transaction.Commit();
                        con.Close();
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    con.Close();
                    TraceTopic.pureConnectTopic.error("Error in updating agent hierarchy.", ex);
                }
            }
        }

        private int setAgent(Agent agent, Dictionary<string, string[]> doneAgents, UpdateAgents upd, string tableName, SqlConnection con, SqlTransaction transaction)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    if (doneAgents.ContainsKey(agent.ID))
                    {
                        foreach (var child in agent.Children)
                        {
                            int childId = setAgent(child, doneAgents, upd, tableName, con, transaction);
                            if (childId > 0)
                            {
                                upd.UpdateTempTable(Convert.ToInt32(doneAgents[agent.ID][0]), childId, tableName, Constants.UpdateTemporaryTableSproc, con, transaction);

                            }
                        }
                        return Convert.ToInt32(doneAgents[agent.ID][0]);
                    }

                    DataTable results = upd.UpdateAgentEntity(agent.ID, agent.Name, Constants.UpdateAgentEntityTblSproc, con, transaction);

                    string id = results.Rows[0]["id"].ToString();
                    string updated = results.Rows[0]["updated"].ToString();
                    string oldName = results.Rows[0]["oldName"].ToString();

                    int agentId;

                    if (id == null)
                    {
                        TraceTopic.pureConnectTopic.warning("Could not add agent " + agent.Name + " because it already exists with a different ID from " + agent.ID);
                        agentId = -1;
                    }
                    else
                    {
                        agentId = Convert.ToInt32(id);
                        doneAgents.Add(agent.ID, new string[] { id, updated, oldName });

                        foreach (var child in agent.Children)
                        {
                            int childId = setAgent(child, doneAgents, upd, tableName, con, transaction);
                            if (childId > 0)
                            {
                                upd.UpdateTempTable(agentId, childId, tableName, Constants.UpdateTemporaryTableSproc, con, transaction);
                            }
                        }
                    }
                    return agentId;
                }
                catch (Exception ex)
                {
                    TraceTopic.pureConnectTopic.exception(ex);
                    throw;
                }
            }
        }

        protected override Agent[] GetAgents()
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    m_agentWorkgroups.Clear();

                    Dictionary<string, Agent> organisations = new Dictionary<string, Agent>();

                    TraceTopic.pureConnectTopic.note("Attempting to Open a CIC Session");
                    if (!CICSession.Instance.Open())
                    {
                        TraceTopic.pureConnectTopic.error("Unable to Open CIC Connection");
                        throw new Exception("Unable to Open CIC Connection");
                        //return false;
                    }

                    _userConfigurationList = CICSession.Instance.CreateUserConfigurationList();

                    if (_userConfigurationList == null || _userConfigurationList.Count == 0)
                    {
                        TraceTopic.pureConnectTopic.warning("No agents found in the session");
                        return null;
                    }

                    if (m_agentsHierarhy.Contains("customattribute"))
                    {
                        organisations = buildCustomAttribHierarchy();
                    }
                    else if (m_agentsHierarhy.Contains("organization"))
                    {
                        organisations = buildWorkgroupAttribHierarchy(m_agentsHierarhy);
                    }
                    else if (m_agentsHierarhy.Contains("supervisor"))
                    {
                        organisations = buildSupervisorAttribHierarchy();
                    }

                    return organisations.Values.ToArray();
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.error("Could not update agent hierarchy, leaving it as is.", e);
                    return null;
                }
                // Commented to avoid the session close during Audio Import.
                //finally
                //{
                //    CICSession.Instance.Close();
                //}
            }
        }

        private Dictionary<string, Agent> buildSupervisorAttribHierarchy()
        {
            Dictionary<string, Agent> organisations = new Dictionary<string, Agent>();
            using (TraceTopic.pureConnectTopic.scope())
            {
                int totalCount = 0;

                foreach (var user in _userConfigurationList)
                {
                    int fileCount = 0;

                    string sUserName, sName;

                    sUserName = user.ConfigurationId.DisplayName.ToString();
                    if (user.PersonalInformation.GivenName != null && user.PersonalInformation.GivenName.Value != null)
                    {
                        sName = user.PersonalInformation.GivenName.Value.ToString();
                    }
                    else
                    {
                        sName = sUserName;
                    }

                    foreach (var v in user.Workgroups.Value)
                    {
                        if (!user.Roles.Value.Any(x => x.DisplayName.ToLower() == m_supervisor))
                        {
                            List<UserConfiguration> supervisors = _userConfigurationList
                                .Where(x => x.ConfigurationId.Id != user.ConfigurationId.Id)
                                .Where(x => x.Workgroups.Value.Any(y => y.Id == v.Id))
                                .Where(x => x.Roles.Value.Any(y => y.DisplayName.ToLower() == m_supervisor))
                                .ToList();
                            foreach (var workgpUser in supervisors)
                            {

                                //Get the Supervisor
                                Agent supervisor = null;

                                if (!organisations.TryGetValue(workgpUser.ConfigurationId.DisplayName, out supervisor))
                                {
                                    supervisor = new Agent(workgpUser.ConfigurationId.DisplayName, workgpUser.ConfigurationId.DisplayName);
                                    organisations.Add(workgpUser.ConfigurationId.DisplayName, supervisor);
                                }
                                else
                                {
                                    TraceTopic.pureConnectTopic.note("Supervisor Exists");
                                }

                                m_agentWorkgroups[sUserName] = "/" + workgpUser.ConfigurationId.DisplayName;
                                Agent agent = null;
                                //Get the NTLOGIN
                                agent = Agent.FindAgentById(sUserName, supervisor.Children);
                                if (agent == null)
                                {
                                    agent = new Agent(sName, sUserName);
                                    supervisor.AddChild(agent);
                                    fileCount++;
                                }
                                TraceTopic.pureConnectTopic.note("Added {} agents to hierarchy from CIC session.", fileCount);
                                totalCount += fileCount;

                            }
                        }
                    }
                }
                TraceTopic.pureConnectTopic.note("Total {} agents added to hierarchy.", totalCount);
            }
            return organisations;
        }

        private Dictionary<string, Agent> buildWorkgroupAttribHierarchy(string m_agentsHierarhy)
        {
            Dictionary<string, Agent> organisations = new Dictionary<string, Agent>();
            using (TraceTopic.pureConnectTopic.scope())
            {
                int totalCount = 0;

                foreach (var user in _userConfigurationList)
                {
                    int fileCount = 0;

                    string sUserName, sName;

                    sUserName = user.ConfigurationId.DisplayName.ToString();

                    if (user.PersonalInformation.GivenName != null && user.PersonalInformation.GivenName.Value != null)
                    {
                        sName = user.PersonalInformation.GivenName.Value.ToString();
                    }
                    else
                    {
                        sName = sUserName;
                    }

                    foreach (var v in user.Workgroups.Value)
                    {
                        Agent organisation = null;
                        if (v.DisplayName != null && !organisations.TryGetValue(v.DisplayName, out organisation))
                        {
                            organisation = new Agent(v.DisplayName, v.DisplayName);
                            organisations.Add(v.DisplayName, organisation);
                        }

                        if (!m_agentsHierarhy.Contains("supervisor"))
                        {
                            m_agentWorkgroups[sUserName] = "/" + v.DisplayName;
                            Agent agent = null;

                            //Get the NTLOGIN
                            agent = Agent.FindAgentById(sUserName, organisation.Children);
                            if (agent == null)
                            {
                                agent = new Agent(sName, sUserName);
                                organisation.AddChild(agent);
                                fileCount++;
                            }

                            TraceTopic.pureConnectTopic.note("Added {} agents to hierarchy from CIC session.", fileCount);
                            totalCount += fileCount;
                        }
                        else
                        {
                            if (!user.Roles.Value.Any(x => x.DisplayName.ToLower() == m_supervisor))
                            {
                                List<UserConfiguration> supervisors = _userConfigurationList
                                    .Where(x => x.ConfigurationId.Id != user.ConfigurationId.Id)
                                    .Where(x => x.Workgroups.Value.Any(y => y.Id == v.Id))
                                    .Where(x => x.Roles.Value.Any(y => y.DisplayName.ToLower() == m_supervisor))
                                    .ToList();

                                if (supervisors.Count == 0)
                                {
                                    m_agentWorkgroups[sUserName] = "/" + v.DisplayName;
                                    Agent agent = null;

                                    //Get the NTLOGIN
                                    agent = Agent.FindAgentById(sUserName, organisation.Children);
                                    if (agent == null)
                                    {
                                        agent = new Agent(sName, sUserName);
                                        organisation.AddChild(agent);
                                        fileCount++;
                                    }

                                    TraceTopic.pureConnectTopic.note("Added {} agents to hierarchy from CIC session.", fileCount);
                                    totalCount += fileCount;
                                }
                                else
                                {
                                    foreach (var workgpUser in supervisors)
                                    {

                                        //Get the Supervisor
                                        Agent supervisor = null;
                                        supervisor = Agent.FindAgentById(workgpUser.ConfigurationId.DisplayName, organisation.Children);

                                        if (supervisor == null)
                                        {
                                            supervisor = new Agent(workgpUser.ConfigurationId.DisplayName, workgpUser.ConfigurationId.DisplayName);
                                            organisation.AddChild(supervisor);
                                        }
                                        else
                                        {
                                            TraceTopic.pureConnectTopic.note("Supervisor Exists");
                                        }

                                        m_agentWorkgroups[sUserName] = "/" + workgpUser.ConfigurationId.DisplayName;
                                        Agent agent = null;

                                        //Get the NTLOGIN
                                        agent = Agent.FindAgentById(sUserName, supervisor.Children);
                                        if (agent == null)
                                        {
                                            agent = new Agent(sName, sUserName);
                                            supervisor.AddChild(agent);
                                            fileCount++;
                                        }

                                        TraceTopic.pureConnectTopic.note("Added {} agents to hierarchy from CIC session.", fileCount);
                                        totalCount += fileCount;

                                    }
                                }
                            }
                        }
                    }
                }
                TraceTopic.pureConnectTopic.note("Total {} agents added to hierarchy.", totalCount);
            }
            return organisations;
        }

        public Dictionary<string, Agent> buildCustomAttribHierarchy()
        {
            Dictionary<string, Agent> organisations = new Dictionary<string, Agent>();
            using (TraceTopic.pureConnectTopic.scope())
            {
                int totalCount = 0;

                foreach (var user in _userConfigurationList)
                {
                    int fileCount = 0;

                    string sUserName, sName, sSupervisor;

                    sUserName = user.ConfigurationId.DisplayName.ToString();
                    if (user.PersonalInformation.GivenName != null && user.PersonalInformation.GivenName.Value != null)
                    {
                        sName = user.PersonalInformation.GivenName.Value.ToString();
                    }
                    else
                    {
                        sName = sUserName;
                    }

                    sSupervisor = null;

                    foreach (var custom in user.StandardProperties.CustomAttributes.Value)
                    {
                        if (custom.Key.ToLower() == m_customAttribute)
                        {
                            sSupervisor = custom.Value.ToString();
                            break;
                        }
                    }

                    //Get the Supervisor
                    Agent supervisor = null;

                    if (sSupervisor != null && !organisations.TryGetValue(sSupervisor, out supervisor))
                    {
                        supervisor = new Agent(sSupervisor, sSupervisor);
                        organisations.Add(sSupervisor, supervisor);
                        m_agentWorkgroups[sUserName] = "/" + sSupervisor;
                        Agent agent = null;
                        //Get the NTLOGIN
                        agent = Agent.FindAgentById(sUserName, supervisor.Children);
                        if (agent == null)
                        {
                            agent = new Agent(sName, sUserName);
                            supervisor.AddChild(agent);
                            fileCount++;
                        }
                        TraceTopic.pureConnectTopic.note("Added {} agents to hierarchy from CIC session.", fileCount);
                        totalCount += fileCount;

                    }
                }
                TraceTopic.pureConnectTopic.note("Total {} agents added to hierarchy.", totalCount);
            }
            return organisations;
        }

        /// <summary>
        /// Used to create a text representation of the Agent Hierarchy
        /// </summary>
        /// <param name="agent"></param>
        /// <param name="sb"></param>
        /// <param name="sep"></param>
        /// <returns></returns>
        private StringBuilder AddIdSet(Agent agent, StringBuilder sb, string sep)
        {
            sep = sep + "---";
            sb.AppendLine(sep + ">" + agent.Name);
            foreach (Agent ag in agent.Children)
            {
                AddIdSet(ag, sb, sep);
            }
            return sb;
        }

        /// <summary>
        /// Print out the Agent Hierarchy as constructed by UConnector
        /// </summary>
        /// <param name="agentEntities"></param>
        private void PrintAgentHierarchy(Agent[] agentEntities)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                StringBuilder sb = new StringBuilder();
                HashSet<string> agentHierarchy = new HashSet<string>();
                foreach (Agent agent in agentEntities)
                {
                    StringBuilder sb1 = new StringBuilder();
                    sb1.AppendLine("");
                    sb.Append(AddIdSet(agent, sb1, ""));
                    sb.AppendLine("");
                }
                TraceTopic.pureConnectTopic.note(sb.ToString());
            }
        }

        #endregion Agents

        #region Import

        /// <summary>
        /// Import the Next Batch of Recordings. UpdateAgents Do SQL Queries Download Recordings Create XML Meta Data
        /// </summary>
        /// <returns></returns>
        protected override bool ImportNextBatch()
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                TraceTopic.pureConnectTopic.note("ImportNextBatch");

                processedDates.Clear();
                var watch = Stopwatch.StartNew();
                bool secondarySite = !Constants.HA_PRIMARY_SITE.Equals(m_haSite);


                TraceTopic.pureConnectTopic.note("Attempting to Open a CIC Session");
                if (!CICSession.Instance.Open())
                {
                    TraceTopic.pureConnectTopic.error("Unable to Open CIC Connection");
                    throw new Exception("Unable to Open CIC Connection");

                }


                if (secondarySite && !haController.IsHAPrimarySiteDown)
                {
                    TraceTopic.pureConnectTopic.note("This UC instance is located on recovery site and primary site is still running.");
                    return false;
                }

                try
                {
                    if (!haController.ElectWorkingInstance())
                    {
                        TraceTopic.pureConnectTopic.note("This UC instance has lost elections, other instance is processing recordings.");
                        return false;
                    }
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.error("There was error during elections of working UC instance.", e);
                    return false;
                }

                try
                {
                    loadFromXML();
                    if (m_shouldUpdateAgents && !bUpdateAgentsInProgress)
                    {
                        UpdateAgents();
                    }
                    else
                    {
                        TraceTopic.pureConnectTopic.note("UpdateAgents already in Progress, do not start another one");
                    }

                    TraceTopic.pureConnectTopic.note("D&G I3 importer - starting next batch");
                    TraceTopic.pureConnectTopic.note("Last imported Date: {}", haController.LastImportedDate.ToString(Constants.ISO_8601_FORMAT));

                    m_currentDate = haController.LastImportedDate;

                    RecordingQueryData recordingsData = new RecordingQueryData();
                    if (!recordingsData.GetData(haController.LastImportedDate.ToString(Constants.ISO_8601_FORMAT), m_batchSize, m_allMetaFileds))
                    {
                        return false;
                    }

                    string sInteractionIds = string.Join(",", recordingsData.interactionIdstoRecordingIds.Keys);

                    if (this.CancelRequested)
                    {
                        return false;
                    }


                    InteractionSummaryQueryData summaryData = new InteractionSummaryQueryData();
                    summaryData.GetData(sInteractionIds, m_allMetaFileds, recordingsData.calls);


                    if (this.CancelRequested)
                    {
                        return false;
                    }


                    /* DailerCallHistoryQueryData dailerQueryData = new DailerCallHistoryQueryData();
                     dailerQueryData.GetData(sInteractionIds, m_allMetaFileds, recordingsData.calls);*/


                    if (this.CancelRequested)
                    {
                        return false;
                    }

                    CallSegmentQueryData callSegment = new CallSegmentQueryData();
                    callSegment.GetData(sInteractionIds, recordingsData.calls);


                    if (this.CancelRequested)
                    {
                        return false;
                    }

                    SecurePauseQueryData securePause = new SecurePauseQueryData();
                    string sRecordingIDs = string.Join(",", recordingsData.recordingIdsToInteractionIds.Keys);

                    securePause.GetData(sRecordingIDs, recordingsData.calls, recordingsData.recordingIdsToInteractionIds);


                    if (this.CancelRequested)
                    {
                        return false;
                    }

                    if (!this.CancelRequested)
                    {
                        RelatedRecordingIdQueryData relatedRecordings = new RelatedRecordingIdQueryData();
                        relatedRecordings.GetData(recordingsData.calls, recordingsData.relatedRecordingIds);
                    }


                    if (!this.CancelRequested)
                    {
                        SurveyDetailQueryData surveyDetails = new SurveyDetailQueryData();
                        surveyDetails.GetData(sInteractionIds, recordingsData.calls, m_surveyAudioFilePath);
                    }

                    if (this.CancelRequested)
                    {
                        return false;
                    }

                    TraceTopic.pureConnectTopic.note("Calls imported: {}", recordingsData.calls.Count);
                    TraceTopic.pureConnectTopic.always("##Import Start");

                    // Split rows between worker threads
                    List<CallData>[] lists = new List<CallData>[m_numThreads];
                    for (int i = 0; i < m_numThreads; i++)
                    {
                        lists[i] = new List<CallData>();
                    }

                    int index = 0;
                    foreach (CallData call in recordingsData.calls.Values)
                    {
                        lists[index % m_numThreads].Add(call);
                        index++;
                    }

                    // Init lock that sync the workers and the current thread
                    m_countdownLatch = new CountdownLatch(m_numThreads);

                    // Start the threads
                    TraceTopic.pureConnectTopic.note("Starting {} processing threads.", m_numThreads);
                    for (int i = 0; i < m_numThreads; i++)
                    {
                        TraceTopic.pureConnectTopic.note("Thread {} gets {1} rows.", i, lists[i].Count);

                        Thread t = new Thread(this.ImporterThreadWorker);
                        t.Name = "ImpWorker(" + i + ")";
                        t.IsBackground = true;
                        t.Start(lists[i]);
                    }

                    TraceTopic.pureConnectTopic.note("Waiting for all worker threads to finish work.");
                    m_countdownLatch.Wait();
                    TraceTopic.pureConnectTopic.note("All workers completed their work - continue main thread.");

                    //Get the date time of the last interaction that was processed
                    if (processedDates.Count > 0)
                    {
                        TraceTopic.pureConnectTopic.note("Processed Records: " + processedDates.Count + " Find LastProcessedDate");
                        foreach (DateTime timestamp in processedDates)
                        {
                            m_currentDate = m_currentDate > timestamp ? m_currentDate : timestamp;
                        }
                        haController.SaveNextDateToImport(m_currentDate);
                        haController.LastImportedDate = m_currentDate.Value;
                    }
                    else
                    {
                        TraceTopic.pureConnectTopic.note("No Records were Processed Succesfully, to avoid a block in UConnector update the last processed date to move on to the next batch");
                        if (recordingsData.calls.Count > 0)
                        {
                            foreach (CallData call in recordingsData.calls.Values)
                            {
                                m_currentDate = m_currentDate > call.CallTime ? m_currentDate : call.CallTime;
                            }
                            // If there was data and everything was imported successfully, update the xml config
                            haController.SaveNextDateToImport(m_currentDate);
                            haController.LastImportedDate = m_currentDate.Value;
                        }
                    }
                    processedDates.Clear();
                    if (recordingsData.EndAfterThisImport)
                    {
                        TraceTopic.pureConnectTopic.always("Records Retrieved was less than Batch Size so end the ImportProcess until the next Poll interval");
                        return false;
                    }
                    return true;
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.error("D&G I3 importer - Error occurred in importNextBatch", e);
                    return false;
                }
                finally
                {
                    try
                    {
                        TraceTopic.pureConnectTopic.note("Delete Election Record");
                        haController.DeleteElectionRecord();
                    }
                    catch (Exception e)
                    {
                        TraceTopic.pureConnectTopic.error("Could not delete election record.", e);
                    }
                    TraceTopic.pureConnectTopic.note("D&G I3 importer - UConnector End of batch.");

                    TraceTopic.pureConnectTopic.always("##Elapsed ##Import End: " + watch.ElapsedMilliseconds + "ms");

                }
            }
        }

        #endregion Import

        #region DBReconnect

        protected bool ReconnectToSMDB()
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    TraceTopic.pureConnectTopic.always("Reconnecting to SMDB.");
                    m_localdbMaster = new DBServices();
                    TraceTopic.pureConnectTopic.note("Speechminer connection string: {}", m_speechMinerConnectionString);
                    m_localdbMaster.setDB(m_speechMinerConnectionString, true);
                    m_dbConnected = true;
                    return true;
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.error("Could not reconnect to SMDB.", e);
                    return false;
                }
            }
        }

        #endregion DBReconnect

        #region Audio

        public override bool GenerateAudioFile(string _filename, string _outPath = "", string _inum = "", string _recorderPath = "", string _callTime = null, System.Data.DataRow m_dataRow = null)
        {
            // Not our case
            throw new NotImplementedException();
        }

        private bool ConvertAudio(string source, string wavPcmOutput)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                if (m_ffmpegPath == null || m_ffmpegArgs == null)
                {
                    try
                    {
                        File.Copy(source, wavPcmOutput);
                        return true;
                    }
                    catch (Exception e)
                    {
                        TraceTopic.pureConnectTopic.error("Error Copying Audio File", e);
                        return false;
                    }
                }
                else
                {
                    return ConvertFileToPCM(source, wavPcmOutput);
                }
            }
        }

        /// <summary>
        /// User ffmeg to convert from .opus to .wav Convert to a local folder first and then move to the SM Output folder
        /// </summary>
        /// <param name="inputAudioPath"></param>
        /// <param name="outputAudioPath"></param>
        /// <returns></returns>
        private bool ConvertFileToPCM(string inputAudioPath, string outputAudioPath)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                long freeSpace = 0, totalNumberOfBytes = 0, totalNumberOfFreeBytes = 0;

                if (GetDiskFreeSpaceEx(Path.GetPathRoot(outputAudioPath), ref freeSpace, ref totalNumberOfBytes, ref totalNumberOfFreeBytes))
                {
                    TraceTopic.pureConnectTopic.note("Available space in : {} - {} bytes  - {} GB", outputAudioPath, freeSpace, (freeSpace / 125000000));
                }
                else
                {
                    TraceTopic.pureConnectTopic.warning("Unable to get the free space for {}", Path.GetPathRoot(outputAudioPath));
                }


                var watch = Stopwatch.StartNew();
                // FFMpeg will wait for confirmation to overwrite if the file exists.
                if (File.Exists(outputAudioPath))
                {
                    File.Delete(outputAudioPath);
                }

                string tempOutput = Path.Combine(m_audioFetchingTempPath, Path.GetFileName(outputAudioPath));
                tempOutput = Path.ChangeExtension(tempOutput, "wav");

                if (File.Exists(tempOutput))
                {
                    File.Delete(tempOutput);
                }
                try
                {
                    string convertCmd = m_ffmpegPath;
                    string convertArg = m_ffmpegArgs
                        .Replace("%INPUT%", inputAudioPath)
                        .Replace("%OUTPUT%", tempOutput);

                    var startInfo = new ProcessStartInfo(convertCmd, convertArg)
                    {
                        CreateNoWindow = true,
                        RedirectStandardError = true,
                        RedirectStandardOutput = true,
                        UseShellExecute = false
                    };

                    var output = new StringBuilder();
                    var outputHandler = new DataReceivedEventHandler(
                        delegate (object sendingProc, DataReceivedEventArgs args)
                        {
                            output.AppendLine(args.Data);
                        });

                    TraceTopic.pureConnectTopic.note("Executing: {} {}", convertCmd, convertArg);

                    var proc = new Process() { StartInfo = startInfo };
                    proc.ErrorDataReceived += outputHandler;
                    proc.OutputDataReceived += outputHandler;

                    proc.Start();

                    proc.BeginErrorReadLine();
                    proc.BeginOutputReadLine();
                    proc.WaitForExit();

                    if (proc.ExitCode != 0) //|| File.Exists(tempOutput) == false)
                    {
                        var logFilePath = Path.ChangeExtension(inputAudioPath, "log");
                        using (var log = new StreamWriter(File.OpenWrite(logFilePath)))
                        {
                            log.Write(output.ToString());
                        }

                        //try
                        //{
                        //    //If the file conversion failed try and delete the .wav file if it is created.
                        //    if (File.Exists(tempOutput))
                        //    {
                        //        TraceTopic.pureConnectTopic.note("File Conversion Failed, Delete File: " + tempOutput);
                        //        File.Delete(tempOutput);
                        //    }
                        //}
                        //catch (Exception e)
                        //{
                        //    TraceTopic.pureConnectTopic.note("Failed to Delete Failed Conversion: " + tempOutput, e);
                        //}

                        TraceTopic.pureConnectTopic.error("Encountered an error converting {} to PCM. See {} for details.", inputAudioPath, logFilePath);
                        //return false;
                        if (!File.Exists(tempOutput))
                        {
                            //Case# 0002804477 - FFMPEG fails to convert few media files of duration less than 1 secs. The meta-data of those medias that fails to convert were not published to speechminer and discarded as error.
                            //Since the media is negligible which is less than a second, the customer wants to upload a dummy media file along with the corresponding meta-data to make the interaction counted under speechminer.
                            //We observed during few instances, speechminer do create wav file although the process exited with error code. 
                            //When the FFMPEG process exited with error code, but the wav file was created, we stopped deleting the converted wav files. Instead we upload the file to speechiner along with meta-data and log the error in uConnector.
                            //If the file  failed to create and the process exited with error code, we use the sample wav file of negligible audio duration stored in the executable path for the speechminer to upload the interaction.
                            //Note: We wont determine the cause of the FFMPEG process error.
                            //Note: This applies to all process errors irrespective of the duration of the media.
                            string path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                            File.Copy(Path.Combine(path, "sample.wav"), tempOutput);
                        }
                    }
                    TraceTopic.pureConnectTopic.always("##Elapsed Converted: " + inputAudioPath + " To: " + tempOutput + " in " + watch.ElapsedMilliseconds + "ms");

                    if (File.Exists(tempOutput))
                    {
                        var watchMove = Stopwatch.StartNew();
                        File.Move(tempOutput, outputAudioPath);

                        TraceTopic.pureConnectTopic.always("##Elapsed File: " + tempOutput + "  Moved to: " + outputAudioPath + "  in: " + watchMove.ElapsedMilliseconds + "ms");
                        return true;
                    }
                    else
                    {
                        TraceTopic.pureConnectTopic.error("There was unexpected error converting {} to PCM.", inputAudioPath);
                        return false;
                    }
                }

                catch (Exception ex)
                {
                    TraceTopic.pureConnectTopic.error("Encountered an error converting {} to PCM. See {} for details.", inputAudioPath, ex);

                    if (File.Exists(tempOutput))
                    {
                        File.Delete(tempOutput);
                    }

                    if (File.Exists(inputAudioPath))
                    {
                        File.Delete(inputAudioPath);
                    }

                    return false;
                }
            }
        }

        /// <summary>
        /// Download the recordings from PureConnect
        /// </summary>
        /// <param name="recordingId"></param>
        /// <param name="saveToPath"></param>
        /// <param name="i3Fetcher"></param>
        private void DownloadAudio(string recordingId, string saveToPath, I3Fetcher i3Fetcher)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                var watch = Stopwatch.StartNew();
                try
                {
                    TraceTopic.pureConnectTopic.always("DownloadAudio: " + recordingId);
                    string tmpAudioPath = Path.Combine(m_audioFetchingTempPath, Path.GetFileName(saveToPath));
                    tmpAudioPath = Path.ChangeExtension(tmpAudioPath, ".opus");

                    if (File.Exists(saveToPath))
                    {
                        TraceTopic.pureConnectTopic.note("Deleting existing audio file from path: {}.", saveToPath);
                        File.Delete(saveToPath);
                    }

                    if (File.Exists(tmpAudioPath))
                    {
                        TraceTopic.pureConnectTopic.note("Deleting existing temp audio file from path: {}", tmpAudioPath);
                        File.Delete(tmpAudioPath);
                    }

                    string args = recordingId + " " + tmpAudioPath;
                    TraceTopic.pureConnectTopic.note("Getting audio file for args: {}", args);
                    bool success = false;
                    if (!m_debugFakeDownload)
                    {
                        try
                        {
                            var watchDownload = Stopwatch.StartNew();
                            success = i3Fetcher.DownloadFile(recordingId, tmpAudioPath);
                            watchDownload.Stop();

                            TraceTopic.pureConnectTopic.verbose("{} Download completed for recording id: {} in {}ms", recordingId, recordingId, watch.ElapsedMilliseconds);

                            if (watchDownload.ElapsedMilliseconds > m_downloadThreshold)
                            {
                                string errorMessage = string.Format("Downloaded time exceeded for  recording id:{0} by {1} ms", recordingId, watchDownload.ElapsedMilliseconds - m_downloadThreshold);
                                logger.WriteEntry(errorMessage, EventLogEntryType.Warning, (int)ErrorCodes.DOWNLOAD_THRESHOLD_EXCEEDED);
                            }

                        }
                        catch (Exception e)
                        {
                            TraceTopic.pureConnectTopic.error("Error Downloading Recording: " + recordingId, e);
                        }
                    }
                    else
                    {
                        var watchDownload = Stopwatch.StartNew();
                        if (!File.Exists(tmpAudioPath))
                        {
                            try
                            {
                                string path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                                File.Copy(Path.Combine(path, "sample.opus"), tmpAudioPath);
                                success = true;
                            }
                            catch (Exception e)
                            {
                                TraceTopic.pureConnectTopic.error("Error doing Fake Download", e);
                            }

                            TraceTopic.pureConnectTopic.note("##Elapsed Simulated Download completed for recording id: {} in {}ms", recordingId, watchDownload.ElapsedMilliseconds + "ms");
                        }
                        watchDownload = null;
                    }

                    if (success)
                    {
                        var watchConvert = Stopwatch.StartNew();
                        if (!ConvertAudio(tmpAudioPath, saveToPath))
                        {
                            TraceTopic.pureConnectTopic.note("Conversion Failed: " + recordingId + " in " + watchConvert.ElapsedMilliseconds + "ms");
                            throw new Exception("Audio Conversion Failed");
                        }
                        watchConvert.Stop();
                        TraceTopic.pureConnectTopic.always("##Elapsed Converted : " + recordingId + " in " + watchConvert.ElapsedMilliseconds + "ms");

                        if (watchConvert.ElapsedMilliseconds > m_transcodeThreshold)
                        {
                            string errorMessage = string.Format("Conversion time exceeded for  recording id:{0} by {1} ms", recordingId,
                                 watchConvert.ElapsedMilliseconds - m_downloadThreshold);
                            logger.WriteEntry(errorMessage, EventLogEntryType.Warning, (int)ErrorCodes.CONVERSION_THRESHOLD_EXCEEDED);
                        }

                        if (File.Exists(tmpAudioPath))
                        {
                            try
                            {
                                File.Delete(tmpAudioPath);
                            }
                            catch (Exception e)
                            {
                                TraceTopic.pureConnectTopic.error("Error Deleting TempAudioFile: " + tmpAudioPath, e);
                            }
                        }

                        if (m_audioDownloadFailed)
                        {
                            m_audioDownloadFailed = false;

                            m_numOfFailedAudioDownloads = 0;
                        }

                        // There is a limit in the I3 system of 23 connections in 3 seconds, So this configurable delay should avoid reaching this limit There is a delay whilst the .opec is converted to wav, so see how long that delay is and if less then the configured delay time wait for that long
                        int elapsed = (int)watch.ElapsedMilliseconds;
                        int delay = (int)m_delayBetweenAudioDownloads.TotalMilliseconds - elapsed;
                        if (delay > 0)
                        {
                            TraceTopic.pureConnectTopic.note("##Elapsed Time to Convert RecordingsId {} {}ms, Configured Download Delay: {}ms, Delay for {}ms ", recordingId, elapsed, m_delayBetweenAudioDownloads.TotalMilliseconds, delay);
                            Thread.Sleep(delay);
                        }
                    }
                    else
                    {
                        // Download failed
                        m_numOfFailedAudioDownloads++;

                        if (m_numOfFailedAudioDownloads >= m_maxAttemptsToDownloadAudio)
                        {
                            TraceTopic.pureConnectTopic.warning("Unable to download audio files, going to sleep after {} attempts.",
                                m_numOfFailedAudioDownloads);

                            // Reset the failed attempts counter
                            m_numOfFailedAudioDownloads = 0;
                            m_audioDownloadFailed = true;

                            //SendNotificationEmail(m_downloadAudioFailedEmailMessage);

                            Thread.Sleep(m_sleepTimeAfterAudioDownloadFailure);
                        }
                        else
                        {
                            int elapsed = (int)watch.ElapsedMilliseconds;
                            int delay = (int)m_delayBetweenAudioDownloads.TotalMilliseconds - elapsed;
                            if (delay > 0)
                            {
                                TraceTopic.pureConnectTopic.verbose("##Elapsed Time for failed Download RecordingsId {} {}ms, Configured Download Delay: {}ms, Delay for {}ms ", recordingId, elapsed, m_delayBetweenAudioDownloads.TotalMilliseconds, delay);
                                Thread.Sleep(delay);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.error("Error in DownloadAudio: " + e);
                    throw;
                }
            }
        }

        #endregion Audio


        private void DownloadText(string recordingId, string saveToPath, I3Fetcher i3Fetcher, CallData call)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                var watch = Stopwatch.StartNew();
                try
                {
                    TraceTopic.pureConnectTopic.always("DownloadChat: " + recordingId);

                    if (File.Exists(saveToPath))
                    {
                        TraceTopic.pureConnectTopic.note("Deleting existing chat file from path: {}.", saveToPath);
                        File.Delete(saveToPath);
                    }

                    //string args = recordingId + " " + tmpAudioPath;
                    //TraceTopic.pureConnectTopic.note("Getting audio file for args: {0}", args);
                    bool success = false;
                    if (!m_debugFakeDownload)
                    {
                        try
                        {
                            success = i3Fetcher.DownloadFile(recordingId, saveToPath, call);
                        }
                        catch (Exception e)
                        {
                            TraceTopic.pureConnectTopic.error("Error Downloading Recording: " + recordingId, e);
                        }
                    }
                    else
                    {
                        var watchDownload = Stopwatch.StartNew();
                        if (!File.Exists(saveToPath))
                        {
                            try
                            {
                                string path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                                File.Copy(Path.Combine(path, "sample.txt"), saveToPath);
                                success = true;
                            }
                            catch (Exception e)
                            {
                                TraceTopic.pureConnectTopic.error("Error doing Fake Download", e);
                            }

                            TraceTopic.pureConnectTopic.note("##Elapsed Simulated Download completed for recording id: {} in {}ms", recordingId, watchDownload.ElapsedMilliseconds + "ms");
                        }
                        watchDownload = null;
                    }
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.error("Error in DownloadAudio: " + e);
                    throw;
                }
            }
        }

        private Dictionary<string, string> ParseKeyValuePairs(string setting)
        {
            if (setting == null)
            {
                return new Dictionary<string, string>();
            }

            Dictionary<string, string> ret = new Dictionary<string, string>();
            string[] fields = setting.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string f in fields)
            {
                string[] parts = f.Split(new char[] { '=' }, 2, StringSplitOptions.RemoveEmptyEntries);
                string key = parts[0].Trim();
                string newName = key;
                if (parts.Length > 1)
                {
                    newName = parts[1];
                }
                ret[key] = newName;
            }

            return ret;
        }
    }
}
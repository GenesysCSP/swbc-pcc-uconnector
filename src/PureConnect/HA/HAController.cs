﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using PureConnect.Data;
using utopy.bDBServices;
using GenUtils.TraceTopic;
using PureConnect.I3Audio;
using ININ.IceLib.Data.TransactionBuilder;
using System.Data;
using System.Data.SqlClient;

namespace PureConnect.HA
{
    public class HAController: BaseQueryData
    {

        private Timer m_heartbeatTimer;
        private TimeSpan m_haHeartbeatSecs;
        private int m_pollIntervalSecs = 600;
        private string m_haSite ;
        private string m_haRole;
        //public bool m_haPrimarySiteIsDown = false;
        private Random _rnd = new Random();

        private string XML_SETTINGS;

        public DBServices HaDb { get; set; }

        private XmlDocument m_xmlDoc;

        public bool IsHAPrimarySiteDown { get; set; }
        public DateTime LastImportedDate { get; internal set; }
        public bool ResetLastImportedDate { get; internal set; }

        private Importer_PureConnect importer;

        public HAController()
        {
        }

        public void SetHAController(int m_pollIntervalSecs, TimeSpan m_haHeartbeatSecs, string m_haSite , string m_haRole, XmlDocument m_xmlDoc, string XML_SETTINGS, Importer_PureConnect importer)
        {
            
            this.importer = importer;
            this.m_xmlDoc = m_xmlDoc;
            this.XML_SETTINGS = XML_SETTINGS;
            this.m_pollIntervalSecs = m_pollIntervalSecs;
            this.m_haHeartbeatSecs = m_haHeartbeatSecs;
            this.m_haSite = m_haSite;
            this.m_haRole = m_haRole;

        }

        public void DeleteElectionRecord()
        {
            // Delete any records with our own guid
            Dictionary<string, List<string>> param = new Dictionary<string, List<string>>();
            param.Add("@guid", new List<string>() { GetHaId(), "varchar", "50" });
            param.Add("@comment", new List<string>() { Constants.HA_COMMENT_DICE, "varchar", "-1" });
            
            var transactionData = new TransactionData { ProcedureName = Constants.DeleteElectionRecordSproc };

            GetDataSql(Constants.DeleteElectionRecordSproc, HaDb.conStr, param);
        }

        /// <summary>
        /// Decide which UConnector should process the next batch of recordings
        /// </summary>
        /// <returns></returns>
        public bool ElectWorkingInstance()
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    DateTime now = DateTime.UtcNow;
                    DateTime lastTime = now.AddSeconds(-m_pollIntervalSecs);

                    // columns: guid, val, ts, comment

                    TraceTopic.pureConnectTopic.note("Elections: own ID is {}, checking other records later than {}", GetHaId(), lastTime.ToString());
                                       
                    var dt = SelectOtherElectionRecords(lastTime);
                    if (dt.Rows.Count > 0)
                    {
                        string otherGUID = dt.Rows[0][0].ToString();
                        TraceTopic.pureConnectTopic.note("Elections: instance {} is already working. Exiting.", otherGUID);
                        return false;
                    }
                    
                    int dice = _rnd.Next(int.MaxValue);

                    while (true)
                    {
                        // insert own record
                        TraceTopic.pureConnectTopic.note("Elections: inserting own record.");
                        InsertElectionRecord(dice);

                        // select any dice record from the last batchinterval time period
                        TraceTopic.pureConnectTopic.note("Elections: checking other records.");
                        dt = SelectOtherElectionRecords(lastTime);
                        if (dt.Rows.Count <= 0)
                        {
                            TraceTopic.pureConnectTopic.note("Elections: no other records, I won.");
                            return true;
                        }
                        else if (dt.Rows.Count >= 1)
                        {
                            var record = dt.Rows[0];
                            var otherDice = (int)dt.Rows[0]["val"];
                            TraceTopic.pureConnectTopic.note("Elections: my dice: {}, other dice: {}.", dice, otherDice);
                            // if there is record with different guid and smaller dice value, continue execution
                            if (otherDice < dice)
                            {
                                TraceTopic.pureConnectTopic.note("Elections: I won.");
                                return true;
                            }

                            // if there is record with different guid and larger dice value, stop
                            if (otherDice > dice)
                            {
                                TraceTopic.pureConnectTopic.note("Elections: I lost.");
                                return false;
                            }
                            // if there is record with different guid and equal dice value, delete own record and loop again
                            TraceTopic.pureConnectTopic.note("Elections: tie. Deleting own record and trying again.");
                            DeleteElectionRecord();
                        }
                    }
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.error("There was error during election process.", e);
                    return false;
                }
            }
        }

        private string GetHaId()
        {
            return m_haSite + "-" + m_haRole;
        }
        
        /// <summary>
        /// Get any recent Heartbeat Records
        /// </summary>
        /// <param name="lastTime"></param>
        /// <returns></returns>
        private DataTable GetRecentHeartbeatRecords(DateTime lastTime)
        {
            Dictionary<string, List<string>> param = new Dictionary<string, List<string>>();
            param.Add("@lasttime", new List<string>() { DateTimeToString(lastTime), "varchar", "50" });
            param.Add("@comment", new List<string>() { Constants.HA_COMMENT_HEARTBEAT, "varchar", "-1" });
            
            return GetDataSql(Constants.GetRecentHeartbeatRecordsSproc, HaDb.conStr, param);
        }
        /// <summary>
        /// Check for other Heartbeats
        /// </summary>
        /// <param name="stateInfo"></param>
        private void Heartbeat(Object stateInfo)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    if (Constants.HA_PRIMARY_SITE.Equals(m_haSite))
                    {
                        // Main site only stores heartbeat records
                        InsertHeartbeatRecord();
                    }
                    else
                    {
                        // Secondary site checks if primary site saves heartbeats
                        DateTime now = DateTime.UtcNow;
                        DateTime lastTime = now.AddSeconds(-m_pollIntervalSecs);

                        var dt = GetRecentHeartbeatRecords(lastTime);

                        IsHAPrimarySiteDown = (dt != null && dt.Rows.Count <= 0);

                    }
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.error("There was error during heartbeat task execution.", e);
                }
            }
        }

        /// <summary>
        /// Insert an Election Record, the UConnector with the highest value wins and can process the batch??
        /// </summary>
        /// <param name="dice"></param>
        private void InsertElectionRecord(int dice)
        {
            Dictionary<string, List<string>> param = new Dictionary<string, List<string>>();
            param.Add("@guid", new List<string>() { GetHaId(), "varchar", "50" });
            param.Add("@val", new List<string>() { dice.ToString(), "int"});
            param.Add("@ts", new List<string>() { DateTimeToString(DateTime.UtcNow), "datetime"});
            param.Add("@comment", new List<string>() { Constants.HA_COMMENT_DICE, "varchar", "-1" });
            
            GetDataSql(Constants.InsertElectionRecordSproc, HaDb.conStr, param);
        }

        /// <summary>
        /// Insert a Heartbeat record
        /// </summary>
        private void InsertHeartbeatRecord()
        {
            Dictionary<string, List<string>> param = new Dictionary<string, List<string>>();
            param.Add("@guid", new List<string>() { GetHaId(), "varchar", "50" });
            param.Add("@val", new List<string>() { "0", "int" });
            param.Add("@ts", new List<string>() { DateTimeToString(DateTime.UtcNow), "datetime" });
            param.Add("@comment", new List<string>() { Constants.HA_COMMENT_HEARTBEAT, "varchar", "-1" });

            GetDataSql(Constants.ManageRecordSproc, HaDb.conStr, param);
        }
        /// <summary>
        /// Get the ElectionRecords for other UConnectors
        /// </summary>
        /// <param name="lastTime"></param>
        /// <returns></returns>
        private DataTable SelectOtherElectionRecords(DateTime lastTime)
        {

            Dictionary<string, List<string>> param = new Dictionary<string, List<string>>();
            param.Add("@guid", new List<string>() { GetHaId(), "varchar", "50" });
            param.Add("@lastTime", new List<string>() { lastTime.ToString(), "datetime" });
            param.Add("@comment", new List<string>() { Constants.HA_COMMENT_DICE, "varchar", "-1" });

            return GetDataSql(Constants.SelectOtherElectionRecordsSproc, HaDb.conStr, param);
        }

        /// <summary>
        /// Start the Heartbeat Thread.
        /// </summary>
        public void StartHeartbeatThread()
        {
            var autoEvent = new AutoResetEvent(false);
            long period = (long)m_haHeartbeatSecs.TotalMilliseconds;
            m_heartbeatTimer = new Timer(Heartbeat, autoEvent, period, period);
        }

        /// <summary>
        /// Stop the Heartbeat Thread
        /// </summary>
        public void StopHeartbeatThread()
        {
            if (m_heartbeatTimer != null)
            {
                m_heartbeatTimer.Dispose();
            }
        }

        public void UpdateLastHandledInteraction(string dt)
        {
            Dictionary<string, List<string>> param = new Dictionary<string, List<string>>();
            param.Add("@guid", new List<string>() { GetHaId(), "varchar", "50" });
            param.Add("@val", new List<string>() { "0", "int" });
            param.Add("@ts", new List<string>() { dt, "datetime" });
            param.Add("@comment", new List<string>() { Constants.HA_COMMENT_TIMESTAMP, "varchar", "-1" });

            GetDataSql(Constants.ManageRecordSproc, HaDb.conStr, param);
        }

        private void DeleteLastHandledRecords()
        {
            // Delete any timestamp records
            Dictionary<string, List<string>> param = new Dictionary<string, List<string>>();
            param.Add("@comment", new List<string>() { Constants.HA_COMMENT_TIMESTAMP, "varchar", "-1" });

            GetDataSql(Constants.DeleteLastHandledRecordsSproc, HaDb.conStr, param);
        }

        public DateTime GetLastHandledInteraction()
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                DateTime result = LastImportedDate;

                if (ResetLastImportedDate)
                {
                    TraceTopic.pureConnectTopic.note("Deleting last handled interaction records from SMDB.");
                    DeleteLastHandledRecords();

                    ResetLastImportedDate = false;
                    m_xmlDoc[XML_SETTINGS][Constants.XML_SETTINGS_RESET_LAST_IMPORTED_DATE].InnerText = "0";
                    importer.SaveToXML();
                }
                else
                {
                    try
                    {
                        Dictionary<string, List<string>> param = new Dictionary<string, List<string>>();
                        param.Add("@comment", new List<string>() { Constants.HA_COMMENT_TIMESTAMP, "varchar", "-1" });
                        
                        var dt = GetDataSql(Constants.GetLastHandledInteractionSproc, HaDb.conStr, param);

                        if (dt!=null && dt.Rows.Count > 0)
                        {
                            result = Convert.ToDateTime(dt.Rows[0][0]);
                            TraceTopic.pureConnectTopic.note("Last handled interaction timestamp in SMDB: {}", result);
                        }
                        else
                        {
                            TraceTopic.pureConnectTopic.warning("Could not retrieve last handled interaction timestamp from SMDB: no rows found.");
                        }
                    }
                    catch (Exception e)
                    {
                        TraceTopic.pureConnectTopic.error("Could not retrieve last handled interaction timestamp from SMDB.", e);
                    }
                }
                return result;
            }
        }

        public void SaveNextDateToImport(DateTime? m_currentDate)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                if (m_currentDate.HasValue)
                {
                    TraceTopic.pureConnectTopic.note("Saving Last Handled Interaction Timestamp: " + m_currentDate.ToString());
                    LastImportedDate = m_currentDate.Value;
                    string lastDate = m_currentDate.Value.ToString(Constants.ISO_8601_FORMAT);
                    TraceTopic.pureConnectTopic.note("Saving LastDate: " + lastDate);
                    m_xmlDoc[XML_SETTINGS][Constants.XML_SETTINGS_LAST_IMPORTED_DATE].InnerText = lastDate;
                    importer.SaveToXML();

                    UpdateLastHandledInteraction(lastDate);
                }
                else
                {
                    TraceTopic.pureConnectTopic.error("m_currentDate is null, can't update configuration parameter {}.",
                        Constants.XML_SETTINGS_LAST_IMPORTED_DATE);
                }
            }
        }
        
        private string DateTimeToString(DateTime dt)
        {
            return dt.ToString(Constants.DateTimeFormat, System.Globalization.CultureInfo.InvariantCulture);
        }

        /*
        protected bool ReconnectToHADB()
        {
            try
            {
                Log.Debug("Reconnecting to HA DB.");
                HaDb = new DBServices();
                Log.DebugFormat("HA DB Connection string: {0}", m_haConnectionString);
                HaDb.setDB(m_haConnectionString, true);
                return true;
            }
            catch (Exception e)
            {
                Log.Error("Could not reconnect to HA DB.", e);
                return false;
            }
        }
        */


    }


}

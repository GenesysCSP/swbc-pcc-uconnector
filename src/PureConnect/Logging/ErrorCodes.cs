﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PureConnect.Data
{
    public enum ErrorCodes
    {
        DOWNLOAD_THRESHOLD_EXCEEDED = 8001,
        CONVERSION_THRESHOLD_EXCEEDED,
        TRANSCODE_FOLDER_FILE_COUNT_EXCEEDED,
        TRANSCODE_FOLDER_FILE_LIFE_EXCEED
    }

}

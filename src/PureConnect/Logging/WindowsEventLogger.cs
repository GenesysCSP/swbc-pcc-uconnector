﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PureConnect.Logging
{
    public class WindowsEventLogger
    {
        private const string LogSource = "UConnector";
        private const string LogName = "UConnector_log";
        private EventLog logger;

        public WindowsEventLogger()
        {
            if (!EventLog.SourceExists(LogSource))
            {
                EventLog.CreateEventSource(LogName, LogSource);
            }
            logger = new EventLog();
            logger.Source = LogSource;
        }

        internal void WriteEntry(string errorMessage, EventLogEntryType warning, int errorCode)
        {
            logger.WriteEntry(errorMessage, warning, errorCode);
        }
    }
}

﻿using MimeKit;
using System;
using System.IO;
using System.Linq;

namespace PureConnect.I3Audio
{
    public class ConvertEmail
    {
        public bool ConvertTextToEMail(string textFile, string fromEmail, string toEmail, string emailFile)
        {
            string str = File.ReadAllText(textFile);
            MimeMessage message = new MimeMessage();
            var from = GetSubString(ref str, "From   :", Environment.NewLine, true, true);
            var names = GetSubString(ref str, "To     :", Environment.NewLine, true, true);
            var cc = GetSubString(ref str, "Cc     :", Environment.NewLine, true, true);
            var bcc = GetSubString(ref str, "Bcc    :", Environment.NewLine, true, true);
            var subject = GetSubString(ref str, "Subject:", Environment.NewLine, true, true);

            message.From.Add(ConstructMailboxAddress(fromEmail, from));
            var emails = toEmail;
            var namesList = names.Split(',').Select(p => p.Trim()).ToList();
            namesList.RemoveAll(p => string.IsNullOrWhiteSpace(p));
            var emailsList = emails.Split(';').Select(p => p.Trim()).ToList();
            emailsList.RemoveAll(p => string.IsNullOrWhiteSpace(p));

            if (namesList.Count == emailsList.Count)
            {
                for (int i = 0; i < namesList.Count; i++)
                {
                    var name = namesList[i];
                    var mail = emailsList[i];
                    var address = ConstructMailboxAddress(mail, name);
                    if (address != null)
                    {
                        message.To.Add(address);
                    }
                }
            }
            else
            {
                foreach (var mail in emailsList)
                {
                    var address = ConstructMailboxAddress(mail);
                    if (address != null)
                    {
                        message.To.Add(address);
                    }
                }
            }
            var ccList = cc.Split(',').Select(p => p.Trim()).ToList();
            ccList.RemoveAll(p => string.IsNullOrWhiteSpace(p));
            foreach (var mail in ccList)
            {
                if ( !string.IsNullOrWhiteSpace(mail))
                {
                    var address = ConstructMailboxAddress(mail);
                    if (address != null)
                    {
                        message.Cc.Add(address);
                    }
                }
            }
            var bccList = cc.Split(',').Select(p => p.Trim()).ToList();
            bccList.RemoveAll(p => string.IsNullOrWhiteSpace(p));
            foreach (var mail in bccList)
            {
                if (!string.IsNullOrWhiteSpace(mail))
                {
                    var address = ConstructMailboxAddress(mail);
                    if (address != null)
                    {
                        message.Bcc.Add(address);
                    }
                }
            }
            message.Subject = subject;
            var body = str;
            var builder = new BodyBuilder();
            builder.TextBody = FormatBody(body);
            message.Body = builder.ToMessageBody();
            message.WriteTo(emailFile);
            File.Delete(textFile);
            return true;
        }
        private MailboxAddress ConstructMailboxAddress(string emailAddress, string displayName = "")
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(displayName) && !displayName.Contains('@'))
                {
                    return new MailboxAddress(displayName, emailAddress);
                }
                else
                {
                    return new MailboxAddress(emailAddress);
                }
            }
            catch(Exception)
            {
                return null;
            }
        }

        private string FormatBody(string body)
        {
            var inlineAttachment = GetSubString(ref body, "[cid:", "]", true, false, true);
            while (!string.IsNullOrWhiteSpace(inlineAttachment))
            {
                inlineAttachment = GetSubString(ref body, "[cid:", "]", true, false, true);
            }
            return body;
        }
        private string GetSubString(ref string content, string start, string end, bool removeSubString = false, bool afterStartString = false, bool removeHyperlink = false)
        {
            var subString = string.Empty;
            try
            {
                var startIndex = content.IndexOf(start);
                if (startIndex != -1)
                {
                    var endIndex = content.IndexOf(end, startIndex);
                    var temp = afterStartString ? (start.Length + 1) : 0;
                    var length = endIndex - startIndex;

                    if (length > 0)
                    {
                        subString = content.Substring(startIndex + temp, length - temp);
                        if (removeSubString)
                        {
                            content = content.Remove(endIndex, end.Length).Remove(startIndex, length);
                        }
                        if (removeHyperlink)
                        {
                            content = RemoveAttachmentHyperLink(content, startIndex);
                        }
                    }
                }
            }
            catch{}
            return subString;
        }

        private string RemoveAttachmentHyperLink(string content, int startIndex)
        {
            try
            {
                if (content.Length > 0 && content[startIndex] == '<')
                {
                    var endIndex = content.IndexOf(">", startIndex);
                    var url = content.Substring(startIndex + 1, endIndex - startIndex - 1);
                    if (Uri.IsWellFormedUriString(url, UriKind.Absolute))
                    {
                        content = content.Replace("<" + url + ">", string.Empty);
                    }
                }
            }
            catch{}
            return content;
        }
    }
}

﻿using ININ.IceLib.Connection;
using System;
using PureConnect.CICConfig;
using ININ.IceLib.Configuration;
using System.Collections.ObjectModel;
using GenUtils.TraceTopic;
using ININ.IceLib.Data.TransactionBuilder;
using System.Data;
using ININ.PSO.Tools.PSOTrace;

namespace PureConnect.I3Audio
{
    // https://msdn.microsoft.com/en-gb/library/ff650316.aspx
    /// <summary>
    /// Singleton class that is used to create a CIC Session, it is then used by each ImporterThread to create the RecordingManager
    /// </summary>
    public sealed class CICSession
    {
        private static volatile CICSession instance;
        private static object syncRoot = new Object();
        private string log_text = "I3Download## ";
        private string m_host;

        //private bool m_opened = false;
        private string m_password;

        private int m_port = HostEndpoint.DefaultPort;
        private Session m_session;
        private Session m_backupsession;
        private string m_username;
        private UserConfigurationList _userConfigurationList = null;
        internal CICCredentials PrimaryCredentials;
        internal CICCredentials BackupCredentials;
        internal int timeout = 60;
        internal int retryCount = 5;

        /// <summary>
        /// Private Constructor, create the logger
        /// </summary>
        private CICSession()
        {
        }

        /// <summary>
        /// Get an Instance of this Class
        /// </summary>
        public static CICSession Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            instance = new CICSession();
                        }
                    }
                }
                return instance;
            }
        }

        internal int Timeout
        {
            get
            {
                return timeout;
            }

            set
            {
                timeout = value;
            }
        }

        internal int RetryCount
        {
            get
            {
                return retryCount;
            }

            set
            {
                retryCount = value;
            }
        }

        /// <summary>
        /// Close the connection to PureConnect
        /// </summary>
        public void Close()
        {
            if (m_session != null)
            {
                try
                {
                    TraceTopic.pureConnectTopic.verbose(log_text + "Closing I3 session...");
                    m_session.Disconnect();
                    TraceTopic.pureConnectTopic.verbose(log_text + "I3 session closed.");
                    m_session.Dispose();
                    m_session = null;
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.error(log_text + "Error Closing I3 Session", e);
                }
            }

            if (m_backupsession != null)
            {
                try
                {
                    TraceTopic.pureConnectTopic.verbose(log_text + "Closing I3 backup session...");
                    m_backupsession.Disconnect();
                    TraceTopic.pureConnectTopic.verbose(log_text + "I3 backup session closed.");
                    m_backupsession.Dispose();
                    m_backupsession = null;
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.error(log_text + "Error Closing I3 Session", e);
                }
            }
            //m_opened = false;
        }

        /// <summary>
        /// Get primary CIC Session
        /// </summary>
        /// <returns></returns>
        public Session GetSession()
        {
            if (m_session != null && m_session.ConnectionState == ININ.IceLib.Connection.ConnectionState.Up)
                return m_session;
            else
                return GetBackupSession();
        }

        /// <summary>
        /// Gets the backup CIC Session
        /// </summary>
        /// <returns></returns>
        public Session GetBackupSession()
        {
            if (m_backupsession == null || m_backupsession.ConnectionState == ININ.IceLib.Connection.ConnectionState.Down)
                Open(BackupCredentials, ref m_backupsession);
            return m_backupsession;
        }

        //public bool IsConnected()
        //{
        //    return m_session != null && m_session.ConnectionState == ININ.IceLib.Connection.ConnectionState.Up;
        //}

        public bool Open()
        {
            if (Open(PrimaryCredentials, ref m_session))
            {
                TraceTopic.pureConnectTopic.verbose(log_text + "Connection to Primary successful");
                return true;
            }
            else if (Open(BackupCredentials, ref m_backupsession))
            {
                TraceTopic.pureConnectTopic.verbose(log_text + "Connection to Backup successful");
                return true;
            }
            else
            {
                TraceTopic.pureConnectTopic.verbose(log_text + "Unable to connect to CIC");
                return false;
            }
        }

        /// <summary>
        /// Open a CIC Session
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        private bool Open(CICCredentials credentials, ref Session session)
        {
            lock (syncRoot)
            {
                using (TraceTopic.pureConnectTopic.scope())
                {
                    TraceTopic.pureConnectTopic.verbose("{} Open CIC Session", log_text);
                    m_username = credentials.User;
                    m_password = credentials.Password;
                    m_host = credentials.Host;

                    for (int i = 0; i < 5; i++)
                    {
                        if (session != null && session.ConnectionState == ININ.IceLib.Connection.ConnectionState.Up)
                        {
                            return true;
                        }
                        else
                        {
                            session = AttemptToOpen(session);
                            if (session != null)
                            {
                                return true;
                            }
                        }
                    }
                    return false;
                }
            }
        }

        /// <summary>
        /// Attempt to Open a CIC Session
        /// </summary>
        /// <returns></returns>
        private Session AttemptToOpen(Session session)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                if (session != null && session.ConnectionState == ININ.IceLib.Connection.ConnectionState.Up)
                {
                    TraceTopic.pureConnectTopic.verbose(log_text + "Session Already Opened and state is Up");
                    return session;
                }

                TraceTopic.pureConnectTopic.verbose(log_text + "Attempting to Open I3 session...");

                try
                {
                    AuthSettings auth = new ICAuthSettings(m_username, m_password);
                    //AuthSettings auth = new AlternateWindowsAuthSettings(m_username, m_password);
                    HostSettings hostSettings = new HostSettings(new HostEndpoint(m_host));
                    //hostSettings.HostEndpoint = new HostEndpoint(m_host, m_port);
                    TraceTopic.pureConnectTopic.verbose(log_text + "Create New Session");
                    session = new Session();
                    session.AutoReconnectEnabled = true;
                    session.ConnectionStateChanged += ConnectionStateChanged;
                    TraceTopic.pureConnectTopic.verbose(log_text + "Connecting to Host: " + m_host + " Port: " + m_port + " User: " + m_username + " Password: ***********");
                    session.Connect(new SessionSettings(), hostSettings, auth, new StationlessSettings());
                    TraceTopic.pureConnectTopic.verbose(log_text + "Connected to Host: " + m_host + " Port: " + m_port);
                    TraceTopic.pureConnectTopic.verbose(log_text + "I3 session opened.");
                    //m_opened = true;
                    return session;
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.error(log_text + "Could not open I3 session.", e);
                    //m_opened = false;
                }
            }
            return null;

        }

        private void ConnectionStateChanged(object sender, ConnectionStateChangedEventArgs e)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    TraceTopic.pureConnectTopic.status("Session State Changed Event: \r\n" +
                                        "State: {}\r\n" +
                                        "Reason: {}\r\n" +
                                        "Message: {}", e.State, e.Reason, e.Message);
                    if (e.State == ININ.IceLib.Connection.ConnectionState.Down)
                    {
                        PSOTrace.WriteRegisteredMessage(EventId.GenericError,
                            "Icelib Session has gone down. A new session will attempt to be Re-established on the next request.");
                    }
                }
                catch (Exception ex)
                {
                    TraceTopic.pureConnectTopic.exception(ex);
                }
            }
        }

        public ReadOnlyCollection<UserConfiguration> CreateUserConfigurationList()
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    Session session = Instance.GetSession();
                    if (_userConfigurationList == null && session.ConnectionState.Equals(ININ.IceLib.Connection.ConnectionState.Up))
                    {
                        _userConfigurationList = new UserConfigurationList(ConfigurationManager.GetInstance(session));

                        var querySettings = _userConfigurationList.CreateQuerySettings();

                        querySettings.SetPropertiesToRetrieve(
                            UserConfiguration.Property.DisplayName,
                            UserConfiguration.Property.PersonalInformation_GivenName,
                            UserConfiguration.Property.PersonalInformation_Surname,
                            UserConfiguration.Property.Workgroups,
                            UserConfiguration.Property.Id,
                            UserConfiguration.Property.StandardProperties_CustomAttributes,
                            UserConfiguration.Property.Roles
                            );

                        querySettings.SetRightsFilterToView();

                        if (!_userConfigurationList.IsCaching)
                        {
                            _userConfigurationList.StartCaching(querySettings);
                        }

                        var users = _userConfigurationList.GetConfigurationList();
                        return users;
                    }
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.exception(e);
                }
                finally
                {
                    if (_userConfigurationList.IsCaching)
                    {
                        _userConfigurationList.StopCaching();
                        _userConfigurationList = null;
                    }
                }
            }
            return null;

        }
    }
}
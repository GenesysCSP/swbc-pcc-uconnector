﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using GenUtils;
using System.Globalization;

namespace Genesys.SpeechMiner.Objects
{
    public sealed class Call
    {
        #region Types

        public enum Field
        {
            Duration,
            EndTime,
            ExternalID,
            ID,
            StartTime
        }

        #endregion

        #region Instance Properties

        public float Duration 
        {
            get
            {
                return _duration;
            }
        }

        public DateTime EndTime
        {
            get
            {
                if (_endTime.HasValue == false)
                {
                    _endTime = new DateTime(_startTime.Ticks).AddSeconds(_duration);
                }

                return _endTime.Value;
            }
        }

        public string ExternalID
        {
            get
            {
                return _externalId;
            }
        }

        public int ID 
        {
            get
            {
                return _id;
            }
        }

        public DateTime StartTime 
        {
            get
            {
                return _startTime;
            }
        }

        public string this[string fieldName]
        {
            get
            {
                string value = null;
                _metaData.TryGetValue(fieldName, out value);
                return value;
            }
            set
            {
                _metaData[fieldName] = value;
            }
        }

        #endregion

        #region Constructors

        public Call(
            int id,
            string externalId,
            DateTime startTime,
            float duration
            )
        {
            externalId = (externalId != null) ? externalId.Trim() : externalId;
            _externalId = (String.IsNullOrEmpty(externalId) == true) ? String.Empty : externalId;
            _id = id;
            _startTime = startTime;
            _duration = duration;
        }

        #endregion

        #region Class Methods

        public static void WriteCSVHeader(
            StreamWriter writer,
            IEnumerable<Field> stdFields = null,
            IEnumerable<string> metaFields = null,
            IDictionary<string, string> metaFieldHeaders = null,
            string commentIndicator = "# ",
            string separator = ", "
            )
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            var fields = new List<string>();
            if (stdFields != null)
            {
                fields.AddRange(Strings.GetStringValues(stdFields));
            }
            else
            {
                fields.AddRange(Enums.GetEnumValues<Field, string>());
            }

            if (metaFields != null && metaFields.Any() == true)
            {
                metaFields.ForEach(field =>
                    {
                        string temp;
                        if (metaFieldHeaders != null && 
                            metaFieldHeaders.TryGetValue(field, out temp) == true)
                        {
                            field = temp;
                        }
                        fields.Add(field);
                    });
            }

            if (fields.Any() == false)
            {
                return;
            }

            var line = new StringBuilder(commentIndicator);
            line.Append(String.Join(separator, fields.ToArray()));

            writer.WriteLine(line.ToString());
        }

        #endregion

        #region Class Data

        public const string META_LEFT_CHANNEL = "LeftChannel";
        public const string META_RIGHT_CHANNEL = "RightChannel";

        #endregion

        #region Instance Methods

        public override int GetHashCode()
        {
            return this.ID.GetHashCode();
        }

        public void WriteAsCSV(
            StreamWriter writer,
            IEnumerable<Field> stdFields = null,
            IEnumerable<string> metaFields = null,
            bool outputLocalTZ = false,
            string separator = ", ",
            IFormatProvider format = null
            )
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }
            else if (stdFields == null)
            {
                stdFields = Enums.GetEnumValues<Field>();
            }
            else if (format == null)
            {
                format = CultureInfo.CurrentCulture;
            }

            var fieldValues = new List<string>();

            if (stdFields != null && stdFields.Any() == true)
            {
                stdFields.ForEach((field) =>
                {
                    string value = String.Empty;
                    switch (field)
                    {
                    case Field.Duration:
                        value = this.Duration.ToString();
                        break;

                    case Field.EndTime:
                        var endTime = (outputLocalTZ == true) ? 
                            this.EndTime.ToLocalTime() : this.EndTime;
                        value = endTime.ToString(format);
                        break;

                    case Field.ExternalID:
                        value = this.ExternalID;
                        break;

                    case Field.ID:
                        value = this.ID.ToString();
                        break;

                    case Field.StartTime:
                        var startTime = (outputLocalTZ == true) ?
                            this.StartTime.ToLocalTime() : this.StartTime;
                        value = startTime.ToString(format);
                        break;
                    }

                    fieldValues.Add(value);
                });
            }

            if (metaFields != null && metaFields.Any() == true)
            {
                metaFields.ForEach((field) =>
                {
                    string value;
                    if (_metaData.TryGetValue(field, out value) == true)
                    {
                        fieldValues.Add(value);
                    }
                    else
                    {
                        fieldValues.Add(String.Empty);
                    }

                });
            }

            writer.WriteLine(String.Join(separator, fieldValues.ToArray()));
        }

        #endregion

        #region Instance Data

        private readonly float _duration;
        private readonly string _externalId;
        private DateTime? _endTime;
        private readonly int _id;
        private readonly IDictionary<string, string> _metaData = new Dictionary<string, string>();
        private readonly DateTime _startTime;

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Genesys.SpeechMiner.Objects
{
    public sealed class Program
    {
        #region Instance Properties

        public int ID
        {
            get
            {
                return _id;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }

        #endregion

        #region Constructors

        public Program(
            int id,
            string name = ""
            )
        {
            _id = id;
            _name = (name != null) ? name.Trim() : name;
        }

        #endregion

        #region Instance Methods

        public override string ToString()
        {
            return (String.IsNullOrEmpty(_name) == false) ? _name : _id.ToString();
        }

        #endregion

        #region Instance Data

        private readonly int _id;
        private readonly string _name;

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using GenUtils;
using System.Threading;
using Genesys.SpeechMiner.Objects;
using Genesys.SpeechMiner.Agents;
using System.Data.Common;
using GenUtils.DateTimes;
using System.Text.RegularExpressions;

namespace Genesys.SpeechMiner.Database
{
    public abstract class SMDatabaseBase : ISMDatabase
    {
        #region Instance Properties

        protected SqlConnection Connection
        {
            get
            {
                return _dbConn;
            }
        }

        protected ReaderWriterLockSlim ConnectionLock
        {
            get
            {
                return _dbLock;
            }
        }

        public Version Version
        {
            get
            {
                return _version;
            }
        }

        public bool IsConnected
        {
            get
            {
                try
                {
                    _dbLock.EnterReadLock();
                    if (_dbConn == null)
                    {
                        return false;
                    }

                    switch (_dbConn.State)
                    {
                    case ConnectionState.Broken:
                    case ConnectionState.Closed:
                        return false;
                    default:
                        return true;
                    }
                }
                finally
                {
                    _dbLock.ExitReadLock();
                }
            }
        }

        #endregion

        #region Constructors

        public SMDatabaseBase(
            SqlConnection dbConn,
            Version version
            )
        {
            if (dbConn == null)
            {
                throw new ArgumentNullException("dbConn");
            }

            version = (version == null) ? new Version("unknown") : version;

            _dbConn = dbConn;
            _supportsMultiExtIds = this.HasTable(Hierarchy.TABLE_EXTERNALIDS);
            _version = version;
        }

        #endregion

        #region Instance Methods

        public SqlTransaction BeginTransaction()
        {
            _dbLock.EnterUpgradeableReadLock();
            try
            {
                _dbLock.EnterWriteLock();
                try
                {
                    return _dbConn.BeginTransaction();
                }
                finally
                {
                    _dbLock.ExitWriteLock();
                }
            }
            finally
            {
                _dbLock.ExitUpgradeableReadLock();
            }
        }

        public void Connect()
        {
            if(_dbLock.IsReadLockHeld == true && this.IsConnected == true)
            {
                return;
            }

            bool lockUpgraded = false;
            try
            {
                _dbLock.EnterUpgradeableReadLock();

                _dbLock.EnterWriteLock();
                lockUpgraded = true;

                // Check after the write lock has been acquired in case this
                // thread was being blocked by another that was also opening the connection.
                if (this.IsConnected == true)
                {
                    return;
                }

                _dbConn.Open();
                _supportsMultiExtIds = this.HasTable(Hierarchy.TABLE_EXTERNALIDS);
            }
            finally
            {
                if (lockUpgraded == true)
                {
                    _dbLock.ExitWriteLock();
                }

                _dbLock.ExitUpgradeableReadLock();
            }
        }

        protected virtual Call CreateCall(
            DataRow row
            )
        {
            if (row == null)
            {
                throw new ArgumentNullException("row");
            }

            var id = row.Field<int>("id");
            var startTime = row.Field<DateTime>("startTime");
            var duration = row.Field<float>("durationInSecs");
            var externalId = row.Field<string>("externalId");

            return new Call(id, externalId, startTime, duration);
        }

        protected virtual IEnumerable<Call> CreateCalls(
            DataTable results,
            bool includeMetaData = false,
            IEnumerable<string> metaFields = null,
            SqlTransaction transxn = null
            )
        {
            if (results == null)
            {
                throw new ArgumentNullException("results");
            }
            else if (results.Rows.Count == 0)
            {
                return new Call[0];
            }

            IEnumerable<Call> calls = null;
            if (includeMetaData == true)
            {
                var callIdx = new Dictionary<int, Call>();
                Call call = null;

                results.ForEach(row =>
                {
                    call = this.CreateCall(row);
                    callIdx.Add(call.ID, call);
                });

                var callMetaData = this.Query(SQL.CallsMetaData(callIdx.Keys, metaFields));
                call = null;
                foreach (DataRow row in callMetaData.Rows)
                {
                    var callId = row.Field<int>("callId");
                    if ((call == null || call.ID != callId) &&
                        callIdx.TryGetValue(callId, out call) == false)
                    {
                        continue;
                    }

                    var name = row.Field<string>("fieldName");
                    call[name] = row.Field<string>("fieldValue");
                }

                calls = callIdx.Values;
            }
            else
            {
                calls = new List<Call>(
                    from row in results.AsEnumerable() select this.CreateCall(row));
            }

            return calls;
        }

        protected virtual Program CreateProgram(
            DataRow row
            )
        {
            if (row == null)
            {
                throw new ArgumentNullException("row");
            }

            var id = row.Field<int>("id");
            var name = row.Field<string>("name");

            return new Program(id, name);
        }

        public void Disconnect()
        {
            try
            {
                _dbLock.EnterWriteLock();
                if (this.IsConnected == false)
                {
                    return;
                }
                
                _dbConn.Close();
            }
            finally
            {
                _dbLock.ExitWriteLock();
            }
        }

        public void Dispose()
        {
            try
            {
                _dbLock.EnterWriteLock();
                this.Disconnect();
            }
            finally
            {
                _dbLock.ExitWriteLock();
            }
        }

        public Call GetCall(
            int id,
            KeyValuePair<string, string>? withMeta = null,
            bool includeMetaData = false,
            IEnumerable<string> metaFields = null,
            SqlTransaction transxn = null
            )
        {
            return this.GetCalls(new int[] { id },
            withMeta: withMeta, includeMetaData: includeMetaData, metaFields: metaFields, 
                transxn: transxn).FirstOrDefault();
        }

        public IEnumerable<Call> GetCalls(
            DateTime date,
            IEnumerable<int> idSet = null,
            long? limitTo = null,
            int? programId = null,
            KeyValuePair<string, string>? withMeta = null,
            bool includeMetaData = false,
            IEnumerable<string> metaFields = null,
            SqlTransaction transxn = null
            )
        {
            DateTime start;
            DateTime end;
            DateTimes.GetDateStartEnd(date, out start, out end);

            return this.GetCalls(start, end, idSet, limitTo, programId, withMeta, 
                includeMetaData, metaFields, transxn);
        }

        public abstract IEnumerable<Call> GetCalls(
            IEnumerable<int> idSet = null,
            long? limitTo = null,
            int? programId = null, 
            KeyValuePair<string, string>? withMeta = null,
            bool includeMetaData = false,
            IEnumerable<string> metaFields = null,
            SqlTransaction transxn = null);

        public abstract IEnumerable<Call> GetCalls(
            DateTime startTime, 
            DateTime endTime, 
            IEnumerable<int> idSet = null,
            long? limitTo = null, 
            int? programId = null, 
            KeyValuePair<string, string>? withMeta = null,
            bool includeMetaData = false,
            IEnumerable<string> metaFields = null,
            SqlTransaction transxn = null);

        public IEnumerable<Call> GetCallsToday(
            int? programId = null,
            KeyValuePair<string, string>? withMeta = null,
            bool includeMetaData = false,
            SqlTransaction transxn = null
            )
        {
            return this.GetCalls(DateTime.UtcNow.Date, programId: programId,
                withMeta: withMeta, includeMetaData: includeMetaData, transxn: transxn);
        }

        public IEnumerable<Call> GetCallsYesterday(
            int? programId = null,
            KeyValuePair<string, string>? withMeta = null,
            bool includeMetaData = false,
            SqlTransaction transxn = null
            )
        {
            return this.GetCalls(DateTime.UtcNow.Date.AddDays(-1), programId: programId,
                withMeta: withMeta, includeMetaData: includeMetaData, transxn: transxn);
        }

        public int GetInternalAgentID(
            string externalId,
            SqlTransaction transxn = null
            )
        {
            if (externalId == null)
            {
                throw new ArgumentNullException("externalId");
            }

            try
            {
                _dbLock.EnterReadLock();
                return this.QueryScalar<int>(
                    SQL.InternalAgentID(externalId, _supportsMultiExtIds), -1, transxn);
            }
            finally
            {
                _dbLock.ExitReadLock();
            }
        }

        public string GetPrimaryAgentID(
            string externalId,
            SqlTransaction transxn = null
            )
        {
            if (externalId == null)
            {
                throw new ArgumentNullException("externalId");
            }

            try
            {
                _dbLock.EnterReadLock();
                return this.QueryScalar<string>(
                    SQL.PrimaryAgentID(externalId, _supportsMultiExtIds), transxn: transxn);
            }
            finally
            {
                _dbLock.ExitReadLock();
            }
        }

        public Program GetProgram(
            int id,
            SqlTransaction transxn = null
            )
        {
            return this.GetPrograms(new int[] { id }, transxn: transxn).FirstOrDefault();
        }

        public Program GetProgram(
            string name,
            SqlTransaction transxn = null
            )
        {
            if (name == null)
            {
                throw new ArgumentNullException("name");
            }

            name = name.Trim();
            return this.GetPrograms(nameSet: new string[] { name }, transxn: transxn).FirstOrDefault();
        }

        public abstract IEnumerable<Program> GetPrograms(
            IEnumerable<int> idSet = null,
            IEnumerable<string> nameSet = null,
            SqlTransaction transxn = null);

        public Version GetVersion(
            string resource = "SM",
            SqlTransaction transxn = null
            )
        {
            if (resource == null)
            {
                throw new ArgumentNullException("resource");
            }
            else if (resource.Equals("SM", StringComparison.InvariantCultureIgnoreCase) == true)
            {
                return _version;
            }

            this.Connect();

            try
            {
                _dbLock.EnterReadLock();
                return SMDatabaseBase.GetVersion(_dbConn, transxn: transxn);
            }
            finally
            {
                _dbLock.ExitReadLock();
            }
        }

        public static Version GetVersion(
            SqlConnection dbConn,
            string resource = "SM",
            SqlTransaction transxn = null         
            )
        {
            if (dbConn == null)
            {
                throw new ArgumentNullException("dbConn");
            }

            var dbCmd = Commands.ComponentVersion(dbConn.CreateCommand(), resource);
            dbCmd.Transaction = transxn;

            return new Version(DB.QueryScalar<string>(dbCmd, "unknown"));
        }

        public bool HasTable(
            string tableName,
            SqlTransaction transxn = null
            )
        {
            if (tableName == null)
            {
                throw new ArgumentNullException("tableName");
            }

            this.Connect();

            _dbLock.EnterReadLock();
            try
            {
                return DB.ObjectExists(_dbConn, tableName, transxn);
            }
            finally
            {
                _dbLock.ExitReadLock();
            }
        }

        public int Modify(
            string sql,
            SqlTransaction transxn = null
            )
        {
            this.Connect();

            _dbLock.EnterReadLock();
            try
            {
                return DB.Modify(_dbConn, sql, transxn);
            }
            finally
            {
                _dbLock.ExitReadLock();
            }
        }

        private void PopulateEntitiesTable(
            DataTable entities,
            string tableName,
            SqlTransaction transxn = null
            )
        {
            if(entities == null)
            {
                throw new ArgumentNullException("entities");
            }
            else if (tableName == null)
            {
                throw (tableName == null) ?
                    new ArgumentNullException("entitiesTableName") :
                    new ArgumentException("no table name specified");
            }

            this.Connect();

            _dbLock.EnterReadLock();
            try
            {
                var bulkCopy = new SqlBulkCopy(
                    _dbConn, SqlBulkCopyOptions.Default, transxn);
                bulkCopy.DestinationTableName = tableName;
                bulkCopy.WriteToServer(entities);
            }
            finally
            {
                _dbLock.ExitReadLock();
            }
        }

        private void PopulateExternalIDsTable(
            DataTable externalIds,
            string tableName,
            SqlTransaction transxn = null
            )
        {
            if (externalIds == null)
            {
                throw new ArgumentNullException("externalIds");
            }
            else if (tableName == null)
            {
                throw (tableName == null) ?
                    new ArgumentNullException("tableName") :
                    new ArgumentException("no table name specified");
            }

            this.Connect();

            _dbLock.EnterReadLock();
            try
            {
                var bulkCopy = new SqlBulkCopy(
                    _dbConn, SqlBulkCopyOptions.Default, transxn);
                bulkCopy.DestinationTableName = tableName;
                bulkCopy.WriteToServer(externalIds);
            }
            finally
            {
                _dbLock.ExitReadLock();
            }
        }

        private void PopulateHierarchyTable(
            DataTable hierarchy,
            string tableName,
            SqlTransaction transxn = null
            )
        {
            _dbLock.EnterReadLock();
            try
            {
                // In order to populate the hierarchy table with internal IDs, which we do not have in memory,
                // a temporary table is created using external IDs and is then converted to internal IDs.
                const string extIdHierarchyTableName = "#extIdHierarchy";
                DB.DropTable(_dbConn, extIdHierarchyTableName, transxn);
                this.Modify("CREATE TABLE #extIdHierarchy ( parentId VARCHAR(256), childId VARCHAR(256) )", transxn);

                var bulkCopy = new SqlBulkCopy(
                    _dbConn, SqlBulkCopyOptions.Default, transxn);
                bulkCopy.DestinationTableName = extIdHierarchyTableName;
                bulkCopy.WriteToServer(hierarchy);

                var sqlConvert = String.Format(
                    @"INSERT INTO {0} (parentId, childId)
                      SELECT CASE WHEN Parent.id IS NULL THEN -1 ELSE Parent.id END As parentId, Child.Id AS childId
                      FROM {1}
                      LEFT OUTER JOIN agentEntityTbl AS Parent ON parentId = Parent.externalId
                      JOIN agentEntityTbl AS Child on childId = Child.externalId", tableName, extIdHierarchyTableName);
                this.Modify(sqlConvert, transxn);
            }
            finally
            {
                _dbLock.ExitReadLock();
            }
        }

        public DataTable Query(
            string sql,
            SqlTransaction transxn = null,
            CommandBehavior cmdBehavior = CommandBehavior.Default
            )
        {
            this.Connect();

            _dbLock.EnterReadLock();
            try
            {
                return DB.Query(_dbConn, sql, transxn, cmdBehavior);
            }
            finally
            {
                _dbLock.ExitReadLock();
            }
        }

        public T QueryScalar<T>(
            string sql,
            T defaultValue = default(T),
            SqlTransaction transxn = null
            )
        {
            this.Connect();

            _dbLock.EnterReadLock();
            try
            {
                return DB.QueryScalar<T>(_dbConn, sql, defaultValue, transxn);
            }
            finally
            {
                _dbLock.ExitReadLock();
            }
        }

        public void UpdateAgentHierarchy(
            Hierarchy hierarchy,
            bool supportForMultiExtIds = false,
            SqlTransaction transxn = null
            )
        {
            if (hierarchy == null)
            {
                throw new ArgumentNullException("hierarchy");
            }

            this.Connect();

            _dbLock.EnterWriteLock();
            bool transxnCreated = false;
            if (transxn == null)
            {
                transxn = this.BeginTransaction();
                transxnCreated = true;
            }

            try
            {
                try
                {
                    // If required, check to make sure the external IDs mapping table is available.
                    if (supportForMultiExtIds == true && _supportsMultiExtIds == false &&
                        this.HasTable(Hierarchy.TABLE_EXTERNALIDS, transxn) == false)
                    {
                        this.Modify(SQL.CREATE_EXTERNALIDS, transxn);
                        this.Modify(SQL.CREATE_EXTERNALIDS_IDX, transxn);
                        _supportsMultiExtIds = true;
                    }
                    
                    const string entitySourceTableName = "#agentEntities";
                    const string hierarchySourceTableName = "#agentHierarchy";
                    DB.DropTable(_dbConn, entitySourceTableName, transxn);
                    DB.DropTable(_dbConn, hierarchySourceTableName, transxn);

                    var dsHierarchy = hierarchy.BuildDataSet(supportForMultiExtIds);

                    // Copy the structure of the agentEntityTbl into a temporary table
                    // and populate the copy.
                    DB.CopyTable(this.Connection, "agentEntityTbl", false,
                        entitySourceTableName, "externalId, name, isPerson", transxn);
                    this.PopulateEntitiesTable(dsHierarchy.Tables[Hierarchy.TABLE_ENTITIES],
                        entitySourceTableName, transxn);

                    // Merge entities from the temporary table.
                    this.Modify(SQL.AgentEntityMerge("agentEntityTbl", entitySourceTableName), transxn);

                    // Load external ID mappings.
                    if (supportForMultiExtIds == true)
                    {
                        this.Modify("DELETE FROM " + Hierarchy.TABLE_EXTERNALIDS, transxn);
                        this.PopulateExternalIDsTable(dsHierarchy.Tables[Hierarchy.TABLE_EXTERNALIDS],
                            Hierarchy.TABLE_EXTERNALIDS, transxn);
                    }

                    // Copy the structure of the agentHierarchyTbl into a temporary table
                    // and populate the copy.
                    DB.CopyTable(this.Connection, "agentHierarchyTbl", false, hierarchySourceTableName, transxn: transxn);
                    this.PopulateHierarchyTable(dsHierarchy.Tables[Hierarchy.TABLE_HIERARCHY],
                        hierarchySourceTableName, transxn);

                    var dbCmd = _dbConn.CreateCommand();
                    dbCmd.Transaction = transxn;
                    Commands.AgentHierarchyUpdate(dbCmd, hierarchySourceTableName).ExecuteNonQuery();

                    if (transxnCreated == true)
                    {
                        transxn.Commit();
                    }
                }
                catch(Exception e)
                {
                    if (transxnCreated == true)
                    {
                        try { transxn.Rollback(); } catch (Exception) { /* intentionally ignored */ }
                    }
                    throw e;
                }
            }
            finally
            {
                _dbLock.ExitWriteLock();
            }
        }

        #endregion

        #region Instance Data

        private readonly SqlConnection _dbConn;
        private readonly ReaderWriterLockSlim _dbLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
        private volatile bool _supportsMultiExtIds = false;
        private readonly Version _version;

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GenUtils;
using Genesys.SpeechMiner.Agents;

namespace Genesys.SpeechMiner.Database
{
    public static class SQL
    {
        public static string AgentEntityMerge(
            string targetTable,
            string sourceTable
            )
        {
            const string sqlFormat =
                @"MERGE {0} AS target
                  USING (SELECT externalId, name, isPerson FROM {1}) AS source(externalId, name, isPerson)
                  ON target.externalId = source.externalId
                  WHEN MATCHED THEN
	                  UPDATE SET name = source.name, isPerson = source.isPerson
                  WHEN NOT MATCHED THEN
	                  INSERT (externalId, name, isPerson) VALUES(source.externalId, source.name, source.isPerson);";

            return String.Format(sqlFormat, targetTable, sourceTable);
        }

        #region Call Queries

        public static string CallIDs(
            IEnumerable<int> idSet = null,
            long? limitTo = null,
            DateTime? startTime = null,
            DateTime? endTime = null,
            int? programId = null
            )
        {
            return Calls(idSet, limitTo, startTime, endTime, programId, CALLID_FIELD);
        }

        public static string CallIDsWithMeta(
            string name,
            string value,
            IEnumerable<int> idSet = null,
            long? limitTo = null,
            DateTime? startTime = null,
            DateTime? endTime = null,
            int? programId = null
            )
        {
            return CallsWithMeta(name, value, idSet, limitTo, 
                startTime, endTime, programId, CALLID_FIELD);
        }

        public static string Calls(
            IEnumerable<int> idSet = null,
            long? limitTo = null,
            DateTime? startTime = null,
            DateTime? endTime = null,
            int? programId = null,
            string fields = CALL_FIELDS
            )
        {
            #region Parmaeter Validation

            if (limitTo != null && limitTo <= 0)
            {
                throw new ArgumentException("limitTo must have a positive value");
            }

            // Ensure dates are in UTC.
            startTime = (startTime != null) ? startTime.Value.ToUniversalTime() : startTime;
            endTime = (endTime != null) ? endTime.Value.ToUniversalTime() : endTime;

            #endregion

            // Format base query
            var idSelect = new StringBuilder("SELECT");
            if (limitTo != null)
            {
                idSelect.Append(" TOP ");
                idSelect.Append(limitTo);
            }
            idSelect.AppendFormat(" {0} FROM callMetaTbl", fields);

            // Does it require a where clause?
            if (idSet == null && limitTo.HasValue == false && 
                startTime.HasValue == false && endTime.HasValue == false && 
                programId.HasValue == false)
            {
                return idSelect.ToString();
            }

            bool first = true;
            if (idSet != null && idSet.Any() == true)
            {
                idSelect.Append((first == true) ? " WHERE" : String.Empty);
                idSelect.AppendFormat(" callId IN ({0})", 
                    String.Join(",", idSet.Select(id => id.ToString()).ToArray()));
                first = false;
            }

            if (programId.HasValue == true)
            {
                idSelect.Append((first == true) ? " WHERE" : String.Empty);
                idSelect.Append((first == false) ? " AND" : String.Empty);
                idSelect.AppendFormat(" programId = {0}", programId.Value);
                first = false;
            }

            if (startTime != null && endTime != null)
            {
                idSelect.Append((first == true) ? " WHERE" : String.Empty);
                idSelect.Append((first == false) ? " AND" : String.Empty);
                idSelect.AppendFormat(" dbo.tod2time(callTime) BETWEEN '{0}' AND '{1}'", startTime, endTime);
                first = false;
            }
            else if (startTime != null)
            {
                idSelect.Append((first == true) ? " WHERE" : String.Empty);
                idSelect.Append((first == false) ? " AND" : String.Empty);
                idSelect.AppendFormat(" dbo.tod2time(callTime) >= '{0}'", startTime);
                first = false;
            }
            else if (endTime != null)
            {
                idSelect.Append((first == true) ? " WHERE" : String.Empty);
                idSelect.Append((first == false) ? " AND" : String.Empty);
                idSelect.AppendFormat(" dbo.tod2time(callTime) <= '{0}'", endTime);
                first = false;
            }

            return idSelect.ToString();
        }

        public static string CallsMetaData(
            IEnumerable<int> idSet = null,
            IEnumerable<string> fieldNames = null
            )
        {
            var metaSelect = new StringBuilder(CALL_METADATA);
            if ((idSet == null || idSet.Any() == false) && 
                (fieldNames == null || fieldNames.Any() == false))
            {
                return metaSelect.ToString();
            }

            metaSelect.Append(" WHERE");
            bool first = true;
            if (idSet != null && idSet.Any() == true)
            {
                metaSelect.AppendFormat(" callId IN ({0})",
                    String.Join(",", idSet.Select(id => id.ToString()).ToArray()));
                first = false;
            }

            if (fieldNames != null && fieldNames.Any() == true)
            {
                metaSelect.Append((first == false) ? " AND" : String.Empty);
                metaSelect.AppendFormat(" fieldName IN ({0})",
                    String.Join(",", fieldNames.Select(name => String.Format("'{0}'", name)).ToArray()));
            }

            return metaSelect.ToString();
        }

        public static string CallsWithMeta(
            string name,
            string value,
            IEnumerable<int> idSet = null,
            long? limitTo = null,
            DateTime? startTime = null,
            DateTime? endTime = null,
            int? programId = null,
            string fields = CALL_FIELDS
            )
        {
            var idSelect = new StringBuilder("SELECT");
            if (limitTo != null)
            {
                idSelect.Append(" TOP ");
                idSelect.Append(limitTo);
            }
            idSelect.Append(" " + fields);

            // Only need a sub-query if dates are involved.
            bool subQueryUsed = false;
            if (startTime != null || endTime != null)
            {
                idSelect.AppendFormat(" FROM ({0}) AS CallIDs", Calls(idSet, null, startTime, endTime, programId, JOIN_CALL_FIELDS));
                idSelect.Append(" JOIN callMetaExTbl ON callId = id");
                subQueryUsed = true;
            }
            else
            {
                idSelect.Append(" FROM callMetaTbl");
                idSelect.Append(" JOIN (SELECT callId AS id, fieldName, fieldValue FROM callMetaExTbl) AS CallMeta ON callId = id");
            }

            idSelect.AppendFormat(
                " WHERE fieldName = '{0}' AND fieldValue = '{1}'", name.Replace("'", "''"), value.Replace("'", "''"));

            if (subQueryUsed == false && idSet != null && idSet.Any() == true)
            {
                idSelect.AppendFormat(" AND callId IN ({0})",
                    String.Join(",", idSet.Select(id => id.ToString()).ToArray()));
            }

            return idSelect.ToString();
        }

        #endregion

        public static string InternalAgentID(
            string externalId,
            bool dbSupportsMultiExtIds
            )
        {
            if (externalId == null)
            {
                throw new ArgumentNullException("externalId");
            }

            string sqlFormat = @"SELECT smExternalId 
                                 FROM agentExternalIds 
                                 WHERE externalId = '{0}'";

            if (dbSupportsMultiExtIds == true)
            {
                sqlFormat = @"SELECT id, name
                              FROM agentEntityTbl
                              WHERE externalId = (SELECT smExternalId 
                                                  FROM agentExternalIds 
                                                  WHERE externalId = '{0}')";
            }

            return String.Format(sqlFormat, externalId.Replace("'", "''"));
        }

        public static string Programs(
            IEnumerable<int> idSet = null,
            IEnumerable<string> nameSet = null,
            bool? active = null
            )
        {
            var sql = new StringBuilder(
                String.Format("SELECT {0} FROM programInfoTbl", PROGRAM_FIELDS));

            // If not filters specified, we're done.
            if(active.HasValue == false && 
               (idSet == null || idSet.Any() == false) && 
               (nameSet == null || nameSet.Any() == false))
            {
                return sql.ToString();
            }

            bool first = true;
            sql.Append(" WHERE");

            if(idSet != null && idSet.Any() == true)
            {
                sql.AppendFormat(" programId IN ({0})", 
                    String.Join(",", idSet.Select(id => id.ToString()).ToArray()));
                first = false;
            }

            if(nameSet != null && nameSet.Any() == true)
            {
                var names = new string[nameSet.Count()];
                int idx = 0;
                nameSet.ForEach(name => { names[idx] = name.Replace("'", "''"); idx++; });

                sql.Append((first == false) ? " AND" : String.Empty);
                sql.AppendFormat(" name IN ('{0}')", String.Join(",", names));
                first = false;
            }

            if(active.HasValue == true)
            {
                sql.Append((first == false) ? " AND" : String.Empty);
                sql.AppendFormat(" isActive={0}", (active.Value == true) ? 1 : 0);
            }

            return sql.ToString();
        }

        public static string PrimaryAgentID(
            string externalId,
            bool dbSupportsMultiExtIds
            )
        {
            if (externalId == null)
            {
                throw new ArgumentNullException("externalId");
            }

            var sqlFormat = @"SELECT externalId FROM agentEntityTbl 
                              WHERE externalId = '{0}'";
            if (dbSupportsMultiExtIds == true)
            {
                sqlFormat = @"SELECT smExternalId FROM agentExternalIds 
                              WHERE externalId = '{0}'";
            }

            return String.Format(sqlFormat, externalId.Replace("'", "''"));
        }

        #region Class Data

        public const string CALL_METADATA = "SELECT callId, fieldName, fieldValue FROM callMetaExTbl";

        public const string CREATE_EXTERNALIDS =
            @"CREATE TABLE agentExternalIds 
              (
	              externalId varchar(256) NOT NULL,
	              smExternalId varchar(256) NOT NULL FOREIGN KEY REFERENCES agentEntityTbl(externalId),
	              CONSTRAINT PK_agentExternalIds PRIMARY KEY CLUSTERED (externalId ASC, smExternalId ASC),
              )";
        public const string CREATE_EXTERNALIDS_IDX =
            @"CREATE INDEX IX_agentExternalIds_smExternalId ON agentExternalIds(smExternalId)";

        private const string CALLID_FIELD = "callId AS id";
        private const string CALL_FIELDS = CALLID_FIELD + ", externalId, dbo.tod2time(callTime) AS startTime, callDuration AS durationInSecs";
        private const string JOIN_CALL_FIELDS = CALLID_FIELD + ", externalId, callTime, callDuration";
        private const string PROGRAM_FIELDS = "programId AS id, name";

        #endregion
    }
}

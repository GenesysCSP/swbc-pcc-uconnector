﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Genesys.SpeechMiner.Database.Objects
{
    public abstract class DBObjectBase : IDBObject
    {
        #region Instance Properties

        public int ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        #endregion

        #region Instanec Data

        private int _id;

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Genesys.SpeechMiner.Database.Query
{
    public abstract class DBQueryBase : IDBQuery
    {
        #region Instance Properties

        public int? ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public ISet<int> IDSet
        {
            get
            {
                return _idSet;
            }
            set
            {
                _idSet = value;
            }
        }

        public ulong? Limit
        {
            get
            {
                return _limit;
            }
            set
            {
                _limit = value;
            }
        }

        public Type ObjectType
        {
            get
            {
                return _objectType;
            }
        }

        #endregion

        #region Constructors

        public DBQueryBase(
            Type objectType
            )
        {
            if (objectType == null)
            {
                throw new ArgumentNullException("objectType");
            }

            _objectType = objectType;
        }

        #endregion

        #region Instance Data

        private int? _id;
        private ISet<int> _idSet;
        private ulong? _limit;
        private readonly Type _objectType;

        #endregion
    }
}

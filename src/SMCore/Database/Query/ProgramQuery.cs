﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Genesys.SpeechMiner.Objects;

namespace Genesys.SpeechMiner.Database.Query
{
    public class ProgramQuery : DBQueryBase
    {
        #region Constructors

        public ProgramQuery() 
            : base(typeof(Program))
        {
        }

        #endregion
    }
}

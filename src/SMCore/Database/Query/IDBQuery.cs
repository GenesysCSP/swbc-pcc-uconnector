﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Genesys.SpeechMiner.Database.Query
{
    public interface IDBQuery
    {
        #region Properties

        int? ID { get; set; }
        ISet<int> IDSet { get; set; }
        ulong? Limit { get; set; }
        Type ObjectType { get; }

        #endregion
    }
}

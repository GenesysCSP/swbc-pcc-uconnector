﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GenUtils.DateTimes;
using Genesys.SpeechMiner.Objects;

namespace Genesys.SpeechMiner.Database.Query
{
    public class CallQuery : DBQueryBase
    {
        #region Constructors

        public CallQuery()
            : base(typeof(Call))
        {
        }

        #endregion

        #region Instance Properties

        public bool IncludeMeta;
        public ISet<string> MetaFields;
        public int ProgramID;
        public Range TimePeriod;

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using GenUtils;
using Genesys.SpeechMiner.Objects;

namespace Genesys.SpeechMiner.Database
{
    public class SMDatabaseDefault : SMDatabaseBase
    {
        #region Constructors

        internal SMDatabaseDefault(
            SqlConnection dbConn,
            Version version
            ) : base(dbConn, version)
        {
        }

        #endregion

        #region Instance Methods

        public override IEnumerable<Call> GetCalls(
            IEnumerable<int> idSet = null,
            long? limitTo = null,
            int? programId = null,
            KeyValuePair<string, string>? withMeta = null,
            bool includeMetaData = false,
            IEnumerable<string> metaFields = null,
            SqlTransaction transxn = null
            )
        {
            this.Connect();

            try
            {
                this.ConnectionLock.EnterReadLock();

                string sqlQuery;
                if (withMeta.HasValue == true)
                {
                    var kvp = withMeta.Value;
                    sqlQuery = SQL.CallsWithMeta(kvp.Key, kvp.Value, idSet, 
                        limitTo: limitTo, programId: programId);
                }
                else
                {
                    sqlQuery = SQL.Calls(idSet, limitTo: limitTo, programId: programId);
                }

                var results = this.Query(sqlQuery, transxn);
                return this.CreateCalls(results, includeMetaData, metaFields, transxn);
            }
            finally
            {
                this.ConnectionLock.ExitReadLock();
            }
        }
   
        public override IEnumerable<Call> GetCalls(
            DateTime startTime,
            DateTime endTime,
            IEnumerable<int> idSet = null,
            long? limitTo = null,
            int? programId = null,
            KeyValuePair<string, string>? withMeta = null,
            bool includeMetaData = false,
            IEnumerable<string> metaFields = null,
            SqlTransaction transxn = null
            )
        {
            this.Connect();

            try
            {
                this.ConnectionLock.EnterReadLock();

                string sqlQuery;
                if (withMeta.HasValue == true)
                {
                    var kvp = withMeta.Value;
                    sqlQuery = SQL.CallsWithMeta(kvp.Key, kvp.Value,
                        idSet, limitTo, startTime, endTime, programId);
                }
                else
                {
                    sqlQuery = SQL.Calls(idSet, limitTo, startTime, endTime, programId);
                }

                var results = this.Query(sqlQuery, transxn);
                return this.CreateCalls(results, includeMetaData, metaFields, transxn);
            }
            finally
            {
                this.ConnectionLock.ExitReadLock();
            }
        }

        public override IEnumerable<Program> GetPrograms(
            IEnumerable<int> idSet = null,
            IEnumerable<string> nameSet = null,
            SqlTransaction transxn = null
            )
        {
            this.Connect();

            try
            {
                this.ConnectionLock.EnterReadLock();

                var results = this.Query(SQL.Programs(idSet, nameSet), transxn);
                if (results.Rows.Count == 0)
                {
                    return new Program[0];
                }

                var programs = new List<Program>(
                    from row in results.AsEnumerable() select this.CreateProgram(row));
                return programs;
            }
            finally
            {
                this.ConnectionLock.ExitReadLock();
            }
        }

        #endregion
    }
}

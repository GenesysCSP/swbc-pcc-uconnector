﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Genesys.SpeechMiner.WS;
using Genesys.SpeechMiner.WS.Impl;

namespace Genesys.SpeechMiner.WS
{
    public sealed class SMCallsClient : ICallsClient
    {
        #region Instance Properties

        public Version SMVersion
        {
            get
            {
                return _wsImpl.SMVersion;
            }
        }

        public TimeSpan? Timeout
        {
            get
            {
                return _wsImpl.Timeout;
            }
            set
            {
                _wsImpl.Timeout = value;
            }
        }

        public Uri URI
        {
            get
            {
                return _wsImpl.URI;
            }
        }

        #endregion

        #region Constructors

        private SMCallsClient(
            ICallsClient wsImpl
            )
        {
            if (wsImpl == null)
            {
                throw new ArgumentNullException("wsImpl");
            }

            _wsImpl = wsImpl;
        }

        #endregion

        #region Class Methods

        public static SMCallsClient Create(
            Uri wsUri,
            Version smVersion,
            TimeSpan? timeout = null
            )
        {
            if (wsUri == null)
            {
                throw new ArgumentNullException("wsUri");
            }
            else if (smVersion == null)
            {
                throw new ArgumentNullException("smVersion");
            }

            ICallsClient wsImpl = null;
            switch (smVersion.Build)
            {
            case 6094:
                wsImpl = new CallsClient6094(wsUri, smVersion, timeout);
                break;

            default:
                // Try using the latest version.
                wsImpl = new CallsClient6094(wsUri, smVersion, timeout);
                break;
            }

            return new SMCallsClient(wsImpl);
        }

        #endregion

        #region Instance Methods

        public void GetTranscript(
            int callId,
            ref IDictionary<AudioChannel, string> transcriptData,
            params AudioChannel[] channels
            )
        {
            _wsImpl.GetTranscript(callId, ref transcriptData, channels);
        }

        public ContentEntry[] GetContent(
            int callId,
            params EntryType[] types
            )
        {
            return _wsImpl.GetContent(callId, types);
        }

        public void Login(
            string userName,
            string password,
            AuthenticationType authType = AuthenticationType.SpeechMiner
            )
        {
            _wsImpl.Login(userName, password, authType);
        }

        public void Logout()
        {
            _wsImpl.Logout();
        }

        #endregion

        #region Instance Data

        private readonly ICallsClient _wsImpl;

        #endregion
    }
}

﻿
namespace Genesys.SpeechMiner.WS
{
    #region Enums

    public enum AudioChannel
    {
        None,
        Left,
        Right,
    }

    public enum AuthenticationType
    {
        SpeechMiner,
        Windows
    }

    public enum EntryType
    {
        NLE,
        Topic,
        Script,
        Comment,
        LVCSR,
    }

    public static class EnumExtensions
    {
        public static string WSValue(
            this AuthenticationType authType
            )
        {
            return authType.ToString().ToUpper();
        }

        public static string MetaFieldName(
            this AudioChannel channel
            )
        {
            switch (channel)
            {
            case AudioChannel.Left:
                return "LeftChannel";

            case AudioChannel.Right:
                return "RightChannel";

            default:
                return string.Format("{0}Channel", channel);
            }
        }
    }

    #endregion

    #region Classes

    public sealed class ContentEntry
    {        
        public string Audit;
        public AudioChannel Channel;
        public float Confidence;
        public string CreationTime;
        public double EndTime;
        public bool Highlight;
        public int ID;
        public bool IsReadOnly;
        public string Name;
        public string NoteType;
        public string Owner;
        public int ResourceID;
        public double StartTime;
        public string Text;
        public string Tooltip;
        public EntryType Type;
    }

    #endregion
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Genesys.SpeechMiner.WS
{
    interface ICallsClient
    {
        Uri URI { get; }
        Version SMVersion { get; }
        TimeSpan? Timeout { get; set; }

        ContentEntry[] GetContent(int callId, params EntryType[] entryTypes);
        void GetTranscript(int callId, ref IDictionary<AudioChannel, string> transcriptData, params AudioChannel[] channels);
        void Login(string userName, string password, AuthenticationType authType = AuthenticationType.SpeechMiner);
        void Logout();
    }
}

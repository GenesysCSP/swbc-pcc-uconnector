﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using GenUtils;


namespace Genesys.SpeechMiner.WS.Impl
{
    public sealed partial class CallsClient6094 : CallsClientBase
    {
        #region Constructors

        public CallsClient6094(
            Uri wsUri,
            Version smVersion,
            TimeSpan? timeout = null
            ) : base(wsUri, smVersion)
        {
            _wsClient = new callsWS(wsUri);
            _wsClient.CookieContainer = new CookieContainer();
            this.Timeout = timeout;
        }

        #endregion

        #region Class Methods

        private static WS.ContentEntry[] ConvertFrom(
            CallsClient6094.contentEntry[] entries,
            params CallsClient6094.entryType[] types
            )
        {
            return (from entry in entries
                    where types.Length == 0 || types.Contains(entry.type) == true
                    select new WS.ContentEntry()
                        {
                            Audit = entry.audit,
                            Channel = ConvertFrom(entry.channel),
                            Confidence = entry.confidence,
                            CreationTime = entry.creationTime,
                            EndTime = entry.endTime,
                            Highlight = entry.highlight,
                            ID = entry.id,
                            IsReadOnly = entry.isReadOnly,
                            Name = entry.name,
                            NoteType = entry.noteType,
                            Owner = entry.owner,
                            ResourceID = entry.resourceId,
                            StartTime = entry.startTime,
                            Text = entry.text,
                            Tooltip = entry.tooltip,
                            Type = ConvertFrom(entry.type)
                        }).ToArray();

        }

        private static WS.AudioChannel ConvertFrom(
            CallsClient6094.AudioChannel channel
            )
        {
            switch (channel)
            {
            case CallsClient6094.AudioChannel.LEFT:
                return WS.AudioChannel.Left;

            case CallsClient6094.AudioChannel.RIGHT:
                return WS.AudioChannel.Right;

            case CallsClient6094.AudioChannel.NONE:
                return WS.AudioChannel.None;
            }

            throw new ArgumentException("unknown channel type");
        }

        private static WS.EntryType ConvertFrom(
            CallsClient6094.entryType type
            )
        {
            switch (type)
            {
            case entryType.NLE:
                return WS.EntryType.NLE;

            case entryType.Topic:
                return WS.EntryType.Topic;

            case entryType.Script:
                return WS.EntryType.Script;

            case entryType.Comment:
                return WS.EntryType.Comment;

            case entryType.LVCSR:
                return WS.EntryType.LVCSR;
            }

            throw new ArgumentException("unknown entry type");
        }

        private static CallsClient6094.entryType ConvertTo(
            WS.EntryType type
            )
        {
            switch (type)
            {
            case WS.EntryType.NLE:
                return entryType.NLE;

            case WS.EntryType.Topic:
                return entryType.Topic;

            case WS.EntryType.Script:
                return entryType.Script;

            case WS.EntryType.Comment:
                return entryType.Comment;

            case WS.EntryType.LVCSR:
                return entryType.LVCSR;
            }

            throw new ArgumentException("unknown entry type");
        }

        #endregion

        #region Instance Methods

        public override ContentEntry[] GetContent(
            int callId,
            params EntryType[] types
            )
        {
            var callContent = _wsClient.getUnifiedCallEvents(
                callId.ToString(), null, this.SessionToken);
            if(callContent.ContentEntries.Length == 0)
            {
                return new ContentEntry[0];
            }

            var wsTypes = new CallsClient6094.entryType[types.Length];
            types.ForEach((idx, type) => { wsTypes[idx] = ConvertTo(type); });

            return ConvertFrom(callContent.ContentEntries, wsTypes);
        }

        public override void Login(
            string userName, 
            string password, 
            AuthenticationType authType
            )
        {
            if (userName == null)
            {
                throw new ArgumentNullException("userName");
            }

            userName = userName.Trim();
            if (String.IsNullOrEmpty(userName) == true)
            {
                throw new ArgumentException("no username specified");
            }

            this.SessionToken = _wsClient.Login(userName, password, authType.WSValue());
        }

        public override void Logout()
        {
            this.SessionToken = null;
            _wsClient.Logout();
        }

        protected override TimeSpan? OnSetTimeout(
            TimeSpan? timeout
            )
        {
            if (timeout.HasValue == false)
            {
                return base.OnSetTimeout(timeout);
            }

            _wsClient.Timeout = (int)timeout.Value.TotalMilliseconds;
            return timeout;
        }

        #endregion

        #region Instance Data

        private readonly CallsClient6094.callsWS _wsClient;

        #endregion
    }
}

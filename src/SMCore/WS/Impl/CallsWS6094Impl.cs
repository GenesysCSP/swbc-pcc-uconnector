﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Genesys.SpeechMiner.WS.Impl
{
    public sealed partial class CallsClient6094 : ICallsClient
    {
        /// <remarks/>
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
        public partial class callsWS : System.Web.Services.Protocols.HttpPostClientProtocol
        {
            private System.Threading.SendOrPostCallback LoginOperationCompleted;

            private System.Threading.SendOrPostCallback PurgeAllCallsOperationCompleted;

            private System.Threading.SendOrPostCallback PurgeCallsOperationCompleted;

            private System.Threading.SendOrPostCallback getCallsByCallIdsIgnorePartitionOperationCompleted;

            private System.Threading.SendOrPostCallback getCallsLuceneOperationCompleted;

            private System.Threading.SendOrPostCallback getRelatedWordsOperationCompleted;

            private System.Threading.SendOrPostCallback getUnifiedCallEventsOperationCompleted;

            private System.Threading.SendOrPostCallback getHighlightTextOperationCompleted;

            private System.Threading.SendOrPostCallback LogoutOperationCompleted;

            /// <remarks/>
            public callsWS(Uri uri)
            {
                this.Url = uri.ToString();
            }

            /// <remarks/>
            public event LoginCompletedEventHandler LoginCompleted;

            /// <remarks/>
            public event PurgeAllCallsCompletedEventHandler PurgeAllCallsCompleted;

            /// <remarks/>
            public event PurgeCallsCompletedEventHandler PurgeCallsCompleted;

            /// <remarks/>
            public event getCallsByCallIdsIgnorePartitionCompletedEventHandler getCallsByCallIdsIgnorePartitionCompleted;

            /// <remarks/>
            public event getCallsLuceneCompletedEventHandler getCallsLuceneCompleted;

            /// <remarks/>
            public event getRelatedWordsCompletedEventHandler getRelatedWordsCompleted;

            /// <remarks/>
            public event getUnifiedCallEventsCompletedEventHandler getUnifiedCallEventsCompleted;

            /// <remarks/>
            public event getHighlightTextCompletedEventHandler getHighlightTextCompleted;

            /// <remarks/>
            public event LogoutCompletedEventHandler LogoutCompleted;

            /// <remarks/>
            [System.Web.Services.Protocols.HttpMethodAttribute(typeof(System.Web.Services.Protocols.XmlReturnReader), typeof(System.Web.Services.Protocols.HtmlFormParameterWriter))]
            [return: System.Xml.Serialization.XmlRootAttribute("string", Namespace = "http://tempuri.org/", IsNullable = true)]
            public string Login(string user, string password, string authenticationType)
            {
                return ((string)(this.Invoke("Login", (this.Url + "/Login"), new object[] {
                        user,
                        password,
                        authenticationType})));
            }

            /// <remarks/>
            public System.IAsyncResult BeginLogin(string user, string password, string authenticationType, System.AsyncCallback callback, object asyncState)
            {
                return this.BeginInvoke("Login", (this.Url + "/Login"), new object[] {
                        user,
                        password,
                        authenticationType}, callback, asyncState);
            }

            /// <remarks/>
            public string EndLogin(System.IAsyncResult asyncResult)
            {
                return ((string)(this.EndInvoke(asyncResult)));
            }

            /// <remarks/>
            public void LoginAsync(string user, string password, string authenticationType)
            {
                this.LoginAsync(user, password, authenticationType, null);
            }

            /// <remarks/>
            public void LoginAsync(string user, string password, string authenticationType, object userState)
            {
                if ((this.LoginOperationCompleted == null))
                {
                    this.LoginOperationCompleted = new System.Threading.SendOrPostCallback(this.OnLoginOperationCompleted);
                }
                this.InvokeAsync("Login", (this.Url + "/Login"), new object[] {
                        user,
                        password,
                        authenticationType}, this.LoginOperationCompleted, userState);
            }

            private void OnLoginOperationCompleted(object arg)
            {
                if ((this.LoginCompleted != null))
                {
                    System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                    this.LoginCompleted(this, new LoginCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
                }
            }

            /// <remarks/>
            [System.Web.Services.Protocols.HttpMethodAttribute(typeof(System.Web.Services.Protocols.NopReturnReader), typeof(System.Web.Services.Protocols.HtmlFormParameterWriter))]
            public void PurgeAllCalls(string _userToken)
            {
                this.Invoke("PurgeAllCalls", (this.Url + "/PurgeAllCalls"), new object[] {
                        _userToken});
            }

            /// <remarks/>
            public System.IAsyncResult BeginPurgeAllCalls(string _userToken, System.AsyncCallback callback, object asyncState)
            {
                return this.BeginInvoke("PurgeAllCalls", (this.Url + "/PurgeAllCalls"), new object[] {
                        _userToken}, callback, asyncState);
            }

            /// <remarks/>
            public void EndPurgeAllCalls(System.IAsyncResult asyncResult)
            {
                this.EndInvoke(asyncResult);
            }

            /// <remarks/>
            public void PurgeAllCallsAsync(string _userToken)
            {
                this.PurgeAllCallsAsync(_userToken, null);
            }

            /// <remarks/>
            public void PurgeAllCallsAsync(string _userToken, object userState)
            {
                if ((this.PurgeAllCallsOperationCompleted == null))
                {
                    this.PurgeAllCallsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnPurgeAllCallsOperationCompleted);
                }
                this.InvokeAsync("PurgeAllCalls", (this.Url + "/PurgeAllCalls"), new object[] {
                        _userToken}, this.PurgeAllCallsOperationCompleted, userState);
            }

            private void OnPurgeAllCallsOperationCompleted(object arg)
            {
                if ((this.PurgeAllCallsCompleted != null))
                {
                    System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                    this.PurgeAllCallsCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
                }
            }

            /// <remarks/>
            [System.Web.Services.Protocols.HttpMethodAttribute(typeof(System.Web.Services.Protocols.NopReturnReader), typeof(System.Web.Services.Protocols.HtmlFormParameterWriter))]
            public void PurgeCalls(string _fromDate, string _toDate, string[] _programs, string _userToken)
            {
                this.Invoke("PurgeCalls", (this.Url + "/PurgeCalls"), new object[] {
                        _fromDate,
                        _toDate,
                        _programs,
                        _userToken});
            }

            /// <remarks/>
            public System.IAsyncResult BeginPurgeCalls(string _fromDate, string _toDate, string[] _programs, string _userToken, System.AsyncCallback callback, object asyncState)
            {
                return this.BeginInvoke("PurgeCalls", (this.Url + "/PurgeCalls"), new object[] {
                        _fromDate,
                        _toDate,
                        _programs,
                        _userToken}, callback, asyncState);
            }

            /// <remarks/>
            public void EndPurgeCalls(System.IAsyncResult asyncResult)
            {
                this.EndInvoke(asyncResult);
            }

            /// <remarks/>
            public void PurgeCallsAsync(string _fromDate, string _toDate, string[] _programs, string _userToken)
            {
                this.PurgeCallsAsync(_fromDate, _toDate, _programs, _userToken, null);
            }

            /// <remarks/>
            public void PurgeCallsAsync(string _fromDate, string _toDate, string[] _programs, string _userToken, object userState)
            {
                if ((this.PurgeCallsOperationCompleted == null))
                {
                    this.PurgeCallsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnPurgeCallsOperationCompleted);
                }
                this.InvokeAsync("PurgeCalls", (this.Url + "/PurgeCalls"), new object[] {
                        _fromDate,
                        _toDate,
                        _programs,
                        _userToken}, this.PurgeCallsOperationCompleted, userState);
            }

            private void OnPurgeCallsOperationCompleted(object arg)
            {
                if ((this.PurgeCallsCompleted != null))
                {
                    System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                    this.PurgeCallsCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
                }
            }

            /// <remarks/>
            [System.Web.Services.Protocols.HttpMethodAttribute(typeof(System.Web.Services.Protocols.XmlReturnReader), typeof(System.Web.Services.Protocols.HtmlFormParameterWriter))]
            public System.Data.DataTable getCallsByCallIdsIgnorePartition(string _listId, string _userToken, string isForm)
            {
                return ((System.Data.DataTable)(this.Invoke("getCallsByCallIdsIgnorePartition", (this.Url + "/getCallsByCallIdsIgnorePartition"), new object[] {
                        _listId,
                        _userToken,
                        isForm})));
            }

            /// <remarks/>
            public System.IAsyncResult BegingetCallsByCallIdsIgnorePartition(string _listId, string _userToken, string isForm, System.AsyncCallback callback, object asyncState)
            {
                return this.BeginInvoke("getCallsByCallIdsIgnorePartition", (this.Url + "/getCallsByCallIdsIgnorePartition"), new object[] {
                        _listId,
                        _userToken,
                        isForm}, callback, asyncState);
            }

            /// <remarks/>
            public System.Data.DataTable EndgetCallsByCallIdsIgnorePartition(System.IAsyncResult asyncResult)
            {
                return ((System.Data.DataTable)(this.EndInvoke(asyncResult)));
            }

            /// <remarks/>
            public void getCallsByCallIdsIgnorePartitionAsync(string _listId, string _userToken, string isForm)
            {
                this.getCallsByCallIdsIgnorePartitionAsync(_listId, _userToken, isForm, null);
            }

            /// <remarks/>
            public void getCallsByCallIdsIgnorePartitionAsync(string _listId, string _userToken, string isForm, object userState)
            {
                if ((this.getCallsByCallIdsIgnorePartitionOperationCompleted == null))
                {
                    this.getCallsByCallIdsIgnorePartitionOperationCompleted = new System.Threading.SendOrPostCallback(this.OngetCallsByCallIdsIgnorePartitionOperationCompleted);
                }
                this.InvokeAsync("getCallsByCallIdsIgnorePartition", (this.Url + "/getCallsByCallIdsIgnorePartition"), new object[] {
                        _listId,
                        _userToken,
                        isForm}, this.getCallsByCallIdsIgnorePartitionOperationCompleted, userState);
            }

            private void OngetCallsByCallIdsIgnorePartitionOperationCompleted(object arg)
            {
                if ((this.getCallsByCallIdsIgnorePartitionCompleted != null))
                {
                    System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                    this.getCallsByCallIdsIgnorePartitionCompleted(this, new getCallsByCallIdsIgnorePartitionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
                }
            }

            /// <remarks/>
            [System.Web.Services.Protocols.HttpMethodAttribute(typeof(System.Web.Services.Protocols.XmlReturnReader), typeof(System.Web.Services.Protocols.HtmlFormParameterWriter))]
            public System.Data.DataTable getCallsLucene(string _searchFilterStr, string[] _filterCallIDs, string _userName, string _userToken)
            {
                return ((System.Data.DataTable)(this.Invoke("getCallsLucene", (this.Url + "/getCallsLucene"), new object[] {
                        _searchFilterStr,
                        _filterCallIDs,
                        _userName,
                        _userToken})));
            }

            /// <remarks/>
            public System.IAsyncResult BegingetCallsLucene(string _searchFilterStr, string[] _filterCallIDs, string _userName, string _userToken, System.AsyncCallback callback, object asyncState)
            {
                return this.BeginInvoke("getCallsLucene", (this.Url + "/getCallsLucene"), new object[] {
                        _searchFilterStr,
                        _filterCallIDs,
                        _userName,
                        _userToken}, callback, asyncState);
            }

            /// <remarks/>
            public System.Data.DataTable EndgetCallsLucene(System.IAsyncResult asyncResult)
            {
                return ((System.Data.DataTable)(this.EndInvoke(asyncResult)));
            }

            /// <remarks/>
            public void getCallsLuceneAsync(string _searchFilterStr, string[] _filterCallIDs, string _userName, string _userToken)
            {
                this.getCallsLuceneAsync(_searchFilterStr, _filterCallIDs, _userName, _userToken, null);
            }

            /// <remarks/>
            public void getCallsLuceneAsync(string _searchFilterStr, string[] _filterCallIDs, string _userName, string _userToken, object userState)
            {
                if ((this.getCallsLuceneOperationCompleted == null))
                {
                    this.getCallsLuceneOperationCompleted = new System.Threading.SendOrPostCallback(this.OngetCallsLuceneOperationCompleted);
                }
                this.InvokeAsync("getCallsLucene", (this.Url + "/getCallsLucene"), new object[] {
                        _searchFilterStr,
                        _filterCallIDs,
                        _userName,
                        _userToken}, this.getCallsLuceneOperationCompleted, userState);
            }

            private void OngetCallsLuceneOperationCompleted(object arg)
            {
                if ((this.getCallsLuceneCompleted != null))
                {
                    System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                    this.getCallsLuceneCompleted(this, new getCallsLuceneCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
                }
            }

            /// <remarks/>
            [System.Web.Services.Protocols.HttpMethodAttribute(typeof(System.Web.Services.Protocols.XmlReturnReader), typeof(System.Web.Services.Protocols.HtmlFormParameterWriter))]
            public System.Data.DataTable getRelatedWords(string _root, string _searchFilterStr, string _getRootNumOfCalls, string _userToken)
            {
                return ((System.Data.DataTable)(this.Invoke("getRelatedWords", (this.Url + "/getRelatedWords"), new object[] {
                        _root,
                        _searchFilterStr,
                        _getRootNumOfCalls,
                        _userToken})));
            }

            /// <remarks/>
            public System.IAsyncResult BegingetRelatedWords(string _root, string _searchFilterStr, string _getRootNumOfCalls, string _userToken, System.AsyncCallback callback, object asyncState)
            {
                return this.BeginInvoke("getRelatedWords", (this.Url + "/getRelatedWords"), new object[] {
                        _root,
                        _searchFilterStr,
                        _getRootNumOfCalls,
                        _userToken}, callback, asyncState);
            }

            /// <remarks/>
            public System.Data.DataTable EndgetRelatedWords(System.IAsyncResult asyncResult)
            {
                return ((System.Data.DataTable)(this.EndInvoke(asyncResult)));
            }

            /// <remarks/>
            public void getRelatedWordsAsync(string _root, string _searchFilterStr, string _getRootNumOfCalls, string _userToken)
            {
                this.getRelatedWordsAsync(_root, _searchFilterStr, _getRootNumOfCalls, _userToken, null);
            }

            /// <remarks/>
            public void getRelatedWordsAsync(string _root, string _searchFilterStr, string _getRootNumOfCalls, string _userToken, object userState)
            {
                if ((this.getRelatedWordsOperationCompleted == null))
                {
                    this.getRelatedWordsOperationCompleted = new System.Threading.SendOrPostCallback(this.OngetRelatedWordsOperationCompleted);
                }
                this.InvokeAsync("getRelatedWords", (this.Url + "/getRelatedWords"), new object[] {
                        _root,
                        _searchFilterStr,
                        _getRootNumOfCalls,
                        _userToken}, this.getRelatedWordsOperationCompleted, userState);
            }

            private void OngetRelatedWordsOperationCompleted(object arg)
            {
                if ((this.getRelatedWordsCompleted != null))
                {
                    System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                    this.getRelatedWordsCompleted(this, new getRelatedWordsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
                }
            }

            /// <remarks/>
            [System.Web.Services.Protocols.HttpMethodAttribute(typeof(System.Web.Services.Protocols.XmlReturnReader), typeof(System.Web.Services.Protocols.HtmlFormParameterWriter))]
            public CallContentData getUnifiedCallEvents(string callId, string searchTerms, string _userToken)
            {
                return ((CallContentData)(this.Invoke("getUnifiedCallEvents", (this.Url + "/getUnifiedCallEvents"), new object[] {
                        callId,
                        searchTerms,
                        _userToken})));
            }

            /// <remarks/>
            public System.IAsyncResult BegingetUnifiedCallEvents(string callId, string searchTerms, string _userToken, System.AsyncCallback callback, object asyncState)
            {
                return this.BeginInvoke("getUnifiedCallEvents", (this.Url + "/getUnifiedCallEvents"), new object[] {
                        callId,
                        searchTerms,
                        _userToken}, callback, asyncState);
            }

            /// <remarks/>
            public CallContentData EndgetUnifiedCallEvents(System.IAsyncResult asyncResult)
            {
                return ((CallContentData)(this.EndInvoke(asyncResult)));
            }

            /// <remarks/>
            public void getUnifiedCallEventsAsync(string callId, string searchTerms, string _userToken)
            {
                this.getUnifiedCallEventsAsync(callId, searchTerms, _userToken, null);
            }

            /// <remarks/>
            public void getUnifiedCallEventsAsync(string callId, string searchTerms, string _userToken, object userState)
            {
                if ((this.getUnifiedCallEventsOperationCompleted == null))
                {
                    this.getUnifiedCallEventsOperationCompleted = new System.Threading.SendOrPostCallback(this.OngetUnifiedCallEventsOperationCompleted);
                }
                this.InvokeAsync("getUnifiedCallEvents", (this.Url + "/getUnifiedCallEvents"), new object[] {
                        callId,
                        searchTerms,
                        _userToken}, this.getUnifiedCallEventsOperationCompleted, userState);
            }

            private void OngetUnifiedCallEventsOperationCompleted(object arg)
            {
                if ((this.getUnifiedCallEventsCompleted != null))
                {
                    System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                    this.getUnifiedCallEventsCompleted(this, new getUnifiedCallEventsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
                }
            }

            /// <remarks/>
            [System.Web.Services.Protocols.HttpMethodAttribute(typeof(System.Web.Services.Protocols.XmlReturnReader), typeof(System.Web.Services.Protocols.HtmlFormParameterWriter))]
            [return: System.Xml.Serialization.XmlRootAttribute("string", Namespace = "http://tempuri.org/", IsNullable = true)]
            public string getHighlightText(string field, string text, string textId, string[] searchedTerms, string highlightPhraseStart, string highlightPhraseEnd, string _userToken)
            {
                return ((string)(this.Invoke("getHighlightText", (this.Url + "/getHighlightText"), new object[] {
                        field,
                        text,
                        textId,
                        searchedTerms,
                        highlightPhraseStart,
                        highlightPhraseEnd,
                        _userToken})));
            }

            /// <remarks/>
            public System.IAsyncResult BegingetHighlightText(string field, string text, string textId, string[] searchedTerms, string highlightPhraseStart, string highlightPhraseEnd, string _userToken, System.AsyncCallback callback, object asyncState)
            {
                return this.BeginInvoke("getHighlightText", (this.Url + "/getHighlightText"), new object[] {
                        field,
                        text,
                        textId,
                        searchedTerms,
                        highlightPhraseStart,
                        highlightPhraseEnd,
                        _userToken}, callback, asyncState);
            }

            /// <remarks/>
            public string EndgetHighlightText(System.IAsyncResult asyncResult)
            {
                return ((string)(this.EndInvoke(asyncResult)));
            }

            /// <remarks/>
            public void getHighlightTextAsync(string field, string text, string textId, string[] searchedTerms, string highlightPhraseStart, string highlightPhraseEnd, string _userToken)
            {
                this.getHighlightTextAsync(field, text, textId, searchedTerms, highlightPhraseStart, highlightPhraseEnd, _userToken, null);
            }

            /// <remarks/>
            public void getHighlightTextAsync(string field, string text, string textId, string[] searchedTerms, string highlightPhraseStart, string highlightPhraseEnd, string _userToken, object userState)
            {
                if ((this.getHighlightTextOperationCompleted == null))
                {
                    this.getHighlightTextOperationCompleted = new System.Threading.SendOrPostCallback(this.OngetHighlightTextOperationCompleted);
                }
                this.InvokeAsync("getHighlightText", (this.Url + "/getHighlightText"), new object[] {
                        field,
                        text,
                        textId,
                        searchedTerms,
                        highlightPhraseStart,
                        highlightPhraseEnd,
                        _userToken}, this.getHighlightTextOperationCompleted, userState);
            }

            private void OngetHighlightTextOperationCompleted(object arg)
            {
                if ((this.getHighlightTextCompleted != null))
                {
                    System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                    this.getHighlightTextCompleted(this, new getHighlightTextCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
                }
            }

            /// <remarks/>
            [System.Web.Services.Protocols.HttpMethodAttribute(typeof(System.Web.Services.Protocols.NopReturnReader), typeof(System.Web.Services.Protocols.HtmlFormParameterWriter))]
            public void Logout()
            {
                this.Invoke("Logout", (this.Url + "/Logout"), new object[0]);
            }

            /// <remarks/>
            public System.IAsyncResult BeginLogout(System.AsyncCallback callback, object asyncState)
            {
                return this.BeginInvoke("Logout", (this.Url + "/Logout"), new object[0], callback, asyncState);
            }

            /// <remarks/>
            public void EndLogout(System.IAsyncResult asyncResult)
            {
                this.EndInvoke(asyncResult);
            }

            /// <remarks/>
            public void LogoutAsync()
            {
                this.LogoutAsync(null);
            }

            /// <remarks/>
            public void LogoutAsync(object userState)
            {
                if ((this.LogoutOperationCompleted == null))
                {
                    this.LogoutOperationCompleted = new System.Threading.SendOrPostCallback(this.OnLogoutOperationCompleted);
                }
                this.InvokeAsync("Logout", (this.Url + "/Logout"), new object[0], this.LogoutOperationCompleted, userState);
            }

            private void OnLogoutOperationCompleted(object arg)
            {
                if ((this.LogoutCompleted != null))
                {
                    System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                    this.LogoutCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
                }
            }

            /// <remarks/>
            public new void CancelAsync(object userState)
            {
                base.CancelAsync(userState);
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
        [System.SerializableAttribute()]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://tempuri.org/", IsNullable = true)]
        public partial class CallContentData
        {

            private contentEntry[] contentEntriesField;

            private double firstTermTimeField;

            /// <remarks/>
            public contentEntry[] ContentEntries
            {
                get
                {
                    return this.contentEntriesField;
                }
                set
                {
                    this.contentEntriesField = value;
                }
            }

            /// <remarks/>
            public double FirstTermTime
            {
                get
                {
                    return this.firstTermTimeField;
                }
                set
                {
                    this.firstTermTimeField = value;
                }
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
        [System.SerializableAttribute()]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
        public partial class contentEntry
        {

            private int idField;

            private entryType typeField;

            private string nameField;

            private string textField;

            private AudioChannel channelField;

            private double startTimeField;

            private double endTimeField;

            private string ownerField;

            private string noteTypeField;

            private float confidenceField;

            private string creationTimeField;

            private string tooltipField;

            private bool highlightField;

            private string auditField;

            private int resourceIdField;

            private bool isReadOnlyField;

            /// <remarks/>
            public int id
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }

            /// <remarks/>
            public entryType type
            {
                get
                {
                    return this.typeField;
                }
                set
                {
                    this.typeField = value;
                }
            }

            /// <remarks/>
            public string name
            {
                get
                {
                    return this.nameField;
                }
                set
                {
                    this.nameField = value;
                }
            }

            /// <remarks/>
            public string text
            {
                get
                {
                    return this.textField;
                }
                set
                {
                    this.textField = value;
                }
            }

            /// <remarks/>
            public AudioChannel channel
            {
                get
                {
                    return this.channelField;
                }
                set
                {
                    this.channelField = value;
                }
            }

            /// <remarks/>
            public double startTime
            {
                get
                {
                    return this.startTimeField;
                }
                set
                {
                    this.startTimeField = value;
                }
            }

            /// <remarks/>
            public double endTime
            {
                get
                {
                    return this.endTimeField;
                }
                set
                {
                    this.endTimeField = value;
                }
            }

            /// <remarks/>
            public string owner
            {
                get
                {
                    return this.ownerField;
                }
                set
                {
                    this.ownerField = value;
                }
            }

            /// <remarks/>
            public string noteType
            {
                get
                {
                    return this.noteTypeField;
                }
                set
                {
                    this.noteTypeField = value;
                }
            }

            /// <remarks/>
            public float confidence
            {
                get
                {
                    return this.confidenceField;
                }
                set
                {
                    this.confidenceField = value;
                }
            }

            /// <remarks/>
            public string creationTime
            {
                get
                {
                    return this.creationTimeField;
                }
                set
                {
                    this.creationTimeField = value;
                }
            }

            /// <remarks/>
            public string tooltip
            {
                get
                {
                    return this.tooltipField;
                }
                set
                {
                    this.tooltipField = value;
                }
            }

            /// <remarks/>
            public bool highlight
            {
                get
                {
                    return this.highlightField;
                }
                set
                {
                    this.highlightField = value;
                }
            }

            /// <remarks/>
            public string audit
            {
                get
                {
                    return this.auditField;
                }
                set
                {
                    this.auditField = value;
                }
            }

            /// <remarks/>
            public int resourceId
            {
                get
                {
                    return this.resourceIdField;
                }
                set
                {
                    this.resourceIdField = value;
                }
            }

            /// <remarks/>
            public bool isReadOnly
            {
                get
                {
                    return this.isReadOnlyField;
                }
                set
                {
                    this.isReadOnlyField = value;
                }
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
        [System.SerializableAttribute()]
        [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
        public enum entryType
        {

            /// <remarks/>
            NLE,

            /// <remarks/>
            Topic,

            /// <remarks/>
            Script,

            /// <remarks/>
            Comment,

            /// <remarks/>
            LVCSR,
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
        [System.SerializableAttribute()]
        [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
        public enum AudioChannel
        {

            /// <remarks/>
            NONE,

            /// <remarks/>
            LEFT,

            /// <remarks/>
            RIGHT,
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
        public delegate void LoginCompletedEventHandler(object sender, LoginCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class LoginCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal LoginCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public string Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((string)(this.results[0]));
                }
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
        public delegate void PurgeAllCallsCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
        public delegate void PurgeCallsCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
        public delegate void getCallsByCallIdsIgnorePartitionCompletedEventHandler(object sender, getCallsByCallIdsIgnorePartitionCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class getCallsByCallIdsIgnorePartitionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal getCallsByCallIdsIgnorePartitionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public System.Data.DataTable Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((System.Data.DataTable)(this.results[0]));
                }
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
        public delegate void getCallsLuceneCompletedEventHandler(object sender, getCallsLuceneCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class getCallsLuceneCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal getCallsLuceneCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public System.Data.DataTable Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((System.Data.DataTable)(this.results[0]));
                }
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
        public delegate void getRelatedWordsCompletedEventHandler(object sender, getRelatedWordsCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class getRelatedWordsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal getRelatedWordsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public System.Data.DataTable Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((System.Data.DataTable)(this.results[0]));
                }
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
        public delegate void getUnifiedCallEventsCompletedEventHandler(object sender, getUnifiedCallEventsCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class getUnifiedCallEventsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal getUnifiedCallEventsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public CallContentData Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((CallContentData)(this.results[0]));
                }
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
        public delegate void getHighlightTextCompletedEventHandler(object sender, getHighlightTextCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class getHighlightTextCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal getHighlightTextCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public string Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((string)(this.results[0]));
                }
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
        public delegate void LogoutCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);
    }
}

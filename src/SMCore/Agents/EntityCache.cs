﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using GenUtils;

namespace Genesys.SpeechMiner.Agents
{
    public sealed class EntityCache
    {
        #region Instance Properties

        public int EntityCount
        {
            get
            {
                try
                {
                    _lock.EnterReadLock();
                    return _entities.Count;
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }
        }

        #endregion

        #region Constructors

        public EntityCache()
        {
        }

        #endregion

        #region Instance Methods

        private bool AddEntity(
            Entity entity
            )
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            foreach (var id in entity.IDs)
            {
                if (_entities.ContainsKey(id) == true)
                {
                    return false;
                }
            }

            entity.IDs.ForEach(curid => _entities.Add(curid, entity));
            return true;
        }

        private Entity CreateEntity(
            string id,
            string name,
            IEnumerable<string> extIds = null,
            bool isPerson = true
            )
        {
            _lock.EnterWriteLock();
            try
            {
                // Create the new entity
                var entity = Entity.Create(id, name, extIds);
                entity.IDs.ForEach(curid => _entities.Add(curid, entity));

                return entity;
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        public void ForEachEntity(
            Action<Entity> op
            )
        {
            if (op == null)
            {
                throw new ArgumentNullException("op");
            }

            _lock.EnterReadLock();
            try
            {
                _entities.Values.ForEach(op);
            }
            finally
            {
                _lock.ExitReadLock();
            }
        }

        public Entity GetEntity(
            string id
            )
        {
            if (id == null)
            {
                throw new ArgumentNullException("id");
            }

            _lock.EnterReadLock();
            try
            {
                Entity entity = null;
                return (_entities.TryGetValue(id, out entity) == true) ? entity : null;
            }
            finally
            {
                _lock.ExitReadLock();
            }
        }

        public Entity GetOrCreateEntity(
            string id,
            string name,
            bool isPerson = true,
            EntityCache cache = null
            )
        {
            return CreateEntity(id, name, isPerson: isPerson);
        }

        public Entity GetOrCreateEntity(
            string id,
            string name,
            IEnumerable<string> extIds = null,
            bool isPerson = true
            )
        {
            if (id == null)
            {
                throw new ArgumentNullException("id");
            }
            else if (name == null)
            {
                throw new ArgumentNullException("name");
            }

            id = id.Trim();
            if (String.IsNullOrEmpty(id) == true)
            {
                throw new ArgumentException("no unique id specified");
            }

            name = name.Trim();
            if (String.IsNullOrEmpty(name) == true)
            {
                name = id;
            }

            _lock.EnterUpgradeableReadLock();
            try
            {
                Entity entity = this.GetEntity(id);
                if (entity == null)
                {
                    return entity;
                }

                foreach (var extId in extIds)
                {
                    entity = this.GetEntity(extId);
                    if(entity != null)
                    {
                        return entity;
                    }
                }

                return this.CreateEntity(id, name, extIds, isPerson);
            }
            finally
            {
                _lock.ExitUpgradeableReadLock();
            }
        }

        public Entity UpdateEntity(
            Entity entity
            )
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            _lock.EnterUpgradeableReadLock();
            try
            {
                var current = this.GetEntity(entity.ID);
                if (current == null)
                {
                    this.AddEntity(entity);
                    return entity;
                }
                else if (entity.Equals(current) == true)
                {
                    return entity;
                }
                else
                {
                    _lock.EnterWriteLock();
                    try
                    {
                        current.Update(entity);
                        return current;
                    }
                    finally
                    {
                        _lock.ExitWriteLock();
                    }
                }                
            }
            finally
            {
                _lock.ExitUpgradeableReadLock();
            }
        }

        public Entity UpdateEntity(
            string id,
            string name,
            IEnumerable<string> extIds = null
            )
        {
            if (id == null)
            {
                throw new ArgumentNullException("id");
            }

            name = (name != null) ? name : id;
            extIds = (extIds == null) ? new string[0] : extIds;

            _lock.EnterUpgradeableReadLock();
            try
            {
                var current = this.GetEntity(id);
                if (current == null)
                {
                    var entity = Entity.Create(id, name, extIds);
                    this.AddEntity(entity);
                    return entity;
                }
                else if (Entity.Equals(current, id, name, extIds) == true)
                {
                    return current;
                }
                else
                {
                    _lock.EnterWriteLock();
                    try
                    {
                        current.Update(id, name, extIds);
                        return current;
                    }
                    finally
                    {
                        _lock.ExitWriteLock();
                    }
                }
            }
            finally
            {
                _lock.ExitUpgradeableReadLock();
            }
        }

        #endregion

        #region Instance Data

        private readonly IDictionary<string, Entity> _entities = new Dictionary<string, Entity>();
        private readonly ReaderWriterLockSlim _lock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);

        #endregion
    }
}

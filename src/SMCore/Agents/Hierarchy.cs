﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wintellect.PowerCollections;
using System.IO;
using GenUtils;
using System.Data;
using System.Threading;
using System.Xml;

namespace Genesys.SpeechMiner.Agents
{
    public sealed class Hierarchy
    {
        #region Instance Properties

        public int EntityCount
        {
            get
            {
                return _cache.EntityCount;
            }
        }

        #endregion

        #region Class Methods

        public static string FormatEntityPath(
            params string[] entityIds
            )
        {
            if (entityIds.Length == 0)
            {
                return String.Empty;
            }

            var path = new StringBuilder();
            foreach (var entityId in entityIds)
            {
                path.AppendFormat("{0}{1}", LEVEL_SEPARATOR, entityId);
            }

            return path.ToString();
        }

        private static string GetEntityId(
            string structureId
            )
        {
            if (structureId == null)
            {
                throw new ArgumentNullException("structureId");
            }

            var idx = structureId.LastIndexOf(LEVEL_SEPARATOR);
            if (idx == -1)
            {
                return structureId;
            }

            return structureId.Substring(idx + 1);
        }

        private static string GetParentId(
            string structureId
            )
        {
            if (structureId == null)
            {
                throw new ArgumentNullException("structureId");
            }

            // If it happenes to start with a LEVEL_SEPARATOR, cut it off.
            if(structureId.StartsWith(LEVEL_SEPARATOR) == true)
            {
                structureId = structureId.Substring(1);
            }

            var idx = structureId.LastIndexOf(LEVEL_SEPARATOR);
            if (idx == -1)
            {
                return null;
            }

            return structureId.Substring(0, idx);
        }

        #endregion

        #region Instance Methods

        public void Add(
            string parentId,
            Entity child
            )
        {
            if (parentId == null)
            {
                throw new ArgumentNullException("parentId");
            }
            else if (child == null)
            {
                throw new ArgumentNullException("child");
            }

            _lock.EnterWriteLock();
            try
            {
                if (parentId.Equals(child.ID) == true)
                {
                    this.AddRoot(child);
                    return;
                }

                child = _cache.UpdateEntity(child);
                this.UpdateEntityInHierarchy(parentId, child);
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        public void Add(
            string parentId,
            string id,
            string name,
            IEnumerable<string> extIds = null
            )
        {
            if (parentId == null)
            {
                throw new ArgumentNullException("parentId");
            }
            else if (id == null)
            {
                throw new ArgumentNullException("id");
            }
            else if (name == null)
            {
                throw new ArgumentNullException("name");
            }

            _lock.EnterWriteLock();
            try
            {
                if (parentId.Equals(id) == true)
                {
                    this.AddRoot(id, name, extIds);
                    return;
                }

                var child = _cache.UpdateEntity(id, name, extIds);
                this.UpdateEntityInHierarchy(parentId, child);
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        public void AddRoot(
            Entity entity
            )
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            _lock.EnterWriteLock();
            try
            {
                if (_structureIds.ContainsKey(entity.ID) == true)
                {
                    var newEntity = _cache.UpdateEntity(entity);
                    if (newEntity != null)
                    {
                        _structureIds.Remove(entity.ID);
                        _structureIds.Add(newEntity.ID, newEntity);
                        this.InvalidateBuiltHierarchy();
                        return;
                    }
                }

                _cache.UpdateEntity(entity);
                _structureIds.Add(entity.ID, entity);
                this.InvalidateBuiltHierarchy();
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        public void AddRoot(
            string id,
            string name = null,
            IEnumerable<string> extIds = null
            )
        {
            if (id == null)
            {
                throw new ArgumentNullException("id");
            }

            name = (name != null) ? name : id;

            _lock.EnterWriteLock();
            try
            {
                if (_structureIds.ContainsKey(id) == true)
                {
                    var newEntity = _cache.UpdateEntity(id, name, extIds);
                    if (newEntity != null)
                    {
                        _structureIds.Remove(id);
                        _structureIds.Add(newEntity.ID, newEntity);
                        this.InvalidateBuiltHierarchy();
                        return;
                    }
                }

                var entity = Entity.Create(id, name, extIds);
                _cache.UpdateEntity(entity);
                _structureIds.Add(id, entity);
                this.InvalidateBuiltHierarchy();
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        public IEnumerable<Entity> Build()
        {
            _lock.EnterReadLock();
            try
            {
                if (_builtHierarchy != null)
                {
                    return _builtHierarchy;
                }
                else if (_structureIds.Count == 0)
                {
                    _builtHierarchy = new Entity[0];
                    return _builtHierarchy;
                }

                var result = new List<Entity>();
                string curStructureId = null;
                Entity curEntity = null;

                foreach (var entry in _structureIds)
                {
                    var structureId = entry.Key;
                    var entity = entry.Value;

                    // Is this a root entity?
                    string parentStructureId = GetParentId(structureId);
                    if (parentStructureId == null)
                    {
                        result.Add(entity);
                        curStructureId = structureId;
                        curEntity = entity;
                        continue;
                    }

                    // Check if it matches the entity we already have.
                    if (parentStructureId.Equals(curStructureId) == true)
                    {
                        curEntity.Add(entity);
                        continue;
                    }

                    Entity parentEntity = null;
                    if (_structureIds.TryGetValue(parentStructureId, out parentEntity) == true)
                    {
                        curEntity = parentEntity;
                        curStructureId = parentStructureId;
                        parentEntity.Add(entity);
                        continue;
                    }
                }

                // Remove empty root nodes
                result.RemoveAll(root => root.Any() == false);
                _builtHierarchy = result;

                return _builtHierarchy;
            }
            finally
            {
                _lock.ExitReadLock();
            }
        }

        public DataSet BuildDataSet(
            bool supportForMultiExtIds = false
            )
        {
            // Create the in-memory tables.
            var dtEntities = new DataTable(TABLE_ENTITIES);
            dtEntities.Columns.Add("externalId", typeof(String));
            dtEntities.Columns.Add("name", typeof(String));
            dtEntities.Columns.Add("isPerson", typeof(int));

            var dtHierarchy = new DataTable(TABLE_HIERARCHY);
            dtHierarchy.Columns.Add("parentId", typeof(String)).AllowDBNull = true;
            dtHierarchy.Columns.Add("childId", typeof(String));

            var dtExternalIds = (supportForMultiExtIds == true) ?
                new DataTable(TABLE_EXTERNALIDS) : null;
            if (dtExternalIds != null)
            {
                dtExternalIds.Columns.Add("externalId", typeof(String));
                dtExternalIds.Columns.Add("smExternalId", typeof(String));
            }

            Action<Entity> processEntity = null;
            Entity curParent = null;

            processEntity = (entity) =>
            {
                var row = dtEntities.NewRow();
                row.SetField<String>("externalId", entity.ID);
                row.SetField<String>("name", entity.Name);
                row.SetField<int>("isPerson", 1);
                dtEntities.Rows.Add(row);

                if (supportForMultiExtIds == true)
                {
                    entity.IDs.ForEach(extId =>
                    {
                        row = dtExternalIds.NewRow();
                        row.SetField<String>("externalId", extId);
                        row.SetField<String>("smExternalId", entity.ID);
                        dtExternalIds.Rows.Add(row);
                    });
                }

                row = dtHierarchy.NewRow();
                row.SetField<String>("parentId", (curParent != null) ? curParent.ID : null);
                row.SetField<String>("childId", entity.ID);
                dtHierarchy.Rows.Add(row);

                var prevParent = curParent;
                curParent = entity;
                curParent.ForEach(processEntity);
                curParent = prevParent;
            };
     
            this.Build().ForEach(processEntity);

            // Create a distinct view of each of the tables to remove duplicates.
            dtEntities = dtEntities.DefaultView.ToTable(
                true, new string[] { "externalId", "name", "isPerson" });
            dtHierarchy = dtHierarchy.DefaultView.ToTable(
                true, new string[] { "parentId", "childId" });
            if (dtExternalIds != null)
            {
                dtExternalIds = dtExternalIds.DefaultView.ToTable(
                    true, new string[] { "externalId", "smExternalId" });
            }

            // Add the tables and create relationships between them, which 
            // will verify all is good.
            var dsAgents = new DataSet("AgentHierarchy");
            dsAgents.Tables.Add(dtEntities);
            dsAgents.Tables.Add(dtHierarchy);
            dsAgents.Relations.Add("Hierarchy2Entity",
                dtEntities.Columns["externalId"], dtHierarchy.Columns["childId"]);

            if (supportForMultiExtIds == true)
            {
                dsAgents.Tables.Add(dtExternalIds);
                dsAgents.Relations.Add("ExternalIDs2Entity",
                    dtEntities.Columns["externalId"], dtExternalIds.Columns["smExternalId"]);
            }

            return dsAgents;
        }

        public void ForEachEntity(
            Action<Entity> op
            )
        {
            if (op == null)
            {
                throw new ArgumentNullException("op");
            }

            _lock.EnterReadLock();
            try
            {
                _cache.ForEachEntity(op);
            }
            finally
            {
                _lock.ExitReadLock();
            }
        }

        private void InvalidateBuiltHierarchy()
        {
            _lock.EnterWriteLock();
            try
            {
                _builtHierarchy = null;
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        public void Print(
            TextWriter writer = null,
            string indentWith = "    "
            )
        {
            writer = (writer == null) ? Console.Out : writer;

            string indent = String.Empty;
            Action<Entity> printEntity = null;
            printEntity = (entity) =>
                {
                    writer.WriteLine(String.Format("{0}{1} ({2})",
                        indent, entity.Name, entity.ID));
                    indent += indentWith;
                    foreach (var child in entity)
                    {
                        printEntity(child);
                    }
                    indent = indent.Substring(indentWith.Length);
                };

            this.Build().ForEach(printEntity);
        }

        private void UpdateEntityInHierarchy(
            string parentId,
            Entity child
            )
        {
            if (parentId == null)
            {
                throw new ArgumentNullException("parentId");
            }
            else if (child == null)
            {
                throw new ArgumentNullException("child");
            }

            _lock.EnterWriteLock();
            try
            {
                var structureId = String.Format("{0}{1}{2}",
                    parentId, LEVEL_SEPARATOR, child.ID);
                bool parent = _structureIds.ContainsKey(parentId);
                bool current = _structureIds.ContainsKey(structureId);
                _structureIds.Remove(structureId);
                _structureIds.Add(structureId, child);

                // Make sure parent entries exist.
                var parentStructureId = GetParentId(structureId);
                while (String.IsNullOrEmpty(parentStructureId) == false)
                {
                    if (_structureIds.ContainsKey(parentStructureId) == true)
                    {
                        break;
                    }

                    var entityId = GetEntityId(parentStructureId);
                    var entity = Entity.Create(entityId, entityId);
                    _cache.UpdateEntity(entity);
                    _structureIds.Add(parentStructureId, entity);

                    parentStructureId = GetParentId(parentStructureId);
                }

                this.InvalidateBuiltHierarchy();
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        public void WriteAsXml(
            XmlWriter writer,
            bool writeDoc = true
            )
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }
            else if (writeDoc == true)
            {
                writer.WriteStartDocument(true);
            }

            Action<Entity> processEntity = null;
            processEntity = (entity) =>
            {
                writer.WriteStartElement("Entity");
                writer.WriteAttributeString("extId", entity.ID);
                writer.WriteAttributeString("name", entity.Name);

                if (entity.IDs.Any(id => id.Equals(entity.ID) == false) == true)
                {
                    writer.WriteStartElement("ExtIDs");
                    entity.IDs.ForEach(id =>
                    {
                        if (entity.ID.Equals(id) == false)
                        {
                            writer.WriteStartElement("ExtID");
                            writer.WriteAttributeString("value", id);
                            writer.WriteEndElement();
                        }
                    });

                    writer.WriteStartElement("ExtID");

                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
            };

            writer.WriteStartElement("AgentHierarchy");
            this.Build().ForEach(processEntity);
            writer.WriteEndElement();

            if (writeDoc == true)
            {
                writer.WriteEndDocument();
            }
        }

        #endregion

        #region Class Data

        public const string LEVEL_SEPARATOR = "/";
        public const string TABLE_ENTITIES = "agentEntitiesTbl";
        public const string TABLE_EXTERNALIDS = "agentExternalIds";
        public const string TABLE_HIERARCHY = "agentHierarchyTbl";

        #endregion

        #region Instance Data

        private readonly EntityCache _cache = new EntityCache();
        private readonly ReaderWriterLockSlim _lock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
        private readonly OrderedDictionary<string, Entity> _structureIds = new OrderedDictionary<string, Entity>((x, y) => { return x.CompareTo(y); });
        private IList<Entity> _builtHierarchy = null;

        #endregion
    }
}

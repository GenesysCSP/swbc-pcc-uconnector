Imports System.Windows.Forms
Imports System.Configuration
Public Class ViewConnectionStrings
  Inherits System.Windows.Forms.Form

  Private Sub ViewConnectionStrings_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    Me.PopulateConnectionListBox()
  End Sub
  Public Sub SetConfigConnectionString(ByVal _connectionName As String, ByVal _connectString As String, ByVal _encrypt As Boolean)
    'Set  <connectionStrings> property in appConnector.exe.config
    Dim settings As New ConnectionStringSettings
    settings.Name = _connectionName
    settings.ConnectionString = _connectString

    Dim config As System.Configuration.Configuration
    config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
    config.ConnectionStrings.ConnectionStrings.Remove(settings)
    config.ConnectionStrings.ConnectionStrings.Remove(_connectionName)
    config.ConnectionStrings.ConnectionStrings.Add(settings)


    If _encrypt Then

      config.ConnectionStrings.SectionInformation.ProtectSection(Nothing)

    End If

    config.ConnectionStrings.SectionInformation.ForceSave = True
    config.Save(ConfigurationSaveMode.Full)


    System.Configuration.ConfigurationManager.RefreshSection("connectionStrings")


  End Sub
  Public Sub DeleteConnectionString(ByVal _name As String)
    Dim settings As New ConnectionStringSettings
    settings.Name = _name

    Dim config As System.Configuration.Configuration
    config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
    config.ConnectionStrings.ConnectionStrings.Remove(settings.Name)
    config.ConnectionStrings.ConnectionStrings.Remove(_name)
    config.Save(ConfigurationSaveMode.Modified)
    
    System.Configuration.ConfigurationManager.RefreshSection("connectionStrings")
    
  End Sub
  Public Sub PopulateConnectionListBox()
    'Clear list box
    Me.lstConnectionStrings.Items.Clear()

    'Now populate it from the configuration file
    Dim settings As New ConnectionStringSettings
    Dim config As System.Configuration.Configuration
    config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
    For i As Integer = 0 To config.ConnectionStrings.ConnectionStrings.Count - 1
      settings = config.ConnectionStrings.ConnectionStrings.Item(i)
      If settings.Name = "LocalSqlServer" Then
        Continue For
      End If
      Me.lstConnectionStrings.Items.Add(settings.Name)
    Next

  End Sub
 
  Private Sub lstConnectionStrings_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstConnectionStrings.SelectedIndexChanged
    'Use the selected connection string to fill the text boxes on a new ConnectionDialog form.

    Dim strNameOfConnection As String = String.Empty
    strNameOfConnection = Me.lstConnectionStrings.SelectedItem().ToString

    Dim config As System.Configuration.Configuration
    config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
    Dim settingsCollection As ConnectionStringSettingsCollection = config.ConnectionStrings.ConnectionStrings

    Dim strConnection As String = String.Empty

    For Each settings As ConnectionStringSettings In settingsCollection
      If settings.Name = strNameOfConnection Then
        strConnection = settings.ConnectionString
        Exit For
      End If
    Next


    'Dim strConnection As String = ConfigurationManager.ConnectionStrings(strNameOfConnection).ToString()
    strConnection = strConnection.Trim()
    Dim frmConnectionDialog As New ConnectionDialog
    frmConnectionDialog.txtName.Text = strNameOfConnection

    frmConnectionDialog.txtConnectionString.Text = strConnection
    
    'Disable chkEncrypt if the connection strings section is encrypted.

    If config.ConnectionStrings.SectionInformation.IsProtected() Then

      frmConnectionDialog.txtConnectionString.UseSystemPasswordChar = True 'Do NOT show unencrypted connection string in text box
    Else
      frmConnectionDialog.txtConnectionString.UseSystemPasswordChar = False
    End If

    'Open the modal form which allows users to edit the connection string.
    frmConnectionDialog.ShowDialog(Me)


  End Sub


  Private Sub btnAddNewConnection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddNewConnection.Click
    Dim frmConnectionDialog As New ConnectionDialog
    frmConnectionDialog.Text() = "Add a new connection"
    frmConnectionDialog.chkLocked.Checked = False
    frmConnectionDialog.ShowDialog(Me)


  End Sub
End Class
Imports System.IO
Imports System.Windows.Forms
Imports UConnector.Configuration
Imports System.ComponentModel

Public Class MainForm
    Inherits Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()
        Me.InitializeComponent()

        _logAppender = New RichTextBoxAppender(_txtLog)
        'Dim root = CType(LogManager.GetRepository(), Repository.Hierarchy.Hierarchy).Root
        'Dim attachable = CType(root, Core.IAppenderAttachable)
        'attachable.AddAppender(_logAppender)


    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents MainPanel As Panel
    Friend WithEvents BLoad As Button
    Friend WithEvents BEditConnectionStrings As Button
    Friend WithEvents _txtLog As System.Windows.Forms.RichTextBox
    Friend WithEvents _btnPause As System.Windows.Forms.Button
    Friend WithEvents _btnClear As System.Windows.Forms.Button
    Friend WithEvents _btnSaveLog As System.Windows.Forms.Button
    Friend WithEvents XmlConfigFolder As TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim gbLogOutput As System.Windows.Forms.GroupBox
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me._txtLog = New System.Windows.Forms.RichTextBox()
        Me.MainPanel = New System.Windows.Forms.Panel()
        Me.XmlConfigFolder = New System.Windows.Forms.TextBox()
        Me.BLoad = New System.Windows.Forms.Button()
        Me.BEditConnectionStrings = New System.Windows.Forms.Button()
        Me._btnPause = New System.Windows.Forms.Button()
        Me._btnClear = New System.Windows.Forms.Button()
        Me._btnSaveLog = New System.Windows.Forms.Button()
        gbLogOutput = New System.Windows.Forms.GroupBox()
        gbLogOutput.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbLogOutput
        '
        gbLogOutput.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        gbLogOutput.Controls.Add(Me._txtLog)
        gbLogOutput.Location = New System.Drawing.Point(10, 189)
        gbLogOutput.Name = "gbLogOutput"
        gbLogOutput.Size = New System.Drawing.Size(914, 213)
        gbLogOutput.TabIndex = 16
        gbLogOutput.TabStop = False
        gbLogOutput.Text = "Ouput"
        '
        '_txtLog
        '
        Me._txtLog.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._txtLog.BackColor = System.Drawing.Color.Gray
        Me._txtLog.Location = New System.Drawing.Point(5, 22)
        Me._txtLog.Name = "_txtLog"
        Me._txtLog.ReadOnly = True
        Me._txtLog.Size = New System.Drawing.Size(903, 184)
        Me._txtLog.TabIndex = 12
        Me._txtLog.Text = ""
        '
        'MainPanel
        '
        Me.MainPanel.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MainPanel.AutoScroll = True
        Me.MainPanel.Location = New System.Drawing.Point(10, 55)
        Me.MainPanel.Name = "MainPanel"
        Me.MainPanel.Size = New System.Drawing.Size(913, 80)
        Me.MainPanel.TabIndex = 7
        '
        'XmlConfigFolder
        '
        Me.XmlConfigFolder.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.XmlConfigFolder.Location = New System.Drawing.Point(19, 18)
        Me.XmlConfigFolder.Name = "XmlConfigFolder"
        Me.XmlConfigFolder.Size = New System.Drawing.Size(837, 22)
        Me.XmlConfigFolder.TabIndex = 8
        '
        'BLoad
        '
        Me.BLoad.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BLoad.Location = New System.Drawing.Point(865, 18)
        Me.BLoad.Name = "BLoad"
        Me.BLoad.Size = New System.Drawing.Size(58, 27)
        Me.BLoad.TabIndex = 9
        Me.BLoad.Text = "Load"
        '
        'BEditConnectionStrings
        '
        Me.BEditConnectionStrings.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BEditConnectionStrings.Location = New System.Drawing.Point(834, 410)
        Me.BEditConnectionStrings.Name = "BEditConnectionStrings"
        Me.BEditConnectionStrings.Size = New System.Drawing.Size(90, 26)
        Me.BEditConnectionStrings.TabIndex = 10
        Me.BEditConnectionStrings.Text = "&Connections"
        Me.BEditConnectionStrings.UseVisualStyleBackColor = True
        '
        '_btnPause
        '
        Me._btnPause.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._btnPause.Location = New System.Drawing.Point(10, 410)
        Me._btnPause.Name = "_btnPause"
        Me._btnPause.Size = New System.Drawing.Size(90, 26)
        Me._btnPause.TabIndex = 13
        Me._btnPause.Text = "Pause"
        Me._btnPause.UseVisualStyleBackColor = True
        '
        '_btnClear
        '
        Me._btnClear.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._btnClear.Location = New System.Drawing.Point(107, 410)
        Me._btnClear.Name = "_btnClear"
        Me._btnClear.Size = New System.Drawing.Size(90, 26)
        Me._btnClear.TabIndex = 14
        Me._btnClear.Text = "Clear"
        Me._btnClear.UseVisualStyleBackColor = True
        '
        '_btnSaveLog
        '
        Me._btnSaveLog.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._btnSaveLog.Location = New System.Drawing.Point(205, 409)
        Me._btnSaveLog.Name = "_btnSaveLog"
        Me._btnSaveLog.Size = New System.Drawing.Size(90, 26)
        Me._btnSaveLog.TabIndex = 15
        Me._btnSaveLog.Text = "Save Log"
        Me._btnSaveLog.UseVisualStyleBackColor = True
        '
        'MainForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 15)
        Me.ClientSize = New System.Drawing.Size(932, 438)
        Me.Controls.Add(gbLogOutput)
        Me.Controls.Add(Me._btnSaveLog)
        Me.Controls.Add(Me._btnClear)
        Me.Controls.Add(Me._btnPause)
        Me.Controls.Add(Me.BEditConnectionStrings)
        Me.Controls.Add(Me.BLoad)
        Me.Controls.Add(Me.XmlConfigFolder)
        Me.Controls.Add(Me.MainPanel)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(950, 485)
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "UConnector Desktop"
        gbLogOutput.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub BLoad_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BLoad.Click
        If Directory.Exists(XmlConfigFolder.Text) Then
            For Each f As String In Directory.GetFiles(XmlConfigFolder.Text, "*.xml")
                Dim impCtl As New ImporterCtl
                impCtl.Anchor = AnchorStyles.Top Or AnchorStyles.Left Or AnchorStyles.Right
                MainPanel.Controls.Add(impCtl)
                impCtl.init(f)
                impCtl.Top = (MainPanel.Controls.Count - 1) * impCtl.Height
                _impCtrls.Add(impCtl)
            Next
            BLoad.Enabled = False
        Else
            MsgBox("Folder does not exist", MsgBoxStyle.Exclamation)
        End If
    End Sub

    Private Sub MainForm_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Me.Text = Me.Text & " - " & My.Application.Info.Version.ToString()
        XmlConfigFolder.Text = My.Application.Info.DirectoryPath & "\config"
    End Sub

    Private Sub BEditConnectionStrings_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BEditConnectionStrings.Click
        Dim frmViewConnectionStrings As New ViewConnectionStrings
        frmViewConnectionStrings.Show()
    End Sub

    Private Sub _btnClear_Click(
        sender As Object,
        args As EventArgs
        ) Handles _btnClear.Click

        _txtLog.Clear()
    End Sub

    Private Sub _btnPause_Click(sender As System.Object, e As System.EventArgs) Handles _btnPause.Click
        _logAppender.ScrollText = Not _logAppender.ScrollText
        _btnPause.Text = If(_logAppender.ScrollText = False, "Pause", "Paused")
    End Sub

    Private Sub _btnSaveLog_Click(
        sender As Object,
        args As EventArgs
        ) Handles _btnSaveLog.Click

        If String.IsNullOrWhiteSpace(_txtLog.Text) = True Then
            Return
        End If

        Dim saveDlg As New SaveFileDialog
        saveDlg.AddExtension = True
        saveDlg.DefaultExt = "log"
        saveDlg.FileName = "UConnector.log"
        saveDlg.Filter = "Log files (*.log)|*.txt|All files (*.*)|*.*"
        If saveDlg.ShowDialog(Me) = DialogResult.Cancel Then
            Return
        End If

        Try
            Using logFile = New StreamWriter(File.OpenWrite(saveDlg.FileName))
                logFile.Write(_txtLog.Text)
            End Using
        Catch e As Exception
            MessageBox.Show(String.Format("Encountered an error saving log file (Reason: {0})", e.Message),
                            "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private ReadOnly _logAppender As RichTextBoxAppender

    Private Sub MainForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        For Each impCtrl In _impCtrls
            impCtrl.Quit()
            impCtrl.Dispose()
        Next
    End Sub

    Private ReadOnly _impCtrls As New Collections.Generic.List(Of ImporterCtl)
End Class

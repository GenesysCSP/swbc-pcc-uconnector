Imports System.IO
Imports System.IO.Compression
Imports Shell32
Imports System.Threading
Imports System.Drawing
Imports System.Reflection
Imports System.Windows.Forms
Imports GenUtils.TraceTopic
Imports QBEUConnector.Configuration
Imports QBEUConnector.Main
Imports QBEUConnector.Data

Public Class ImporterCtl
    Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents StartStopB As System.Windows.Forms.Button
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents TitleL As System.Windows.Forms.Label
    Friend WithEvents OpenXmlB As System.Windows.Forms.Button
    Friend WithEvents StatusL As System.Windows.Forms.Label
    Friend WithEvents LastCallL As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents HeartBeats As System.Windows.Forms.ImageList
    Friend WithEvents HeartBeatPB As System.Windows.Forms.PictureBox
    Friend WithEvents GetAgents As System.Windows.Forms.Button
    Friend WithEvents NumCallsL As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ImporterCtl))
        Me.StartStopB = New System.Windows.Forms.Button()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.StatusL = New System.Windows.Forms.Label()
        Me.OpenXmlB = New System.Windows.Forms.Button()
        Me.TitleL = New System.Windows.Forms.Label()
        Me.LastCallL = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.HeartBeats = New System.Windows.Forms.ImageList(Me.components)
        Me.HeartBeatPB = New System.Windows.Forms.PictureBox()
        Me.NumCallsL = New System.Windows.Forms.Label()
        Me.GetAgents = New System.Windows.Forms.Button()
        CType(Me.HeartBeatPB, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'StartStopB
        '
        Me.StartStopB.ImageIndex = 0
        Me.StartStopB.ImageList = Me.ImageList1
        Me.StartStopB.Location = New System.Drawing.Point(368, 8)
        Me.StartStopB.Name = "StartStopB"
        Me.StartStopB.Size = New System.Drawing.Size(26, 24)
        Me.StartStopB.TabIndex = 2
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        '
        'StatusL
        '
        Me.StatusL.Location = New System.Drawing.Point(542, 14)
        Me.StatusL.Name = "StatusL"
        Me.StatusL.Size = New System.Drawing.Size(56, 16)
        Me.StatusL.TabIndex = 3
        Me.StatusL.Text = "Stopped"
        '
        'OpenXmlB
        '
        Me.OpenXmlB.Location = New System.Drawing.Point(150, 7)
        Me.OpenXmlB.Name = "OpenXmlB"
        Me.OpenXmlB.Size = New System.Drawing.Size(80, 24)
        Me.OpenXmlB.TabIndex = 1
        Me.OpenXmlB.Text = "Open Xml.."
        '
        'TitleL
        '
        Me.TitleL.Location = New System.Drawing.Point(8, 12)
        Me.TitleL.Name = "TitleL"
        Me.TitleL.Size = New System.Drawing.Size(136, 16)
        Me.TitleL.TabIndex = 0
        '
        'LastCallL
        '
        Me.LastCallL.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LastCallL.Location = New System.Drawing.Point(475, 12)
        Me.LastCallL.Name = "LastCallL"
        Me.LastCallL.Size = New System.Drawing.Size(322, 16)
        Me.LastCallL.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(422, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 16)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Last Call:"
        '
        'HeartBeats
        '
        Me.HeartBeats.ImageStream = CType(resources.GetObject("HeartBeats.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.HeartBeats.TransparentColor = System.Drawing.Color.Transparent
        Me.HeartBeats.Images.SetKeyName(0, "")
        Me.HeartBeats.Images.SetKeyName(1, "")
        '
        'HeartBeatPB
        '
        Me.HeartBeatPB.Location = New System.Drawing.Point(400, 13)
        Me.HeartBeatPB.Name = "HeartBeatPB"
        Me.HeartBeatPB.Size = New System.Drawing.Size(16, 16)
        Me.HeartBeatPB.TabIndex = 6
        Me.HeartBeatPB.TabStop = False
        '
        'NumCallsL
        '
        Me.NumCallsL.Location = New System.Drawing.Point(668, 12)
        Me.NumCallsL.Name = "NumCallsL"
        Me.NumCallsL.Size = New System.Drawing.Size(107, 16)
        Me.NumCallsL.TabIndex = 7
        '
        'GetAgents
        '
        Me.GetAgents.Location = New System.Drawing.Point(236, 8)
        Me.GetAgents.Name = "GetAgents"
        Me.GetAgents.Size = New System.Drawing.Size(126, 23)
        Me.GetAgents.TabIndex = 9
        Me.GetAgents.Text = "Update Hierarchy"
        Me.GetAgents.UseVisualStyleBackColor = True
        '
        'ImporterCtl
        '
        Me.Controls.Add(Me.GetAgents)
        Me.Controls.Add(Me.NumCallsL)
        Me.Controls.Add(Me.HeartBeatPB)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.LastCallL)
        Me.Controls.Add(Me.TitleL)
        Me.Controls.Add(Me.OpenXmlB)
        Me.Controls.Add(Me.StatusL)
        Me.Controls.Add(Me.StartStopB)
        Me.Name = "ImporterCtl"
        Me.Size = New System.Drawing.Size(800, 40)
        CType(Me.HeartBeatPB, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private m_configFile As String
    Private m_lastStatus As IImporter.Status = IImporter.Status.STOPPED
    Private m_callCounter As Integer = 0
    Private WithEvents m_imp As IImporter

    Public Sub init(ByVal _configFile As String)
        m_configFile = _configFile
        Dim file As New IO.FileInfo(m_configFile)
        TitleL.Text = file.Name
    End Sub

    Private Function CreateImporter() As IImporter
        Dim conf As IConfig = ConfigManager.ReadConfiguration(m_configFile)
        Dim imp As IImporter = ImporterFactory.CreateImporter(conf.ImporterType, conf)
        If imp Is Nothing Then
            imp = Importer.createImporter(Me, m_configFile)
        End If
        Return imp
    End Function

    Private Sub StartStopB_Click(ByVal sender As System.Object, ByVal args As System.EventArgs) Handles StartStopB.Click
        Using (TraceTopic.connectorAppTopic.scope())
            If StartStopB.ImageIndex = 0 Then
                Try
                    m_imp = CreateImporter()
                    m_imp.Init()
                    m_imp.Start()
                    StartStopB.ImageIndex = 1
                    HeartBeatPB.Visible = True
                Catch e As Exception
                    If TypeOf e Is ApplicationException Then
                        Dim appE = DirectCast(e, ApplicationException)
                        TraceTopic.connectorAppTopic.error(appE.Message, appE.InnerException)
                    Else
                        TraceTopic.connectorAppTopic.error(String.Format(
                            "Encountered an error starting import (Reason: {})", e.Message), e)
                    End If

                    m_imp_statusChanged(IImporter.Status.ERROR, "ERROR!")
                End Try
            Else
                m_imp.Cancel()
                StartStopB.Enabled = False
            End If
        End Using
    End Sub

    Private Sub OpenXmlB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenXmlB.Click
        Dim confEditor As New ConfigFileEditor
        confEditor.init(StartStopB.ImageIndex = 1, m_configFile)
        confEditor.ShowDialog()
    End Sub

    Private Sub GetAgents_Click(ByVal sender As System.Object, ByVal args As System.EventArgs) Handles GetAgents.Click
        Try
            m_imp = CreateImporter()
            Dim t As New Thread(AddressOf m_imp.UpdateAgents)
            t.Start()
        Catch e As Exception
            m_imp_statusChanged(IImporter.Status.ERROR, "ERROR!")
            MessageBox.Show(Me, e.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Delegate Sub InteractionHandledDlg(ByVal _callId As IInteractionData, ByVal _status As IItemStatus)

    Private Sub m_imp_callImported(ByVal _callId As IInteractionData, ByVal _status As IItemStatus) Handles m_imp.InteractionHandled
        Me.BeginInvoke(New InteractionHandledDlg(AddressOf CallImported), _callId, _status)
    End Sub

    Private Sub CallImported(ByVal _callId As IInteractionData, ByVal _status As IItemStatus)
        Me.LastCallL.Text = _callId.ID
        If _status.Success Then
            m_callCounter += 1
            NumCallsL.Text = "Fetched: " & m_callCounter
        End If
    End Sub

    Private Delegate Sub UpdateStatusDlg(ByVal _status As IImporter.Status, ByVal _description As String)

    Private Sub m_imp_statusChanged(ByVal _status As IImporter.Status, ByVal _description As String) Handles m_imp.StatusChanged
        Me.BeginInvoke(New UpdateStatusDlg(AddressOf UpdateStatus), _status, _description)
    End Sub

    Private Sub UpdateStatus(ByVal _status As IImporter.Status, ByVal _description As String)
        Me.StatusL.Text = _description
        If m_lastStatus <> _status Then
            Dim thisExe = Assembly.GetExecutingAssembly()

            If _status = IImporter.Status.ERROR Then
                HeartBeatPB.Image = Image.FromStream(thisExe.GetManifestResourceStream("UConnector.beatRed.gif"))
            ElseIf _status = IImporter.Status.STOPPED Then
                HeartBeatPB.Image = Nothing
                StartStopB.ImageIndex = 0
                HeartBeatPB.Visible = False
                StartStopB.Enabled = True
            Else
                HeartBeatPB.Image = Image.FromStream(thisExe.GetManifestResourceStream("UConnector.beatGreen.gif"))
            End If
            m_lastStatus = _status
        End If
    End Sub

    Public Sub Quit()
        If Not (m_imp Is Nothing) Then
            m_imp.Quit()
            m_imp.Dispose()
        End If
    End Sub

End Class

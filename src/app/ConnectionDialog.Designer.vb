<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ConnectionDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.lblName = New System.Windows.Forms.Label()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.txtConnectionString = New System.Windows.Forms.TextBox()
        Me.lblConnectionString = New System.Windows.Forms.Label()
        Me.btnTest = New System.Windows.Forms.Button()
        Me.chkLocked = New System.Windows.Forms.CheckBox()
        Me.radSqlServer = New System.Windows.Forms.RadioButton()
        Me.radOther = New System.Windows.Forms.RadioButton()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.btnSave, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(467, 165)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(3, 3)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(67, 23)
        Me.btnSave.TabIndex = 0
        Me.btnSave.TabStop = False
        Me.btnSave.Text = "Save"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.TabStop = False
        Me.Cancel_Button.Text = "Cancel"
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(105, 14)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(136, 20)
        Me.txtName.TabIndex = 0
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Location = New System.Drawing.Point(4, 14)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(93, 13)
        Me.lblName.TabIndex = 6
        Me.lblName.Text = "Connection name:"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(13, 165)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(57, 23)
        Me.btnDelete.TabIndex = 14
        Me.btnDelete.TabStop = False
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'txtConnectionString
        '
        Me.txtConnectionString.Enabled = False
        Me.txtConnectionString.Location = New System.Drawing.Point(4, 73)
        Me.txtConnectionString.Name = "txtConnectionString"
        Me.txtConnectionString.Size = New System.Drawing.Size(603, 20)
        Me.txtConnectionString.TabIndex = 1
        '
        'lblConnectionString
        '
        Me.lblConnectionString.AutoSize = True
        Me.lblConnectionString.Location = New System.Drawing.Point(4, 56)
        Me.lblConnectionString.Name = "lblConnectionString"
        Me.lblConnectionString.Size = New System.Drawing.Size(92, 13)
        Me.lblConnectionString.TabIndex = 17
        Me.lblConnectionString.Text = "Connection string:"
        '
        'btnTest
        '
        Me.btnTest.Location = New System.Drawing.Point(256, 14)
        Me.btnTest.Name = "btnTest"
        Me.btnTest.Size = New System.Drawing.Size(54, 23)
        Me.btnTest.TabIndex = 18
        Me.btnTest.TabStop = False
        Me.btnTest.Text = "Test"
        Me.btnTest.UseVisualStyleBackColor = True
        '
        'chkLocked
        '
        Me.chkLocked.AutoSize = True
        Me.chkLocked.Checked = True
        Me.chkLocked.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkLocked.Location = New System.Drawing.Point(512, 55)
        Me.chkLocked.Name = "chkLocked"
        Me.chkLocked.Size = New System.Drawing.Size(99, 17)
        Me.chkLocked.TabIndex = 19
        Me.chkLocked.Text = "Write protected"
        Me.chkLocked.UseVisualStyleBackColor = True
        '
        'radSqlServer
        '
        Me.radSqlServer.AutoSize = True
        Me.radSqlServer.Checked = True
        Me.radSqlServer.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.radSqlServer.Location = New System.Drawing.Point(319, -1)
        Me.radSqlServer.Name = "radSqlServer"
        Me.radSqlServer.Size = New System.Drawing.Size(80, 17)
        Me.radSqlServer.TabIndex = 20
        Me.radSqlServer.TabStop = True
        Me.radSqlServer.Text = "SQL Server"
        Me.radSqlServer.UseVisualStyleBackColor = True
        '
        'radOther
        '
        Me.radOther.AutoSize = True
        Me.radOther.Location = New System.Drawing.Point(319, 22)
        Me.radOther.Name = "radOther"
        Me.radOther.Size = New System.Drawing.Size(51, 17)
        Me.radOther.TabIndex = 22
        Me.radOther.Text = "Other"
        Me.radOther.UseVisualStyleBackColor = True
        '
        'ConnectionDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(625, 203)
        Me.Controls.Add(Me.radOther)
        Me.Controls.Add(Me.radSqlServer)
        Me.Controls.Add(Me.chkLocked)
        Me.Controls.Add(Me.btnTest)
        Me.Controls.Add(Me.lblConnectionString)
        Me.Controls.Add(Me.txtConnectionString)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ConnectionDialog"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Edit connection"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
  Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
  Friend WithEvents btnSave As System.Windows.Forms.Button
  Friend WithEvents Cancel_Button As System.Windows.Forms.Button
  Friend WithEvents txtName As System.Windows.Forms.TextBox
  Friend WithEvents lblName As System.Windows.Forms.Label
  Friend WithEvents btnDelete As System.Windows.Forms.Button
  Friend WithEvents txtConnectionString As System.Windows.Forms.TextBox
  Friend WithEvents lblConnectionString As System.Windows.Forms.Label
  Friend WithEvents btnTest As System.Windows.Forms.Button
  Friend WithEvents chkLocked As System.Windows.Forms.CheckBox
  Friend WithEvents radSqlServer As System.Windows.Forms.RadioButton
    Friend WithEvents radOther As System.Windows.Forms.RadioButton

End Class

﻿Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports System.Threading

Public Class RichTextBoxAppender

#Region "Instance Properties"

    Public Property ScrollText As Boolean

#End Region

#Region "Constructors"

    Public Sub New(
            textBox As RichTextBox,
            Optional conversionPattern As String = "%date{HH:mm:ss} [%logger] %-5level - %message%newline%exception"
            )
        MyBase.New()

        If textBox Is Nothing Then
            Throw New ArgumentNullException("textBox")
        End If

        _textBox = textBox
        _textBox.BackColor = Color.Gray
        _textBox.Font = New Font("Consolas", _textBox.Font.Size + 1, FontStyle.Bold)
        _textBox.ForeColor = Color.White

        ' Initialize the layout.
        'conversionPattern = If(String.IsNullOrWhiteSpace(conversionPattern) = True,
        '                       PatternLayout.DefaultConversionPattern, conversionPattern)
        'Dim layout As New PatternLayout(conversionPattern)
        'Me.Layout = layout
    End Sub

#End Region

#Region "Instance Methods"

    'Protected Overloads Overrides Sub Append(
    '    logEvt As LoggingEvent
    '    )

    '    If logEvt Is Nothing Then
    '        Throw New ArgumentNullException("logEvt")
    '    ElseIf logEvt.Level = Level.Off Then
    '        Return
    '    End If

    '    SyncLock _textBox
    '        If _textBox.InvokeRequired = True Then
    '            Dim add As Action(Of LoggingEvent) =
    '                Sub(evt As LoggingEvent)
    '                    Me.Append(evt)
    '                End Sub
    '            _textBox.BeginInvoke(add, logEvt)
    '            Return
    '        End If
    '        If (_textBox.TextLength > MaxTextLength) Then
    '            _textBox.Clear()
    '            _textBox.AppendText("Cleared Log. Length Max Reached." + Environment.NewLine)
    '        End If
    '        Me.UpdateForEvent(logEvt)
    '        _textBox.AppendText(Me.RenderLoggingEvent(logEvt))

    '        If Me.ScrollText = True Then
    '            _textBox.Select(_textBox.TextLength, 0)
    '            _textBox.ScrollToCaret()
    '        End If
    '    End SyncLock
    'End Sub

    'Private Sub UpdateForEvent(
    '    logEvt As LoggingEvent
    '    )

    '    Dim msgColor As Color = _textBox.ForeColor

    '    Select Case logEvt.Level
    '        Case Level.Error
    '            msgColor = FATAL_COLOR
    '        Case Level.Fatal
    '            msgColor = FATAL_COLOR
    '        Case Level.Warn
    '            msgColor = WARN_COLOR
    '        Case Level.Info
    '            msgColor = INFO_COLOR
    '    End Select

    '    _textBox.SelectionFont = _textBox.Font
    '    _textBox.SelectionColor = msgColor
    'End Sub

#End Region

#Region "Class Data"

    Private Shared ReadOnly FATAL_COLOR As Color = Color.Black
    Private Shared ReadOnly INFO_COLOR As Color = Color.White
    Private Shared ReadOnly WARN_COLOR As Color = Color.Yellow
    Private ReadOnly MaxTextLength As Int32 = 1000000

#End Region

#Region "Instance Data"

    Private ReadOnly _textBox As RichTextBox

#End Region

End Class

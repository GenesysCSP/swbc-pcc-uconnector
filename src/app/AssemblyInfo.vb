Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("UConnector Desktop")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Genesys")> 
<Assembly: AssemblyProduct("UConnector Desktop")> 
<Assembly: AssemblyCopyright("Copyright � Genesys 2014")> 
<Assembly: AssemblyTrademark("")> 
<Assembly: CLSCompliant(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("B0D7352C-F64B-4697-8767-49D885F88759")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("1.1.0.1")>
<Assembly: AssemblyFileVersion("1.1.0.1")>
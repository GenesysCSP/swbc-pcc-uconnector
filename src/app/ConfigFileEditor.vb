Public Class ConfigFileEditor
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
  Friend WithEvents BOK As System.Windows.Forms.Button
  Friend WithEvents BCancel As System.Windows.Forms.Button
  Friend WithEvents XmlContent As System.Windows.Forms.TextBox
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ConfigFileEditor))
    Me.XmlContent = New System.Windows.Forms.TextBox
    Me.BOK = New System.Windows.Forms.Button
    Me.BCancel = New System.Windows.Forms.Button
    Me.SuspendLayout()
    '
    'XmlContent
    '
    Me.XmlContent.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.XmlContent.Location = New System.Drawing.Point(8, 8)
    Me.XmlContent.Multiline = True
    Me.XmlContent.Name = "XmlContent"
    Me.XmlContent.ScrollBars = System.Windows.Forms.ScrollBars.Both
    Me.XmlContent.Size = New System.Drawing.Size(664, 520)
    Me.XmlContent.TabIndex = 2
    '
    'BOK
    '
    Me.BOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
    Me.BOK.Location = New System.Drawing.Point(260, 536)
    Me.BOK.Name = "BOK"
    Me.BOK.Size = New System.Drawing.Size(75, 23)
    Me.BOK.TabIndex = 1
    Me.BOK.Text = "Save"
    '
    'BCancel
    '
    Me.BCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
    Me.BCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.BCancel.Location = New System.Drawing.Point(340, 536)
    Me.BCancel.Name = "BCancel"
    Me.BCancel.Size = New System.Drawing.Size(75, 23)
    Me.BCancel.TabIndex = 0
    Me.BCancel.Text = "Cancel"
    '
    'ConfigFileEditor
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.CancelButton = Me.BCancel
    Me.ClientSize = New System.Drawing.Size(680, 565)
    Me.Controls.Add(Me.BCancel)
    Me.Controls.Add(Me.BOK)
    Me.Controls.Add(Me.XmlContent)
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "ConfigFileEditor"
    Me.ShowInTaskbar = False
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "Config File Editor"
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

  Dim m_file As String

  Public Sub init(ByVal _readonly As Boolean, ByVal _file As String)
    XmlContent.ReadOnly = _readonly
    BOK.Enabled = Not _readonly
    m_file = _file
    Dim f As New IO.StreamReader(m_file)
    XmlContent.Text = f.ReadToEnd()
    f.Close()
  End Sub

  Private Sub BOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BOK.Click
    Dim f As New IO.StreamWriter(m_file)
    f.Write(XmlContent.Text)
    f.Close()
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
    End Sub
End Class

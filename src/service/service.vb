Imports System.ServiceProcess
Imports System.ComponentModel
Imports System.IO
Imports System.Threading
Imports GenUtils.TraceTopic
Imports QBEUConnector.Main
Imports QBEUConnector.Configuration

Public Class QBEUConnector
    Inherits ServiceBase
    Implements ISynchronizeInvoke

#Region " Component Designer generated code "

    Public Sub New()
        MyBase.New()

        ' This call is required by the Component Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

    End Sub

    'UserService overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    ' The main entry point for the process
    <MTAThread()>
    Shared Sub Main()
        Dim ServicesToRun() As System.ServiceProcess.ServiceBase

        ' More than one NT Service may run within the same process. To add
        ' another service to this process, change the following line to
        ' create a second service object. For example,
        '
        '   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
        '
        ServicesToRun = New System.ServiceProcess.ServiceBase() {New QBEUConnector}

        System.ServiceProcess.ServiceBase.Run(ServicesToRun)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    ' NOTE: The following procedure is required by the Component Designer
    ' It can be modified using the Component Designer.  
    ' Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        '
        'UConnector
        '
        Me.ServiceName = "UConnector"

    End Sub

#End Region

    Private m_impList As New Generic.List(Of IImporter)
    Private m_configFolder As String
    Private m_mainThread As System.Threading.Thread

    Protected Overrides Sub OnStart(ByVal args() As String)

        m_mainThread = New Thread(AddressOf startMain)
        m_mainThread.Start()
    End Sub

    Private Sub startMain()

        My.Application.ChangeCulture("en-US")
        My.Application.ChangeUICulture("en-US")
        TraceTopic.Initialize()
        m_configFolder = My.Application.Info.DirectoryPath & "\config"
        Dim doRestart As Boolean = False
        Dim restartInterval As TimeSpan = TimeSpan.MaxValue
        Using (TraceTopic.connectorSvcTopic.scope())
            If Directory.Exists(m_configFolder) Then
                For Each f As String In Directory.GetFiles(m_configFolder, "*.xml")
                    Try
                        Dim imp As IImporter = CreateImporter(f)
                        imp.Init()
                        imp.Start()

                        If imp.DoRestart Then
                            doRestart = True
                            If imp.RestartInterval < restartInterval Then
                                restartInterval = imp.RestartInterval
                            End If
                        End If

                        m_impList.Add(imp)
                        System.Threading.Thread.Sleep(20000)
                    Catch ex As Exception
                        TraceTopic.connectorSvcTopic.error("Exception starting importer " & f, ex)
                    End Try
                Next
            Else
                TraceTopic.connectorSvcTopic.error("Folder does not exist " & m_configFolder)
            End If
            If doRestart Then
                System.Threading.Thread.Sleep(restartInterval)
                TraceTopic.connectorSvcTopic.note("Restarting Service")
                RestartService()
            End If
        End Using
    End Sub

    Private Function CreateImporter(ByVal configFile As String) As IImporter
        Dim conf As IConfig = ConfigManager.ReadConfiguration(configFile)
        Dim imp As IImporter = ImporterFactory.CreateImporter(conf.ImporterType, conf)
        If imp Is Nothing Then
            imp = Importer.createImporter(Me, configFile)
        End If
        Return imp
    End Function

    Protected Overrides Sub OnStop()
        Using (TraceTopic.connectorSvcTopic.scope())
            StopImporters()
            m_mainThread.Abort()
            TraceTopic.connectorSvcTopic.note("UConnector stopped.")
        End Using
    End Sub

    Protected Sub RestartService()
        Using (TraceTopic.connectorSvcTopic.scope())
            StopImporters()
            TraceTopic.connectorSvcTopic.note("UConnector restarting.")
            Environment.Exit(1)
        End Using
    End Sub

    Protected Sub StopImporters()
        For Each imp As IImporter In m_impList
            'imp.Quit()
            'Changed to Cancel to allow the Importer to tidy up things before stopping
            imp.Cancel()
        Next
    End Sub

    Public Function BeginInvoke(ByVal method As System.Delegate, ByVal args() As Object) As IAsyncResult Implements ISynchronizeInvoke.BeginInvoke
        Dim ret As Object = method.DynamicInvoke(args)
        Return Nothing
    End Function

    Public Function EndInvoke(ByVal result As IAsyncResult) As Object Implements ISynchronizeInvoke.EndInvoke
        Return Nothing
    End Function

    Public Function Invoke(ByVal method As System.Delegate, ByVal args() As Object) As Object Implements ISynchronizeInvoke.Invoke
        Return method.DynamicInvoke(args)
    End Function

    Public ReadOnly Property InvokeRequired() As Boolean Implements ISynchronizeInvoke.InvokeRequired
        Get
            Return False
        End Get
    End Property
End Class

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace GenUtils
{
    public static partial class Func
    {
        #region Methods

        public static void ForEach<T>(
            this IEnumerable<T> items,
            Action<T> action
            )
        {
            if (items == null)
            {
                throw new ArgumentNullException("items");
            }
            else if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            foreach (T item in items)
            {
                action(item);
            }
        }

        public static bool ForEach<T>(
            this IEnumerable<T> items,
            Func<T, bool> action
            )
        {
            if (items == null)
            {
                throw new ArgumentNullException("items");
            }
            else if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            foreach (T item in items)
            {
                if (action(item) == false)
                {
                    return false;
                }
            }

            return true;
        }

        public static void ForEach(
            this DataRowCollection rows,
            Action<DataRow> op
            )
        {
            if (op == null)
            {
                throw new ArgumentNullException("op");
            }

            foreach (DataRow item in rows)
            {
                op(item);
            }
        }

        public static void ForEach<T>(
            this Array items,
            Action<int, T> action
            )
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            for (var idx = 0; idx < items.Length; idx++)
            {
                action(idx, (T)items.GetValue(idx));
            }
        }

        public static void ForEach<T>(
            this T[] items,
            Action<int, T> action
            )
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            for (var idx = 0; idx < items.Length; idx++)
            {
                action(idx, items[idx]);
            }
        }

        public static void ForEach(
            this DataTable table,
            Action<DataRow> action
            )
        {
            if (action == null)
            {
                throw new ArgumentNullException("op");
            }

            table.Rows.ForEach(action);
        }

        #endregion
    }
}

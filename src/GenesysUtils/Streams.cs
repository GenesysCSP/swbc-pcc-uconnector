﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace GenUtils
{
    public static partial class Streams
    {
        #region Extension Methods

        public static void ReadFrom(
            this Stream output,
            Stream input,
            byte[] buffer = null
            )
        {
            if (input == null)
            {
                throw new ArgumentNullException("input");
            }

            input.WriteTo(output, buffer);
        }

        public static void WriteTo(
            this Stream input,
            Stream output,
            byte[] buffer = null
            )
        {
            if (output == null)
            {
                throw new ArgumentNullException("output");
            }

            // If not buffer specified, use a default buffer size of 16KB
            buffer = (buffer == null || buffer.Length == 0) ? new byte[16384] : buffer;

            try
            {
                int bytesRead = 0;
                while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    output.Write(buffer, 0, bytesRead);
                }
            }
            catch (EndOfStreamException)
            {
                // Intentionally ignored
            }
        }

        #endregion
    }
}

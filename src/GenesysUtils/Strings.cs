﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace GenUtils
{
    public static partial class Strings
    {
        #region Methods

        public static IEnumerable<string> GetStringValues<T>(
            IEnumerable<T> values
            )
        {
            if (values == null)
            {
                throw new ArgumentNullException("values");
            }

            if (values.Any() == false)
            {
                return new string[0];
            }

            var strValues = new List<string>();
            values.ForEach((value) =>
            {
                if (value != null)
                {
                    strValues.Add(value.ToString());
                }
            });

            return strValues;
        }

        public static IEnumerable<string> Split(
            string line,
            string separator = ","
            )
        {
            Regex expr = null;
            lock (SPLITEX)
            {
                if (SPLITEX.TryGetValue(separator, out expr) == false)
                {
                    expr = new Regex(String.Format(
                        "{0}(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))", Regex.Escape(separator)));
                    SPLITEX.Add(separator, expr);
                }
            }

            var values = expr.Split(line);
            for (var i = 0; i < values.Length; i++)
            {
                if (values[i].Length > 1)
                {
                    values[i] = values[i].Trim('\"');
                }
            }

            return values;
        }

        #endregion

        #region Data

        private readonly static IDictionary<string, Regex> SPLITEX = new Dictionary<string, Regex>();

        #endregion
    }
}

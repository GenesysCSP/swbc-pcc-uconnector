﻿using ININ.PSO.Tools.PSOTrace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenUtils.TraceTopic
{
    public class TraceTopic
    {
        public static readonly Topic pureConnectTopic = new Topic("Genesys.PS.UConnector.PureConnect");
        public static readonly Topic importerTopic = new Topic("Genesys.PS.UConnector.Importer");
        public static readonly Topic connectorAppTopic = new Topic("Genesys.PS.UConnector.ConnectorApp");
        public static readonly Topic connectorSvcTopic = new Topic("Genesys.PS.UConnector.ConnectorSvc");
        public static void Initialize()
        {
            PSOTrace.Initialize(typeof(EventId), "Genesys.PS.UConnector");
        }
    }
}

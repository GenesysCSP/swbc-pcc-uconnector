﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace GenUtils
{
    public static partial class DB
    {
        #region Methods

        public static void CopyTable(
            IDbConnection dbConn,
            string tableName,
            bool copyContents = true,
            string tempTableName = null,
            string columns = null,
            IDbTransaction transxn = null
            )
        {
            if (dbConn == null)
            {
                throw new ArgumentNullException("dbConn");
            }
            else if (tableName == null)
            {
                throw new ArgumentNullException("tableName");
            }

            tableName = tableName.Trim();
            if (String.IsNullOrEmpty(tableName) == true)
            {
                throw new ArgumentException("no table name specified");
            }

            tempTableName = tempTableName.Trim();
            tempTableName = (String.IsNullOrEmpty(tempTableName) == false) ? tempTableName : '#' + tableName;

            columns = (columns != null) ? columns.Trim() : columns;
            columns = (String.IsNullOrEmpty(columns) == false) ? columns : "*";

            var limit = (copyContents == false) ? "TOP 0" : String.Empty;

            DB.Modify(dbConn, String.Format("SELECT {0} {1} INTO {2} FROM {3}",
                limit, columns, tempTableName, tableName), transxn);
        }

        public static void DropTable(
            SqlConnection dbConn,
            string tableName,
            SqlTransaction transxn = null
            )
        {
            if (dbConn == null)
            {
                throw new ArgumentNullException("dbConn");
            }
            else if (tableName == null)
            {
                throw new ArgumentNullException("tableName");
            }

            tableName = tableName.Trim();
            if (String.IsNullOrEmpty(tableName) == true)
            {
                throw new ArgumentException("no table name specified");
            }

            var cmd = dbConn.CreateCommand();
            cmd.CommandText = "IF OBJECT_ID(@ObjectName) IS NOT NULL DROP TABLE " + tableName;
            cmd.Transaction = transxn;
            cmd.Parameters.AddWithValue("@ObjectName", String.Format(
                (tableName.StartsWith("#") == true) ? "tempdb..{0}" : "{0}", tableName));

            cmd.ExecuteNonQuery();
        }

        public static int Modify(
            IDbConnection dbConn,
            string sql,
            IDbTransaction transxn = null
            )
        {
            if (dbConn == null)
            {
                throw new ArgumentNullException("dbConn");
            }
            else if (sql == null)
            {
                throw new ArgumentNullException("sql");
            }

            sql = sql.Trim();
            if (String.IsNullOrEmpty(sql) == true)
            {
                throw new ArgumentException("no SQL specified");
            }

            var cmd = dbConn.CreateCommand();
            cmd.Connection = dbConn;
            cmd.CommandText = sql;
            cmd.Transaction = transxn;

            return cmd.ExecuteNonQuery();
        }

        public static SqlCommand ObjectExists(
            SqlCommand dbCmd,
            string objectName
            )
        {
            if (dbCmd == null)
            {
                throw new ArgumentNullException("dbCmd");
            }
            else if (objectName == null)
            {
                throw new ArgumentNullException("objectName");
            }

            dbCmd.CommandText = "SELECT CASE WHEN OBJECT_ID(@ObjectName) IS NULL THEN 0 ELSE 1 END";
            dbCmd.Parameters.AddWithValue("@ObjectName", objectName);

            return dbCmd;
        }

        public static bool ObjectExists(
            SqlConnection dbConn,
            string objectName,
            SqlTransaction transxn = null
            )
        {
            if (dbConn == null)
            {
                throw new ArgumentNullException("dbConn");
            }
            else if (objectName == null)
            {
                throw new ArgumentNullException("objectName");
            }

            var dbCmd = ObjectExists(dbConn.CreateCommand(), objectName);
            dbCmd.Transaction = transxn;

            return QueryScalar<int>(dbCmd) == 1;
        }

        public static DataTable Query(
            IDbConnection dbConn,
            string sql,
            IDbTransaction transxn = null,
            CommandBehavior behavior = CommandBehavior.SingleResult
            )
        {
            if (dbConn == null)
            {
                throw new ArgumentNullException("dbConn");
            }
            else if (sql == null)
            {
                throw new ArgumentNullException("sql");
            }

            sql = sql.Trim();
            if (String.IsNullOrEmpty(sql) == true)
            {
                throw new ArgumentException("no SQL specified");
            }

            var cmd = dbConn.CreateCommand();
            cmd.Connection = dbConn;
            cmd.CommandText = sql;

            using (var reader = cmd.ExecuteReader(behavior))
            {
                var results = new DataTable();
                results.Load(reader);
                return results;
            }
        }

        public static void QueryMany(
            IDbConnection dbConn,
            string sql,
            IDbTransaction transxn,
            Action<DataTable> action
            )
        {
            if (dbConn == null)
            {
                throw new ArgumentNullException("dbConn");
            }
            else if (sql == null)
            {
                throw new ArgumentNullException("sql");
            }
            else if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            sql = sql.Trim();
            if (String.IsNullOrEmpty(sql) == true)
            {
                throw new ArgumentException("no SQL specified");
            }

            var cmd = dbConn.CreateCommand();
            cmd.Connection = dbConn;
            cmd.CommandText = sql;

            using (var reader = cmd.ExecuteReader())
            {
                int count = 0;
                var result = new DataTable(count.ToString());
                result.Load(reader);
                action(result);
                count++;

                while (reader.NextResult() == true)
                {
                    result = new DataTable(count.ToString());
                    result.Load(reader);
                    action(result);
                    count++;
                }
            }
        }

        public static IEnumerable<DataTable> QueryMany(
            IDbConnection dbConn,
            string sql,
            IDbTransaction transxn = null
            )
        {
            var results = new List<DataTable>();
            QueryMany(dbConn, sql, transxn, result => results.Add(result));
            return results;
        }

        public static T QueryScalar<T>(
            IDbConnection dbConn,
            string sql,
            T defaultValue = default(T),
            IDbTransaction transxn = null            
            )
        {
            if (dbConn == null)
            {
                throw new ArgumentNullException("dbConn");
            }
            else if (sql == null)
            {
                throw new ArgumentNullException("sql");
            }

            sql = sql.Trim();
            if (String.IsNullOrEmpty(sql) == true)
            {
                throw new ArgumentException("no SQL specified");
            }

            var dbCmd = dbConn.CreateCommand();
            dbCmd.CommandText = sql;
            dbCmd.Transaction = transxn;

            var result = dbCmd.ExecuteScalar();
            return (result != null) ? (T)result : defaultValue;
        }

        public static T QueryScalar<T>(
            IDbCommand dbCmd,
            T defaultValue = default(T)
            )
        {
            if (dbCmd == null)
            {
                throw new ArgumentNullException("dbCmd");
            }

            var result = dbCmd.ExecuteScalar();
            return (result != null) ? (T)result : defaultValue;
        }

        #endregion
    }
}
